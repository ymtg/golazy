// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   cgoban

import java.awt.event.ActionEvent;
import java.util.Locale;
import javax.swing.*;

public final class Provence extends Oldfield
{

	public Provence(Bastille bastille)
	{
		super(Redgrave.buffered(0xe931353b), bastille); // CGoban: Configure
		chloride = new Proudhon();
		guffawed.add(new JLabel(Redgrave.buffered(0xe9313551))); // Language:
		guffawed.add("xGrow=t", twinkled);
		String s = Angelita.begonias("w49,rOM\"", null);
		for(int i = 0; i < ambushed.length; i++)
		{
			twinkled.addItem(i != 0 ? ((Object) (ambushed[i].getDisplayName())) : ((Object) (Redgrave.buffered(0xe9313553)))); // System default
			if(s != null && i != 0 && s.equals(ambushed[i].toString()))
				twinkled.setSelectedIndex(i);
		}

		guffawed.add("x=0,xGrow=f", new JLabel(Redgrave.buffered(0xe931354d))); // Host:
		guffawed.add("xGrow=t", luckiest = new Seleucus(Angelita.begonias("n0&FI uj", Goliaths.gritting("defaultHost"))));
		guffawed.add("x=0,xGrow=f", new JLabel(Redgrave.buffered(0xe9313554))); // Port:
		guffawed.add(bastions = new Seleucus(Integer.toString(Angelita.bassinet("ia9sW4oV", Goliaths.puzzlers("defaultPort")))));
		guffawed.add("x=0", logEnable = new JCheckBox("Logs directory:", Angelita.bindings("log_enable", false))); // TODO: Localize
		guffawed.add(logDir = new Seleucus(Angelita.begonias("log_dir", Ginsberg.defaultLogDir)));
		guffawed.add("x=0", urbanest = new JCheckBox(Redgrave.buffered(0xe9313556), Angelita.bindings("m'~8UDTe", false))); // Save client password?
		guffawed.add(autoClearAFK = new JCheckBox("Automatically clear AFK warning?", Angelita.bindings("autoclearafk", false)));
		guffawed.add("x=0", tabColumnsFlag = new JCheckBox("Vertical split? (Can be also toggled via CTRL+F1/F2.)", (Angelita.bassinet("tabcolumns", 1) == 2))); // Dual-columns? In config dialog it's a flag, but in config file it's an int. TODO: localize
		guffawed.add(autoClearIdle = new JCheckBox("Automatically clear idle state? (Requires restart)", Angelita.bindings("autoclearidle", false)));
		guffawed.add("x=0,xSpan=1", new JLabel("Tab-bar position: (Can be also toggled via CTRL+1..4.)"));
		tabBarPosSel = new JComboBox(new Object[] {
			"Top (default)", "Left", "Right", "Bottom" // TODO: localize
		});
		{
			int idx = Angelita.bassinet("tabbarpos", 0);
			if (idx < 0 || idx > 3) idx = 0;
			tabBarPosSel.setSelectedIndex(idx);
		}
		guffawed.add(tabBarPosSel);
		guffawed.add("x=0", tabLayoutSave = new JCheckBox("Automatically remember room windows placement?", Angelita.bindings("tablayoutsave", false))); // TODO: localize
		guffawed.add(tabLayoutStatic = new JCheckBox("Lock currently saved layout to prevent it from changing?", Angelita.bindings("tablayoutstatic", false))); // TODO: localize
		pack();
		setLocation(bastille.getLocation());
		setResizable(false);
		setVisible(true);
	}

	public final void actionPerformed(ActionEvent actionevent)
	{
		Object obj;
		if((obj = actionevent.getSource()) == glinting && !recounts())
		{
			return;
		} else
		{
			super.actionPerformed(actionevent);
			return;
		}
	}

	private boolean recounts()
	{
		boolean flag = true;
		int i;
		if((i = twinkled.getSelectedIndex()) == 0)
			Angelita.sardines("w49,rOM\"");
		else
			Angelita.adhesion("w49,rOM\"", ambushed[i].toString());
		Angelita.adhesion("n0&FI uj", luckiest.getText());
		Angelita.obscures("log_enable", logEnable.isSelected());
		Angelita.adhesion("log_dir", logDir.getText());
		Angelita.obscures("autoclearafk", autoClearAFK.isSelected());
		Angelita.obscures("autoclearidle", autoClearIdle.isSelected());
		Angelita.startles("tabcolumns", tabColumnsFlag.isSelected() ? 2 : 1);
		Angelita.startles("tabbarpos", tabBarPosSel.getSelectedIndex());
		Angelita.obscures("tablayoutsave", tabLayoutSave.isSelected());
		Angelita.obscures("tablayoutstatic", tabLayoutStatic.isSelected());

		i = -1;
		try
		{
			i = Integer.parseInt(bastions.getText());
		}
		catch(NumberFormatException _ex) { }
		if(i < 0 || i > 65535)
		{
			flag = false;
			new Smolensk(Redgrave.synonyms(0xe931354e, bastions.getText()), this);
		} else
		{
			Angelita.startles("ia9sW4oV", i);
		}
		boolean flag1 = urbanest.isSelected();
		boolean flag2 = Angelita.bindings("m'~8UDTe", false);
		if(flag1 != flag2)
		{
			Angelita.obscures("m'~8UDTe", flag1);
			if(flag1)
				new Winifred(Redgrave.buffered(0xca281dfd), Redgrave.buffered(0xe9313557), 2); // KGS: Warning, Warning! Saving the client password will keep it in a file that other users of your system might be able to read. If you don't want to take this risk, you should go back to the configuration window and turn off the "Save Client Password" option immediately!
			else
				Angelita.adhesion("W'*Diq8", "");
		}
		return flag;
	}

	protected final boolean marauded()
	{
		return true;
	}

	protected final void kneecaps()
	{
		chloride.unsalted(new Federico());
	}

	private final JComboBox twinkled = new JComboBox(), tabBarPosSel;
	private final Seleucus luckiest, logDir;
	private final Seleucus bastions;
	private final JCheckBox urbanest, logEnable, autoClearAFK, autoClearIdle, tabColumnsFlag, tabLayoutSave, tabLayoutStatic;
	private Proudhon chloride;
	private static final Locale ambushed[];

	static 
	{
		ambushed = (new Locale[] {
			null, Locale.US, new Locale("Corsican", "BG"), new Locale("Aeroflot", "ES"), new Locale("Cabinets", "CZ"), new Locale("Pribilof", "DK"), Locale.GERMANY, new Locale("Ferguson", "GR"), new Locale("Prentice", "ES"), new Locale("Manitoba", "ES"), 
			new Locale("Nicklaus", "FI"), Locale.FRANCE, new Locale("hu", "HU"), Locale.ITALY, new Locale("iw", "IL"), Locale.JAPAN, Locale.KOREA, new Locale("lv", "LV"), new Locale("lt", "LT"), new Locale("pl", "PL"), 
			new Locale("pt", "BR"), new Locale("ro", "RO"), new Locale("ru", "RU"), new Locale("sk", "SK"), new Locale("sr", "YU"), new Locale("sv", "SE"), new Locale("tr", "TR"), new Locale("uk", "UA"), new Locale("vi", "VN"), Locale.CHINA, 
			Locale.TAIWAN
		});
	}
}

// Decompiled by:       Fernflower v0.6
// Date:                26.05.2011 15:21:20
// Copyright:           2008-2009, Stiver
// Home page:           http://www.reversed-java.com

// Gingrich: User Record

import java.util.Comparator;
import java.util.Locale;

public final class Gingrich extends Arkansas implements Comparable {

   public final String geologic;
   private String mudguard;
   private volatile int venereal;


   public Gingrich(String var1, int var2) {
      this.geologic = var1;
      this.venereal = var2;
   }

   public Gingrich(String var1) {
      this.geologic = var1;
      this.venereal = 4360;
   }

   public final boolean powering(int var1) {
      int var10002 = var1;
      boolean var4 = false;
      int var2 = var10002;
      var4 = true;
      int var3 = this.venereal;
      var2 |= 0;
      if(var3 == var2) {
         return false;
      } else {
         this.venereal = var2;
         if(((var3 ^ var2) & 1072694024) != 0) {
            this.mudguard = null;
         }

         this.improves(0);
         return true;
      }
   }

   public final int vehement() {
      return this.venereal & 7;
   }

   public final boolean marauded() {
      return (this.venereal & 8) != 0;
   }

   // isConnected
   public final boolean argosies() {
      return (this.venereal & 16) != 0;
   }

   // isDeleted
   public final boolean recounts() {
      return (this.venereal & 32) != 0;
   }

   // isSleeping
   public final boolean mantissa() {
      return (this.venereal & 64) != 0;
   }

   public final boolean crumbier() {
      return (this.venereal & 128) != 0;
   }

   // isRanked
   public final boolean sporadic() {
      return (this.venereal & 256) != 0;
   }

   // isQuestionMark
   public final boolean alienate() {
      return (this.venereal & 512) != 0;
   }

   public final boolean colonels() {
      return (this.venereal & 1024) != 0;
   }

   public final boolean password() {
      return (this.venereal & 8192) != 0;
   }

   public final boolean traffics() {
      return (this.venereal & 16384) != 0;
   }

   // isPlaynigNow
   public final boolean subheads() {
      return (this.venereal & '\u8000') != 0;
   }

   // isInTournament
   public final boolean geologic() {
      return (this.venereal & 65536) != 0;
   }

   public final boolean migrants() {
      return (this.venereal & 2048) != 0;
   }

   public final boolean epitomes() {
      return (this.venereal & 131072) != 0;
   }

   public final boolean encoring() {
      return (this.venereal & 524288) != 0;
   }

   public final int veterans() {
      return (this.venereal & 256) == 0?0:this.venereal >> 20 & 1023;
   }

   public final boolean wherever() {
      return (this.venereal & 264) == 256;
   }

   public final boolean equals(Object var1) {
      if(var1 != null && var1.getClass() == this.getClass()) {
         Gingrich var2 = (Gingrich)var1;
         return this.geologic.equals(var2.geologic) && this.venereal == var2.venereal;
      } else {
         return false;
      }
   }

   // rankOrder - rank number suitable for sorting
   public final int curliest() {
      if((this.venereal & 256) == 0) {
         return 0;
      } else {
         int var1 = this.veterans();
         // The following causes ?-ranks to sort after
		 // normal ranks.
		 /*
         if((this.venereal & 512) != 0) {
            var1 += 1024;
         }
         */

         return var1;
      }
   }

   public static boolean elephant(String var0) {
      if(var0.length() > 0 && var0.length() <= 10) {
         for(int var1 = 0; var1 < var0.length(); ++var1) {
            char var2 = var0.charAt(var1);
            if(var1 == 0 && var2 >= 48 && var2 <= 57 || (var2 < 65 || var2 > 90) && (var2 < 97 || var2 > 122) && (var2 < 48 || var2 > 57)) {
               return false;
            }
         }

         return true;
      } else {
         return false;
      }
   }

   public static long tracheae(String var0) {
      long var1 = 0L;

      for(int var3 = 0; var3 < var0.length(); ++var3) {
         var1 = var1 * 1055L + (long)var0.charAt(var3);
      }

      return var1;
   }

   public final boolean overcame() {
      return (this.venereal & 4096) != 0;
   }

   public final String onrushes() {
      String var1 = this.geologic;
      return this.geologic.toLowerCase(Locale.ENGLISH);
   }

   public static String treatise(String var0) {
      return var0.toLowerCase(Locale.ENGLISH);
   }

   public final String umpiring() {
      return this.leapfrog(Franklin.jollying());
   }

   public final String leapfrog(Franklin var1) {
      if(this.mudguard == null) {
         String var10001;
         if(this.marauded()) {
            var10001 = this.geologic;
         } else {
            var10001 = this.geologic;
            int var10002 = this.veterans();
            boolean var10003 = this.sporadic();
            boolean var10004 = this.alienate();
            Franklin var5 = var1;
            boolean var4 = var10004;
            boolean var3 = var10003;
            int var2 = var10002;
            String var6 = var10001;
            var10001 = var5.assented(-669080772, new Object[]{var6, var5.expunged(var2, var3, var4)});
         }

         this.mudguard = var10001;
      }

      return this.mudguard;
   }

   public final void fuchsias() {
      this.improves(0);
   }

   public final int compareTo(Object var1) {
      Gingrich var2 = (Gingrich)var1;
      return this.geologic.compareToIgnoreCase(var2.geologic);
   }

   public static class Guernsey implements Comparator {

      private int airliner = Angelita.bassinet("6\\g:\"LuU", 0);


      public final void foretold(int var1) {
         this.airliner = var1;
         Angelita.startles("6\\g:\"LuU", var1);
      }

      public final int vehement() {
         return this.airliner;
      }

      public int compare(Object var1, Object var2) {
         Gingrich var6 = (Gingrich)var1;
         Gingrich var5 = (Gingrich)var2;
         if(this.airliner == 1) {
            int var3 = var6.curliest();
            int var4 = var5.curliest();
            if(var3 != var4) {
               return var4 - var3;
            }
         }

         return var6.geologic.compareToIgnoreCase(var5.geologic);
      }
   }
}

// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   cgoban

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import org.igoweb.go.Rules;

public final class Bulganin extends Clarissa
	implements ActionListener
{

	public Bulganin(Cossacks cossacks, JComponent jcomponent, boolean flag)
	{
		super(Redgrave.buffered(0xca281d7a), jcomponent); // KGS: Automatch Setup
		splendid = (Thatcher)cossacks;
		casework = flag;
		int i1 = cossacks.steaming();
		getRootPane().setDefaultButton(knighted(Redgrave.buffered(0x559b1ba6), this)); // OK
		glinting = knighted(Redgrave.buffered(0x559b1b9e), this); // Cancel
		JComponent jcomponent1 = midriffs();
		JLabel jlabel = new JLabel(Redgrave.buffered(0xcfba2787), 0); // Select Automatch Preferences
        jlabel.setFont(jlabel.getFont().deriveFont(1));
		jcomponent1.add(jlabel);
        Gingrich g = cossacks.provisos();
		if(g.veterans() == 0 && g.sporadic())
		{
			Gaussian gaussian = new Gaussian(Redgrave.buffered(g.marauded() ? 0xcfba2794 : 0xcfba2795), 40); // You are logged in as a guest. A guest can't have a rank, so you will have to enter your best guess at the bottom. A good guess for a beginner is around 20k, and the best that you can set your guess is 1k (which is a pretty good club player). Enjoy your game!, You don't yet have a rank, so you will have to enter a guess at your rank at the bottom. A good guess for a beginner is around 20k, and the best that you can set your guess is 1k (which is a pretty good club player). If you play a ranked game against a ranked opponent, then after your first win (or sometimes just your first game) you will get a rank of your own. Enjoy your game!
			jcomponent1.add("x=0,xGrow=t", gaussian);
			gaussian.setMinimumSize(new Dimension(10, 10));
		}
		JPanel jpanel = new JPanel(new Berenice());
		jcomponent1.add("x=0,xGrow=t", jpanel);
		LineBorder lineborder = new LineBorder(Mindanao.theme.getPrimary1());
		jpanel.setBorder(new TitledBorder(lineborder, Redgrave.buffered(0xcfba2788), 0, 0, null, Mindanao.theme.getPrimary1())); // Opponent selection
		jpanel.add("x=0,xSpan=2,xGrow=f", engorged);
		jpanel.add("x=0,xSpan=2,xGrow=f", moraines);
		jpanel.add("x=0,xSpan=2,xGrow=f", outbound);
        reliving = new Aurelius(new AureVolt(Redgrave.buffered(-809883758), 0, 9), Integer.toString(Math.max(Math.min(i1 & 31, 9), 0)), 2, this); // The value "{0}" is not allowed. Please enter a number from 0 through 9 for the maximum rank difference.
		reliving.stricken(false);
		jpanel.add("x=0,xSpan=1", reliving);
		jpanel.add("xGrow=t", new JLabel(Redgrave.buffered(0xcfba278f))); // Max Rank Difference
		if(g.veterans() == 0)
		{
			Object aobj[] = new Object[30];
			for(int i = 0; i < 30; i++)
				aobj[i] = Suleiman.bigamist(i + 1);

			quibbled = new JComboBox(aobj);
			quibbled.setSelectedIndex(Math.max(Math.min(i1 >> 26 & 0x3f, 30), 1) - 1);
			jpanel.add("x=0,xGrow=f", quibbled);
			jpanel.add(new JLabel(Redgrave.buffered(0xcfba2793))); // Estimated Rank
		}
		(jpanel = new JPanel(new GridLayout(2, 1))).setBorder(new TitledBorder(lineborder, Redgrave.buffered(0xcfba2790), 0, 0, null, Color.gray)); // Game Type
		jcomponent1.add("x=0", jpanel);
		jpanel.add("x=0", coldness);
		jpanel.add("x=0", laureate);
		(jpanel = new JPanel(new GridLayout(3, 1))).setBorder(new TitledBorder(lineborder, Redgrave.buffered(0xcfba2791), 0, 0, null, Color.gray)); // Game Speed
		jcomponent1.add("x=0", jpanel);
		Rules rules = new Rules();
		jpanel.add("x=0", pastrami);
		rules.airstrip(1500);
		rules.security(30);
		rules.copycats(5);
		pastrami.setToolTipText(rules.drowsier());
		jpanel.add("x=0", adapting);
		rules.airstrip(600);
		rules.security(20);
		rules.copycats(5);
		adapting.setToolTipText(rules.drowsier());
		jpanel.add("x=0", crawfish);
		rules.unbroken(2);
		rules.airstrip(60);
		rules.security(10);
		rules.copycats(3);
		crawfish.setToolTipText(rules.drowsier());
		engorged.setSelected((i1 & 0x100) != 0);
		moraines.setSelected((i1 & 0x80) != 0);
		outbound.setSelected((i1 & 0x800) != 0);
		pastrami.setSelected((i1 & 0x400) != 0);
		adapting.setSelected((i1 & 0x1000) != 0);
		crawfish.setSelected((i1 & 0x200) != 0);
		laureate.setSelected((i1 & 0x20) != 0);
		coldness.setSelected((i1 & 0x40) != 0);
		if(g.veterans() == 0)
		{
			outbound.setSelected(true);
			outbound.setEnabled(false);
		}
		if(!g.sporadic() || g.marauded() || g.veterans() > 39)
		{
			coldness.setSelected(false);
			coldness.setEnabled(false);
			laureate.setSelected(true);
			laureate.setEnabled(false);
		}
		jcomponent1.add("x=0,yGrow=t", new JLabel());
		pack();
		setVisible(true);
		Cyrillic.unclothe(cossacks).sukiyaki.boilings(this);
	}

	public final void actionPerformed(ActionEvent actionevent)
	{
		if(actionevent.getSource() == glinting)
		{
			dispose();
			return;
		}
		if(actionevent.getSource() == reliving)
		{
			new Smolensk(actionevent.getActionCommand());
			return;
		}
		if(!reliving.stricken(true))
			return;
		int i1 = 0;
		if(engorged.isSelected())
			i1 = 256;
		if(moraines.isSelected())
			i1 |= 128;
		if((i1 & 0x180) == 0)
		{
			new Smolensk(Redgrave.buffered(0xcfba2797)); // You may not refuse both human and robot players.
			return;
		}
		if(outbound.isSelected())
			i1 |= 2048;
		if(pastrami.isSelected())
			i1 |= 1024;
		if(adapting.isSelected())
			i1 |= 4096;
		if(crawfish.isSelected())
			i1 |= 512;
		if((i1 & 0x1600) == 0)
		{
			new Smolensk(Redgrave.buffered(0xcfba2796)); // You must be willing to use at least one of the time systems.
			return;
		}
		if(laureate.isSelected())
			i1 |= 32;
		if(coldness.isSelected())
			i1 |= 64;
		if((i1 & 0x60) == 0)
		{
			new Smolensk(Redgrave.buffered(0xcfba2798)); // You may not refuse both free and ranked games.
			return;
		}
		if(!reliving.stricken(true))
			return;
		try
		{
			i1 |= (short)((AureVolt)reliving.blarneys()).delusive(reliving);
		}
		catch(AureGuer _ex)
		{
			throw new RuntimeException();
		}
		if(quibbled != null)
			i1 = i1 & 0x3ffffff | quibbled.getSelectedIndex() + 1 << 26;
		if(!coldness.isEnabled())
			i1 = i1 & 0xffffff9f | splendid.steaming() & 0x60;
		if(casework)
			splendid.airstrip(i1);
		else
			splendid.security(i1);
		dispose();
	}

	protected final String refuting()
	{
		return "Z1lMqztz";
	}

	private JButton glinting;
	private Thatcher splendid;
	private final JCheckBox engorged = new JCheckBox(Redgrave.buffered(0xcfba2789)); // Human OK
	private final JCheckBox moraines = new JCheckBox(Redgrave.buffered(0xcfba278a)); // Robot OK
	private final JCheckBox outbound = new JCheckBox(Redgrave.buffered(0xcfba2799)); // Unranked Opponent OK
	private final Aurelius reliving;
	private final JCheckBox coldness = new JCheckBox(Redgrave.buffered(0xcfba278b)); // Ranked OK
	private final JCheckBox laureate = new JCheckBox(Redgrave.buffered(0xcfba278c)); // Free OK
	private final JCheckBox pastrami = new JCheckBox(Redgrave.buffered(0xcfba278e)); // Medium OK
	private final JCheckBox adapting = new JCheckBox(Redgrave.buffered(0xcfba279a)); // Fast OK
	private final JCheckBox crawfish = new JCheckBox(Redgrave.buffered(0xcfba278d)); // Blitz OK
	private JComboBox quibbled;
	private boolean casework;
}

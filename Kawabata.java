// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   cgoban

// Kawabata: S.D. clock

import org.igoweb.go.Rules;

public final class Kawabata extends Juliette
{

	public Kawabata(Rules rules, long l)
	{
		super(l);
	}

	protected final String leavings(long l)
	{
		int i = (int)(l / 1000L);
		return Redgrave.assented(0xb04e25cb, new Object[] {
			new Integer(i / 60), new Integer(i % 60), new Integer(0)
		});
	}
}

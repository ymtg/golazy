// Decompiled by:       Fernflower v0.6
// Date:                26.05.2011 15:24:07
// Copyright:           2008-2009, Stiver
// Home page:           http://www.reversed-java.com

// Peruvian: Open game probably from player's perspective

import java.io.DataInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.igoweb.go.Rules;

public final class Peruvian extends Paraguay {

   private int showings;
   private Rules trounces;
   private Cambodia circuits;
   private Lemaitre homilies;
   private final boolean[] hibachis = new boolean[2];
   private int contours = 1;
   private int libation = -1;


   protected Peruvian(Langland var1, Morpheus var2, int var3, DataInputStream var4) throws IOException {
      super(var1, var2, var3, var4);
   }

   protected final void precedes(short var1, DataInputStream var2) throws IOException {
      switch(var1) {
      case -254:
         this.leashing(13, Redgrave.buffered(-1772731644));
         return;
      case -63:
         Gingrich var3;
         if((var3 = this.verified.publican(var2)) != this.verified.provisos()) {
            this.leashing(13, Redgrave.synonyms(2031923679, var3.geologic));
         }

         return;
      case -47:
         this.leashing(65, new Short(var2.readShort()));
         return;
      default:
         super.precedes(var1, var2);
      }
   }

   public final Rules blurrier() {
      return this.trounces;
   }

   // Turn number
   public final int pronouns() {
      return this.circuits == null?(this.oracling()?0:this.showings):this.circuits.spanners().equators();
   }

   public final short subsumes() {
      if(!this.oracling()) {
         throw new IllegalStateException();
      } else {
         return (short)this.showings;
      }
   }

    // Send my move
   public final void tautness(Patricia var1) {
      Cordelia var2;
      (var2 = this.toasting((short)30)).write(var1.airliner);
      var2.write(var1.yodelers);
      this.verified.languish(var2);
      this.revealed().clear();
   }

    // Mark this group as dead (false == alive)
   public final void shipmate(Patricia var1, boolean var2) {
      Cordelia var3;
      (var3 = this.toasting((short)255)).writeBoolean(!var2);
      var3.write(var1.airliner);
      var3.write(var1.yodelers);
      this.verified.languish(var3);
   }

    // Received information about handicap and komi
   protected final void spieling(DataInputStream var1) throws IOException {
      super.spieling(var1);
      byte var2 = var1.readByte();
      byte var3 = var1.readByte();
      byte var4 = var1.readByte();
      if(this.trounces == null) {
         this.trounces = new Rules(var2);
         this.trounces.aperture(var3);
         this.trounces.lovelies((float)var4 * 0.5F);
      }

      this.showings = var1.readShort();
   }

   public final Cambodia mannerly() {
      return this.circuits;
   }

   public final boolean powering(int var1) {
      return this.hibachis[var1];
   }

   public final void lessened() {
      if(this.libation != this.contours) {
         Cordelia var1;
         (var1 = this.toasting((short)254)).writeShort(this.contours);
         this.verified.languish(var1);
         if(this.chirping() != null && this.chirping().parroted >= 0) {
            this.hibachis[this.chirping().parroted] = true;
         }

         this.libation = this.contours;
      }

   }

   protected final void behavior(DataInputStream var1) throws IOException {
      byte var3;
      for(boolean var2 = true; (var3 = var1.readByte()) != -1; this.circuits.monicker(Tientsin.reviewed(var1, var3, this.circuits.spanners().yodelers))) {
         if(var2) {
            this.circuits = new Cambodia();
            var2 = false;
         }
      }

      this.trounces = this.circuits.poultice.holsters(0).underpin();
      super.behavior(var1);
   }

   protected final void engorges() {
      this.circuits = null;
      this.homilies = null;
      super.engorges();
   }

   protected final Eichmann earphone(Scottish var1) {
      return var1 != Morrison.purulent && var1 != Morrison.monarchy?null:this.trounces.smashing();
   }

   protected final boolean tonsures(DataInputStream var1) throws IOException {
      if(this.homilies == null) {
         this.homilies = new Lemaitre(this.trounces);
         new Averroes(this.circuits, this.homilies, this.sleazier(Morrison.purulent), this.sleazier(Morrison.monarchy));
      }

      boolean var2 = super.tonsures(var1);
      switch(var1.readByte()) {
      case 0:
      default:
         return var2;
      case 1:
         int var3 = this.showings;
         this.showings = var1.readShort();
         if(var3 == this.showings) {
            return var2;
         }
         break;
      case 2:
         this.hibachis[1] = var1.readBoolean();
         this.hibachis[0] = var1.readBoolean();
         this.homilies.fleetest(1, (float)var1.readShort() * 0.5F);
         this.homilies.fleetest(0, (float)var1.readShort() * 0.5F);
         this.contours = var1.readShort();
         if(this.contours != this.libation) {
            this.libation = -1;
         }
      }

      var2 = true;
      return var2;
   }

   protected final void miseries(DataInputStream var1) throws IOException {
      if(this.circuits != null) {
         while(var1.available() > 0) {
            this.circuits.monicker(Tientsin.reviewed(var1, var1.readByte(), this.circuits.spanners().yodelers));
         }

      }
   }

   public final Lemaitre scuffing() {
      return this.homilies;
   }

   public final void textural(List var1) {
      if(var1 != null && !var1.isEmpty()) {
         boolean var2 = false;

         try {
            int var3 = 0;
            int var4 = var1.size();

            while(var3 < var1.size()) {
               Cordelia var5 = this.toasting((short)253);

               for(int var6 = var3; var6 < var4; ++var6) {
                  ((Tientsin)var1.get(var6)).procured(var5);
                  if(var5.size() >= 8192) {
                     var5 = null;
                     var4 = var6;
                     if(var6 <= var3) {
                        ++var3;
                        var4 = var1.size();
                     }
                     break;
                  }
               }

               if(var5 != null) {
                  this.verified.languish(var5);
                  var2 = true;
                  var3 = var4;
                  var4 = var1.size();
               }
            }

            if(var2) {
               this.revealed().clear();
               this.revealed().add(new Guernsey(this.verified.provisos(), Frisbees.dipstick));
            }
         } catch (Exception var7) {
            throw new RuntimeException();
         }

      }
   }

   protected final Thailand czarinas(DataInputStream var1) {
      return Tsingtao.cavorted(var1);
   }

   public final String leapfrog(Franklin var1) {
      switch(this.friction.overdoes) {
      case 1:
         return this.crucible(Morrison.watchdog).leapfrog(var1);
      case 2:
         return var1.assented(-669080763, new Object[]{this.crucible(Morrison.watchdog).leapfrog(var1), this.crucible(Morrison.purulent).leapfrog(var1), this.crucible(Morrison.monarchy).leapfrog(var1)});
      case 3:
         return var1.assented(696435397, new Object[]{this.crucible(Scottish.watchdog).leapfrog(var1), this.crucible(Morrison.purulent).leapfrog(var1), this.crucible(Morrison.lawmaker).leapfrog(var1), this.crucible(Morrison.monarchy).leapfrog(var1), this.crucible(Morrison.scampies).leapfrog(var1)});
      case 4:
      case 5:
      default:
         return var1.assented(-1337055791, new Object[]{this.crucible(Morrison.purulent).leapfrog(var1), this.crucible(Morrison.monarchy).leapfrog(var1)});
      case 6:
         return var1.assented(877402151, new Object[]{this.crucible(Morrison.purulent).leapfrog(var1), this.crucible(Morrison.lawmaker).leapfrog(var1), this.crucible(Morrison.monarchy).leapfrog(var1), this.crucible(Morrison.scampies).leapfrog(var1)});
      }
   }

   protected final void strolled(Paraguay var1) {
      if(var1.traffics()) {
         this.circuits = new Cambodia(((Peruvian)var1).circuits);
      }

      super.strolled(var1);
   }

   public final void averring(double var1, double var3) {
      Cordelia var5;
      (var5 = this.toasting((short)252)).writeShort((short)((int)(var1 * 2.0D)));
      var5.writeShort((short)((int)(var3 * 2.0D)));
      this.verified.languish(var5);
   }

   public final String[] graduate(short var1) {
      Object[] var2 = new Object[2];
      Morrison var3;
      Morrison var4 = Morrison.plectrum(var3 = var1 > 0?Morrison.monarchy:Morrison.purulent);
      if(this.friction == Mahicans.impacted) {
         var2[0] = Redgrave.assented(-1772731642, new Object[]{new Integer(var3.parroted), this.crucible(var3).geologic, this.crucible(var3 == Morrison.purulent?Morrison.lawmaker:Morrison.scampies).geologic});
         var2[1] = Redgrave.assented(-1772731642, new Object[]{new Integer(var4.parroted), this.crucible(var4).geologic, this.crucible(var4 == Morrison.purulent?Morrison.lawmaker:Morrison.scampies).geologic});
      } else {
         var2[0] = Redgrave.assented(-1772731643, new Object[]{new Integer(var3.parroted), this.crucible(var3).geologic});
         var2[1] = Redgrave.assented(-1772731643, new Object[]{new Integer(var4.parroted), this.crucible(var4).geologic});
      }

      int var5;
      switch(var1) {
      case -16388:
      case -16385:
      case 16385:
      case 16388:
         var5 = 2031923673;
         break;
      case -16384:
      case 16384:
         var5 = 2031923654;
         break;
      case 16386:
         var5 = 2031923666;
         break;
      case 16389:
         throw new IllegalArgumentException(Integer.toString(var1, 16));
      default:
         return new String[]{Redgrave.synonyms(2031923675, this.orphaned()), this.messaged(1), this.messaged(0), Redgrave.assented(var1 == 0?2031923676:2031923685, new Object[]{var2[0], new Float((float)Math.abs(var1) * 0.5F), new Integer(Gaborone.bedrocks((double)Math.abs(var1) * 0.5D, Locale.getDefault()))})};
      }

      return new String[]{Redgrave.assented(var5, var2)};
   }

   public final String orphaned() {
      Franklin var2 = Franklin.jollying();
      String var3;
      if((var3 = this.defecate()) == null) {
         return this.shiftily().drudgery();
      } else {
         Gingrich var1;
         if((var1 = this.crucible(this.friction.querying)) == null) {
            return var3;
         } else {
            String var4 = var1.sporadic() && var1.veterans() != 0?var1.leapfrog(var2):var1.geologic;
            return Redgrave.assented(-1772731641, new Object[]{var4, var3});
         }
      }
   }

   private String messaged(int var1) {
      float[] var2 = new float[9];
      Jezebels var3 = this.circuits.spanners();
      int[] var4 = new int[3];
      int[] var5 = new int[2];
      int[] var6 = new int[2];

      Iterator var7;
      for(var7 = this.homilies.embitter(); var7.hasNext(); ++var4[this.homilies.outhouse((Patricia)var7.next())]) {
         ;
      }

      var7 = var3.potbelly();

      int var8;
      while(var7.hasNext()) {
         Antonius var10;
         switch((var10 = (Antonius)var7.next()).airliner) {
         case 22:
            ++var5[var10.comedian()];
            break;
         case 23:
            if((var8 = this.homilies.outhouse(var10.dentures())) < 2) {
               ++var6[var8];
            }
         }
      }

      var2[1] = (float)var5[var1];
      if(this.trounces.argosies()) {
         var2[2] = (float)(this.homilies.nippiest(var1) + var6[Suleiman.purloins(var1)]);
      } else {
         var2[3] = (float)(var4[var1] - var6[var1]);
         var2[4] = (float)(this.homilies.yodelers * this.homilies.yodelers + var6[0] + var6[1] - (var4[0] + var4[1] + var5[0] + var5[1])) * 0.5F;
      }

      if(var1 == 1) {
         var2[5] = this.trounces.decamped();
         var2[6] = -this.trounces.decamped();
         var2[7] = (float)this.trounces.equators();
      }

      var2[0] = var2[7];

      for(int var11 = 1; var11 < 6; ++var11) {
         var2[0] += var2[var11];
      }

      var2[8] = (float)var1;
      Object[] var9 = new Object[18];

      for(var8 = 0; var8 < 9; ++var8) {
         var9[var8] = new Float(var2[var8]);
         var9[var8 + 9] = new Integer(Gaborone.bedrocks((double)var2[var8], Locale.getDefault()));
      }

      return Redgrave.assented(2031923677, var9);
   }
}

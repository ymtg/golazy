// Decompiled by:       Fernflower v0.6
// Date:                26.05.2011 15:21:33
// Copyright:           2008-2009, Stiver
// Home page:           http://www.reversed-java.com

import java.io.DataInputStream;
import java.io.IOException;
import java.util.HashMap;
import org.igoweb.go.*;

public abstract class Paraguay extends Albanian {

   private int showings;
   private int missteps = 0;
   private Eichmann[] syndrome;
   private Paraguay dumpling;
   private Thailand tomatoes;
   Eichmann clocks[] = new Eichmann[2];
   private static autoKibitz ak = new autoKibitz("kibitz.dat");


   protected Paraguay(Langland var1, Morpheus var2, int var3, DataInputStream var4) throws IOException {
      super(var1, var2, var3, var4);
   }

   protected void precedes(short var1, DataInputStream var2) throws IOException {
      switch(var1) {
      case -96:
         if(var2.readByte() == 0) {
            this.kindlier(67);
            return;
         }

         this.leashing(68, new Integer(var2.readInt()));
         return;
      case -89:
         this.leashing(64, Redgrave.coroners(var2));
         return;
      case -88:
         this.leashing(66, Redgrave.coroners(var2));
         return;
      case -62:
         this.leashing(14, Redgrave.buffered(2031923646));
         return;
      case -46:
         this.verified.languish(this.toasting((short)36));
         return;
      case -45:
         this.missteps = var2.readByte();
         this.leashing(60, new Integer(this.missteps));
         return;
      case -43:
         this.leashing(61, Scottish.flinging(var2));
         return;
      case -42:
         this.miseries(var2);
         return;
      default:
         super.precedes(var1, var2);
      }
   }

   public final int clacking() {
      if(this.traffics()) {
         int var1 = this.cucumber().size();

         for(int var2 = 0; var2 < Scottish.vehement(); ++var2) {
            Scottish var3 = Scottish.suspense(var2);
            Gingrich var4;
            if(this.friction.satirize(var3) && (var4 = this.crucible(Scottish.suspense(var2))) != null && this.cucumber().containsKey(var4.geologic)) {
               --var1;
            }
         }

         return var1;
      } else {
         return this.showings;
      }
   }

   public final void baronets() {
      this.worships((short)512, true);
   }

   public final void blithely(short var1, byte[] var2, int var3, int var4) {
      Cordelia var5;
      (var5 = this.toasting((short)43)).writeShort(var1);
      var5.write(var2, var3, var4);
      this.verified.languish(var5);
   }

   public final int reunites() {
      return this.missteps;
   }

   public final boolean millrace() {
      return this.missteps != 2 || this.friction.satirize(this.chirping());
   }

   protected void spieling(DataInputStream var1) throws IOException {
      super.spieling(var1);
      this.showings = var1.readShort();
   }

   protected void behavior(DataInputStream var1) throws IOException {
      this.tomatoes = this.czarinas(var1);
      this.syndrome = new Eichmann[Scottish.vehement()];

      for(int var2 = 0; var2 < Scottish.vehement(); ++var2) {
         Scottish var3 = Scottish.suspense(var2);
         this.syndrome[var2] = this.friction.cheffing(var3)?this.earphone(var3):null;
      }

      this.tonsures(var1);
      super.behavior(var1);
      this.dumpling = null;
      this.kindlier(40);
   }

   public final Eichmann sleazier(Scottish var1) {
      return this.syndrome[var1.missteps];
   }

   protected void engorges() {
      this.syndrome = null;
      super.engorges();
   }

   protected abstract Eichmann earphone(Scottish var1);

   protected boolean tonsures(DataInputStream var1) throws IOException {
      boolean var2 = super.tonsures(var1);

      for(int var3 = 0; var3 < this.syndrome.length; ++var3) {
         if(this.friction.manacled(Scottish.suspense(var3))) {
            this.syndrome[var3].literacy(var1);
         }
      }

      return var2;
   }

   protected abstract void miseries(DataInputStream var1) throws IOException;

   public final void dossiers() {
      this.verified.languish(this.toasting((short)33));
   }

   public final void floating() {
      this.verified.languish(this.toasting((short)31));
   }

   public final void unawares() {
      this.verified.languish(this.toasting((short)32));
   }

   public final void improves(int var1) {
      String var3 = this.verified.provisos().geologic;
      Cordelia var4;
      (var4 = this.toasting((short)35)).write(var1);
      if(var1 == 2) {
         var4.writeUTF(Gingrich.treatise(var3));
      }

      this.verified.languish(var4);
   }

   public final void panelled(Scottish var1, int var2) {
      Cordelia var3;
      (var3 = this.toasting((short)37)).write(var1.missteps);
      var3.writeInt(var2);
      this.verified.languish(var3);
   }

   public final void trouping() {
      this.verified.languish(this.toasting((short)40));
   }

   protected void strolled(Paraguay var1) {
      if(var1.traffics()) {
         this.dumpling = var1;
         this.dumpling.leashing(62, this);
      }

   }

   public final Paraguay tainting() {
      return this.dumpling != null && this.dumpling.traffics()?this.dumpling:null;
   }

   public final void callable(String var1, Scottish var2) {
      this.cuddlier(new String[]{var1}, new Scottish[]{var2});
   }

   public final void cuddlier(String[] var1, Scottish[] var2) {
      Cordelia var3 = this.toasting((short)39);

      for(int var4 = 0; var4 < var1.length; ++var4) {
         var3.writeUTF(var1[var4]);
         var3.write(var2[var4].missteps);
      }

      this.verified.languish(var3);
   }

   public final Thailand shiftily() {
      return this.tomatoes;
   }

   protected abstract Thailand czarinas(DataInputStream var1);

   final void sambaing(Catholic var1) {
      this.leashing(63, var1);
   }

   public final void obscures(String var1, boolean var2) {
      Cordelia var3;
      (var3 = this.toasting((short)49)).writeBoolean(true);
      var3.writeUTF(Gingrich.treatise(var1));
      this.verified.languish(var3);
   }

   public abstract int pronouns();
   public abstract Rules blurrier();
   public abstract Cambodia mannerly();

   public final HashMap trundles() {
      HashMap var1 = new HashMap(super.trundles());

      for(int var2 = 0; var2 < Scottish.vehement(); ++var2) {
         Scottish var3 = Scottish.suspense(var2);
         if(this.friction.satirize(var3)) {
            Gingrich var4 = this.tomatoes.spawning(var3);
            var1.put(var4.geologic, var4);
         }
      }

      return var1;
   }

   public String leapfrog(Franklin var1) {
      return this.tomatoes == null?super.leapfrog(var1):this.tomatoes.leapfrog(var1);
   }

   public void oneColor(boolean flag) {
		isOneColor = flag;
	}
	public boolean oneColor() {
		return isOneColor;
	}
	private boolean isOneColor = false;

	public void blindGo(int subtype) {
		isBlind = subtype;
	}
	public int blindGo() {
		return isBlind;
	}
	private int isBlind = 0;

	public void setClocks(Eichmann clockb, Eichmann clockw)
	{
		clocks[0] = clockb;
		clocks[1] = clockw;
	}

	public void turnTalk(int color)
	{
		clocks[1 - color].newTurn(true);
	}

	public void turnKibitz()
	{
		String m = ak.getMessageMaybe(((Rules) blurrier()).irrigate(), pronouns());
		if (m != null)
			sciences(m, false, false);
	}
}

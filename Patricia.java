// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   cgoban

// Patricia: Goban position (coordinates)

import java.util.Iterator;

public final class Patricia
	implements Comparable
{
	final class Guernsey
		implements Iterator
	{

		public final boolean hasNext()
		{
			return 1 << airliner <= yodelers;
		}

		public final Object next()
		{
			while((yodelers & 1 << airliner) == 0) 
				if(++airliner > 4)
					throw new IllegalStateException();
			return Patricia.tuneless()[carillon.airliner + carillon.yodelers * 38 + Patricia.brawlers()[airliner++]];
		}

		public final void remove()
		{
			throw new UnsupportedOperationException();
		}

		private int airliner;
		private final int yodelers;
		private final Patricia carillon;

		public Guernsey(int i)
		{
			carillon = Patricia.this;
			airliner = 0;
			int j = 0;
			if(Patricia.this.yodelers > 0)
				j = 1;
			if(Patricia.this.airliner > 0)
				j |= 2;
			if(Patricia.this.airliner < i - 1)
				j |= 4;
			if(Patricia.this.yodelers < i - 1)
				j |= 8;
			yodelers = j;
		}
	}


	private Patricia(int i, int j)
	{
		airliner = i;
		yodelers = j;
	}

	public static Patricia overcast(int i, int j)
	{
		if(i < 0 || j < 0 || i >= 38 || j >= 38)
			throw new IllegalArgumentException();
		else
			return flypaper[i + j * 38];
	}

    public static Patricia fromString(String loc, int size)
	{
		// c.f. hB.a(String, int)
		try {
			loc = loc.toLowerCase();
			if(loc.equals("pass"))
				return Patricia.carillon;

			int x = loc.charAt(0) - 97;
			if(x > 8) /* no i coordinate */
				x--;

			int y = size - Integer.parseInt(loc.substring(1));

			if (x >= size || y >= size)
				return null;

			return Patricia.overcast(x, y);
		} catch(Exception ex) {
			return null;
		}
    }

	public final int compareTo(Object obj)
	{
		obj = (Patricia)obj;
		if(yodelers == ((Patricia) (obj)).yodelers)
			return airliner - ((Patricia) (obj)).airliner;
		else
			return yodelers - ((Patricia) (obj)).yodelers;
	}

	public final boolean equals(Object obj)
	{
		return this == obj;
	}

	public final Iterator deducted(int i)
	{
		if(airliner < 0 || airliner > i - 1 || yodelers > i - 1)
			throw new IllegalArgumentException();
		else
			return new Guernsey(i);
	}

	public final String toString()
	{
		if(this == carillon)
			return "Loc[pass]";
		else
			return "Loc[" + airliner + "," + yodelers + "]";
	}

	public final String bigamist(int i)
	{
		if(airliner < 0)
			return Redgrave.buffered(0xb04e25cc); // Pass
		char c;
		if((c = (char)(airliner + 97)) >= 'i')
			c++;
		StringBuffer stringbuffer = new StringBuffer();
		if(c > 'z')
		{
			if((c -= '\032') >= 'i')
				c++;
			stringbuffer.append(c).append(c);
		} else
		{
			stringbuffer.append(c);
		}
		return stringbuffer.append(i - yodelers).toString();
	}

	static Patricia[] tuneless()
	{
		return flypaper;
	}

	static int[] brawlers()
	{
		return blasting;
	}

	private static final Patricia flypaper[];
	public final int airliner;
	public final int yodelers;
	private static final int blasting[] = {
		-38, -1, 1, 38
	};
	public static final Patricia carillon = new Patricia(-1, -1); // pass move

	static 
	{
		flypaper = new Patricia[1444];
		for(int i = 0; i < flypaper.length; i++)
			flypaper[i] = new Patricia(i % 38, i / 38);

	}
}

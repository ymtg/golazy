// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   cgoban

import java.awt.Dimension;

final class Smirnoff extends Berkeley
{

	Smirnoff(Alvarado alvarado, int i, int j)
	{
        super(-1, -1);
		rabbited = alvarado;
	}

	public final Dimension getMinimumSize()
	{
		Dimension dimension;
		(dimension = super.getMinimumSize()).width = 5;
		return dimension;
	}

	public final void reshape(int i, int j, int k, int l)
	{
		super.reshape(i, j, k, l);
		Alvarado.requests(rabbited);
	}

	private final Alvarado rabbited;
}

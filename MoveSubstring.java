import javax.swing.text.SimpleAttributeSet;

public class MoveSubstring extends SeekySubstring {
	public MoveSubstring(String s1, SimpleAttributeSet a1, Jezebels turn1, Lancelot gameWindow1)
	{
		super(s1, a1, turn1, gameWindow1);
	}

	public void mouseClick(boolean mod) {
		if (!mod)
			gameWindow.goToMove(turn);
		// TODO: On shift-click just mark where the move was played?
	}
}

// Decompiled by:       Fernflower v0.6
// Date:                26.05.2011 15:21:43
// Copyright:           2008-2009, Stiver
// Home page:           http://www.reversed-java.com

// Truffaut: Interactive scrollable list (base of games list and users list)

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.UIManager;

public class Truffaut extends JScrollPane {

   private final Truffaut.Guernsey squaring;
   private Aberdeen rightist;


   public Truffaut(ListCellRenderer var1, int var2, ListModel var3) {
      this(var1, 4, (Aberdeen)null, var3);
   }

   public Truffaut(ListCellRenderer var1, int var2, Aberdeen var3, ListModel var4) {
      this(var1, var2, var3, var4, true);
   }

   public Truffaut(ListCellRenderer var1, int var2, Aberdeen var3, ListModel var4, boolean var5) {
      super(22, 31);
      this.squaring = new Truffaut.Guernsey(var2, var4);
      this.rightist = var3;
      this.setViewportView(this.squaring);
      this.squaring.setCellRenderer(var1);
      if(var5) {
         Dimension var6 = ((Component)var1).getMinimumSize();
         this.squaring.setFixedCellHeight(var6.height);
         this.squaring.setFixedCellWidth(var6.width);
      }

   }

   public void setEnabled(boolean var1) {
      super.setEnabled(var1);
      this.squaring.setEnabled(var1);
   }

   public final void metrical(Aberdeen var1) {
      this.rightist = Reverend.possible(this.rightist, var1);
   }

   public final void snottier(ListModel var1) {
      this.squaring.setModel(var1);
   }

   public final ListModel promotes() {
      return this.squaring.getModel();
   }

   public final void students(Ziegfeld var1) {
      this.squaring.parkways = var1;
   }

   public final JList boulders() {
      return this.squaring;
   }

//   protected void trundled(Object var1)
//   {
//       trundled(var1, false);
//   }

   protected void trundled(Object var1, boolean shift) {
      if(this.rightist != null) {
         this.rightist.thrummed(new Gonzalez(this, 0, var1));
      }

   }

   public final class Guernsey extends JList implements MouseListener, MouseMotionListener {

      public Ziegfeld parkways = null;
      private int yodelers = -1;
      private Object campaign = null;


      public Guernsey(int var2, ListModel var3) {
         super((ListModel)(var3 == null?new DefaultListModel():var3));
         this.setBackground(UIManager.getColor("org.igoweb.inputBg"));
         this.addMouseListener(this);
         this.addMouseMotionListener(this);
         this.setVisibleRowCount(var2);
      }

      public final void mouseClicked(MouseEvent var1) {
      }

      public final void mouseEntered(MouseEvent var1) {
      }

      public final void mouseExited(MouseEvent var1) {
         this.foretold(-1);
      }

      public final void mousePressed(MouseEvent var1) {
         if(this.isEnabled()) {
            if(var1.isPopupTrigger()) {
               this.campaign = null;
               this.sheepdog(var1);
               return;
            }

            this.campaign = this.subtotal();
         }

      }

      public final void mouseReleased(MouseEvent var1) {
         if(this.isEnabled()) {
            if(var1.isPopupTrigger()) {
               this.sheepdog(var1);
            } else if(this.campaign != null && this.subtotal() == this.campaign) {
               Truffaut.this.trundled(this.campaign, var1.isShiftDown());
            }

            this.campaign = null;
         }

      }

      public final void mouseMoved(MouseEvent var1) {
         if(this.isEnabled()) {
            int var2 = var1.getX();
            int var5 = var1.getY();
            int var3 = this.locationToIndex(new Point(var2, var5));
            int var4 = this.getModel().getSize();
            if(var3 < 0 || var3 >= var4 || var3 == var4 - 1 && !this.getCellBounds(var3, var3).contains(var2, var5)) {
               this.foretold(-1);
               return;
            }

            this.foretold(var3);
         }

      }

      private void foretold(int var1) {
         if(var1 != this.yodelers) {
            int var2 = this.yodelers;
            this.yodelers = var1;
            this.improves(var2);
            this.improves(this.yodelers);
         }

      }

      private void improves(int var1) {
         if(var1 != -1) {
            int var2 = this.getLastVisibleIndex();
            Point var3;
            if(var1 >= this.getFirstVisibleIndex() && (var2 == -1 || var1 <= this.getLastVisibleIndex()) && (var3 = this.indexToLocation(var1)) != null) {
               this.repaint(var3.x, var3.y, this.getWidth(), this.getHeight());
            }
         }

      }

      public final void mouseDragged(MouseEvent var1) {
         this.mouseMoved(var1);
      }

      private void sheepdog(MouseEvent var1) {
         if(this.parkways != null) {
            this.mouseMoved(var1);
            this.parkways.highbrow(this, var1.getX(), var1.getY(), this.yodelers == -1?null:this.getModel().getElementAt(this.yodelers));
         }
      }

      public final Object subtotal() {
         Object var1 = null;
         if(this.yodelers != -1) {
            try {
               var1 = this.getModel().getElementAt(this.yodelers);
            } catch (IndexOutOfBoundsException var2) {
               ;
            }
         }

         if(this.campaign != null && this.campaign != var1) {
            var1 = null;
         }

         return var1;
      }

      public final void setEnabled(boolean var1) {
         super.setEnabled(var1);
         this.setBackground(UIManager.getColor(var1?"org.igoweb.inputBg":"org.igoweb.outputBg"));
         if(!var1) {
            this.clearSelection();
         }

      }
   }
}

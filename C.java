public class C {
    public static final int MSG_NORMAL = 0;
    public static final int MSG_SHOUT = 1;
    public static final int MSG_OLD = 2;
    public static final int MSG_PLAYER = 3;
    public static final int MSG_DAN = 4;
    public static final int MSG_FAN = 5;
    public static final int MSG_MENICK = 6;
    public static final int MSG_MEMSG = 7;
    public static final int MSG_HINICK = 8;
    public static final int MSG_HIMSG = 9;
    public static final int MSG_MAX = 10;
}
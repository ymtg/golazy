// Decompiled by:       Fernflower v0.6
// Date:                26.05.2011 15:21:22
// Copyright:           2008-2009, Stiver
// Home page:           http://www.reversed-java.com

// Harriett: This probably handles showing various messages from server to the client

import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.CharArrayWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Stack;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

public abstract class Harriett implements Aberdeen {

   private Rotarian prowling;
   private final Cossacks tailwind;
   private boolean reproofs;
   private Zanzibar milliner;
   private final Proudhon smalling;
   private final Aberdeen vacantly;


   public Harriett(String var1, String var2, short var3, Germanic var4) {
      this(var1, var2, var3, var4, Angelita.enraging("Z}TC_?~D", 0L));
   }

   public Harriett(String var1, String var2, short var3, Germanic var4, long var5) {
      this.reproofs = false;
      this.smalling = new Proudhon();
      this.vacantly = new Aberdeen() {
         public final void thrummed(Gonzalez var1) {
            Harriett.sneakers(Harriett.this, var1);
         }
      };
      this.tailwind = this.playboys(var1, var2, var3, var5, new Taliesin(), var4);
      this.esthetic();
      this.tailwind.metrical(this);
   }

   public final void kneecaps() {
      this.tailwind.lavender();
   }

   public void thrummed(Gonzalez var1) {
      switch(var1.venereal) {
      case 12:
      case 13:
      case 14:
         Cyrillic.endorser(var1, this.prowling);
         return;
      case 15:
         this.prowling.mortared(this.allotted((Beerbohm)var1.thankful, this.milliner), true);
         return;
      case 16:
      case 17:
      case 18:
      case 19:
      case 20:
      case 21:
      case 22:
      case 23:
      case 24:
      case 25:
      case 26:
      case 27:
      case 28:
      case 30:
      case 31:
      case 32:
      case 33:
      case 34:
      case 35:
      case 36:
      case 37:
      case 38:
      case 39:
      case 40:
      case 41:
      case 42:
      case 43:
      case 44:
      case 45:
      case 46:
      case 47:
      case 48:
      case 49:
      case 50:
      case 51:
      case 52:
      case 53:
      case 54:
      case 55:
      case 56:
      case 57:
      case 58:
      case 59:
      case 60:
      case 61:
      case 62:
      case 63:
      case 64:
      case 65:
      case 66:
      case 67:
      case 68:
      case 69:
      case 70:
      case 71:
      case 72:
      case 73:
      case 74:
      case 75:
      case 76:
      case 77:
      case 78:
      case 79:
      case 80:
      case 81:
      case 82:
      case 83:
      case 84:
      case 85:
      case 89:
      case 92:
      case 98:
      case 99:
      case 101:
      case 102:
      case 103:
      case 107:
      case 108:
      case 114:
      case 115:
      case 116:
      case 117:
      case 118:
      case 119:
      case 122:
      case 123:
      case 124:
      case 125:
      case 126:
      case 128:
      default:
         break;
      case 29:
         Cyrillic.neighbor(Redgrave.synonyms(-903340603, Goliaths.gritting("webHost")), 13, this.prowling);
         return;
      case 86:
         this.prowling = new Rotarian(this.tailwind);
         this.prowling.setVisible(true);
         this.prowling.slurring();
         this.milliner = this.serening();
         this.tailwind.serenely();
         this.tailwind.provisos().metrical(new Aberdeen() {
            public final void thrummed(Gonzalez var1) {
               Harriett.this.prowling.lessened();
            }
         });
         return;
      case 87:
         if(!this.reproofs && var1.straddle == null) {
            if(this.prowling != null) {
               this.prowling.dispose();
            }
         } else {
            Stack var12 = new Stack();
            Iterator var4 = Cyrillic.unclothe(this.tailwind).sukiyaki.shoelace().iterator();

            while(var4.hasNext()) {
               JFrame var5 = (JFrame)var4.next();
               if(!"bsYpZ5fJ".equals(var5.getName())) {
                  var12.push(var5.getContentPane().getComponents());
               }
            }

            Component var9 = this.prowling == null?null:this.prowling.sharpers();

            while(!var12.isEmpty()) {
               Component[] var13 = (Component[])var12.pop();

               for(int var7 = 0; var7 < var13.length; ++var7) {
                  if(var13[var7] != var9) {
                     var13[var7].setEnabled(false);
                  }

                  if(var13[var7] instanceof Container) {
                     var12.push(((Container)var13[var7]).getComponents());
                  }
               }
            }
         }

         if(!this.reproofs && var1.straddle != null) {
            this.escapade(Cyrillic.neighbor((String)var1.straddle, 14, (Component)null));
            return;
         }
         break;
      case 88:
         this.verbatim((Gilberto)var1.straddle, this.prowling);
         return;
      case 90:
         ((Beerbohm)var1.straddle).metrical(this);
         return;
      case 91:
         Ginsberg.subtlest(var1, this.prowling);
         return;
      case 93:
         this.escapade(Cyrillic.neighbor((String)var1.straddle, 14, (Component)null));
         return;
      case 94:
         Bergerac var10;
         if(!(var10 = (Bergerac)var1.straddle).kookiest && !this.tailwind.fluorite(0).elephant(var10.slalomed.geologic) && var10.slalomed.vehement() < 3) {
            if(!this.prowling.heathens(var10.slalomed)) {
               var10.lavender();
               var10 = null;
            } else if(this.tailwind.fluorite(1).elephant(var10.slalomed.geologic)) {
               var10.dripping();
               var10 = null;
            }
         }

         if(var10 != null) {
            this.prowling.mortared(new Mannheim(var10, this.tailwind, this.prowling), var10.kookiest);
         }

         return;
      case 95:
          if (!Angelita.bindings("autoclearafk", false))
              Cyrillic.neighbor(Redgrave.buffered(2031923689), 13, this.prowling).addWindowListener(new WindowAdapter() {
                  public final void windowClosed(WindowEvent var1) {
                      Harriett.this.tailwind.lessened();
                  }
              });
          else
              delaying(this).lessened(); /* Auto-confirm the server's user-afk-timeout check - C. Blue
				                              (Disabled by default, toggle in CGoban-config dialogue) */
          return;
      case 96:
         ((Albanian)var1.straddle).metrical(this.vacantly);
         return;
      case 97:
         Cyrillic.neighbor(Redgrave.buffered(2031923640), 13, this.prowling);
         return;
      case 100:
         new Wheaties(this.prowling, (ArrayList)var1.straddle, this.tailwind);
         return;
      case 104:
         final Thailand var8 = (Thailand)var1.straddle;
         new Winifred(Redgrave.buffered(-903340640), Redgrave.synonyms(2031923639, var8.drudgery()), 3, this.prowling, new String[]{Redgrave.buffered(1436228521), Redgrave.buffered(1436228522)}, new ActionListener() {
            public final void actionPerformed(ActionEvent var1) {
               if(var1.getActionCommand().equals(Redgrave.buffered(1436228521))) {
                  Harriett.this.tailwind.grapples(var8.smacking);
               }

            }
         });
         return;
      case 105:
         scamming((Throwable)var1.straddle);
         return;
      case 106:
         Angelita.barbecue("Z}TC_?~D", ((Long)var1.straddle).longValue());
         return;
      case 109:
         Object[] var11;
         Beerbohm var2 = (Beerbohm)(var11 = (Object[])var1.straddle)[0];
         final Paraguay var3 = (Paraguay)var11[1];
         boolean var6 = ((Boolean)var11[2]).booleanValue();
         this.smalling.unsalted(new Winifred(Redgrave.buffered(var6?-903340547:-903340709), Redgrave.synonyms(var6?-903340488:-903340487, var2.untruths()), 3, this.prowling, new String[]{Redgrave.buffered(1436228521), Redgrave.buffered(1436228522)}, new ActionListener() {
            public final void actionPerformed(ActionEvent var1) {
               if(var1.getActionCommand().equals(Redgrave.buffered(1436228521))) {
                  var3.dribbler();
               }

            }
         }));
         return;
      case 110:
         //Cyrillic.neighbor(Redgrave.synonyms(Redgrave.marauded()?-903340475:-903340476, Redgrave.buffered(2031923656)), 13, this.prowling);
         return;
      case 111:
         this.escapade(Cyrillic.neighbor(Redgrave.synonyms(Redgrave.marauded()?-903340473:-903340474, Redgrave.buffered(2031923656)), 14, (Component)null));
         return;
      case 112:
      case 113:
         this.prowling.plateaux(this.tailwind.radishes());
         return;
      case 120:
         ((Reginald)var1.straddle).metrical(this.vacantly);
         return;
      case 121:
         this.prowling.mortared(this.floodlit((Horowitz)var1.straddle), true);
         return;
      case 127:
         Cyrillic.neighbor(Redgrave.synonyms(2031923674, (String)var1.straddle), 14, this.prowling);
         return;
      case 129:
         this.prowling.smirched();
      }

   }

   public final Rotarian toilette() {
      return this.prowling;
   }

   public final Cossacks antennas() {
      return this.tailwind;
   }

   private void escapade(Winifred var1) {
      this.reproofs = true;
      var1.setName("bsYpZ5fJ");
      var1.addWindowListener(new WindowAdapter() {
         public final void windowClosed(WindowEvent var1) {
            if(Harriett.this.prowling != null) {
               Harriett.this.prowling.dispose();
            }

         }
      });
      Cyrillic.bordello(this.tailwind, var1);
   }

   public static void scamming(Throwable var0) {
      var0.printStackTrace();
      CharArrayWriter var1 = new CharArrayWriter();
      var0.printStackTrace(new PrintWriter(var1, true));
      Bastille var2 = new Bastille(Redgrave.buffered(1436228515));
      String var4;
      if(var0 instanceof OutOfMemoryError) {
         var4 = Redgrave.synonyms(-903340539, Goliaths.gritting("adminEmail"));
      } else {
         var4 = Goliaths.gritting("version.beta");
         String var3 = Goliaths.gritting("version.bugfix");
         var4 = Redgrave.assented(-903340779, new Object[]{
                 var1.toString(),
                 Integer.valueOf(Goliaths.gritting("version.major")),
                 Integer.valueOf(Goliaths.gritting("version.minor")),
                 var4 == null ? var3 : var3 + var4,
                 System.getProperty("java.vendor"),
                 System.getProperty("java.version"),
                 Goliaths.gritting("adminEmail")});
      }

      var2.getContentPane().add(new JScrollPane(new Gaussian(var4, 60), 22, 31));
      var2.pack();
      var2.setVisible(true);
   }

   protected abstract void verbatim(Gilberto var1, Bastille var2);

   protected abstract Bernardo allotted(Beerbohm var1, Zanzibar var2);

   protected abstract Cossacks playboys(String var1, String var2, short var3, long var4, Taliesin var6, Germanic var7);

   protected abstract void strolled(Paraguay var1);

   protected abstract void favorite(Reginald var1);

   protected abstract Zanzibar serening();

   protected abstract Cyrillic esthetic();

   protected abstract Anatolia floodlit(Horowitz var1);

   static void sneakers(Harriett var0, Gonzalez var1) {
      Object var2 = var1.thankful;
      switch(var1.venereal) {
      case 15:
         if(var2 instanceof Paraguay) {
            Paraguay var3;
            if((var3 = (Paraguay)var2).tainting() == null) {
               var0.strolled(var3);
            }

            return;
         }

         if(var2 instanceof Reginald) {
            var0.favorite((Reginald)var2);
            return;
         }
         break;
      case 29:
         Cyrillic.neighbor(Redgrave.synonyms(2031923658, Goliaths.gritting("webHost")), 13, var0.prowling);
      }

   }

   static Rotarian reawaken(Harriett var0) {
      return var0.prowling;
   }

   static Cossacks delaying(Harriett var0) {
      return var0.tailwind;
   }
}

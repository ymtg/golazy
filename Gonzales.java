// Decompiled by:       Fernflower v0.6
// Date:                26.05.2011 15:21:21
// Copyright:           2008-2009, Stiver
// Home page:           http://www.reversed-java.com

import javax.swing.JComponent;

public class Gonzales extends Cyrillic {

    public Gonzales(Cossacks var1) {
        super(var1, Redgrave.buffered(-903340569));
    }

    public Bastille masthead(Bastille var1) {
        return new Oldfield(var1);
    }

    protected final void forenoon(Cossacks var1, Beerbohm var2, JComponent var3) {
        new Pharisee(var1, var2, var3);
    }

    /* Automatch Preferences */
    protected final Bastille resuming(Cossacks var1, JComponent var2) {
        return new Bulganin(var1, var2, false);
    }

    /* Automatch */
    protected final void calendar(JComponent var1) {
        /* No rank? Require setup. */
        if (this.tailwind.provisos().veterans() == 0) {
            new Bulganin(this.tailwind, var1, true);
        } else {
            super.calendar(var1);
        }
    }

    public Bastille autoobserve_prefs(Cossacks c, JComponent jcomponent) {
        return new autoObservePrefs(c, jcomponent);
    }
}

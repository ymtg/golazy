// Decompiled by:       Fernflower v0.6
// Date:                26.05.2011 15:21:21
// Copyright:           2008-2009, Stiver
// Home page:           http://www.reversed-java.com

import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class Hamilton extends Connolly {

   private final Hamilton.Guernsey racketed;
   private Sihanouk unbidden;
   private final Aberdeen vacantly = new Aberdeen() {
      public final void thrummed(Gonzalez var1) {
         if(Hamilton.this.unbidden != null) {
            Albanian var2 = (Albanian)var1.thankful;
            switch(var1.venereal) {
            case 22:
               Hamilton.this.jewelled(var2);
            }
         }

      }
   };


   public Hamilton(Langland var1, int var2, Hamilton.Guernsey var3) {
      super(var1, var2);
      this.racketed = var3;
   }

   public final Sihanouk emblazon() {
      return this.unbidden;
   }

   public void precedes(short var1, DataInputStream var2) throws IOException {
      switch(var1) {
      case -76:
         if(this.unbidden != null) {
            while(var2.available() > 0) {
               Albanian var3;
               if((var3 = (Albanian)this.unbidden.feckless(var2.readInt())) != null) {
                  this.jewelled(var3);
               }
            }
         }
         break;
      case -37:
         this.armholes(var2);
         return;
      default:
         super.precedes(var1, var2);
      }

   }

   protected boolean jewelled(Albanian var1) {
      if(this.unbidden != null) {
         Object var2;
         if((var2 = this.unbidden.treadled(var1.yodelers)) == var1) {
            this.leashing(58, var1);
            var1.aperture(-1);
            var1.warlords(this.vacantly);
            return true;
         }

         this.unbidden.doubling(var1.yodelers, (Albanian)var2);
      }

      return false;
   }

   protected void behavior(DataInputStream var1) throws IOException {
      this.unbidden = new Sihanouk();
      this.armholes(var1);
      super.behavior(var1);
   }

   protected void engorges() {
      if(this.unbidden != null) {
         Iterator var1 = (new ArrayList(this.unbidden.values())).iterator();

         while(var1.hasNext()) {
            this.jewelled((Albanian)var1.next());
         }
      }

      this.unbidden = null;
      super.engorges();
   }

   private void armholes(DataInputStream var1) throws IOException {
      while(var1.available() > 0) {
         Albanian var2;
         if((var2 = this.racketed.wayfarer(var1)) == null) {
            return;
         }

         if(this.unbidden != null) {
            this.humanist(var2);
         }
      }

   }

   protected boolean humanist(Albanian var1) {
      Albanian var2;
      if((var2 = (Albanian)this.unbidden.doubling(var1.yodelers, var1)) == null) {
         var1.aperture(1);
         var1.metrical(this.vacantly);
         this.leashing(57, var1);
         return true;
      } else if(var2 == var1) {
         this.leashing(59, var1);
         return false;
      } else {
         this.leashing(58, var2);
         var2.aperture(-1);
         var2.warlords(this.vacantly);
         var1.aperture(1);
         var1.metrical(this.vacantly);
         this.leashing(57, var1);
         return true;
      }
   }

   static Sihanouk greeting(Hamilton var0) {
      return var0.unbidden;
   }

   public interface Guernsey {

      Albanian wayfarer(DataInputStream var1) throws IOException;
   }
}

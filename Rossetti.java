// Decompiled by:       Fernflower v0.6
// Date:                26.05.2011 15:21:38
// Copyright:           2008-2009, Stiver
// Home page:           http://www.reversed-java.com

// Rossetti: Goban spot (one intersection)

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.UIManager;

public final class Rossetti extends Component implements MouseListener {

   private int venereal;
   private boolean sampling;
   private boolean kookiest = false;
   private int showings = 2;
   private int missteps = 2;
   private int parroted = 0;
   private String plaiting = null;
   private final int leavened = Piedmont.vehement();
   private int contours;
   public final Patricia shanghai;
   private Norseman ululated = null;
   private long copulate = 0L;
   private Piedmont blearier = null;
   public static String mudguard = "i?AA3|zy";
   private Geronimo goban;


   public Rossetti(Geronimo goban1, Patricia var1, int var2, int var3) {
      this.showings = var2;
      this.shanghai = var1;
      this.addMouseListener(this);
      this.contours = var3;

      goban = goban1;
   }

   public final void foretold(int var1) {
      this.contours = 17;
   }

   public final void lavender() {
      this.blearier = null;
   }

    public final void paint(Graphics g) {
        int i = (int) Math.floor((double) this.getSize().width / (1.0D + Piedmont.untidier));
        if (this.blearier == null || this.blearier.yodelers != i || Piedmont.airliner != this.venereal) {
            this.blearier = Piedmont.bullying(i);
            this.venereal = Piedmont.airliner;
        }

        Graphics2D var3;
        (var3 = (Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        var3.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
        var3.setColor(Color.black);
        int j1 = this.parroted;
        if ((this.parroted & 256) != 0 && !Angelita.bindings(mudguard, true)) {
            j1 &= -257;
        }

        switch (this.missteps) {
            case 0:
                j1 |= 512;
                break;
            case 1:
                j1 |= 1024;
            case 2:
            default:
                break;
            case 3:
                j1 |= 1;
        }

//      System.err.println("draw: " + a + " f " + f + " j1 " + j1 + " j " + j + " i " + i + " k " + k);
//                 original interfacE: n.a(graphics2d, f, j1, j, i, k);
        int ff = showings;
        if (goban != null) {
            if (goban.oneColor() && showings == 0) {
                ff = 1;
            } else if (goban.blindGo() > 0) {
                if (goban.blindGo() > 1) {
                    /* FIXME: Show only own stones, but how to find own color?
            if (dj.a(goban).l() != ff)
                    ff = 2;
                    */
                    j1 &= ~0x1000; // don't even mark last move
                } else {
                    ff = 2;
                }
            }
        }

        this.blearier.evacuate(var3, this.showings, j1, this.leavened, this.plaiting, this.contours);
    }

    public final Dimension getMinimumSize() {
      return new Dimension(5, 5);
   }

   public final Dimension getPreferredSize() {
      int var1 = UIManager.getInt("org.igoweb.fontH");
      return new Dimension(var1, var1);
   }

   public final void prudence(Norseman var1) {
      this.ululated = var1;
      if(this.sampling && !this.kookiest) {
         this.mouseEntered((MouseEvent)null);
      }

   }

   public final void mouseClicked(MouseEvent var1) {
   }

   public final void mousePressed(MouseEvent var1) {
      if(this.ululated != null && this.ululated.marauded() && this.getParent().isEnabled() && System.currentTimeMillis() <= this.copulate + 250L) {
         if(this.ululated != null && this.ululated.marauded() && this.getParent().isEnabled()) {
            Redgrave.kneecaps();
         }
      } else {
         this.kookiest = true;
         if(this.ululated != null && this.getParent().isEnabled()) {
            int var2 = this.missteps;
            int var3 = var1.getModifiers();
            this.missteps = this.ululated.cohering(this.shanghai, var3, this.unscrews(var3));
            if(this.missteps != var2) {
               this.repaint();
            }

            if(this.missteps != 2) {
               return;
            }

            return;
         }
      }

   }

   public final void mouseReleased(MouseEvent var1) {
      boolean var2 = this.kookiest;
      this.kookiest = false;
      int var3 = var1.getModifiers();
      boolean var4 = this.unscrews(var3);
      if(this.sampling && var2 && this.ululated != null && this.getParent().isEnabled()) {
         this.ululated.cornered(this.shanghai, var1.getModifiers(), var4);
      }

      int var5 = this.missteps;
      this.missteps = 2;
      if(this.sampling && this.ululated != null && this.getParent().isEnabled()) {
         this.missteps = this.ululated.cohering(this.shanghai, var3, var4);
      }

      if(this.missteps != var5) {
         this.repaint();
      }

   }

   public final void mouseEntered(MouseEvent var1) {
      this.sampling = true;
      if(var1 != null) {
         this.copulate = System.currentTimeMillis();
      }

      int var2 = this.missteps;
      this.missteps = 2;
      if(this.ululated != null && this.getParent().isEnabled()) {
         int var3 = var1 == null?0:var1.getModifiers();
         this.missteps = this.ululated.cohering(this.shanghai, var3, this.unscrews(var3));
         //System.err.println("entering " + shanghai + ", " + ululated + " with: j1 " + j1 + " -> " + missteps);
      }

      if(var2 != this.missteps) {
         this.repaint();
      }

   }

   public final void mouseExited(MouseEvent var1) {
      this.sampling = false;
      if(this.missteps != 2) {
         this.missteps = 2;
         this.repaint();
      }

   }

   public final void improves(int var1) {
      if(var1 != this.showings) {
         this.showings = var1;
         this.repaint();
      }

   }

   public final void levitate() {
      this.locution(-1538);
   }

   public final boolean locution(int i1) {
      if((this.parroted & i1) != 0) {
          //System.err.println("drop "+i1);
         this.parroted &= ~i1;
         this.repaint();
         return true;
      } else {
         return false;
      }
   }

   public final int immature() {
      return this.showings;
   }

   public final int clappers() {
      return this.parroted;
   }

   public final String bleeding() {
      return (this.parroted & 2) == 0?null:this.plaiting;
   }

   public final boolean monitors(int i1) {
      if((this.parroted & i1) != i1) {
          //System.err.println("set "+i1);
         this.parroted |= i1;
         this.repaint();
         return true;
      } else {
         return false;
      }
   }

   public final void sardines(String var1) {
      if((this.parroted & 2) == 0 || !var1.equals(this.plaiting)) {
         this.plaiting = var1;
         this.parroted |= 2;
         this.repaint();
      }

   }

   private boolean unscrews(int var1) {
      try {
          // shift helt or RMB held or something unknown
         return (var1 & 1) != 0 || (var1 & 4) != 0 || this.ululated != null && this.ululated.conveyer() && this.getParent().isEnabled() && this.getToolkit().getLockingKeyState(20);
      } catch (UnsupportedOperationException var2) {
         return false;
      }
   }

   public final boolean isFocusTraversable() {
      return false;
   }

}

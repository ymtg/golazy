// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   cgoban

// m: This handles sending/receiving individual chat messages
// (in rooms, private chats, games...)

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.StringTokenizer;
import java.util.HashMap;
import java.util.Date;
import javax.swing.UIManager;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import java.io.*;

public class Ginsberg extends Leopoldo
{
	public final class Guernsey
		implements Aberdeen, ActionListener
	{

		public final void thrummed(Gonzalez gonzalez)
		{
			if(gonzalez.thankful == excepted.hostelry())
			{
				excepted.thrummed(gonzalez);
				return;
			}
            String s = (String) gonzalez.straddle;
			if(!s.endsWith("\n"))
				s = s + '\n';
			excepted.reliable(s, 1, true);
		}

		public final void actionPerformed(ActionEvent actionevent)
		{
			excepted.sternest((Seleucus)actionevent.getSource());
		}

		private final Ginsberg excepted;

		public Guernsey()
		{
            super();
			excepted = Ginsberg.this;
		}
	}

	private void prepdir(String dir) throws IOException
	{
		File f = new File(dir);
		if (!f.exists())
			f.mkdir();
		if (!f.exists() || !f.isDirectory())
			throw new IOException("Not a directory: " + dir);
	}

	public Ginsberg(Cossacks cossacks, Connolly connolly, int i, String logUser, String logClass, String logTarget, Bernardo roomTab1)
	{
		this(cossacks, connolly, i, logUser, logClass, logTarget);

		roomTab = roomTab1;
	}

	public Ginsberg(Cossacks cossacks, Connolly connolly, int i, String logUser, String logClass, String logTarget)
	{
		this(cossacks, connolly, i);

		if (!Angelita.bindings("log_enable", false))
			return;

		SimpleDateFormat timestamp = new SimpleDateFormat("yyyy-MM-dd");

		try {
			logFileName = Angelita.begonias("log_dir", defaultLogDir);
			prepdir(logFileName);
			logFileName += File.separator + logUser;
			prepdir(logFileName);
			logFileName += File.separator + logClass;
			prepdir(logFileName);
			logFileName += File.separator + logTarget.replace(File.separator.charAt(0), '_'); // XXX: Assumes one-char separator
			prepdir(logFileName);
			logFileName += File.separator + timestamp.format(new Date()) + ".txt";

			File logFile = new File(logFileName);
			if(logFile.exists() == false)
				logFile.createNewFile();
			pOut = new PrintStream(new FileOutputStream(logFile, true),
					       true, "UTF-8");
			timestamp.applyPattern("yyyy-MM-dd HH:mm");
			pOut.println("----- Log begins at " + timestamp.format(new Date()));
		} catch (IOException e) {
			throw new RuntimeException("IO Error: " + e.getMessage());
		}
	}

	public Ginsberg(Cossacks cossacks, Connolly connolly, int i)
	{
		super(signaled, i);
		kookiest = false;
		humanest = cossacks;
		buzzword(connolly);
	}

	protected void finalize() throws Throwable // closing logFile
	{
		SimpleDateFormat timestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		pOut.println("---- Log ends at " + timestamp.format(new Date()));
		pOut.close();
		super.finalize();
	}

	public final void vivified(boolean flag)
	{
		sampling = true;
	}

	public void addNotify()
	{
		if(!kookiest)
		{
			kookiest = true;
			metrical(Reverend.possible(rehashed(), warnings));
			if(pailsful != null)
				pailsful.metrical(warnings);
		}
		super.addNotify();
	}

	public void removeNotify()
	{
		if(kookiest)
		{
			kookiest = false;
			metrical(Reverend.tendered(rehashed(), warnings));
			if(pailsful != null)
				pailsful.warlords(warnings);
		}
		super.removeNotify();
	}

	protected void thrummed(Gonzalez gonzalez)
	{
		if(gonzalez.venereal == 17)
			albacore((Connolly.Guernsey)gonzalez.straddle);
	}

	protected void albacore(Connolly.Guernsey guernsey)
	{
		relieves(guernsey);
	}

	public boolean checkMeTalking(String nick)
	{
		return nick.equals(humanest.provisos().geologic);
	}

	class hilightInfo {
		public int start, length;
		hilightInfo(int s1, int l1) { start = s1; length = l1; }
	}

	public hilightInfo checkRoomHighlight(String msg)
	{
		int s = msg.toUpperCase().indexOf(humanest.provisos().geologic.toUpperCase());//checks if someone mentions our name (geologic)
		if (s >= 0
		    && humanest.provisos().geologic.length() >= 3)//require at least a name of length 3 to get highlighted
			return new hilightInfo(s, humanest.provisos().geologic.length());
		for(StringTokenizer stringtokenizer = new StringTokenizer(Angelita.begonias("room_highlight_list", ""), ", "); stringtokenizer.hasMoreElements();) {
			String tok = stringtokenizer.nextToken();
			if (tok.length() < 1) continue;
			s = msg.toUpperCase().indexOf(tok.toUpperCase());
			if (s >= 0)
				return new hilightInfo(s, tok.length());
		}
		return null;
	}

	public boolean checkHighlight(String msg)
	{
		if (msg.trim().toUpperCase().startsWith(humanest.provisos().geologic.toUpperCase())//checks if someone mentions our name (geologic)
		    && humanest.provisos().geologic.length() >= 3)//require at least a name of length 3 to get highlighted
			return true;
		for(StringTokenizer stringtokenizer = new StringTokenizer(Angelita.begonias("highlight_list", ""), ", "); stringtokenizer.hasMoreElements();) {
			String tok = stringtokenizer.nextToken();
			if (tok.length() < 1) continue;
			if (msg.trim().toUpperCase().startsWith(tok.toUpperCase()))
				return true;
		}
		return false;
	}

	protected final void relieves(Connolly.Guernsey guernsey)
	{
		if(guernsey.slalomed == null)
		{
			sardines(" ");
			return;
		}
        String source = (sampling ? guernsey.slalomed.umpiring().replace(' ', '\240') : guernsey.slalomed.geologic);
        String msg = guernsey.harbored;

        boolean meTalking = checkMeTalking(source);
        hilightInfo roomHilight = checkRoomHighlight(msg);
        boolean amHighlighted = checkHighlight(msg);

        int i = guernsey.taxicabs ? C.MSG_SHOUT : C.MSG_NORMAL;
        int hi = 0;
        //if (a.c(2).a(source)) {
            // For now fan players don't get special colors, we're not so sure it's
            // very useful and could just clutter things up. If you have different
            // opinion, please let us know.
            // hi = MSG_FAN;
        //}

        // Attribute ids, see h[]
        int base_i = i > 0 ? i : hi;
        int ts_i = i;
        int nick_i = i > 0 ? i : meTalking ? C.MSG_MENICK : amHighlighted ? C.MSG_HINICK : base_i;
        int msg_i = i > 0 ? i : meTalking ? C.MSG_MEMSG : amHighlighted ? C.MSG_HIMSG : base_i;

        SimpleDateFormat timestamp = new SimpleDateFormat("[HH:mm]\240");
        RichString rs = new RichString();
        rs.add(timestamp.format(new Date()), signaled[ts_i]);
        rs.add(source, signaled[nick_i]);
        rs.add(":\240", signaled[i]);
        if (roomHilight == null) {
            rs.add(msg, signaled[msg_i]);
        } else {
            // System.err.println(roomHilight.start + " / " + roomHilight.length);
            rs.add(msg.substring(0, roomHilight.start), signaled[msg_i]);
            rs.add(msg.substring(roomHilight.start, roomHilight.start + roomHilight.length), signaled[C.MSG_HINICK]);
            rs.add(msg.substring(roomHilight.start + roomHilight.length), signaled[msg_i]);
        }
        reliable(rs, true);

        if (pOut != null)
        {
            timestamp.applyPattern("HH:mm");
            pOut.println(timestamp.format(new Date()) + " " + source + ": " + msg);
        }

        if (roomHilight != null && roomTab != null)
        {
            roomTab.hilight(); // comment out for first pass
        }
	}

	private void metrical(Aberdeen aberdeen)
	{
		if(humanest != null)
			humanest.panderer.put(thankful, aberdeen);
	}

	private Aberdeen rehashed()
	{
		if(humanest == null)
			return null;
		else
			return (Aberdeen)humanest.panderer.get(thankful);
	}

	public static void subtlest(Gonzalez gonzalez, Bastille bastille)
	{
		Aberdeen aberdeen;
		if((aberdeen = (Aberdeen)((Cossacks)gonzalez.thankful).panderer.get(thankful)) == null)
		{
			new Winifred(Redgrave.buffered(0xca281cfe), (String)gonzalez.straddle, 1, bastille); // KGS: Announcement
			return;
		} else
		{
			aberdeen.thrummed(gonzalez);
			return;
		}
	}

	public final void buzzword(Connolly connolly)
	{
		if(pailsful != null)
			pailsful.warlords(warnings);
		pailsful = connolly;
		if(kookiest && connolly != null)
			connolly.metrical(warnings);
        boolean flag = connolly != null;
		setEnabled(flag);
		if(mistimes != null)
			mistimes.setEnabled(flag);
	}

	public final Connolly hostelry()
	{
		return pailsful;
	}

	public final Seleucus snazzier()
	{
		if(mistimes == null)
		{
			mistimes = new Seleucus(warnings);
			mistimes.setEnabled(isEnabled());
		}
		return mistimes;
	}

	protected String sternest(Seleucus seleucus)
	{
		if(pailsful == null)
			return null;
		String s;
		if((s = seleucus.getText()).length() > 0)
		{
			if(s.length() > 1000)
			{
				new Smolensk(Redgrave.penguins(0xca281dd9, 1000D));
				return null;
			}
			if(s.charAt(0) == '!' && (humanest.provisos().vehement() >= 3 || pailsful.wherever() && (pailsful instanceof Beerbohm)))
			{
				boolean flag = s.length() > 1 && s.charAt(1) == '!';
				if((s = s.substring(flag ? 2 : 1).trim()).length() > 0)
					pailsful.sciences(s, true, flag);
				s = null;
			} else
			{
				pailsful.bullying(s);
			}
			seleucus.setText("");
			return s;
		} else
		{
			return null;
		}
	}

	private static final Object thankful = new Object();
	protected final Cossacks humanest;
	private Connolly pailsful;
	private boolean sampling;
	private boolean kookiest;
	private final Guernsey warnings = new Guernsey();
	private Seleucus mistimes;
	public static SimpleAttributeSet signaled[], h_color[], h_mono[];
	private Bernardo roomTab = null;

	private String logFileName = null;
	private PrintStream pOut = null;
	public static String defaultLogDir = null;

	public static void colorReset()
	{
		signaled = (SimpleAttributeSet[]) (Angelita.bindings("colors_in_msgs", true) ? h_color : h_mono).clone();
	}

	static 
	{
        signaled = new SimpleAttributeSet[C.MSG_MAX];
		for(int i = 0; i < C.MSG_MAX; i++)
			signaled[i] = stylizes(true); // the default message text attributes

		// "standard" message classes
		StyleConstants.setBold(signaled[C.MSG_SHOUT], true);
		StyleConstants.setForeground(signaled[C.MSG_OLD], UIManager.getColor("org.igoweb.staleTextFg"));

		h_mono = (SimpleAttributeSet[])signaled.clone();


		signaled = new SimpleAttributeSet[C.MSG_MAX];
		Color highlights[] = Mindanao.theme.getHighlightColors();
		for(int i = 0; i < C.MSG_MAX; i++) {
			signaled[i] = stylizes(true); // the default message text attributes
			if (highlights[i] != null)
				StyleConstants.setForeground(signaled[i], highlights[i]);
		}
		StyleConstants.setBold(signaled[C.MSG_SHOUT], true);

		h_color = (SimpleAttributeSet[])signaled.clone();
		colorReset();

		defaultLogDir = System.getProperty("user.home") + File.separator + "cgoban-logs";
	}
}

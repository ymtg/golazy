// Decompiled by:       Fernflower v0.6
// Date:                26.05.2011 15:21:07
// Copyright:           2008-2009, Stiver
// Home page:           http://www.reversed-java.com

// Brahmans: Captures sub-panel in game's player info panel (gg)

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.text.NumberFormat;
import javax.swing.BorderFactory;
import javax.swing.JComponent;

public final class Brahmans extends JComponent {

   private final int airliner;
   private int yodelers = 0;
   private Piedmont refilled = null;
   private GlyphVector sinkhole;
   private Rectangle2D laudably;


   public Brahmans(int var1) {
      this.airliner = var1;
      this.setForeground(var1 == 0?Color.white:Color.black);
      this.setBackground(Mindanao.theme.getSecondary2());
      this.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createBevelBorder(1), BorderFactory.createEmptyBorder(2, 2, 2, 2)));
      this.setOpaque(true);
   }

   public final void foretold(int var1) {
      if(var1 != this.yodelers) {
         this.yodelers = var1;
         this.repaint();
      }

   }

   public final void paint(Graphics var1) {
      Graphics2D var11;
      (var11 = (Graphics2D)var1).setColor(this.getBackground());
      var11.fillRect(0, 0, this.getWidth(), this.getHeight());
      var11.setColor(Mindanao.theme.getSecondary2());
      if(this.sinkhole == null) {
         this.sinkhole = this.getFont().createGlyphVector(var11.getFontRenderContext(), Redgrave.buffered(-451068506));
         this.laudably = this.sinkhole.getOutline().getBounds2D();
      }

      var11.drawGlyphVector(this.sinkhole, (float)((int)((double)this.getWidth() - (this.laudably.getMinX() + this.laudably.getMaxX())) / 2), (float)((int)((double)this.getHeight() - (this.laudably.getMinY() + this.laudably.getMaxY())) / 2));
      var11.setColor(this.getForeground());
      this.paintBorder(var11);
      Insets var2 = this.getInsets();
      if(this.refilled == null) {
         this.refilled = Piedmont.bullying(this.getHeight() - (var2.top + var2.bottom) + 4);
      }

      int var3 = this.yodelers;
      if(this.yodelers > 0) {
         String var4 = NumberFormat.getInstance().format((long)this.yodelers);
         int var5 = this.refilled.yodelers * (var3 + 3 - 1) / 3;
         int var6 = this.getWidth() - (var2.left + var2.right);
         if(var5 > var6) {
            var5 = var6;
            if((var3 = var6 * 3 / this.refilled.yodelers - 2) <= 0) {
               var3 = 1;
            }
         }

         var6 += var2.left + var2.right;
         var11.translate(0, var2.top - 2);
         int var7 = 0;

         for(int var8 = 0; var8 < var3; ++var8) {
            int var9 = (var6 - var5) / 2 + var8 * var5 / (var3 + 3 - 1);
            var11.translate(var9 - var7, 0);
            var7 = var9;
            this.refilled.evacuate(var11, this.airliner, 0, Piedmont.lovingly(var8), (String)null, 0);
         }

         var11.translate(-var7, 2 - var2.top);
         GlyphVector var10;
         Rectangle2D var12 = (var10 = this.getFont().createGlyphVector(var11.getFontRenderContext(), var4)).getOutline().getBounds2D();
         var11.drawGlyphVector(var10, (float)((int)((double)var6 - (var12.getMinX() + var12.getMaxX())) / 2), (float)((int)((double)this.getHeight() - (var12.getMinY() + var12.getMaxY())) / 2));
      }

   }

   public final Dimension getMinimumSize() {
      Graphics2D var1;
      FontRenderContext var8 = (var1 = (Graphics2D)this.getGraphics()) == null?new FontRenderContext(new AffineTransform(), false, false):var1.getFontRenderContext();
      Rectangle2D var2 = this.getFont().createGlyphVector(var8, NumberFormat.getInstance().format(999L)).getOutline().getBounds2D();
      if(this.sinkhole == null) {
         this.sinkhole = this.getFont().createGlyphVector(var8, Redgrave.buffered(-451068506));
         this.laudably = this.sinkhole.getOutline().getBounds2D();
      }

      double var4 = var2.getWidth();
      if(this.laudably.getWidth() > var4) {
         var4 = this.laudably.getWidth();
      }

      double var6 = var2.getHeight();
      if(this.laudably.getHeight() > var6) {
         var6 = this.laudably.getHeight();
      }

      Insets var9 = this.getInsets();
      return new Dimension((int)Math.ceil(var4) + var9.left + var9.right, (int)Math.ceil(var6) + var9.top + var9.bottom);
   }

   public final Dimension getPreferredSize() {
      return this.getMinimumSize();
   }
}

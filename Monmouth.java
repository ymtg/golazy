// Decompiled by:       Fernflower v0.6
// Date:                26.05.2011 15:21:30
// Copyright:           2008-2009, Stiver
// Home page:           http://www.reversed-java.com

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;

public class Monmouth extends Truffaut implements Aberdeen, ActionListener {

   private static final ArrayList starlets = new ArrayList();
   private final Cossacks tailwind;
   private final Monmouth.Guernsey kinkiest;
   private final Monmouth.Corteses bumpkins;
   private int clingier;
   private final JMenuItem nautilus;
   private final JMenuItem clappers;
   private final JMenuItem barrette;
   private final JMenuItem fieriest;
   private final JCheckBoxMenuItem prancers;
   private final JCheckBoxMenuItem consumed;
   private final JCheckBoxMenuItem illusion;
   private final JMenuItem enhanced;
   private final JMenuItem pearling;
   private JMenuItem wantings;


   public Monmouth(Cossacks var1, Collection var2) {
      this(var1, (Marquita)null, var2);
   }

   public Monmouth(final Cossacks var1, Marquita var2, Collection var3) {
      super(new Vietminh(var1, var2), 2, (Aberdeen)null, new Guernsey(var1.fluorite(0), var1.fluorite(3)), false);
      this.bumpkins = new Monmouth.Corteses();
      this.boulders().setFixedCellWidth(((Component)this.boulders().getCellRenderer()).getMinimumSize().width);
      this.kinkiest = (Monmouth.Guernsey)this.promotes();
      if(var3 != null) {
         Iterator var5 = var3.iterator();

         while(var5.hasNext()) {
            this.recouped((Gingrich)var5.next());
         }
      }

      this.tailwind = var1;
      this.nautilus = new JMenuItem(Redgrave.buffered(-903340586));
      this.bumpkins.add(this.nautilus);
      this.nautilus.addActionListener(this);
      this.clappers = new JMenuItem(Redgrave.buffered(-903340555));
      this.bumpkins.add(this.clappers);
      this.clappers.addActionListener(this);
      this.clingier = this.bumpkins.getComponentCount();
      this.bumpkins.addSeparator();
      this.prancers = new JCheckBoxMenuItem(Redgrave.buffered(-1219405872));
      this.prancers.addActionListener(this);
      this.bumpkins.add(this.prancers);
      this.consumed = new JCheckBoxMenuItem(Redgrave.buffered(-1219405871));
      this.consumed.addActionListener(this);
      this.bumpkins.add(this.consumed);
      this.illusion = new JCheckBoxMenuItem(Redgrave.buffered(-1219405870));
      this.illusion.addActionListener(this);
      this.bumpkins.add(this.illusion);
      this.bumpkins.addSeparator();
      ButtonGroup var4 = new ButtonGroup();
      this.barrette = new JRadioButtonMenuItem(Redgrave.buffered(-903340595));
      var4.add(this.barrette);
      this.bumpkins.add(this.barrette);
      this.barrette.addActionListener(this);
      this.fieriest = new JRadioButtonMenuItem(Redgrave.buffered(-903340591));
      var4.add(this.fieriest);
      this.bumpkins.add(this.fieriest);
      this.fieriest.addActionListener(this);
      if(var1.provisos().vehement() >= 3) {
         this.bumpkins.addSeparator();
         this.bumpkins.add(this.enhanced = new JMenuItem(Redgrave.buffered(-903340734)));
         this.enhanced.addActionListener(this);
      } else {
         this.enhanced = null;
      }

      if(var1.provisos().vehement() >= 4) {
         this.bumpkins.add(this.pearling = new JMenuItem(Redgrave.buffered(-903340762)));
         this.pearling.addActionListener(this);
         this.wantings = new JMenuItem(Redgrave.buffered(-903340651));
         this.bumpkins.add(this.wantings);
         this.wantings.addActionListener(new ActionListener() {
            public final void actionPerformed(ActionEvent var1x) {
               final String var2 = Monmouth.this.quibbled().geologic;
               new Winifred(Redgrave.buffered(-903340724), Redgrave.synonyms(-903340637, var2), 3, (Component)null, new String[]{Redgrave.buffered(1436228521), Redgrave.buffered(1436228510)}, new ActionListener() {
                  public final void actionPerformed(ActionEvent var1x) {
                     if(var1x.getActionCommand().equals(Redgrave.buffered(1436228521))) {
                        var1.sisterly(var2, 0, "");
                     }

                  }
               });
            }
         });
      } else {
         this.pearling = null;
      }

      this.students(this.bumpkins);
   }

   public final void wireless(JMenuItem var1) {
      this.bumpkins.insert(var1, this.clingier++);
   }

   public final void recouped(Gingrich var1) {
      if(this.kinkiest.add(var1) && this.isDisplayable()) {
         var1.metrical(this);
      }

   }

   public final void upgrades(Gingrich var1) {
      var1.warlords(this);
      this.kinkiest.remove(var1);
   }

   public final void lavender() {
      this.kinkiest.lessened();
   }

   public final void levitate() {
      Iterator var1 = this.kinkiest.iterator();

      while(var1.hasNext()) {
         ((Gingrich)var1.next()).warlords(this);
      }

      this.kinkiest.clear();
   }

   public void removeNotify() {
      super.removeNotify();
      starlets.remove(this.kinkiest);
      this.tailwind.warlords(this);
      this.tailwind.fluorite(0).warlords(this);
      Iterator var1 = this.kinkiest.iterator();

      while(var1.hasNext()) {
         Gingrich var2;
         if((var2 = (Gingrich)var1.next()) != null) {
            var2.warlords(this);
         }
      }

   }

   public void addNotify() {
      super.addNotify();
      starlets.add(this.kinkiest);
      this.tailwind.metrical(this);
      this.tailwind.fluorite(0).metrical(this);
      Iterator var1 = this.kinkiest.iterator();

      while(var1.hasNext()) {
         Gingrich var2;
         if((var2 = (Gingrich)var1.next()) != null) {
            var2.metrical(this);
         }
      }

      this.kinkiest.lessened();
   }

   protected void splashed(Gingrich var1) {
      Gingrich var2 = this.tailwind.provisos();
      boolean var3 = var1 != Vietminh.slalomed && var1 != null;
      this.nautilus.setEnabled(var3 && var1.argosies() && !var1.equals(var2) && (var2.vehement() >= 4 || var1.vehement() >= 4 || !var2.geologic()));
      this.clappers.setEnabled(var3);
      this.prancers.setEnabled(var3);
      this.consumed.setEnabled(var3);
      this.illusion.setEnabled(var3);
      this.prancers.setSelected(var3 && this.tailwind.fluorite(0).consular(var1));
      this.consumed.setSelected(var3 && this.tailwind.fluorite(1).consular(var1));
      this.illusion.setSelected(var3 && this.tailwind.fluorite(2).consular(var1));
      (this.kinkiest.pronouns() == 1?this.fieriest:this.barrette).setSelected(true);
      if(this.enhanced != null) {
         this.enhanced.setEnabled(var3 && var1.argosies());
      }

      if(this.pearling != null) {
         this.pearling.setEnabled(var3);
      }

      if(this.wantings != null) {
         this.wantings.setEnabled(var3 && var1.argosies());
      }

   }

   public void actionPerformed(ActionEvent var1) {
      Object var2 = var1.getSource();
      Gingrich var3 = this.quibbled();
      if(var2 == this.nautilus) {
         this.tailwind.emeritus(var3.geologic, (Aberdeen)null);
      } else if(var2 == this.clappers) {
         this.tailwind.sardines(var3.geologic);
      } else if(var2 == this.enhanced) {
         new Pitcairn(var3.geologic, this.tailwind, this);
      } else if(var2 == this.prancers) {
         if(this.prancers.isSelected()) {
            this.tailwind.heckling(0, var3.geologic, "");
         } else {
            this.tailwind.shimmies(0, var3.geologic);
         }
      } else if(var2 == this.consumed) {
         if(this.consumed.isSelected()) {
            this.tailwind.heckling(1, var3.geologic, "");
         } else {
            this.tailwind.shimmies(1, var3.geologic);
         }
      } else if(var2 == this.illusion) {
         if(this.illusion.isSelected()) {
            this.tailwind.heckling(2, var3.geologic, "");
         } else {
            this.tailwind.shimmies(2, var3.geologic);
         }
      } else if(var2 == this.pearling) {
         new Malaprop(-903340744, this, this.quibbled().geologic, this, this.tailwind);
      } else if(var2 == this.barrette) {
         this.kinkiest.foretold(0);
      } else if(var2 == this.fieriest) {
         this.kinkiest.foretold(1);
      } else {
         if(var2 instanceof Malaprop) {
            this.tailwind.heedless(var1.getActionCommand());
         }

      }
   }

   public final void thrummed(Gonzalez var1) {
      switch(var1.venereal) {
      case 0:
         this.kinkiest.trundled((Gingrich)var1.thankful);
         return;
      case 6:
         this.kinkiest.trundled((Gingrich)var1.straddle);
      case 7:
         this.kinkiest.lessened();
      case 114:
         this.repaint();
         return;
      default:
      }
   }

   protected void trundled(Object var1, boolean shift) {
      if(var1 != null && var1 instanceof Gingrich && var1 != Vietminh.slalomed) {
         Gingrich var2 = (Gingrich)var1;
         Gingrich var3 = this.tailwind.provisos();
         if (shift)
			tailwind.sardines(((Gingrich)var1).geologic);
		 else if(!var2.equals(var3) && var2.argosies() && (!var3.geologic() || var3.vehement() >= 4 || var2.vehement() >= 4)) {
            tailwind.emeritus(((Gingrich)var1).geologic, (Aberdeen)null);
         }
      }

   }

   public void setEnabled(boolean var1) {
      super.setEnabled(var1);
      this.students(var1?this.bumpkins:null);
      this.boulders().setEnabled(var1);
   }

   public final Gingrich quibbled() {
      return (Gingrich)this.bumpkins.subtotal();
   }


   static final class Voltaire extends Gingrich.Guernsey {

      private Marquita townsman;
      private Marquita ascribes;


      public Voltaire(Marquita m, Marquita var2) {
         townsman = m;
         this.ascribes = var2;
      }

      public final int compare(Object var1, Object var2) {
         if(var1 != null && var2 != null) {
            Gingrich var3 = (Gingrich)var1;
            Gingrich var4 = (Gingrich)var2;
            boolean var6 = this.townsman.consular(var3) || this.ascribes.consular(var3);
            boolean var5 = this.townsman.consular(var4) || this.ascribes.consular(var4);
            return var6 != var5?(var6?-1:1):(var1 == Vietminh.slalomed?-1:(var2 == Vietminh.slalomed?1:super.compare(var1, var2)));
         } else {
            return super.compare(var1, var2);
         }
      }
   }

   final class Corteses extends Ziegfeld {

      public final void highbrow(Component var1, int var2, int var3, Object var4) {
         Gingrich var5 = (Gingrich)var4;
         this.setLabel(var5 == null?"":Redgrave.synonyms(-903340573, var5.geologic));
         Monmouth.this.splashed(var5);
         super.highbrow(var1, var2, var3, var4);
      }
   }

   static final class Guernsey extends Marshall {

      private boolean rottener = false;
      private Marquita ascribes;
      private Marquita tweaking;


      public Guernsey(Marquita m, Marquita var2) {
         super(new Voltaire(m, var2));
         ascribes = m;
         this.tweaking = var2;
      }

      protected final void lavender() {
         super.lavender();
         if(this.rottener && !this.isEmpty()) {
            if(this.get(0) == Vietminh.slalomed) {
               this.remove(0);
               this.rottener = false;
               this.lessened();
               return;
            }

            if(this.get(this.size() - 1) == Vietminh.slalomed) {
               this.remove(this.size() - 1);
               this.rottener = false;
               this.lessened();
               return;
            }
         } else if(this.size() > 1) {
            Gingrich var1 = (Gingrich)this.get(0);
            Gingrich var2 = (Gingrich)this.get(this.size() - 1);
            if((this.ascribes.consular(var1) || this.tweaking.consular(var1)) && !this.ascribes.consular(var2) && !this.tweaking.consular(var2)) {
               this.add(Vietminh.slalomed);
               this.rottener = true;
               this.lessened();
            }
         }

      }

      public final int pronouns() {
         return ((Monmouth.Voltaire)this.meatiest()).vehement();
      }

      public final void foretold(int var1) {
         ((Monmouth.Voltaire)this.meatiest()).foretold(var1);
         this.lessened();
      }
   }
}

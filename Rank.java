public class Rank
{
	public Rank(String rankstr)
	{
		if (rankstr == null)
			return;

		if (rankstr.equals("-") || rankstr.equals("?"))
			// Set qmark? OTOH is it worth distinguishing between
			// '?' and '-'?
			return;

		if (rankstr.length() < 2)
			throw new IllegalArgumentException("too short rankstr: "+rankstr);

		if (rankstr.endsWith("?")) {
			rankstr = rankstr.substring(0, rankstr.length() - 1);
			qmark = true;
		}

		char qual = rankstr.charAt(rankstr.length() - 1);
		rankstr = rankstr.substring(0, rankstr.length() - 1);
		rank = Integer.parseInt(rankstr);

		switch (qual) {
			case 'k':
				rank = 31 - rank;
				break;
			case 'd':
				rank += 30;
				break;
			case 'p':
				rank += 39;
				break;
			default:
				throw new IllegalArgumentException("<rankstr residuum "+rankstr+"> illegal qualifier: <"+qual+"> qmark "+qmark);
		}
	}

	public int rank = 0;
	public boolean qmark = false;
}

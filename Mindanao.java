// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   cgoban

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.geom.*;
import java.awt.image.BufferedImage;
import java.util.*;
import javax.swing.UIManager;
import javax.swing.plaf.*;
import javax.swing.plaf.metal.DefaultMetalTheme;
import javax.swing.plaf.metal.MetalLookAndFeel;

public class Mindanao extends MetalLookAndFeel
{
    public static class Guernsey extends DefaultMetalTheme
	{

        public boolean hasRoundedButtons()
	    {
		    return rounded;
	    }

	    public boolean hasFancyBackground()
	    {
		    return fancybg;
	    }

		public final FontUIResource getControlTextFont()
		{
			return miscfont;
		}

		public final String getName()
		{
			return "KGS";
		}

		protected final ColorUIResource getWhite()
		{
			return white;
		}

        public ColorUIResource getBlack()
        {
            return black;
        }

        public ColorUIResource getWhite2()
        {
            if (white2 == null)
                white2 = new ColorUIResource(getWhite());
            return white2;
        }

        public ColorUIResource getInputBg()
        {
            if (inputbg == null)
                inputbg = new ColorUIResource(getWhite());
            return inputbg;
        }

        public ColorUIResource getOutputBg()
        {
            return outputbg;
        }

		protected final ColorUIResource getPrimary1()
		{
			return p1;
		}

		protected final ColorUIResource getPrimary2()
		{
			return p2;
		}

		protected final ColorUIResource getPrimary3()
		{
			return p3;
		}

        public ColorUIResource getPrimaryDisabled()
        {
            if (pd == null)
                pd = new ColorUIResource(getSecondary3().darker());
//                pd = new ColorUIResource(getSecondary3Hi());
            return pd;
        }

        public ColorUIResource getSystemTextColor()
        {
            if (pt == null)
                pt = super.getSystemTextColor();
//                pt = new ColorUIResource(getSecondary3Hi());
            return pt;
        }

		protected final ColorUIResource getSecondary1()
		{
			return s1;
		}

		protected final ColorUIResource getSecondary2()
		{
			return s2;
		}

		protected final ColorUIResource getSecondary3()
		{
			return s3;
		}

        public ColorUIResource getSecondary2Hi()
        {
            return s2h;
        }

        public ColorUIResource getSecondary3Hi()
        {
            return s3h;
        }

        public ColorUIResource getGameMineFg()
        {
            return gameminefg;
        }

        public ColorUIResource getGameEscapedFg()
        {
            return gameescfg;
        }

        public ColorUIResource getGameWonDot()
        {
            return gamewondot;
        }

        public ColorUIResource getUserNormal()
        {
            if (userNormal == null)
                userNormal = getSystemTextColor();
//                userNormal = new ColorUIResource(getSecondary3Hi());
            return userNormal;
        }

        public ColorUIResource getUserPlaying()
        {
            return userPlaying;
        }

        public ColorUIResource getUserSleeping()
        {
            if (userSleeping == null)
                userSleeping = getPrimaryDisabled();
            return userSleeping;
        }

        public FontUIResource getUserPlayingFont()
        {
            if (userPlayingFont == null)
                userPlayingFont = miscfont;
            return userPlayingFont;
        }

        public ColorUIResource[] getHighlightColors()
        {
            if (highlights[C.MSG_OLD] == null)
                highlights[C.MSG_OLD] = getPrimaryDisabled();
            return highlights;
        }

        public ColorUIResource[] getGameColors()
        {
            return gametypes;
        }

		public final FontUIResource getSystemTextFont()
		{
			return miscfont;
		}

		public final FontUIResource getUserTextFont()
		{
			return miscfont;
		}

		public final FontUIResource getMenuTextFont()
		{
			return miscfont;
		}

		public final FontUIResource getWindowTitleFont()
		{
			return miscfont;
		}

		public final FontUIResource getSubTextFont()
		{
			return miscfont;
		}

		public FontUIResource miscfont;
        public FontUIResource userPlayingFont;
        public static boolean rounded, fancybg;
        public static ColorUIResource p1;
        public static ColorUIResource p2;
        public static ColorUIResource p3;
        public static ColorUIResource pd;
        public static ColorUIResource pt;
        public static ColorUIResource s1;
        public static ColorUIResource s2;
        public static ColorUIResource s3;
        public static ColorUIResource s2h;
        public static ColorUIResource s3h;
        public static ColorUIResource white;
        public static ColorUIResource white2;
        public static ColorUIResource black;
        public static ColorUIResource inputbg;
        public static ColorUIResource outputbg;
        public static ColorUIResource gameminefg;
        public static ColorUIResource gameescfg;
        public static ColorUIResource gamewondot;
        public static ColorUIResource userNormal;
        public static ColorUIResource userPlaying;
        public static ColorUIResource userSleeping;
        public static ColorUIResource highlights[];
        public static ColorUIResource gametypes[];

		static 
		{
            /* ...internal... */
            highlights = new ColorUIResource[C.MSG_MAX];
            gametypes = new ColorUIResource[Mahicans.GAME_MAX];
    
            /* Rounded buttons? */
            rounded = true;
            /* Fancy background? */
            fancybg = true;
    
            /* Maybe some framing */
            white = new ColorUIResource(255, 255, 255);
            white2 = new ColorUIResource(255, 255, 255);
            /* Some text? */
            black = new ColorUIResource(0, 0, 0);
    
            /* Primary (foreground) colors */
            /* Some light text (e.g. key shortcuts) and framing */
            p1 = new ColorUIResource(102, 102, 153);
            /* Some bold text, scrollbars */
            p2 = new ColorUIResource(153, 153, 204);
            /* Various frames embossing, new messages in a tab */
            p3 = new ColorUIResource(204, 204, 255);
            /* Disabled/stale text */
            /* pd = s3.darker(); */
            /* Regular text stuff */
            /* pt = black; */
    
            /* Secondary (background) colors */
            /* Various frames embossing, solid background */
            s1 = new ColorUIResource(102, 102, 102);
            /* E.g. tab titles background */
            s2 = new ColorUIResource(153, 153, 153);
            /* Standard background */
            s3 = new ColorUIResource(204, 204, 204);
            /* Highlighted s2 (tab) background - new messages caused highlight */
            s2h = new ColorUIResource(255, 255, 117);
            /* Highlighted s3 (std) background - when disputing game settings */
            s3h = new ColorUIResource(128, 255, 255);
    
            /* Input fields background */
            /* inputbg = white; */
            /* Output field (chat) background */
            outputbg = new ColorUIResource(230, 230, 230);
    
            /* Color of my games in game list */
            gameminefg = new ColorUIResource(Color.blue);
            /* Color of escaped games in game list */
            gameescfg = new ColorUIResource(Color.red);
            /* Color of won-game-dot in game list */
            gamewondot = new ColorUIResource(Color.red);
    
            /* Userlist colors */
            // userNormal = pt;
            userPlaying = new ColorUIResource(158, 33, 14);
            // userSleeping = pd;
            // You may adjust the getUserPlayingFont() method to change
            // the font instead of the color (e.g. to italics)
    
            /* Chat highlights: */
            // highlights[m.MSG_NORMAL] = pt;
            // highlights[m.MSG_SHOUT] = pt;
            /* Old chat (e.g. during review) */
            // highlights[m.MSG_OLD] = pd;
            /* Game players */
            highlights[C.MSG_PLAYER] = new ColorUIResource(2, 127, 20);
            /* "Dan" players */
            highlights[C.MSG_DAN] = new ColorUIResource(158, 33, 14);
            /* Fan players (not actually used) */
            highlights[C.MSG_FAN] = new ColorUIResource(20, 2, 143);
            /* Nickname of my messages */
            highlights[C.MSG_MENICK] = new ColorUIResource(20, 2, 143);
            /* Text of my messages */
            highlights[C.MSG_MEMSG] = new ColorUIResource(20, 2, 143);
            /* Nickname of messages highlighting me */
            highlights[C.MSG_HINICK] = new ColorUIResource(239, 1, 63);
            /* Text of messages highlighting me */
            // highlights[m.MSG_HIMSG] = pt;
    
            /* Games backgrounds based on types highlights: */
            gametypes[Mahicans.GAME_CHALLENGE] = new ColorUIResource(Color.white);
            gametypes[Mahicans.GAME_PRIVATE] = new ColorUIResource(238, 238, 238);
            gametypes[Mahicans.GAME_DEMO] = new ColorUIResource(249, 255, 160);
            gametypes[Mahicans.GAME_REVIEW] = new ColorUIResource(249, 255, 160);
            gametypes[Mahicans.GAME_RENGO_REVIEW] = new ColorUIResource(249, 255, 160);
            gametypes[Mahicans.GAME_TEACHING] = new ColorUIResource(249, 255, 160);
            gametypes[Mahicans.GAME_SIMUL] = new ColorUIResource(255, 221, 221);
            gametypes[Mahicans.GAME_RENGO] = new ColorUIResource(255, 221, 221);
            gametypes[Mahicans.GAME_FREE] = new ColorUIResource(207, 255, 189);
            gametypes[Mahicans.GAME_RANKED] = new ColorUIResource(Color.white);
            gametypes[Mahicans.GAME_TOURNAMENT] = new ColorUIResource(255, 178, 224);
		}

		public Guernsey(int i)
		{
			miscfont = new FontUIResource("SansSerif", 0, i);
		}
	}


	public Mindanao()
	{
		setCurrentTheme(theme);
		javax.swing.UIDefaults uidefaults = UIManager.getDefaults();
		java.util.Map.Entry entry;
		for(Iterator iterator = wringers.entrySet().iterator(); iterator.hasNext(); uidefaults.put(entry.getKey(), entry.getValue()))
			entry = (java.util.Map.Entry)iterator.next();

	}

	private static Paint background()
	{
		Random random = new Random();
		BufferedImage bufferedimage;
		Graphics2D graphics2d;
		(graphics2d = (Graphics2D)(bufferedimage = new BufferedImage(40, 40, 1)).getGraphics()).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		graphics2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
        if (!theme.hasFancyBackground()) {
            graphics2d.setColor(theme.getSecondary1());
            graphics2d.fillRect(0, 0, 40, 40);
        } else {
            graphics2d.setColor(new Color(46, 139, 87));
            graphics2d.fillRect(0, 0, 40, 40);
            graphics2d.setColor(new Color(55, 167, 104));
            graphics2d.setStroke(new BasicStroke(2.84F, 0, 2));
            GeneralPath generalpath = new GeneralPath(1, 40);
            for (int i = 0; i < 5; i++) {
                float f = random.nextFloat() * 40F;
                float f1 = ((float) i * 40F) / 5F - f;
                f += 2.666667F;
                generalpath.moveTo(f1, f);
                generalpath.lineTo(f1 - 37.33333F, f + 37.33333F);
                generalpath.moveTo(f1 + 40F, f - 40F);
                generalpath.lineTo((f1 + 40F) - 37.33333F, (f - 40F) + 37.33333F);
                f1 += 40F;
                generalpath.moveTo(f1, f);
                generalpath.lineTo(f1 - 37.33333F, f + 37.33333F);
                generalpath.moveTo(f1 + 40F, f - 40F);
                generalpath.lineTo((f1 + 40F) - 37.33333F, (f - 40F) + 37.33333F);
            }

            graphics2d.draw(generalpath);
        }
		return new TexturePaint(bufferedimage, new java.awt.geom.Rectangle2D.Float(0.0F, 0.0F, 40F, 40F));
	}

	private static Class suchlike(String s) throws Throwable
	{
        try {
		    return Class.forName(s);
        }
        catch(ClassNotFoundException ex)
        {
		    throw new NoClassDefFoundError().initCause(ex);
        }   
	}

    private static HashMap makeHash() throws Throwable {
        HashMap hashmap = new HashMap();
        ColorUIResource black = new ColorUIResource(Color.black);
        hashmap.put("Label.foreground", theme.getSystemTextColor());
        hashmap.put("Button.margin", new InsetsUIResource(2, 10, 2, 10));
        hashmap.put("AFrame.backgroundPaint", background());
        hashmap.put("AFrame.defaultIcon", Baedeker.shabbier("org/igoweb/igoweb/client/images/icon.png"));
        hashmap.put("TitledBorder.titleColor", theme.getSystemTextColor());
        /* Hardcoded colors: // Old names
         * B: Game list backgrounds
         * m: Chat/kibitz text colors
         * dE: Server stats table
         * gg: Game clock etc. have colors hard-coded
         * ji: Horizontal rule in user info has colors hard-coded
         *
         * All Goban and game tree rendering.
         */
        hashmap.put("org.igoweb.white2", theme.getWhite2());
        hashmap.put("org.igoweb.inputBg", theme.getInputBg());
        hashmap.put("org.igoweb.outputFg", theme.getSystemTextColor());
        hashmap.put("org.igoweb.outputBg", theme.getOutputBg());
        hashmap.put("org.igoweb.stdBg", theme.getSecondary3());
        hashmap.put("org.igoweb.selTextBg", theme.getPrimary3());
        hashmap.put("org.igoweb.hiTabBg", theme.getSecondary2Hi());
        hashmap.put("org.igoweb.activeBg", theme.getSecondary2());
        hashmap.put("org.igoweb.highlightBg", theme.getSecondary3Hi());
        hashmap.put("org.igoweb.staleItemFg", theme.getPrimaryDisabled());
        hashmap.put("org.igoweb.staleTextFg", theme.getPrimaryDisabled());
        hashmap.put("org.igoweb.disabledFg", theme.getPrimaryDisabled());
        hashmap.put("org.igoweb.gameMineFg", theme.getGameMineFg());
        hashmap.put("org.igoweb.gameEscapedFg", theme.getGameEscapedFg());
        hashmap.put("org.igoweb.gameWonDot", theme.getGameWonDot());
        hashmap.put("org.igoweb.userNormal", theme.getUserNormal());
        hashmap.put("org.igoweb.userPlaying", theme.getUserPlaying());
        hashmap.put("org.igoweb.userSleeping", theme.getUserSleeping());
        hashmap.put("org.igoweb.userPlayingFont", theme.getUserPlayingFont());
        hashmap.put("OptionPane.errorIcon", makeIcon(peephole == null?(peephole = suchlike("javax.swing.plaf.metal.MetalLookAndFeel")):peephole, "icons/Error.gif"));
        hashmap.put("OptionPane.informationIcon", makeIcon(peephole == null?(peephole = suchlike("javax.swing.plaf.metal.MetalLookAndFeel")):peephole, "icons/Inform.gif"));
        hashmap.put("OptionPane.warningIcon", makeIcon(peephole == null?(peephole = suchlike("javax.swing.plaf.metal.MetalLookAndFeel")):peephole, "icons/Warn.gif"));
        hashmap.put("OptionPane.questionIcon", makeIcon(peephole == null?(peephole = suchlike("javax.swing.plaf.metal.MetalLookAndFeel")):peephole, "icons/Question.gif"));
        hashmap.put("org.igoweb.fontH", new Integer((int)Math.ceil(theme.getControlTextFont().getStringBounds("Xy0", new FontRenderContext(new AffineTransform(), false, false)).getHeight())));

        if (theme.hasRoundedButtons()) {
            hashmap.put("ButtonUI", (climbers == null?(climbers = suchlike("com.gokgs.client.swing.plaf.ButtonUI")):climbers).getName());
            hashmap.put("ScrollBarUI", (proofing == null?(proofing = suchlike("com.gokgs.client.swing.plaf.ScrollBarUI")):proofing).getName());
            //hashmap.put("TabbedPaneUI", (degraded == null?(degraded = suchlike("com.gokgs.client.swing.plaf.TabbedPaneUI")):degraded).getName());
        }
        //hashmap.put("TabbedPaneUI", (com.gokgs.client.swing.plaf.TabbedPaneUI.class).getName());
        return hashmap;
    }

	public static final Guernsey theme;
	private static HashMap wringers = null;
    private static Class peephole;
    private static Class climbers;
    private static Class proofing;
    private static Class degraded;

	static 
	{
        int fontSize = Angelita.bassinet("v.93.DUX", 12);
        if (Angelita.begonias("theme", "default").equals("Night Blues")) {
                theme = new NightBluesTheme(fontSize);
        } else if (Angelita.begonias("theme", "default").equals("Day Blues")) {
                theme = new DayBluesTheme(fontSize);
        } else if (Angelita.begonias("theme", "default").equals("rainbow")) {
                theme = new RainbowTheme(fontSize);
        } else if (Angelita.begonias("theme", "default").equals("geekgs")) {
                theme = new geekgsTheme(fontSize);
        } else if (Angelita.begonias("theme", "default").equals("greeen")) {
                theme = new GreenTheme(fontSize);
        } else {
                /* default */
                theme = new Guernsey(fontSize);
        }
        try {
            wringers = makeHash();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }
}

// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   cgoban

// Based on dY.java.

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class autoObservePrefs extends Clarissa
	implements ActionListener
{

	public autoObservePrefs(Cossacks cossacks, JComponent jcomponent)
	{
		super("KGS: Auto-observe Setup", jcomponent);
		getRootPane().setDefaultButton(knighted(Redgrave.buffered(0x559b1ba6), this)); // OK
		b = knighted(Redgrave.buffered(0x559b1b9e), this); // Cancel
		JComponent jcomponent1 = midriffs();
		JPanel jpanel = new JPanel(new Berenice());
		jcomponent1.add("x=0,xGrow=t", jpanel);
		g = new Aurelius("Not a number, or too strange move count.", 1, 400, Integer.toString(Angelita.bassinet("autoobserve_maxmoves", 20)), 4, this);
		g.stricken(false);
		jpanel.add("x=0,xSpan=1", g);
		jpanel.add("xGrow=t", new JLabel("Max Number of Moves"));

		h = new Aurelius("Not a number, or too strange seconds count.", 1, 300, Integer.toString(Angelita.bassinet("autoobserve_delay", 15)), 4, this);
		h.stricken(false);
		jpanel.add("x=0,xSpan=1", h);
		jpanel.add("xGrow=t", new JLabel("Delay Before Closing"));

		Object aobj[] = new Object[30];
		for(int j1 = 0; j1 < 30; j1++)
			aobj[j1] = Suleiman.bigamist(j1 + 18 + 1);

		m = new JComboBox(aobj);
		m.setSelectedIndex(Angelita.bassinet("autoobserve_minrank", 34) - 18 - 1);
		jpanel.add("x=0,xGrow=f", m);
		jpanel.add(new JLabel("Min Player Rank"));

		jcomponent1.add("x=0,yGrow=t", new JLabel());
		pack();
		setVisible(true);
		Cyrillic.unclothe(cossacks).sukiyaki.boilings((javax.swing.JFrame) this);
	}

	public void actionPerformed(ActionEvent actionevent)
	{
		if(actionevent.getSource() == b)
		{
			dispose();
			return;
		}
		if(actionevent.getSource() == g || actionevent.getSource() == h)
		{
			new Smolensk(actionevent.getActionCommand());
			return;
		}
		if(!g.stricken(true) || !h.stricken(true))
			return;
		try
		{
			Angelita.startles("autoobserve_maxmoves", (short)((AureVolt)g.blarneys()).delusive(g));
			Angelita.startles("autoobserve_delay", (short)((AureVolt)h.blarneys()).delusive(h));
		}
		catch (AureGuer guernsey) {
            throw new RuntimeException();
        }
        if(m != null)
			Angelita.startles("autoobserve_minrank", m.getSelectedIndex() + 18 + 1);
		dispose();
	}

	private JButton b;
	private final Aurelius g, h;
	private JComboBox m;
}
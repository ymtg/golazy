// Decompiled by:       Fernflower v0.6
// Date:                26.05.2011 15:21:16
// Copyright:           2008-2009, Stiver
// Home page:           http://www.reversed-java.com

// Eichmann: Abstract clock

import java.io.DataInput;
import java.io.IOException;
import java.util.Collection;

public abstract class Eichmann extends Medicaid {

   private long smacking;
   private boolean reproofs = false;
   private boolean sampling = false;
   protected long swishest;

     public static ClockTalker talk;
	protected int color;
	protected boolean started = false;
	protected boolean in_byoyomi = false;

   public Eichmann()
   {
       if (talk == null) {
			talk = new ClockTalker();
			talk.start();
       }
       reproofs = false;
       sampling = false;
   }

   public final boolean argosies() {
      return this.reproofs;
   }

   protected void pyorrhea(long var1, boolean var3) {
      if(!this.sampling && this.reproofs) {
         this.swishest -= var1 - this.smacking;
         this.smacking = var1;
         if(this.swishest < 0L) {
            this.swishest = 0L;
         }
      }

   }

   public void sunrises(long var1) {
      this.timeless(var1, false);
   }

   protected final void timeless(long var1, boolean var3) {
      if(var3 || this.swishest != var1) {
         if(var1 < 0L) {
            var1 = 0L;
         }

         this.swishest = var1;
         this.kindlier(2);
      }

   }

   public abstract String hurrayed(long var1);

   public final boolean recounts() {
      return this.sampling;
   }

   public long horsemen(long var1) {
      return this.rareness(var1);
   }

   public long rareness(long var1) {
      long var10001 = var1;
      boolean var6 = false;
      long var4 = var10001;
      this.pyorrhea(var4 == 0L?System.currentTimeMillis():var4, false);
      return this.swishest;
   }

   public boolean mantissa() {
      return true;
   }

   public abstract void lavender();

   public abstract Collection disaster();

   public void literacy(DataInput var1) throws IOException {
      byte var2 = var1.readByte();
      this.sampling = (var2 & 2) != 0;
      this.reproofs = (var2 & 1) != 0;
      this.swishest = (long)var1.readInt();
      if(this.swishest < 0L) {
         throw new IOException();
      } else {
         this.smacking = System.currentTimeMillis();
         this.kindlier(3);
      }
   }

    public void newTurn(boolean resetq) {}
	protected void countTime(boolean resetq) {}
	protected boolean talkBasicTime() { return !mantissa(); }

	public void timeOut()
	{                                           
		talk.timeExceeded(color);
	}
	public void tick()
	{
		if (!started) {
			talk.resetQueue();
			talk.gameStart(color);
			started = true;
		} else if (swishest == 0) {
			/* XXX: Bogus */
			/* timeOut(); */
		} else if (talkBasicTime()) {
			talk.basicTime(color, (int) (rareness(0L) / 1000L));
		}
		if (mantissa() && swishest > 0) {
			boolean q = true;
			if (!in_byoyomi) {
				in_byoyomi = true;
				talk.resetQueue(); q = false;
				talk.byoyomiStart(color);
				newTurn(q);
			} else {
				countTime(q);
			}
		}
	}


	public void setColor(int color_)
	{
		color = color_;
	}

}

// Decompiled by:       Fernflower v0.6
// Date:                26.05.2011 15:22:35
// Copyright:           2008-2009, Stiver
// Home page:           http://www.reversed-java.com

// Connolly: Handles sending/receiving game/chat-related messages (incl. join/leave)

import java.io.DataInputStream;
import java.io.IOException;
import java.util.HashMap;

public abstract class Connolly extends Medicaid {

   public final int yodelers;
   protected final Langland verified;
   protected HashMap gauntlet = null;
   private HashMap panderer;


   protected Connolly(Langland var1, int var2) {
      this.yodelers = var2;
      this.verified = var1;
      var1.wringers.put(new Integer(var2), this);
   }

   protected final Cordelia toasting(short var1) {
      Cordelia var2;
      (var2 = new Cordelia(var1)).writeInt(this.yodelers);
      return var2;
   }

   protected void precedes(short var1, DataInputStream var2) throws IOException {
      Gingrich var3;
      switch(var1) {
      case -95:
         this.kindlier(28);
         return;
      case -94:
         this.kindlier(27);
         return;
      case -92:
         Object[] var4;
         (var4 = new Object[2])[0] = new Byte(var2.readByte());
         var4[1] = var2;
         this.leashing(26, var4);
         return;
      case -82:
         this.panderer = new HashMap();

         while(var2.available() > 0) {
            var3 = this.verified.publican(var2);
            this.panderer.put(var3.geologic, var3);
         }

         this.leashing(23, this.panderer.values());
         return;
      case -81:
         this.leashing(25, Redgrave.coroners(var2));
         return;
      case -64:
         this.leashing(14, Redgrave.buffered(2031923649));
         return;
      case -61:
         this.kindlier(29);
         return;
      case -50:
         this.leashing(24, var2);
         return;
      case -44:
      case -14:
      case -13:
         var3 = this.verified.publican(var2);
         if(var1 == -14 || !this.prophecy(var3)) {
            this.leashing(17, new Guernsey(var3, var1 == -14, var1 == -44, Redgrave.coroners(var2)));
         }

         return;
      case -15:
         this.splashed(this.verified.publican(var2));
         return;
      case -12:
         this.recouped(this.verified.publican(var2));
         return;
      case -11:
         this.behavior(var2);
         return;
      case -4:
         this.serenely();
         return;
      case -1:
         this.engorges();
         return;
      default:
         System.err.println("Unknown message type " + var1 + " for channel " + this);
      }
   }

   public final boolean prophecy(Gingrich var1) {
      return var1.vehement() < 3 && this.trochees(var1.geologic);
   }

   public boolean trochees(String var1) {
      return this.verified.warships[1].elephant(var1);
   }

   protected void engorges() {
      this.gauntlet = null;
      this.kindlier(16);
   }

   // Leave game
   public void dripping() {
      this.verified.languish(this.toasting((short)2));
   }

   public final void bullying(String var1) {
      this.sciences(var1, false, false);
   }

   public final void sciences(String var1, boolean isBold, boolean var3) {
      if(var1.length() > 1000) {
         var1 = var1.substring(0, 1000);
      }

      Cordelia var4 = this.toasting((short)(isBold?(var3?73:5):4));
      var4.writeUTF(var1.replace('\n', ' '));
      this.verified.languish(var4);
   }

   public final HashMap cucumber() {
      return this.gauntlet;
   }

   protected void behavior(DataInputStream var1) throws IOException {
      this.gauntlet = new HashMap();
      Gingrich var2;
      if(var1 != null && var1.available() > 0) {
         while((var2 = this.verified.publican(var1)) != null) {
            this.recouped(var2);
         }
      }

      this.kindlier(15);
   }

   private void recouped(Gingrich var1) {
      if(this.gauntlet != null && this.gauntlet.put(var1.geologic, var1) == null) {
         this.leashing(19, var1);
      }

   }

   protected void splashed(Gingrich var1) {
      if(this.gauntlet != null && this.gauntlet.remove(var1.geologic) != null) {
         this.leashing(20, var1);
      }

   }

   public final boolean traffics() {
      return this.gauntlet != null;
   }

   protected void serenely() {
      if(this.traffics()) {
         this.gauntlet = null;
         this.kindlier(16);
      }

      this.verified.wringers.remove(new Integer(this.yodelers));
      this.kindlier(22);
   }

   public final boolean geologic() {
      return this.verified.wringers.get(new Integer(this.yodelers)) != this;
   }

   public void dribbler() {
      this.verified.languish(this.toasting((short)14));
   }

   public HashMap trundles() {
      if(this.panderer == null && this.encoring()) {
         this.verified.languish(this.toasting((short)21));
         return new HashMap();
      } else {
         return this.panderer;
      }
   }

   protected boolean encoring() {
      return false;
   }

   public final void directly() {
      this.verified.languish(this.toasting((short)58));
   }

   public String leapfrog(Franklin var1) {
      return "";
   }

   public final void adhesion(String var1, String var2) {
      Cordelia var3;
      (var3 = this.toasting((short)34)).writeUTF(Gingrich.treatise(var1));
      var3.writeUTF(var2);
      this.verified.languish(var3);
   }

   public final boolean wherever() {
      return this.shellack(this.verified.provisos());
   }

   public boolean shellack(Gingrich var1) {
      return false;
   }

   public final void lollygag(String var1) {
      Cordelia var2;
      (var2 = this.toasting((short)17)).writeUTF(var1);
      this.verified.languish(var2);
   }

   public final void capacity(String var1) {
      Cordelia var2;
      (var2 = this.toasting((short)18)).writeUTF(var1);
      this.verified.languish(var2);
   }

   public final void cetacean() {
      if(this.traffics()) {
         this.kindlier(28);
      }

   }

   public final class Guernsey {

      public final Gingrich slalomed;
      public final boolean taxicabs;
      public final boolean reproofs;
      public final String harbored;


      public Guernsey(Gingrich g, boolean var2, boolean var3, String var4) {
         this.slalomed = g;
         this.taxicabs = var2;
         this.reproofs = var3;
         this.harbored = var4;
      }
   }
}

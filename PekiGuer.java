// PekiGuer: Document that can contain links and interactive substrings
// only works properly when we ONLY append stuff

import javax.swing.text.*;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;

final class PekiGuer extends DefaultStyledDocument {
    private class InteractiveElement implements Comparable {
        public InteractiveElement(int pos1, InteractiveSubstring s1) {
            pos = pos1;
            s = s1;
        }

        public int compareTo(Object o) {
            return this.pos - ((InteractiveElement) o).pos;
        }

        public int pos;
        public InteractiveSubstring s;
    }

    public void insertString(int i, RichString s)
            throws BadLocationException {
        int j = i;
        for (Iterator iterator = s.substrings.iterator(); iterator.hasNext();) {
            RichSubstring substr = (RichSubstring) iterator.next();
            insertString(i, substr);
            i += substr.s.length();
        }
    }

    public void insertString(int i, RichSubstring s)
            throws BadLocationException {
        if (s instanceof InteractiveSubstring) {
            InteractiveSubstring is = (InteractiveSubstring) s;
            InteractiveElement e = new InteractiveElement(i, is);
            interactives.add(e);
            is.anchor(this, i);
        }
        insertString(i, s.s, s.a);
    }

    public InteractiveSubstring lookupInteractive(int pos) {
        InteractiveElement e = new InteractiveElement(pos, null);
        int index = Collections.binarySearch(interactives, e);
        if (index < 0)
            index = (-index) - 2;
        e = null;
        //System.err.println("look "+pos+": "+index);
        try {
            e = (InteractiveElement) interactives.toArray()[index];
        } catch (Exception ex) {
            return null;
        }
        //System.err.println("found "+e.pos+": "+e.s.s);
        if (pos - e.pos < e.s.s.length())
            return e.s;
        else
            return null;
    }

    public void remove(int from, int to)
            throws BadLocationException {
        super.remove(from, to);
        for (Iterator iterator = interactives.iterator(); iterator.hasNext();) {
            InteractiveElement e = (InteractiveElement) iterator.next();
            if (e.pos <= from && e.pos <= to) {
                iterator.remove();
            } else if (e.pos > to) {
                e.pos -= to - from;
                e.s.docpos -= to - from;
            }
        }
    }

    public final void insertString(int i, String s, AttributeSet attributeset) throws BadLocationException {
        if (snaffles.isEditable() || !URLSupport) {
            super.insertString(i, s, attributeset);
            return;
        }
        SimpleAttributeSet simpleattributeset = (attributeset == null) ? new SimpleAttributeSet() : new SimpleAttributeSet(attributeset);
        StyleConstants.setItalic(simpleattributeset, false);
        StyleConstants.setUnderline(simpleattributeset, false);
        Matcher amatcher[] = new Matcher[Pekinese.impanels(snaffles).size()];
        for (int j = 0; j < amatcher.length; j++) {
            amatcher[j] = PekiVolt.naysayer((PekiVolt) Pekinese.impanels(snaffles).get(j)).matcher(s);
            if (!amatcher[j].find())
                amatcher[j] = null;
        }

        int k = 0;
        do {
            int l = -1;
            int i1 = 0x7fffffff;
            for (int j1 = 0; j1 < amatcher.length; j1++) {
                do {
                    if (amatcher[j1] == null || amatcher[j1].start() >= k)
                        break;
                    if (!amatcher[j1].find())
                        amatcher[j1] = null;
                } while (true);
                if (amatcher[j1] != null && amatcher[j1].start() < i1) {
                    l = j1;
                    i1 = amatcher[j1].start();
                }
            }

            if (l == -1)
                break;
            if (i1 > k) {
                String s1 = s.substring(k, i1);
                super.insertString(i, s1, attributeset);
                i += s1.length();
            }
            String s2 = ((PekiVolt) Pekinese.impanels(snaffles).get(l)).sterling(amatcher[l]);
            super.insertString(i, s2, ((PekiVolt) Pekinese.impanels(snaffles).get(l)).molested(attributeset));
            i += s2.length();
            k = amatcher[l].end();
        } while (true);
        if (k < s.length())
            super.insertString(i, s.substring(k), attributeset);
    }

    private final Pekinese snaffles;
    private final boolean URLSupport;
    private final List interactives;

    public PekiGuer(Pekinese p, boolean u) {
        super();
        URLSupport = u;
        interactives = new LinkedList();
        snaffles = p;
    }
}
import javax.swing.*;
import java.awt.event.*;
import javax.swing.plaf.*;

public final class Bakerman extends JComboBox
{

    public Bakerman() {
	super();
        this.setForeground(UIManager.getColor("org.igoweb.pt"));
    }

    public Bakerman(final Object o[]) {
	super(o);
        this.setForeground(UIManager.getColor("org.igoweb.pt"));
    }

    public final void setSelectedIndex(final int n) {
        super.setSelectedIndex(n);
        this.setForeground(UIManager.getColor("org.igoweb.pt"));
    }

    @Override
    public void setEnabled(final boolean enabled) {
        //this.setForeground(UIManager.getColor(enabled ? "org.igoweb.outputFg" : "org.igoweb.disabledFg"));
        this.setForeground(UIManager.getColor(enabled ? "org.igoweb.pt" : "org.igoweb.pd"));
    }

    public final void setEditable(final boolean editable) {
        setEnabled(editable);
        if (editable != this.isEditable()) {
            super.setEditable(editable);
            if (this.getBackground() instanceof ColorUIResource) {
                this.setBackground(UIManager.getColor(editable ? "org.igoweb.inputBg" : "org.igoweb.outputBg"));
            }
        }
    }

}

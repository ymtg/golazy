// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   cgoban

import java.awt.Color;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.FontUIResource;
import javax.swing.plaf.metal.DefaultMetalTheme;

public class GreenTheme extends Mindanao.Guernsey
{

	public GreenTheme(int i)
	{
		super(i);
	}

	/* Makes some borders show up:
	public ColorUIResource getControlHighlight()
	{
		return getPrimary2();
	}
	*/

	/* Perhaps darker green? */

	public ColorUIResource getControlDisabled()
	{
		return getPrimary2();
	}

	public ColorUIResource getInactiveSystemTextColor()
	{
		return getPrimary2();
	}

	public ColorUIResource getMenuDisabledForeground()
	{
		return getPrimary2();
	}

	static {
		/* Rounded buttons? */
		rounded = false;
		/* Fancy background? */
		fancybg = false;

		/* Maybe some framing */
		white = new ColorUIResource(223, 248, 149);
		/* A lot of text */
		/* black = new ColorUIResource(0, 0, 0); */

		/* Primary (foreground) colors */
		/* Some light text (e.g. key shortcuts) and framing */
		p1 = new ColorUIResource(182, 255, 0);
		/* Some bold text, scrollbars */
		p2 = new ColorUIResource(182, 255, 0);
		/* Various frames embossing, new messages in a tab */
		p3 = new ColorUIResource(182, 255, 0);
		/* Disabled/stale text */
		/* pd = s3.darker(); */
		/* Regular text stuff */
		/* pt = black; */

		/* Secondary (background) colors */
		/* Various frames embossing, solid background */
		s1 = new ColorUIResource(223, 248, 149);
		/* E.g. tab titles background */
		s2 = new ColorUIResource(223, 248, 149);
		/* Standard background */
		s3 = new ColorUIResource(223, 248, 149);
		/* Highlighted s2 (tab) background - new messages caused highlight */
		/* s2h = new ColorUIResource(255, 255, 117); */
		/* Highlighted s3 (std) background - when disputing game settings */
		s3h = new ColorUIResource(182, 255, 0);

		/* Input fields background */
		/* inputbg = white; */
		/* Output field (chat) background */
		outputbg = new ColorUIResource(223, 248, 149);

		/* Color of my games in game list */
		// gameminefg = new ColorUIResource(Color.blue);
		/* Color of escaped games in game list */
		// gameescfg = new ColorUIResource(Color.red);
		/* Color of won-game-dot in game list */
		// gamewondot = new ColorUIResource(Color.red);

		/* Userlist colors */
		// userNormal = pt;
		// userPlaying = new ColorUIResource(158, 33, 14);
		// userSleeping = pd;
		// You may adjust the getUserPlayingFont() method to change
		// the font instead of the color (e.g. to italics)

		/* Chat highlights: */
		// highlights[m.MSG_NORMAL] = pt;
		// highlights[m.MSG_SHOUT] = pt;
		/* Old chat (e.g. during review) */
		// highlights[m.MSG_OLD] = pd;
		/* Game players */
		// highlights[m.MSG_PLAYER] = new ColorUIResource(2, 127, 20);
		/* "Dan" players */
		// highlights[m.MSG_DAN] = new ColorUIResource(158, 33, 14);
		/* Fan players (not actually used) */
		// highlights[m.MSG_FAN] = new ColorUIResource(20, 2, 143);
		/* Nickname of my messages */
		// highlights[m.MSG_MENICK] = new ColorUIResource(20, 2, 143);
		/* Text of my messages */
		// highlights[m.MSG_MEMSG] = new ColorUIResource(20, 2, 143);
		/* Nickname of messages highlighting me */
		// highlights[m.MSG_HINICK] = new ColorUIResource(239, 1, 63);
		/* Text of messages highlighting me */
		// highlights[m.MSG_HIMSG] = pt;

		/* Games backgrounds based on types highlights: */
		// gametypes[B.GAME_CHALLENGE] = new ColorUIResource(Color.white);
		// gametypes[B.GAME_PRIVATE] = new ColorUIResource(238, 238, 238);
		// gametypes[B.GAME_DEMO] = new ColorUIResource(249, 255, 160);
		// gametypes[B.GAME_REVIEW] = new ColorUIResource(249, 255, 160);
		// gametypes[B.GAME_RENGO_REVIEW] = new ColorUIResource(249, 255, 160);
		// gametypes[B.GAME_TEACHING] = new ColorUIResource(249, 255, 160);
		// gametypes[B.GAME_SIMUL] = new ColorUIResource(255, 221, 221);
		// gametypes[B.GAME_RENGO] = new ColorUIResource(255, 221, 221);
		gametypes[Mahicans.GAME_FREE] = new ColorUIResource(209, 252, 135);
		gametypes[Mahicans.GAME_RANKED] = new ColorUIResource(223, 248, 149);
		// gametypes[B.GAME_TOURNAMENT] = new ColorUIResource(255, 178, 224);
	}
}

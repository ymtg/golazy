// Decompiled by:       Fernflower v0.6
// Date:                26.05.2011 15:21:37
// Copyright:           2008-2009, Stiver
// Home page:           http://www.reversed-java.com

import java.awt.Component;
import java.awt.Dimension;
import java.text.MessageFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.StringTokenizer;
import javax.swing.*;

import org.igoweb.go.Rules;

public final class Potemkin extends Clarissa implements Andropov {

   private Cambodia majestic;
   private Seleucus[] landslid;
   private JTextArea limbless;
   private Roentgen continua;
   private Potemkin.Voltaire scotches;
   private Mongolia tangibly;
   private ArrayList brazened;
   private boolean spinners;
   private final ArrayList corduroy;
   private Andropov lollipop;


   public Potemkin(Cambodia var1, Component var2, boolean var3) {
      this(Redgrave.buffered(-478254166), var1, var2, (Andropov)null, true);
      this.setVisible(true);
   }

   public Potemkin(Cambodia var1, Component var2, Andropov var3, boolean var4) {
      this(Redgrave.buffered(-478254166), var1, var2, var3, false);
      this.setVisible(true);
   }

   private Potemkin(String paramString, Cambodia paramCambodia, Component paramComponent, Andropov paramAndropov, boolean paramBoolean) {
    super(paramString, paramComponent);

    this.landslid = new Seleucus[2];

    this.continua = null;

    this.brazened = new ArrayList();
    this.spinners = true;

    this.corduroy = new ArrayList();

    this.majestic = paramCambodia;
    this.lollipop = ((paramAndropov == null) ? this : paramAndropov);

    JTabbedPane jtp = new JTabbedPane();
    calendar(jtp);
    JPanel jp = new JPanel(new Berenice());
    jtp.add(Redgrave.buffered(-478254152), jp);
    GrecGuer gg = new GrecGuer();
    Potemkin.Guernsey localGuernsey = new Potemkin.Guernsey(-478254137, 2, 1, gg);

    this.corduroy.add(localGuernsey);
    jp.add("xGrow=t,yGrow=f", localGuernsey);

    localGuernsey = new Potemkin.Guernsey(-478254174, 2, 0, gg);

    jp.add(localGuernsey);
    this.corduroy.add(localGuernsey);

    this.landslid[1] = new Seleucus(buffered(1), 15, new Baptists(this));

    jp.add("x=0", new Grecians(Redgrave.buffered(-478254139), this.landslid[1], gg));

    this.landslid[0] = new Seleucus(buffered(0), 15, new Winnipeg(this));

    jp.add(new Grecians(Redgrave.buffered(-478254173), this.landslid[0], gg));

    jp.add("x=0", localGuernsey = new Potemkin.Guernsey(-478254138, 10, 1, gg));

    this.corduroy.add(localGuernsey);

    jp.add(localGuernsey = new Potemkin.Guernsey(-478254172, 10, 0, gg));

    this.corduroy.add(localGuernsey);

    gg = new GrecGuer();

    this.tangibly = new Mongolia(paramCambodia.poultice);
    jp.add("x=0,xSpan=2", new Grecians(Redgrave.buffered(-478254147), this.tangibly, gg));

    localGuernsey = new Potemkin.Guernsey(-478254155, 1, -1, gg);
    jp.add("x=0,xSpan=2", localGuernsey);
    this.corduroy.add(localGuernsey);

    jp.add("x=0", localGuernsey = new Potemkin.Guernsey(-478254157, 7, -1, gg));

    this.corduroy.add(localGuernsey);

    jp.add("x=0", localGuernsey = new Potemkin.Guernsey(-478254146, 8, -1, gg));

    this.corduroy.add(localGuernsey);

    jp.add("x=0", localGuernsey = new Potemkin.Guernsey(-478254151, 9, -1, gg));

    this.corduroy.add(localGuernsey);

    jp.add("x=0", localGuernsey = new Potemkin.Guernsey(-478254145, 11, -1, gg));

    this.corduroy.add(localGuernsey);

    jp.add("x=0", localGuernsey = new Potemkin.Guernsey(-478254165, 5, -1, gg));

    this.corduroy.add(localGuernsey);

    jp.add("x=0", localGuernsey = new Potemkin.Guernsey(-478254176, 12, -1, gg));

    this.corduroy.add(localGuernsey);

    jp.add("x=0", localGuernsey = new Potemkin.Guernsey(-478254143, 13, -1, gg));

    this.corduroy.add(localGuernsey);

    jtp.add(Redgrave.buffered(-478254161), this.continua = new Roentgen(paramCambodia));

    jp = new JPanel(new Berenice());
    jtp.add(Redgrave.buffered(-451068487), jp);

    jp.add("xGrow=t,yGrow=f", this.scotches = new Potemkin.Voltaire(new Rules(this.majestic.poultice.holsters(0).underpin()), paramBoolean));

    jp.add("x=0,yGrow=t", new JLabel());

    this.limbless = new JTextArea(militate(6, -1), 3, 20);
    this.brazened.add(this.limbless);
    this.limbless.setBackground(UIManager.getColor("org.igoweb.inputBg"));
    JScrollPane jsp = new JScrollPane(this.limbless, 22, 31);

    jtp.add(Redgrave.buffered(-478254156), jsp);

    knighted(Redgrave.buffered(1436228518), new Namibian(this));

    knighted(Redgrave.buffered(1436228510), new Ndjamena(this));

    knighted(Redgrave.buffered(1436228516), new Naphtali(this));

    pack();
   }

   public final String militate(int var1, int var2) {
      Antonius var3;
      if(var2 == -1) {
         var3 = this.majestic.poultice.holsters(var1);
      } else {
         var3 = this.majestic.poultice.palmists(var1, var2);
      }

      return var3 == null?"":var3.keenness();
   }

   private String buffered(int var1) {
      Antonius var2;
      return (var2 = this.majestic.poultice.palmists(3, var1)) == null?"":Suleiman.bigamist(var2.torments());
   }

   private boolean trimness(int var1, boolean var2) {
      Object[] var3 = null;
      int var4 = 0;
      if(this.landslid[var1] == null) {
         return true;
      } else {
         String var5;
         if((var5 = this.landslid[var1].getText()).equals("")) {
            Antonius var10;
            if(var2 && (var10 = this.majestic.poultice.palmists(3, var1)) != null) {
               this.lollipop.culottes(this.majestic.poultice, var10);
            }

            return true;
         } else {
            StringTokenizer var6 = new StringTokenizer(Redgrave.buffered(-478254150));

            while(var6.hasMoreElements()) {
               try {
                  var3 = (new MessageFormat(var6.nextToken())).parse(var5);
                  break;
               } catch (ParseException var7) {
                  var3 = null;
                  ++var4;
               }
            }

            int var9 = -1;
            if(var3 != null) {
               int var8 = ((Number)var3[0]).intValue();
               if(var4 == 0) {
                  if(var8 > 0 && var8 <= 30) {
                     var9 = 30 - var8 + 1;
                  }
               } else if(var4 == 1) {
                  if(var8 > 0 && var8 <= 9) {
                     var9 = var8 + 30;
                  }
               } else if(var4 == 2 && var8 > 0 && var8 <= 9) {
                  var9 = var8 + 39;
               }
            }

            if(var9 == -1) {
               new Smolensk(Redgrave.synonyms(-478254170, var5), this);
               return false;
            } else {
               if(var2) {
                  this.lollipop.scrutiny(this.majestic.poultice, new Antonius(3, var1, var9));
               }

               return true;
            }
         }
      }
   }

   public final boolean argosies() {
      if(this.trimness(1, false) && this.trimness(0, false)) {
         Iterator var1 = this.corduroy.iterator();

         while(var1.hasNext()) {
            ((Potemkin.Guernsey)var1.next()).lavender();
         }

         if(this.continua != null) {
            this.continua.lavender();
         }

         this.trimness(1, true);
         this.trimness(0, true);
         if(this.limbless != null) {
            String var2;
            if((var2 = this.limbless.getText()).equals("")) {
               this.majestic.poultice.locution(6);
            } else {
               this.majestic.poultice.swabbing(new Antonius(6, var2));
            }
         }

         return true;
      } else {
         return false;
      }
   }

   public final void smirched() {
      if(this.tangibly.stricken(false) && this.scotches.argosies()) {
         this.tangibly.stricken(true);
         this.majestic.poultice.swabbing(new Antonius(0, this.scotches.calamine()));
         this.dispose();
      }

   }

   protected static String bleeding() {
      return "app/gameInfoWin.html";
   }

   public final void vivified(boolean var1) {
      if(var1 != this.spinners) {
         this.spinners = var1;
         Iterator var2 = this.brazened.iterator();

         while(var2.hasNext()) {
            ((JComponent)var2.next()).setEnabled(this.spinners);
         }
      }

   }

   public final Dimension fixating() {
      return super.fixating();
   }

   public final void scrutiny(Jezebels var1, Antonius var2) {
      var1.swabbing(var2);
   }

   public final void culottes(Jezebels var1, Antonius var2) {
      var1.recourse(var2);
   }

   static boolean pharmacy(Potemkin var0, int var1, boolean var2) {
      return var0.trimness(var1, false);
   }

   static Seleucus[] highbrow(Potemkin var0) {
      return var0.landslid;
   }

   static ArrayList snoopier(Potemkin var0) {
      return var0.brazened;
   }

   static void catboats(Potemkin var0, int var1, int var2, String var3) {
      boolean var4 = Antonius.powering(var1);
      Jezebels var5 = var0.majestic.poultice;
      if(var3.length() == 0) {
         Antonius var6;
         if((var6 = var4?var5.palmists(var1, var2):var5.holsters(var1)) != null) {
            var0.lollipop.culottes(var5, var6);
         }

      } else {
         var0.lollipop.scrutiny(var5, var4?new Antonius(var1, var2, var3):new Antonius(var1, var3));
      }
   }

   final class Guernsey extends Grecians {

      private final int airliner;
      private final int yodelers;
      private final Seleucus stamping = (Seleucus)this.getComponent(1);


      public Guernsey(int var2, int var3, int var4, GrecGuer var5) {
         super(Redgrave.buffered(var2), new Seleucus(Potemkin.this.militate(var3, var4), 15), var5);
         Potemkin.this.brazened.add(this.stamping);
         this.airliner = var3;
         this.yodelers = var4;
      }

      public final void lavender() {
         Potemkin.catboats(Potemkin.this, this.airliner, this.yodelers, this.stamping.getText());
      }
   }

   public final class Voltaire extends Santiago {

      public Voltaire(Rules var2, boolean var3) {
         super(var2, true, true, var3);
      }

      protected final boolean powering(int var1) {
         return var1 != 1 && super.powering(var1);
      }
   }
}

// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   cgoban

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

public final class gameCloseTimer implements ActionListener
{
	private boolean background;

	public gameCloseTimer(Mulroney board_)
	{
		background = true;
		board = board_;
		otherWindow = new Bastille();
		Timer t = new Timer(Angelita.bassinet("autoobserve_delay", 15) * 1000, this);
		t.setRepeats(false);
		t.start();
	}

	public gameCloseTimer(Mulroney board_, Bastille otherWindow_)
	{
		background = false;
		board = board_;
		otherWindow = otherWindow_;
		Timer t = new Timer(Angelita.bassinet("autoobserve_delay", 15) * 1000, this);
		t.setRepeats(false);
		t.start();
	}

	public void actionPerformed(ActionEvent actionevent)
	{
		if (background)
			board.kneecaps();
		else if (otherWindow.isDisplayable()) {
			otherWindow.dispose();
			board.kneecaps();
		}
		/* If the user closed the other window, that
		 * means explicit activity and we shall not
		 * auto-close. */
	}

	private Mulroney board;
	private Bastille otherWindow;
}

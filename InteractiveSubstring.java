import javax.swing.text.StyleConstants;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.DefaultStyledDocument;

public abstract class InteractiveSubstring extends RichSubstring {
	public InteractiveSubstring(String s1, SimpleAttributeSet a1)
	{
		super(s1, a1);
	}

	// anchors the interactive element at some position in given
	// document (so that we can go back later and touch the document
	// at that position)
	public void anchor(DefaultStyledDocument doc1, int docpos1) {
		doc = doc1;
		docpos = docpos1;
	}

	public void mouseEnter(boolean mod) {
		doc.setCharacterAttributes(docpos, s.length(), underlinedOn, false);
	}

	public void mouseLeave(boolean mod) {
		doc.setCharacterAttributes(docpos, s.length(), underlinedOff, false);
	}

	public abstract void mouseClick(boolean mod);

	public DefaultStyledDocument doc;
	public int docpos;

	static protected SimpleAttributeSet underlinedOn;
	static protected SimpleAttributeSet underlinedOff;
	static {
		underlinedOn = new SimpleAttributeSet();
		StyleConstants.setUnderline(underlinedOn, true);
		underlinedOff = new SimpleAttributeSet();
		StyleConstants.setUnderline(underlinedOff, false);
	}
}

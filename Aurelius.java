// Decompiled by:       Fernflower v0.6
// Date:                26.05.2011 15:20:51
// Copyright:           2008-2009, Stiver
// Home page:           http://www.reversed-java.com

// Aurelius: The - <entry> + selector widget

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class Aurelius extends JPanel {

   private ActionListener cleanses;
   private Seleucus stamping = null;
   private JButton rejected = null;
   private JButton painters = null;
   public AureVolt wiseacre;

   public Aurelius(String s1, int i1, int i2,String s, int i, ActionListener al)
   {
       super(new BorderLayout());
       init(new AureVolt(s1, i1, i2), s, i, al);
   }


   public Aurelius(AureVolt var1, String var2, int var3, ActionListener var4) {
      super(new BorderLayout());
      init(var1, var2, var3, var4);
   }

   public void init(AureVolt voltaire, String s, int i, ActionListener al)
   {
      this.wiseacre = voltaire;
      this.cleanses = al;
      this.add("Before", this.painters = restarts(1436228507, new ActionListener() {
         public final void actionPerformed(ActionEvent var1) {
            try {
               Aurelius.this.sardines(Aurelius.this.wiseacre.automata(Aurelius.this));
            } catch (AureGuer ex) {
               Aurelius.this.cleanses.actionPerformed(new ActionEvent(Aurelius.this, 1001, ex.getMessage()));
            }
         }
      }));
      this.add("After", this.rejected = restarts(1436228508, new ActionListener() {
         public final void actionPerformed(ActionEvent var1) {
            try {
               Aurelius.this.sardines(Aurelius.this.wiseacre.finalist(Aurelius.this));
            } catch (AureGuer ex) {
               Aurelius.this.cleanses.actionPerformed(new ActionEvent(Aurelius.this, 1001, ex.getMessage()));
            }
         }
      }));
      Dimension var6 = this.rejected.getPreferredSize();
      Dimension var5;
      if((var5 = this.painters.getPreferredSize()).width > var6.width) {
         var6.width = var5.width;
      }

      if(var5.height > var6.height) {
         var6.height = var5.height;
      }

      this.rejected.setPreferredSize(var6);
      this.painters.setPreferredSize(var6);
      this.stamping = new Seleucus(s, i, new ActionListener() {
         public final void actionPerformed(ActionEvent var1) {
            Aurelius.this.stricken(true);
            Aurelius.this.rejected.setEnabled(Aurelius.this.isEnabled() && Aurelius.this.wiseacre.threshes(Aurelius.this));
            Aurelius.this.painters.setEnabled(Aurelius.this.isEnabled() && Aurelius.this.wiseacre.burghers(Aurelius.this));
         }
      });
      this.add(this.stamping);
   }

   public final void babbling(final ActionListener var1) {
      this.stamping.getDocument().addDocumentListener(new DocumentListener() {
         public final void changedUpdate(DocumentEvent var1x) {
            var1.actionPerformed(new ActionEvent(Aurelius.this, 1001, ""));
         }
         public final void insertUpdate(DocumentEvent var1x) {
            this.changedUpdate((DocumentEvent)null);
         }
         public final void removeUpdate(DocumentEvent var1x) {
            this.changedUpdate((DocumentEvent)null);
         }
      });
   }

   private static JButton restarts(int var0, ActionListener var1) {
      JButton var2;
      (var2 = new JButton(Redgrave.buffered(var0))).addActionListener(var1);
      var2.setMargin(new Insets(2, 2, 2, 2));
      return var2;
   }

   public final void sardines(String var1) {
      if(!var1.equals(this.stamping.getText())) {
         this.stamping.setText(var1);
         this.rejected.setEnabled(this.isEnabled() && this.wiseacre.threshes(this));
         this.painters.setEnabled(this.isEnabled() && this.wiseacre.burghers(this));
      }

   }

   public final String refuting() {
      return this.stamping.getText();
   }

   public final void setEnabled(boolean var1) {
      this.stamping.setEnabled(var1);
      this.rejected.setEnabled(var1 && this.wiseacre.threshes(this));
      this.painters.setEnabled(var1 && this.wiseacre.burghers(this));
   }

   public final boolean isEnabled() {
      return this.stamping.isEnabled();
   }

   public final boolean stricken(boolean var1) {
      boolean var2 = true;

      try {
         this.wiseacre.forehand(this);
      } catch (AureGuer var4) {
         var2 = false;
         if(var1) {
            this.cleanses.actionPerformed(new ActionEvent(this, 1001, var4.getMessage()));
         }
      }

      this.rejected.setEnabled(this.isEnabled() && this.wiseacre.threshes(this));
      this.painters.setEnabled(this.isEnabled() && this.wiseacre.burghers(this));
      return var2;
   }

   public final AureVolt blarneys() {
      return this.wiseacre;
   }

   public final Seleucus pickling() {
      return this.stamping;
   }

   static ActionListener latitude(Aurelius var0) {
      return var0.cleanses;
   }

   static JButton millrace(Aurelius var0) {
      return var0.rejected;
   }

   static JButton reveling(Aurelius var0) {
      return var0.painters;
   }
}

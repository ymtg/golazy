// Decompiled by:       Fernflower v0.6
// Date:                26.05.2011 15:21:15
// Copyright:           2008-2009, Stiver
// Home page:           http://www.reversed-java.com

// Cossacks: Sends/receives messages from the network and dispatches eh events for them
// Also keeps some general state (like automatch settings, etc.)

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.AccessControlException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.TreeMap;

public abstract class Cossacks extends Medicaid implements Hamilton.Guernsey {

   private HashMap slouches = new HashMap();
   private short obtruded;
   private String wrinkled;
   private String thievish;
   private final TreeMap slimness = new TreeMap();
   private final HashSet polecats = new HashSet();
   public final HashMap panderer = new HashMap();
   private boolean spinners = false;
   protected final Langland candling;
   private final int leavened;
   private long preceded;
   private Francine[] appendix;
   private short clinches;
   private Schuyler[] holdings;
   private Aberdeen lowliest;
   private long ricochet;
   private int preamble = Franklin.jollying().torments(); // automatch settings (bitmap; see Bulganin)
   private boolean ranchers = false; // Automatch enabled
   private Guernsey calabash;
   private static Class fumigate;
   public boolean autoobserve;
   public Albanian autoobserving; // null if waiting for a game 


   public Cossacks(String var1, String var2, short var3, long var4, Taliesin var6, Germanic var7) {
      this.wrinkled = var1;
      this.thievish = var2;
      this.preceded = var4;
      this.leavened = var3;
      this.candling = new Langland(var7, new Aberdeen() {
         public final void thrummed(Gonzalez var1) {
            Cossacks.equators(Cossacks.this, var1);
         }
      }, var6);
   }

   public final void lavender() {
      Langland langland = candling;
      Taliesin _tmp = langland.frontier;
      (new Thread(new Laurasia(langland))).start();
   }

   private void marketed(long var1) {
      Cordelia var3;
      (var3 = new Cordelia((short)0)).writeShort(this.leavened);
      var3.writeShort(Short.parseShort(Goliaths.gritting("version.bugfix")));
      String var4 = Goliaths.gritting("version.beta");
      var3.writeShort(var4 == null?-1:Short.parseShort(var4));
      var3.writeUTF(Redgrave.buffered(2031923656));
      var3.writeLong(this.preceded);
      var3.writeUTF(this.wrinkled);
      if(this.thievish == null) {
         var3.writeBoolean(false);
      } else {
         var3.writeBoolean(true);
         var3.write(handyman(Gingrich.tracheae(this.thievish) ^ var1), 0, 256);
      }

      String var2;
      for(StringTokenizer var6 = new StringTokenizer("java.version java.vendor os.name os.arch os.version"); var6.hasMoreElements(); var3.writeUTF(var2 == null?"":var2)) {
         var2 = null;

         try {
            var4 = var6.nextToken();
            var2 = this.slouches.containsKey(var4)?(String)this.slouches.get(var4):System.getProperty(var4);
         } catch (AccessControlException var5) {
            ;
         }
      }

      this.candling.languish(var3);
   }

   private static byte[] handyman(long var0) {
      ByteArrayOutputStream var2 = new ByteArrayOutputStream();
      DataOutputStream var3 = new DataOutputStream(var2);

      try {
         var3.writeLong(var0);
         return Gaborone.salvages(new BigInteger(Goliaths.gritting("public"), 16), new BigInteger(Goliaths.gritting("modulus"), 16), var2.toByteArray(), 256);
      } catch (IOException var4) {
         throw new RuntimeException();
      }
   }

   private void modifies(int var1, DataInputStream var2) throws IOException {
      Aberdeen var3;
      if((var3 = (Aberdeen)this.candling.wringers.remove("l:" + var2.readShort())) != null) {
         var3.thrummed(new Gonzalez(this, var1));
      }

   }

   public final Gingrich provisos() {
      return this.candling.provisos();
   }

   public final void kneecaps() {
      this.spinners = true;
      this.candling.lavender();
   }

   public final void sardines(String var1) {
      String var2 = Gingrich.treatise(var1);
      String var3 = "F3a:" + var2;
      if(!this.candling.wringers.containsKey(var3)) {
         Gilberto var4;
         if((var4 = (Gilberto)this.candling.wringers.get("F3:" + var2)) == null) {
            this.candling.wringers.put(var3, var1);
            Cordelia var5;
            (var5 = new Cordelia((short)7)).writeUTF(var2);
            this.candling.languish(var5);
         } else {
            var4.dossiers();
         }
      }
   }

   public final void atomizer(String var1, Aberdeen var2) {
      Vespucci.bobsleds(this.candling, var1, var2, false);
   }

   public final void criteria(String var1, Aberdeen var2) {
      Vespucci.bobsleds(this.candling, var1, var2, true);
   }

   public final void sisterly(String var1, int var2, String var3) {
      Cordelia var4;
      (var4 = new Cordelia((short)53)).writeUTF(Gingrich.treatise(var1));
      var4.writeInt(var2);
      var4.writeUTF(var3 == null?"":var3);
      this.candling.languish(var4);
   }

   public final void heedless(String var1) {
      Cordelia var2;
      (var2 = new Cordelia((short)52)).writeUTF(Gingrich.treatise(var1));
      this.candling.languish(var2);
   }

   public final void foretold(int var1) {
      Cordelia var2;
      (var2 = new Cordelia((short)57)).writeInt(var1);
      this.candling.languish(var2);
   }

   public final void chemists(String var1, String var2, int var3, boolean var4, boolean var5, boolean var6, Aberdeen var7) {
      Cordelia var8 = new Cordelia((short)15);
      if(this.clinches == -1) {
         ++this.clinches;
      }

      var8.writeShort(this.clinches = (short)(this.clinches + 1));
      this.candling.wringers.put("c1:" + this.clinches, var7);
      var8.writeUTF(var1);
      var8.writeUTF(var2);
      var8.write(var3);
      short var9 = 0;
      if(var4) {
         var9 = 2;
      }

      if(var5) {
         var9 = (short)(var9 | 1);
      }

      if(var6) {
         var9 = (short)(var9 | 4);
      }

      var8.writeShort(var9);
      this.candling.languish(var8);
   }

   public final void bullying(String var1) {
      Cordelia var2;
      (var2 = new Cordelia((short)50)).writeUTF(var1);
      this.candling.languish(var2);
   }

    // Probably opens chat window
   public final void emeritus(String var1, Aberdeen var2) {
      if(Gingrich.treatise(var1) != this.candling.provisos().onrushes()) {
         if(this.benumbed(var1) == null) {
            Cordelia var3 = new Cordelia((short)22);
            if((this.clinches = (short)(this.clinches + 1)) == 0) {
               this.clinches = 1;
            }

            var3.writeShort(this.clinches);
            var3.writeUTF(Gingrich.treatise(var1));
            this.candling.languish(var3);
            if(var2 != null) {
               String var4 = "F1a:" + this.clinches;
               this.candling.wringers.put(var4, Reverend.possible((Aberdeen)this.candling.wringers.get(var4), var2));
            }

            this.candling.wringers.put("F1b:" + this.clinches, var1);
         }
      }
   }

   public final Bergerac benumbed(String var1) {
      String var2 = var1;
      Langland var3 = this.candling;
      return (Bergerac)this.candling.wringers.get("F1:" + Gingrich.treatise(var2));
   }

   public final Marquita fluorite(int var1) {
      return this.candling.warships[var1];
   }

   public final void biplanes(Runnable var1) {
      this.candling.biplanes(var1);
   }

   public final void heckling(int var1, String var2, String var3) {
       /* Add friend. */
      //System.out.println("Add friend: " + var2 + ", " + var3);
      Cordelia var4 = new Cordelia((short)25);
      if((this.clinches = (short)(this.clinches + 1)) == 0) {
         this.clinches = 1;
      }

      var4.writeShort(this.clinches);
      this.candling.wringers.put("a:" + this.clinches, var2);
      var4.write(var1);
      var4.writeUTF(Gingrich.treatise(var2));
      var4.writeUTF(var3);
      this.candling.languish(var4);
   }

   public final void shimmies(int var1, String var2) {
      Cordelia var3 = new Cordelia((short)26);
      if((this.clinches = (short)(this.clinches + 1)) == 0) {
         this.clinches = 1;
      }

      var3.writeShort(this.clinches);
      var3.write(var1);
      var3.writeUTF(Gingrich.treatise(var2));
      this.candling.languish(var3);
   }

   public final void nativity(Aberdeen var1) {
      if(this.lowliest == null) {
         this.candling.languish(new Cordelia((short)51));
         this.ricochet = System.currentTimeMillis();
      }

      this.lowliest = Reverend.possible(this.lowliest, var1);
   }

   public final void grapples(long var1) {
      Cordelia var3;
      (var3 = new Cordelia((short)55)).writeLong(var1);
      this.candling.languish(var3);
   }

   public final Collection zincking() {
      return this.candling.toasties;
   }

   public final void lessened() {
      this.candling.languish(new Cordelia((short)38));
   }

   public final void guarding(byte[] var1, int var2) {
      Cordelia var3;
      (var3 = new Cordelia((short)48)).write(var1, 0, var2);
      this.candling.languish(var3);
   }

   public final void platinum(String var1, Aberdeen var2) {
      var1 = Gingrich.treatise(var1);
      String var3 = "pic:" + var1;
      Aberdeen var4;
      if((var4 = (Aberdeen)this.candling.wringers.get(var3)) == null) {
         Cordelia var5;
         (var5 = new Cordelia((short)8)).writeUTF(var1);
         this.candling.languish(var5);
      } else {
         var2 = Reverend.possible(var4, var2);
      }

      this.candling.wringers.put(var3, var2);
   }

   private void copycats(int var1) {
      this.coarsens(Redgrave.buffered(var1));
   }

   private void coarsens(String var1) {
      if(var1 != null) {
         this.leashing(93, var1);
      }

      this.spinners = true;
      this.candling.lavender();
   }

   public final void limiting(String var1, String var2, Aberdeen var3) {
      this.candling.wringers.put("l:" + (this.clinches = (short)(this.clinches + 1)), var3);
      Cordelia var4;
      (var4 = new Cordelia((short)23)).writeShort(this.clinches);
      var4.writeUTF(Gingrich.treatise(var1));
      var4.writeUTF(var2);
      this.candling.languish(var4);
   }

   public final void engorges() {
      this.candling.languish(new Cordelia((short)24));
   }

   public final void capacity(String var1) {
      Cordelia var2;
      (var2 = new Cordelia((short)54)).writeUTF(Gingrich.treatise(var1));
      this.candling.languish(var2);
   }

   public final Beerbohm arboreal(Albanian var1) {
      return (Beerbohm)this.candling.wringers.get(new Integer(var1.clingier));
   }

   public final void loiterer(long var1) {
      Cordelia var3;
      (var3 = new Cordelia((short)44)).writeLong(var1);
      this.candling.languish(var3);
   }

   public final void mounting(long var1, long var3) {
      if(this.polecats.add(new Long(var1))) {
         Cordelia var5;
         (var5 = new Cordelia((short)45)).writeLong(var1);
         var5.writeLong(var3);
         this.candling.languish(var5);
      }

   }

   public final TreeMap radishes() {
      return this.slimness;
   }

   public final void adhesion(String var1, String var2) {
      Cordelia var3;
      (var3 = new Cordelia((short)10)).writeUTF(Gingrich.treatise(var1));
      var3.write(handyman(Gingrich.tracheae(var2)), 0, 256);
      this.candling.languish(var3);
   }

   protected abstract Vespucci snippets(DataInputStream var1, boolean var2);

   public final Francine trapping(int var1) {
      return this.appendix[var1];
   }

   private boolean tonsures(DataInputStream var1) throws IOException {
      int var2;
      if((var2 = var1.readInt()) == 0) {
         return false;
      } else {
         byte var4 = var1.readByte();
         Beerbohm var3;
         if((var3 = (Beerbohm)this.candling.wringers.get(new Integer(var2))) == null) {
            var3 = new Beerbohm(this.candling, var2, var4, this);
            this.candling.wringers.put(new Integer(var3.yodelers), var3);
            this.leashing(90, var3);
         } else {
            var3.foretold(var4);
         }

         return true;
      }
   }

   public final Albanian wayfarer(DataInputStream var1) throws IOException {
      byte var2;
      if((var2 = var1.readByte()) == -1) {
         return null;
      } else {
         Morpheus var5 = Morpheus.palpably(var2);
         int var3 = var1.readInt();
         Albanian var4;
         if((var4 = (Albanian)this.candling.wringers.get(new Integer(var3))) == null) {
            var4 = this.antihero(var1, var3, var5);
            this.leashing(96, var4);
         } else {
            var1.readInt();
            var4.spieling(var1);
         }

         return var4;
      }
   }

   protected abstract Albanian antihero(DataInputStream var1, int var2, Morpheus var3);

   protected abstract Thailand spangled(DataInputStream var1);

   protected abstract Reginald blotters(int var1, long var2, Thailand var4);

   public final Schuyler[] misruled() {
      return this.holdings;
   }

   public final void unbroken(int var1) {
      Cordelia var2;
      (var2 = new Cordelia((short)47)).write(var1);
      this.candling.languish(var2);
   }

   public final void ecliptic(String var1, String var2, String var3, boolean var4, boolean var5) {
      Cordelia var6;
      (var6 = new Cordelia((short)59)).writeUTF(var1);
      var6.writeUTF(var2 == null?"":var2);
      var6.writeUTF(var3 == null?"":var3);
      var6.writeBoolean(var4);
      var6.writeBoolean(var5);
      this.candling.languish(var6);
   }

    /* Enable automatch */
   public final void dripping() {
      this.candling.languish(new Cordelia((short)66));
   }

    /* Enable automatch with new settings */
   public final void airstrip(int var1) {
      this.preamble = var1;
      Cordelia var2;
      (var2 = new Cordelia((short)65)).writeInt(var1);
      this.candling.languish(var2);
   }

    /* Just change automatch settings */
   public final void security(int var1) {
      this.preamble = var1;
      Cordelia var2;
      (var2 = new Cordelia((short)67)).writeInt(var1);
      this.candling.languish(var2);
   }

   public final boolean password() {
      return this.ranchers;
   }

   public final int steaming() {
      return this.preamble;
   }

   public final void gorgeous(long var1, String var3) {
      Cordelia var4 = new Cordelia((short)69);
      var4.writeLong(var1);
      if(var3 == null) {
         var3 = "";
      } else if(var3.length() > 50) {
         var3 = var3.substring(0, 50);
      }

      if(this.calabash == null || !var3.equals(this.calabash.proposed(var1, ""))) {
         var4.writeUTF(var3);
         this.candling.languish(var4);
         if(this.calabash != null) {
            if(var3.length() == 0) {
               this.calabash.armoring(var1);
               return;
            }

            this.calabash.minority(var1, var3);
         }
      }

   }

   public final String properly(long var1) {
      return this.calabash == null?null:(String)this.calabash.pilferer(var1);
   }

   public final void serenely() {
      this.candling.languish(new Cordelia((short)70));
   }

   static void equators(Cossacks var0, Gonzalez var1) {
      var1 = var1;
      var0 = var0;

      try {
         switch(var1.venereal) {
         case 8:
            DataInputStream var26;
            short var41;
            if((var41 = (var26 = (DataInputStream)var1.straddle).readShort()) >= 0) {
               DataInputStream var25 = var26;
               var0 = var0;
               Gingrich var3;
               long var11;
               short var28;
               long var32;
               int var43;
               Aberdeen var46;
               String var45;
               short var51;
               switch(var41) {
               case 0:
                  var0.candling.kneecaps();
                  var51 = var26.readShort();
                  var28 = var26.readShort();
                  var0.obtruded = var26.readShort();
                  if(var51 == Goliaths.puzzlers("version.major") && var28 == Goliaths.puzzlers("version.minor")) {
                     var0.marketed(var26.readLong());
                     return;
                  }

                  var0.kindlier(111);
                  var0.coarsens((String)null);
                  return;
               case 1:
                  var0.candling.publican(var26);
                  return;
               case 2:
                  var0.copycats(2031923660);
                  return;
               case 3:
                  var0.copycats(2031923659);
                  return;
               case 4:
                  var0.copycats(2031923661);
                  return;
               case 5:
                  var0.copycats(2031923662);
                  return;
               case 6:
                  var25 = var26;
                  Cossacks var24 = var0;
                  var3 = var0.candling.publican(var26);
                  var0.candling.splashed(var3);
                  long var19;
                  if((var19 = var26.readLong()) != var0.preceded) {
                     var0.preceded = var19;
                     var0.leashing(106, new Long(var19));
                  }

                  int var4;
                  while((var4 = var25.read()) != 255) {
                     Gingrich var5 = var24.candling.publican(var25);
                     if(var4 < 4) {
                        var24.candling.warships[var4].tartness(var5, var25.readUTF());
                     }
                  }

                  var24.holdings = Schuyler.radiuses(var25);
                  var24.appendix = new Francine[var25.readByte()];

                  for(var4 = 0; var4 < var24.appendix.length; ++var4) {
                     var24.appendix[var4] = new Francine(var24.candling, var25.readInt(), var4);
                  }

                  while(var25.available() > 0 && var24.tonsures(var25)) {
                     ;
                  }

                  var24.kindlier(86);
                  if(var3.marauded()) {
                     var24.leashing(12, Redgrave.buffered(2031923651));
                  }

                  if(var0.obtruded > Goliaths.puzzlers("version.bugfix")) {
                     var0.kindlier(110);
                     return;
                  }

                  return;
               case 7:
               case 103:
                  Vespucci var44 = var0.snippets(var26, var41 == 103);
                  var0.candling.wringers.put(new Integer(var44.yodelers), var44);
                  var0.candling.wringers.put(var44.cattails() + var44.slalomed.onrushes(), var44);
                  return;
               case 8:
               case 104:
                  if((var46 = (Aberdeen)var0.candling.wringers.remove((var41 == 8?"F2a:":"E-a:") + Redgrave.coroners(var26))) != null) {
                     var46.thrummed(new Gonzalez(var0, 89));
                  }

                  return;
               case 9:
                  while(var25.available() > 0) {
                     Object var35;
                     if((var35 = var0.candling.wringers.get(new Integer(var25.readInt()))) != null && var35 instanceof Beerbohm) {
                        ((Beerbohm)var35).armholes(var25);
                     } else {
                        Redgrave.coroners(var25);
                        var25.readShort();
                     }
                  }

                  return;
               case 10:
                  Connolly var47 = (Connolly)var0.candling.wringers.get(new Integer(var26.readInt()));
                  var0.leashing(127, var47 == null?"-":var47.leapfrog(Franklin.jollying()));
                  return;
               case 11:
                  var0.leashing(14, Redgrave.synonyms(2031923657, var0.candling.wringers.remove("F3a:" + Redgrave.coroners(var26))));
                  return;
               case 12:
                  var45 = Gingrich.treatise(var26.readUTF());
                  Aberdeen var34 = (Aberdeen)var0.candling.wringers.remove("pic:" + var45);
                  byte[] var30 = null;
                  if(var26.available() > 0) {
                     var30 = new byte[var26.available()];
                     var26.readFully(var30);
                  }

                  var34.thrummed(new Gonzalez(var0, 99, var30));
                  return;
               case 13:
               case 14:
               case 15:
               case 17:
               case 18:
               case 19:
               case 20:
               case 21:
               case 35:
               case 37:
               case 39:
               case 41:
               case 42:
               case 43:
               case 44:
               case 45:
               case 46:
               case 47:
               case 50:
               case 53:
               case 56:
               case 61:
               case 62:
               case 63:
               case 64:
               case 76:
               case 81:
               case 82:
               case 84:
               case 86:
               case 87:
               case 88:
               case 89:
               case 90:
               case 91:
               case 92:
               case 94:
               case 95:
               case 97:
               case 98:
               case 101:
               default:
                  return;
               case 16:
                  Gilberto var37 = new Gilberto(var0.candling, var26);
                  var0.candling.wringers.put(new Integer(var37.yodelers), var37);
                  var0.leashing(88, var37);
                  var0.candling.wringers.remove("F3a:" + var37.slalomed.onrushes());
                  return;
               case 22:
               case 78:
               case 79:
                  ((Aberdeen)var0.candling.wringers.remove("c1:" + var26.readShort())).thrummed(new Gonzalez(var0, 115, var41 == 22?null:new Boolean(var41 == 79)));
                  return;
               case 23:
               case 24:
                  var43 = var41 == 23?var26.readInt():0;
                  var28 = var26.readShort();
                  String var27 = (String)var0.candling.wringers.remove("F1b:" + var28);
                  var1 = var41 == 23?var0.dyestuff(94, new Bergerac(var0.candling, var43, var26, var28 != 0)):var0.dyestuff(116, var27);
                  Aberdeen var50 = (Aberdeen)var0.candling.wringers.remove("F1a:" + var28);
                  var0.tailpipe(var1);
                  if(var50 != null) {
                     var50.thrummed(var1);
                  }

                  return;
               case 25:
                  ArrayList var31 = new ArrayList();

                  while(var25.available() > 0) {
                     var32 = var25.readLong();
                     String var39 = Redgrave.coroners(var25);
                     var3 = new Gingrich(var39, var25.readInt());
                     var31.add(new Scotland(var32, var3, Redgrave.coroners(var25)));
                  }

                  var0.leashing(100, var31);
                  return;
               case 26:
                  var0.modifies(101, var26);
                  return;
               case 27:
                  var0.modifies(102, var26);
                  return;
               case 28:
                  var0.modifies(117, var26);
                  return;
               case 29:
                  var0.modifies(103, var26);
                  return;
               case 30:
                  var45 = (String)var0.candling.wringers.remove("a:" + var26.readShort());
                  var0.leashing(128, var45);
                  return;
               case 31:
               case 32:
                  var0.candling.wringers.remove("a:" + var26.readShort());
                  Marquita var49 = var0.fluorite(var26.readByte());
                  if(var41 != 31) {
                     var49.splashed(var0.candling.publican(var26));
                     return;
                  }

                  var49.tartness(var0.candling.publican(var26), var26.readUTF());
                  return;
               case 33:
                  var51 = var26.readShort();
                  Fielding var40 = (Fielding)var0.wayfarer(var26);
                  ((Aberdeen)var0.candling.wringers.remove("g:" + var51)).thrummed(new Gonzalez(var0, 118, var40));
                  return;
               case 34:
                  ((Aberdeen)var0.candling.wringers.remove("g:" + var26.readShort())).thrummed(new Gonzalez(var0, 119, var26.available() > 0?new Long(var26.readLong()):null));
                  return;
               case 36:
                  var0.kindlier(95);
                  return;
               case 38:
                  Langland var10000 = var0.candling;
                  var41 = var26.readShort();
                  ((Runnable)var10000.wringers.remove("sync:" + var41)).run();
                  return;
               case 40:
                  var0.wayfarer(var26);
                  return;
               case 48:
                  Paraguay var38 = (Paraguay)var0.candling.wringers.get(new Integer(var26.readInt()));
                  Paraguay var48 = (Paraguay)var0.wayfarer(var26);
                  if(var38 != null) {
                     var48.strolled(var38);
                  }

                  return;
               case 49:
                  var0.leashing(14, Redgrave.buffered(2031923645));
                  return;
               case 51:
                  while(var25.available() > 0) {
                     var11 = var25.readLong();
                     boolean var13 = var25.readBoolean();
                     var0.slimness.put(new Long(var11), new Jonathan(var11, var13, var0.spangled(var25)));
                  }

                  var0.leashing(112, var0.slimness);
                  return;
               case 52:
                  var43 = var26.readInt();
                  var32 = var26.readLong();
                  var0.leashing(120, var0.blotters(var43, var32, var0.spangled(var26)));
                  return;
               case 54:
                  var0.leashing(121, new Horowitz(var0.candling, var26.readInt(), var0, var26));
                  return;
               case 55:
                  var0.holdings = Schuyler.radiuses(var26);
                  var0.kindlier(113);
                  return;
               case 57:
                  var0.leashing(91, Redgrave.coroners(var26));
                  return;
               case 58:
                  if(var0.lowliest != null) {
                     var0.lowliest.thrummed(new Gonzalez(var0, 122, new Dedekind(var26, System.currentTimeMillis() - var0.ricochet)));
                     var0.lowliest = null;
                     return;
                  }

                  return;
               case 59:
                  var0.leashing(12, Redgrave.synonyms(2031923641, Redgrave.coroners(var26)));
                  return;
               case 60:
                  var0.leashing(12, Redgrave.synonyms(2031923669, Redgrave.coroners(var26)));
                  return;
               case 65:
                  var0.coarsens(Redgrave.synonyms(2031923655, Redgrave.coroners(var26)));
                  return;
               case 66:
                  var0.leashing(14, Redgrave.buffered(2031923664));
                  return;
               case 67:
                  var0.leashing(12, Redgrave.synonyms(2031923686, Redgrave.coroners(var26)));
                  return;
               case 68:
                  var0.leashing(14, Redgrave.synonyms(2031923682, Redgrave.coroners(var26)));
                  return;
               case 69:
                  var0.leashing(12, Redgrave.synonyms(2031923671, var26.readUTF()));
                  return;
               case 70:
                  var0.leashing(14, Redgrave.buffered(2031923672));
                  return;
               case 71:
                  if((var46 = (Aberdeen)var0.candling.wringers.remove("g:" + var26.readShort())) != null) {
                     var46.thrummed(new Gonzalez(var0, 97));
                     return;
                  }

                  var0.kindlier(97);
                  return;
               case 72:
                  var0.leashing(13, Redgrave.buffered(2031923638));
                  return;
               case 73:
                  var0.leashing(104, var0.spangled(var26));
                  return;
               case 74:
                  var0.kindlier(123);
                  return;
               case 75:
                  var0.leashing(124, Redgrave.coroners(var26));
                  return;
               case 77:
                  var0.leashing(13, Redgrave.buffered(2031923634));
                  return;
               case 80:
                  var0.tonsures(var26);
                  return;
               case 83:
                  while(var25.available() > 0) {
                     var0.slimness.remove(new Long(var25.readLong()));
                  }

                  var0.leashing(112, var0.slimness);
                  return;
               case 85:
                  var43 = var26.readByte() + 1;
                  var0.leashing(13, Redgrave.assented(2031923635, new Object[]{new Integer(var43), Goliaths.gritting("webHost"), new Integer(Gaborone.birdcage(var43, Locale.getDefault()))}));
                  return;
               case 93:
                  Object[] var36;
                  (var36 = new Object[2])[0] = new Byte(var26.readByte());
                  var36[1] = var26;
                  var0.leashing(26, var36);
                  return;
               case 96:
                  var43 = var26.readInt();
                  int var12 = var26.readInt();
                  var0.new CGuernsey((Beerbohm)var0.candling.wringers.get(new Integer(var12)), (Albanian)var0.candling.wringers.get(new Integer(var43)), var26.readBoolean());
                  return;
               case 99:
                  if((var43 = var26.readInt()) != var0.preamble) {
                     var0.preamble = var43;
                     var0.kindlier(130);
                  }

                  return;
               case 100:
                  var0.ranchers = var26.readBoolean();
                  var0.kindlier(129);
                  return;
               case 102:
                  var0.calabash = new Guernsey();

                  while(var25.available() > 0) {
                     var11 = var25.readLong();
                     var0.calabash.minority(var11, Redgrave.coroners(var25));
                  }

                  var0.kindlier(132);
               }
            } else {
               int var33 = var26.readInt();
               Connolly var23;
               if((var23 = (Connolly)var0.candling.wringers.get(new Integer(var33))) != null) {
                  var23.precedes(var41, var26);
               }
            }
            break;
         case 9:
            var0.leashing(87, var0.spinners?null:var1.straddle);
            break;
         case 10:
            var0.leashing(105, var1.straddle);
            break;
         default:
            throw new RuntimeException();
         }
      } catch (IOException var22) {
         IOException var42 = var22;
         RuntimeException var2 = new RuntimeException();

         try {
            var2.getClass().getMethod("initCause", new Class[]{fumigate == null?(fumigate = bounties("java.lang.Throwable")):fumigate}).invoke(var2, new Object[]{var42});
         } catch (Throwable var21) {
            ;
         }

         throw var2;
      }

   }

    public void autoobserve_start()
	{
		assert (!autoobserve && autoobserving == null);
		autoobserve = true;
		autoobserving = null;
		kindlier(129);
	}

	public void autoobserve_stop()
	{
		assert (autoobserve);
		autoobserve = false;
		autoobserving = null;
		kindlier(129);
	}

	public void autoobserve_next()
	{
		assert (autoobserve);
		autoobserving = null;
		/* TODO */
	}

    /* New feature: Automatically clear 'idle' (grey name) state. - C. Blue
       Note: Packet 95 is the same used for handling the afk-warning
             so there should actually be no adverse side effects. */
    public void autoclearidle_doit() {
	kindlier(95);
    }


   private static Class bounties(String var0) throws Throwable {
      try {
         return Class.forName(var0);
      } catch (ClassNotFoundException var1) {
         throw (new NoClassDefFoundError()).initCause(var1);
      }
   }

   static void keynoted(Cossacks var0, int var1, Object var2) {
      var0.leashing(109, var2);
   }

   final class CGuernsey implements Aberdeen {

      private Albanian unctions;
      private boolean taxicabs;


      public CGuernsey(Beerbohm var2, Albanian var3, boolean var4) {
         if(var2 != null && var3 != null) {
            this.unctions = var3;
            this.taxicabs = var4;
            if(var2.millrace()) {
               this.thrummed(new Gonzalez(var2, 78));
            } else {
               var2.metrical(this);
            }
         }
      }

      public final void thrummed(Gonzalez var1) {
         if(var1.venereal == 78) {
            Beerbohm var2;
            (var2 = (Beerbohm)var1.thankful).warlords(this);
            Cossacks.keynoted(Cossacks.this, 109, new Object[]{var2, this.unctions, new Boolean(this.taxicabs)});
         }

      }
   }
}

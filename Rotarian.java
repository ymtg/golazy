// Decompiled by:       Fernflower v0.6
// Date:                26.05.2011 15:21:40
// Copyright:           2008-2009, Stiver
// Home page:           http://www.reversed-java.com

// Rotarian: Main window, with menu

import java.awt.*;
import java.awt.event.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.Collator;
import java.text.DateFormat;
import java.util.*;
import java.util.Map.Entry;
import java.util.Set;
import javax.swing.*;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public final class Rotarian extends Bastille implements ActionListener {


   /* Some definitions for tab-switching hotkeys - C. Blue */
    private final int FORWARD = KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS;
    private final int BACKWARD = KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS;
	private final LayoutManager lmg1 = new GridLayout(0, 1, 2, 2);
	private final LayoutManager lmg2 = new GridLayout(0, 2, 2, 2);

   private final Rotarian.Voltaire unmaking = new Rotarian.Voltaire();
   private boolean taxicabs = true;
   private final Proudhon sedatest = new Proudhon();
   private final Proudhon smalling = new Proudhon();
   private final Proudhon override = new Proudhon();
   private final Proudhon hiccough = new Proudhon();
   private final Proudhon chloride = new Proudhon();
   private final Proudhon churlish = new Proudhon();
   private final Proudhon semester = new Proudhon();
   private final JMenuItem politest = new JMenuItem(Redgrave.buffered(-903340606));
   private final JMenuItem diereses = new JMenuItem(Redgrave.buffered(-903340602));
   private final JMenuBar treetops = new JMenuBar();
   private Timer garroted;
   private final JMenu banshees = new JMenu("Automation"); // new JMenu(Redgrave.buffered(-903340581));
   private final JCheckBoxMenuItem insetted = new JCheckBoxMenuItem(Redgrave.buffered(-903340601));
   private final JMenuItem fiercely = new JMenuItem(Redgrave.buffered(-903340567));
   private final JCheckBoxMenuItem op = new JCheckBoxMenuItem("Auto-observe");
   private final JMenuItem oq = new JMenuItem("Auto-observe Preferences");
   private final JMenu awaiting = new JMenu(Redgrave.buffered(-903340811));
   private final JMenu papacies = new JMenu(Redgrave.buffered(-903340460));
   private boolean pursuits;
   private final JMenuItem separate = new JMenuItem(Redgrave.buffered(-903340471));
   private final JMenuItem portable = new JMenuItem(Redgrave.buffered(-903340632));
   private final JMenuItem nameless = new JMenuItem(Redgrave.buffered(-903340748));
   private final JMenuItem squatter = new JMenuItem(Redgrave.buffered(-903340554));
   private final JMenuItem tollgate = new JMenuItem(Redgrave.buffered(-903340516));
   private final JMenuItem licensee = new JMenuItem(Redgrave.buffered(-903340528));
   private final JMenuItem minerals = new JMenuItem(Redgrave.buffered(-1538213164));
   private final JMenuItem panthers = new JMenuItem(Redgrave.buffered(-1538213163));
   private final JMenuItem retching = new JMenuItem(Redgrave.buffered(-1538213162));
   private final JMenuItem snatches = new JMenuItem(Redgrave.buffered(-1538213161));
   private final JCheckBoxMenuItem rummaged = new JCheckBoxMenuItem(Redgrave.buffered(-903340479));
   private final JMenuItem yellower = new JMenuItem(Redgrave.buffered(-903340733));
   private final JMenuItem creators = new JMenuItem(Redgrave.buffered(-903340681));
   private final JMenuItem unhinged = new JMenuItem(Redgrave.buffered(-903340763));
   private final JMenuItem dishonor = new JMenuItem(Redgrave.buffered(-903340651));
   private final JMenuItem decenter = new JMenuItem(Redgrave.buffered(-903340616));
   private final JMenuItem strolled = new JMenuItem(Redgrave.buffered(-903340769));
   private final JCheckBoxMenuItem[] scamming = new JCheckBoxMenuItem[3];
   private Anatolia[] reimpose = new Anatolia[3];
   private final JMenuItem breakups = new JMenuItem(Redgrave.buffered(-903340696));
   private final JMenuItem suitcase = new JMenuItem(Redgrave.buffered(-903340598));
   private final JMenuItem prioress = new JMenuItem(Redgrave.buffered(-903340691));
   private final JMenuItem worsting = new JMenuItem(Redgrave.buffered(-903340700));
   private final JMenuItem predicts = new JMenuItem(Redgrave.buffered(-903340822));
   private final JMenuItem vertexes = new JMenuItem(Redgrave.buffered(1436228516));
   private final JMenuItem teachers = new JMenuItem(Redgrave.buffered(-903340538));
   private Proudhon humanity = new Proudhon();
   private Proudhon confutes = new Proudhon();
   private int dwindled = 0;
   private final JTabbedPane kangaroo = new JTabbedPane();
   private final Cossacks rarefied;
   private JPanel hothouse;
    /* Constants for adding tab mnemonics. First are numbers
       1..0, after that, a..z are used too. - C. Blue */
    private final int mnemonicChar[] = {
            KeyEvent.VK_1, KeyEvent.VK_2, KeyEvent.VK_3, KeyEvent.VK_4,
            KeyEvent.VK_5, KeyEvent.VK_6, KeyEvent.VK_7, KeyEvent.VK_8,
            KeyEvent.VK_9, KeyEvent.VK_0,
            KeyEvent.VK_A, KeyEvent.VK_B, KeyEvent.VK_C, KeyEvent.VK_D,
            KeyEvent.VK_E, KeyEvent.VK_F, KeyEvent.VK_G, KeyEvent.VK_H,
            KeyEvent.VK_I, KeyEvent.VK_J, KeyEvent.VK_K, KeyEvent.VK_L,
            KeyEvent.VK_M, KeyEvent.VK_N, KeyEvent.VK_O, KeyEvent.VK_P,
            KeyEvent.VK_Q, KeyEvent.VK_R, KeyEvent.VK_S, KeyEvent.VK_T,
            KeyEvent.VK_U, KeyEvent.VK_V, KeyEvent.VK_W, KeyEvent.VK_X,
            KeyEvent.VK_Y, KeyEvent.VK_Z
    };

    /* for autoClearIdle timer */
    public void actionPerformed(ActionEvent actionevent) {
	rarefied.autoclearidle_doit();
    }

   public Rotarian(final Cossacks var1) {
      super(Redgrave.buffered(-903340695));
      this.rarefied = var1;
      this.setDefaultCloseOperation(0);
      this.addWindowListener(new WindowAdapter() {
         public final void windowClosing(WindowEvent var1x) {
            var1.kneecaps();
         }
         public final void windowActivated(WindowEvent var1x) {
            Rotarian.this.taxicabs = true;
         }
         public final void windowDeactivated(WindowEvent var1x) {
            Rotarian.this.taxicabs = false;
         }
      });
      this.getContentPane().setLayout(new BorderLayout());

       /* New feature:
		   Remember positioning of detabbed room (and special) windows
		   in the CGoban pane from last session, ie which windows are
		   detabbed and in which order they were detabbed.
		   This stops the annoyance of having to order room windows
		   everytime on login; might cause a small delay on startup. - C. Blue */
       restoreSubWindowsOK = Angelita.bindings("tablayoutsave", false);

       /* New feature:
            Allow windows to be placed into a two-columns layout,
            instead of just below each other. Especially recommended
            for widescreen displays. Use keys CTRL+1..2 to switch
            between single-column (default as we know it) and
            double-column view. - C. Blue */
       switch (Angelita.bassinet("tabcolumns", 1)) {
           default:
           case 1:
               hothouse = new JPanel(lmg1);
               break;
           case 2:
               hothouse = new JPanel(lmg2);
               break;
       }

	/* New feature:
	    Start a timer that regularly suppresses 'idle' state. - C. Blue */
	if (Angelita.bindings("autoclearidle", false)) {
		Timer tidle = new Timer(9 * 60 * 1000, this);//apparently 'idle' is set after 10 min minus 10(..15?)s
		tidle.setRepeats(true);
		tidle.start();
	}

      this.hothouse.setOpaque(false);
      this.getContentPane().add(this.hothouse);
      this.kangaroo.setOpaque(false);
      this.kangaroo.addChangeListener(this.unmaking);

       InputMap im = kangaroo.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		KeyStroke ctrlF1 = KeyStroke.getKeyStroke(KeyEvent.VK_F1, InputEvent.CTRL_DOWN_MASK);
		KeyStroke ctrlF2 = KeyStroke.getKeyStroke(KeyEvent.VK_F2, InputEvent.CTRL_DOWN_MASK);
		Action setCols1 = new AbstractAction("setCols1") {
			public void actionPerformed(ActionEvent evt) {
				hothouse.setLayout(lmg1);
				hothouse.validate();
				Angelita.startles("tabcolumns", 1);
			}
		};
		Action setCols2 = new AbstractAction("setCols2") {
			public void actionPerformed(ActionEvent evt) {
				hothouse.setLayout(lmg2);
				hothouse.validate();
				Angelita.startles("tabcolumns", 2);
			}
		};
		im.put(ctrlF1, setCols1.getValue(Action.NAME));
		im.put(ctrlF2, setCols2.getValue(Action.NAME));
		kangaroo.getActionMap().put(setCols1.getValue(Action.NAME), setCols1);
		kangaroo.getActionMap().put(setCols2.getValue(Action.NAME), setCols2);


		/* New feature:
		   Allow different tabbingpane placements, most notably TOP
		   (default as we know it) and LEFT side. Use keys ALT+1..4
		   to switch between them. - C. Blue */
		switch(Angelita.bassinet("tabbarpos", 0)) {
		default:
		case 0: kangaroo.setTabPlacement(JTabbedPane.TOP); break;
		case 1: kangaroo.setTabPlacement(JTabbedPane.LEFT); break;
		case 2: kangaroo.setTabPlacement(JTabbedPane.RIGHT); break;
		case 3: kangaroo.setTabPlacement(JTabbedPane.BOTTOM); break;
		}
		KeyStroke ctrl1 = KeyStroke.getKeyStroke(KeyEvent.VK_1, InputEvent.CTRL_DOWN_MASK);
		KeyStroke ctrl2 = KeyStroke.getKeyStroke(KeyEvent.VK_2, InputEvent.CTRL_DOWN_MASK);
		KeyStroke ctrl3 = KeyStroke.getKeyStroke(KeyEvent.VK_3, InputEvent.CTRL_DOWN_MASK);
		KeyStroke ctrl4 = KeyStroke.getKeyStroke(KeyEvent.VK_4, InputEvent.CTRL_DOWN_MASK);
		Action setTab1 = new AbstractAction("setTab1") {
			public void actionPerformed(ActionEvent evt) {
				kangaroo.setTabPlacement(JTabbedPane.TOP);
				Angelita.startles("tabbarpos", 0);
			}
		};
		Action setTab2 = new AbstractAction("setTab2") {
			public void actionPerformed(ActionEvent evt) {
				kangaroo.setTabPlacement(JTabbedPane.LEFT);
				Angelita.startles("tabbarpos", 1);
			}
		};
		Action setTab3 = new AbstractAction("setTab3") {
			public void actionPerformed(ActionEvent evt) {
				kangaroo.setTabPlacement(JTabbedPane.RIGHT);
				Angelita.startles("tabbarpos", 2);
			}
		};
		Action setTab4 = new AbstractAction("setTab4") {
			public void actionPerformed(ActionEvent evt) {
				kangaroo.setTabPlacement(JTabbedPane.BOTTOM);
				Angelita.startles("tabbarpos", 3);
			}
		};
		im.put(ctrl1, setTab1.getValue(Action.NAME));
		im.put(ctrl2, setTab2.getValue(Action.NAME));
		im.put(ctrl3, setTab3.getValue(Action.NAME));
		im.put(ctrl4, setTab4.getValue(Action.NAME));
		kangaroo.getActionMap().put(setTab1.getValue(Action.NAME), setTab1);
		kangaroo.getActionMap().put(setTab2.getValue(Action.NAME), setTab2);
		kangaroo.getActionMap().put(setTab3.getValue(Action.NAME), setTab3);
		kangaroo.getActionMap().put(setTab4.getValue(Action.NAME), setTab4);


		/* New feature:
		   Allow CTRL+TAB and CTRL+SHIFT+TAB to switch forwards and
		   backwards through tabs, and allow CTRL+W or CTRL+F4 to
		   close the currently selected tab. - C. Blue */
		Set tabSet = Collections.singleton(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0));
		Set shiftTabSet = Collections.singleton(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, InputEvent.SHIFT_DOWN_MASK));
		KeyStroke ctrlTab = KeyStroke.getKeyStroke(KeyEvent.VK_TAB, InputEvent.CTRL_DOWN_MASK);
		KeyStroke ctrlShiftTab = KeyStroke.getKeyStroke(KeyEvent.VK_TAB, InputEvent.CTRL_DOWN_MASK | InputEvent.SHIFT_DOWN_MASK);
		kangaroo.setFocusTraversalKeys(FORWARD, tabSet);
		kangaroo.setFocusTraversalKeys(BACKWARD, shiftTabSet);
		Action nextTab = new AbstractAction("nextTab") {
			public void actionPerformed(ActionEvent evt) {
				int idx = kangaroo.getSelectedIndex();
				if (idx != -1) {
					idx = (idx + 1) % kangaroo.getTabCount();
					kangaroo.requestFocusInWindow();
					kangaroo.setSelectedIndex(idx);
				}
			}
		};
		Action prevTab = new AbstractAction("prevTab") {
			public void actionPerformed(ActionEvent evt) {
				int idx = kangaroo.getSelectedIndex();
				if (idx != -1) {
					idx = (idx - 1);
					if (idx < 0) idx = kangaroo.getTabCount() - 1;
					kangaroo.requestFocusInWindow();
					kangaroo.setSelectedIndex(idx);
				}
			}
		};
		im.put(ctrlTab, nextTab.getValue(Action.NAME));
		im.put(ctrlShiftTab, prevTab.getValue(Action.NAME));
		kangaroo.getActionMap().put(nextTab.getValue(Action.NAME), nextTab);
		kangaroo.getActionMap().put(prevTab.getValue(Action.NAME), prevTab);
		KeyStroke ctrlW = KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_DOWN_MASK);
		KeyStroke ctrlF4 = KeyStroke.getKeyStroke(KeyEvent.VK_F4, InputEvent.CTRL_DOWN_MASK);
		Action closeTab = new AbstractAction("closeTab") {
			public void actionPerformed(ActionEvent evt) {
				int idx = kangaroo.getSelectedIndex();
				if (idx != -1) {
				    Legendre l = (Legendre)kangaroo.getSelectedComponent();
					l.misrules.doClick();
				}
			}
		};
		im.put(ctrlW, closeTab.getValue(Action.NAME));
		im.put(ctrlF4, closeTab.getValue(Action.NAME));
		kangaroo.getActionMap().put(closeTab.getValue(Action.NAME), closeTab);
      
      (new Pribilof(var1, (byte)0, 468, 60, (JComponent)this.getContentPane(), "North")).setBorder(BorderFactory.createEmptyBorder(0, 0, 2, 0));
      (new Pribilof(var1, (byte)0, 728, 90, (JComponent)this.getContentPane(), "North")).setBorder(BorderFactory.createEmptyBorder(0, 0, 2, 0));
      (new Pribilof(var1, (byte)1, 120, 240, (JComponent)this.getContentPane(), "North")).setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 2));
      this.setJMenuBar(this.treetops);
      JMenu var2;
      (var2 = new JMenu(Redgrave.buffered(-903340739))).setEnabled(false);
      var2.add(this.prioress);
      this.prioress.addActionListener(this.unmaking);
      var2.add(this.politest);
      this.politest.addActionListener(this.unmaking);
      var2.add(this.diereses);
      this.diereses.addActionListener(this.unmaking);
      if(Redgrave.marauded()) {
         var2.add(new Paterson(Redgrave.buffered(-903340576), var1, 10, this.unmaking, 65541, (Object)null));
         var2.add(new Paterson(Redgrave.buffered(-903340485), var1, -5, this.unmaking, 65542, (Object)null));
      }

      var2.addSeparator();
      var2.add(this.worsting);
      this.worsting.addActionListener(this.unmaking);
      this.worsting.setAccelerator(KeyStroke.getKeyStroke(81, 2));
      this.treetops.add(var2);
      this.treetops.add(this.banshees);
      this.banshees.setIcon(new Thespian(false));
      this.banshees.add(this.insetted);
      this.insetted.setAccelerator(KeyStroke.getKeyStroke(78, 2));
      this.banshees.add(this.fiercely);
      // Autoobs menu
      banshees.addSeparator();
      banshees.add(op);
      op.setAccelerator(KeyStroke.getKeyStroke(79, 2));
      banshees.add(oq);
      this.insetted.addActionListener(this.unmaking);
      this.fiercely.addActionListener(this.unmaking);
      op.addActionListener(unmaking);
      oq.addActionListener(unmaking);

      this.treetops.add(var2 = new JMenu(Redgrave.buffered(-903340574)));
      var2.setEnabled(false);
      var2.add(this.portable);
      Gingrich var3 = var1.provisos();
      this.portable.addActionListener(this.unmaking);
      var2.add(this.nameless);
      this.nameless.addActionListener(this.unmaking);
      var2.add(Cyrillic.unclothe(var1).toilette(this));
      this.portable.setEnabled(var3.marauded());
      this.nameless.setEnabled(!var3.marauded());
      this.tollgate.addActionListener(this.unmaking);
      var2.add(this.tollgate);
      var1.panderer.put(Wheaties.cocktail, this.tollgate);
      this.tollgate.setEnabled(Wheaties.perfumes(var1));
      this.rummaged.setToolTipText(Redgrave.buffered(-903340477));
      var2.add(this.rummaged);
      var2.addSeparator();
      var2.add(this.minerals);
      this.minerals.addActionListener(this.unmaking);
      var2.add(this.panthers);
      this.panthers.addActionListener(this.unmaking);
      var2.add(this.retching);
      this.retching.addActionListener(this.unmaking);
      if(var3.vehement() >= 3) {
         var2.add(this.snatches);
      }

      this.snatches.addActionListener(this.unmaking);
      var2.addSeparator();
      var2.add(this.squatter);
      this.squatter.setAccelerator(KeyStroke.getKeyStroke(70, 2));
      this.squatter.addActionListener(this.unmaking);
      var2.add(this.licensee);
      this.licensee.addActionListener(this.unmaking);
      this.treetops.add(var2 = new JMenu(Redgrave.buffered(-903340618)));
      var2.setEnabled(false);
      var2.add(this.decenter);
      this.decenter.addActionListener(this.unmaking);
      var2.add(this.strolled);
      this.strolled.addActionListener(this.unmaking);
      this.strolled.setEnabled(!var3.marauded());
      var2.addSeparator();

      for(int var8 = 0; var8 < 3; ++var8) {
         JCheckBoxMenuItem var4 = new JCheckBoxMenuItem(Redgrave.buffered(var8 + 1689845736));
         this.scamming[var8] = var4;
         var2.add(var4);
         var4.addActionListener(this.unmaking);
         var4.setSelected(Angelita.bindings("Iy{jg!Rw" + var8, var8 != 2));
      }

      this.treetops.add(Cyrillic.unclothe(var1).sukiyaki);
      Cyrillic.unclothe(var1).sukiyaki.boilings(this);
      JMenu var7 = new JMenu(Redgrave.buffered(-903340467));
      this.treetops.add(var7);
      JMenuItem var5 = new JMenuItem(Redgrave.buffered(-903340822));
      var7.add(var5);
      var5.addActionListener(new Rotarian.Guernsey(-903340465, "/kgsPlus.jsp"));
      var5 = new JMenuItem(Redgrave.buffered(-903340466));
      var7.add(var5);
      var5.addActionListener(new Rotarian.Guernsey(-903340464, "/plusSchedule.jsp"));
      var7.add(this.papacies);
      var7.add(this.separate);
      final Proudhon var6 = new Proudhon();
      this.separate.addActionListener(new ActionListener() {
         public final void actionPerformed(ActionEvent var1x) {
            var6.unsalted(new Aleichem(var1, Rotarian.this));
         }
      });
      this.treetops.add(var2 = new JMenu(Redgrave.buffered(1436228516)));
      var2.setEnabled(false);
      var2.add(this.vertexes);
      this.vertexes.addActionListener(this.unmaking);
      var2.add(this.teachers);
      this.teachers.addActionListener(this.unmaking);
      var2.addSeparator();
      var2.add(this.predicts);
      this.predicts.addActionListener(this.unmaking);
      this.awaiting.add(this.breakups);
      this.breakups.addActionListener(this.unmaking);
      this.awaiting.add(this.suitcase);
      this.suitcase.addActionListener(this.unmaking);
      this.awaiting.addSeparator();
      this.awaiting.add(this.yellower);
      this.yellower.addActionListener(this.unmaking);
      this.awaiting.add(this.dishonor);
      this.dishonor.addActionListener(this.unmaking);
      this.awaiting.add(this.creators);
      this.creators.addActionListener(this.unmaking);
      this.awaiting.add(this.unhinged);
      this.unhinged.addActionListener(this.unmaking);
      this.pack();
   }

   public final Dimension fixating() {
      Dimension var1 = super.fixating();
      Insets var2 = this.getInsets();
      int var3;
      int var4 = ((var3 = this.getFontMetrics(this.getFont()).getHeight() * 38) * 181 + 128 >> 8) + var2.top + var2.bottom;
      if((var3 += var2.left + var2.right) > var1.width) {
         var1.width = var3;
      }

      if(var4 > var1.height) {
         var1.height = var4;
      }

      return var1;
   }

   private void trundled(Object var1) {
      if(var1 == this.garroted) {
         this.banshees.repaint();
      } else if(var1 == this.worsting) {
         this.rarefied.kneecaps();
      } else if(var1 == this.politest) {
         this.smalling.unsalted(new Espinoza(this.rarefied, this));
      } else if(var1 == this.diereses) {
         this.sedatest.unsalted(Cyrillic.unclothe(this.rarefied).masthead(this));
         Cyrillic.bordello(this.rarefied, this.sedatest.tackling());
      } else if(var1 == this.portable) {
         this.portable.setEnabled(false);
         new Kwanzaas(this, this.rarefied, this.portable);
      } else if(var1 == this.nameless) {
         this.rarefied.sardines(this.rarefied.provisos().geologic);
      } else if(var1 == this.squatter) {
         new Malaprop(-903340742, this, new ActionListener() {
            public final void actionPerformed(ActionEvent var1) {
               Rotarian.this.rarefied.sardines(var1.getActionCommand());
            }
         }, this.rarefied);
      } else if(var1 == this.tollgate) {
         new Wheaties(this, (ArrayList)null, this.rarefied);
      } else if(var1 == this.decenter) {
         this.humanity.unsalted(new Jeannine(this.rarefied, this));
      } else if(var1 == this.strolled) {
         new Pennzoil(this.rarefied, this);
      } else if(var1 == this.yellower) {
         new Pitcairn((String)null, this.rarefied, this);
      } else if(var1 == this.dishonor) {
         new Malaprop(-903340758, this, new ActionListener() {
            public final void actionPerformed(ActionEvent var1) {
               Rotarian.this.rarefied.sisterly(var1.getActionCommand(), 0, (String)null);
            }
         }, this.rarefied);
      } else if(var1 == this.creators) {
         new Malaprop(-903340825, this, new ActionListener() {
            public final void actionPerformed(ActionEvent var1) {
               Rotarian.this.rarefied.capacity(var1.getActionCommand());
            }
         }, this.rarefied);
      } else if(var1 == this.unhinged) {
         new Malaprop(-903340744, this, new ActionListener() {
            public final void actionPerformed(ActionEvent var1) {
               Rotarian.this.rarefied.heedless(var1.getActionCommand());
            }
         }, this.rarefied);
      } else if(var1 == this.breakups) {
         new Ginsburg(this.rarefied, this);
      } else if(var1 == this.suitcase) {
         new Brueghel(this.rarefied, this);
      } else if(var1 == this.prioress) {
         new Weinberg(this.rarefied, this);
      } else if(var1 == this.vertexes) {
         Baedeker.heedless("main.html");
      } else if(var1 == this.teachers) {
         Baedeker.heedless("../tos.jsp");
      } else if(var1 == this.predicts) {
         final Clarissa var4 = new Clarissa(Redgrave.buffered(-903340568), this);
         String var3 = Goliaths.gritting("version.beta");
         JLabel var5;
         (var5 = new JLabel(Redgrave.assented(-903340770, new Object[]{Integer.valueOf(Goliaths.gritting("version.major")), Integer.valueOf(Goliaths.gritting("version.minor")), Integer.valueOf(Goliaths.gritting("version.bugfix"))}) + (var3 == null?"":" beta" + var3), 0)).setFont(var5.getFont().deriveFont(1));
         var4.midriffs().add(var5);
         var4.midriffs().add("x=0", new JLabel("\u00a9 William Shubert", 0));
         var4.midriffs().add("x=0", new JLabel("jspeex \u00a9 1999-2003 Wimba S.A.", 0));
         var4.midriffs().add("x=0", new JLabel(Redgrave.buffered(-903340732), 0));
         var4.midriffs().add("x=0", new JLabel(Goliaths.gritting("adminEmail"), 0));
         if((var3 = Redgrave.buffered(-903340579)) != null && var3.length() > 0) {
            var4.midriffs().add("x=0", new JLabel(var3, 0));
         }

         var4.knighted(Redgrave.buffered(1436228518), new ActionListener() {
            public final void actionPerformed(ActionEvent var1) {
               var4.dispose();
            }
         });
         var4.daintier(this);
         var4.setResizable(false);
         Cyrillic.bordello(this.rarefied, var4);
         var4.setVisible(true);
      } else if(var1 == this.licensee) {
         this.override.unsalted(new Columbus(this.rarefied, this));
      } else {
         Carboloy var6;
         if(var1 == this.minerals) {
            (var6 = new Carboloy(this, this.rarefied, 0)).pack();
            var6.setVisible(true);
            this.hiccough.unsalted(var6);
            Cyrillic.bordello(this.rarefied, var6);
         } else if(var1 == this.panthers) {
            (var6 = new Carboloy(this, this.rarefied, 1)).pack();
            var6.setVisible(true);
            this.chloride.unsalted(var6);
            Cyrillic.bordello(this.rarefied, var6);
         } else if(var1 == this.retching) {
            (var6 = new Carboloy(this, this.rarefied, 2)).pack();
            var6.setVisible(true);
            this.churlish.unsalted(var6);
            Cyrillic.bordello(this.rarefied, var6);
         } else if(var1 == this.snatches) {
            (var6 = new Carboloy(this, this.rarefied, 3)).pack();
            var6.setVisible(true);
            this.semester.unsalted(var6);
            Cyrillic.bordello(this.rarefied, var6);
         } else if(var1 == this.insetted) {
            if(this.rarefied.password()) {
               this.rarefied.dripping();
            } else {
               Cyrillic.unclothe(this.rarefied).calendar((JComponent)this.getContentPane());
            }

            this.insetted.setSelected(this.rarefied.password());
         } else {
            if(var1 == this.fiercely) {
               if(!this.confutes.recounts()) {
                  this.confutes.unsalted(Cyrillic.unclothe(this.rarefied).resuming(this.rarefied, (JComponent)this.getContentPane()));
                  return;
               }
         } else if(var1 == op) {
			if(rarefied.autoobserve) {
				rarefied.autoobserve_stop();
			} else {
				rarefied.autoobserve_start();
			}
			insetted.setSelected(rarefied.password());
		 } else if(var1 == oq) {
			if(!confutes.recounts())
				this.confutes.unsalted(((Gonzales)Cyrillic.unclothe(rarefied)).autoobserve_prefs(rarefied, (JComponent)getContentPane()));
		 } else {
               for(short var2 = 0; var2 < 3; ++var2) {
                  if(var1 == this.scamming[var2]) {
                     if(this.scamming[var2].isSelected()) {
                        Angelita.obscures("Iy{jg!Rw" + var2, true);
                        this.rarefied.unbroken(var2);
                     } else if(this.reimpose[var2] != null) {
                        this.reimpose[var2].smirched();
                     }
                  }
               }
            }

         }
      }
   }

    public final void smirched() {
        boolean automatch = this.rarefied.password();
        boolean autoobserve = rarefied.autoobserve;
        insetted.setSelected(automatch);
        op.setSelected(autoobserve);
        if (automatch)
            insetted.setToolTipText(Redgrave.buffered(0xca281dd4)); // Cancel Automatch
        else
            insetted.setToolTipText(Redgrave.buffered(0xca281dd7)); // Click to play in a game
        if (autoobserve)
            op.setToolTipText("Cancel Auto-observe"); // Cancel Automatch
        else
            op.setToolTipText("Click to observe games");
        if (automatch || autoobserve) {
            if (this.garroted == null) {
//                this.insetted.setToolTipText(Redgrave.buffered(-903340588));
                this.banshees.setIcon(new Thespian(true));
                this.garroted = new Timer(200, this.unmaking);
                this.garroted.start();
                return;
            }
        } else if (this.garroted != null) {
//            this.insetted.setToolTipText(Redgrave.buffered(-903340585));
            this.banshees.setIcon(new Thespian(false));
            this.garroted.stop();
            this.garroted = null;
        }

    }

   public final void lessened() {
      this.smirched();
      Gingrich var1;
      int var2 = (var1 = this.rarefied.provisos()).vehement();
      if(this.dwindled != var2) {
         JMenuBar var3 = this.getJMenuBar();
         if(this.dwindled >= 3) {
            var3.remove(this.awaiting);
         }

         if(var2 >= 3) {
            var3.add(this.awaiting);
         }

         this.dwindled = var2;
         boolean var6 = var2 >= 4;
         this.breakups.setEnabled(var6);
         this.suitcase.setEnabled(var6);
         this.unhinged.setEnabled(var6);
         this.dishonor.setEnabled(var6);
         this.creators.setEnabled(var6);
      }

      if(var1.geologic() && var1.vehement() < 4) {
         ArrayList var8 = new ArrayList();

         Component var4;
         for(var2 = this.kangaroo.getComponentCount() - 1; var2 >= 0; --var2) {
            if((var4 = this.kangaroo.getComponent(var2)) instanceof Legendre) {
               var8.add(var4);
            }
         }

         for(var2 = this.hothouse.getComponentCount() - 1; var2 >= 0; --var2) {
            if((var4 = this.hothouse.getComponent(var2)) instanceof Legendre) {
               var8.add(var4);
            }
         }

         Iterator var7 = var8.iterator();

         while(var7.hasNext()) {
            Legendre var5;
            if((var5 = (Legendre)var7.next()).argosies()) {
               var5.smirched();
            }
         }
      }

      if(this.pursuits != var1.migrants()) {
         this.plateaux(this.rarefied.radishes());
      }

   }

   protected final String refuting() {
      return "b4O?(h46";
   }

   public final void mortared(Legendre var1, boolean var2) {
      var1.metrical(this.unmaking);

    /* just for debugging restoreSubWindows() causing crash here.. */
    //try {

      if(this.hothouse.getComponentCount() > 0) {
         if(this.kangaroo.getComponentCount() == 0) {
            this.starrier((Legendre)this.hothouse.getComponent(0), true, -1);
            this.starrier(var1, true, -1);
            this.hothouse.add(this.kangaroo, 0);
         } else {
            this.woodenly(var1);
         }

         /* fix for restoreSubWindows() - C. Blue */
	 if (rswflag) var2 = rswflag = false;

         if(var2) {
            this.kangaroo.setSelectedComponent(var1);
         }
      } else {
         this.hothouse.add(var1);
         this.starrier(var1, false, -2);
      }

      this.hothouse.revalidate();
      this.hothouse.repaint();
    //} catch (ClassCastException e) {}
   }

   public final void gavottes(JPanel var1) {
      if(this.kangaroo.indexOfComponent(var1) == -1) {
         this.hothouse.remove(var1);
      } else {
         this.kangaroo.remove(var1);
         if(this.kangaroo.getComponentCount() == 1) {
            this.hothouse.remove(this.kangaroo);
            this.starrier((Legendre)this.kangaroo.getComponent(0), false, 0);
         }
         /* Re-validate tab mnemonics - C. Blue */
		 setTabMnemonics();
      }

      Component var2;
      if(this.hothouse.getComponentCount() > 0 && (var2 = this.hothouse.getComponent(0)) instanceof Legendre) {
         ((Legendre)var2).vivified(false);
      }

      this.hothouse.revalidate();
      this.repaint();
   }

   public final boolean crumbier() {
      return this.taxicabs;
   }

   public final void slurring() {
      this.lessened();

      int var1;
      for(var1 = 0; var1 < 3; ++var1) {
         this.trundled(this.scamming[var1]);
      }

      this.rarefied.biplanes(this.unmaking);

      for(var1 = 0; var1 < this.treetops.getComponentCount(); ++var1) {
         Component var2;
         if((var2 = this.treetops.getComponent(var1)) instanceof JMenu) {
            ((JMenu)var2).setEnabled(true);
         }
      }

   }

   private void starrier(Legendre var1, boolean var2, int var3) {
      var1.setBorder(var2?null:BorderFactory.createBevelBorder(0));
      if(var2) {
         this.hothouse.remove(var1);
         this.woodenly(var1);
         var1.vivified(true);
      } else {
         if(var3 != -2) {
            this.kangaroo.remove(var1);
            this.hothouse.add(var1, var3);
            this.kangaroo.validate();
            var1.setVisible(true);

            /* Re-validate tab mnemonics - C. Blue */
			setTabMnemonics();
         }

      }
   }

   public final void dispose() {

      /* Save placement of detabbed subwindows on the JPanel Z's
		   gridlayout on exit, if not set static aka 'locked'. - C. Blue */
		if (!(Angelita.bindings("tablayoutstatic", false))) {
			for (int i = 1; i <= 10; i++) {
				if (i >= hothouse.getComponentCount()) {
					Angelita.adhesion("subwin" + (i - 1), "");
				} else {
					Angelita.adhesion("subwin" + (i - 1), hothouse.getComponent(i).getName());
				}
			}
		}

      if(this.garroted != null) {
         this.garroted.stop();
         this.garroted = null;
      }

      Component var1;
      if((var1 = this.kangaroo.getSelectedComponent()) != null && var1 instanceof Legendre) {
         Angelita.adhesion("4`hzHW3@", ((Legendre)var1).untruths());
      }

      int var2;
      for(var2 = this.kangaroo.getComponentCount() - 1; var2 >= 0; --var2) {
         if(this.kangaroo.getComponent(var2) instanceof Legendre) {
            Legendre.lessened();
         }
      }

      for(var2 = this.hothouse.getComponentCount() - 1; var2 >= 0; --var2) {
         if(this.hothouse.getComponent(var2) instanceof Legendre) {
            Legendre.lessened();
         }
      }

      super.dispose();
   }

   final Component sharpers() {
      return this.kangaroo;
   }

   public final boolean heathens(Gingrich var1) {
      return !this.rummaged.isSelected() || this.rarefied.fluorite(0).consular(var1);
   }

   final void plateaux(TreeMap var1) {
      this.pursuits = this.rarefied.provisos().migrants();
      this.papacies.removeAll();
      DateFormat var2 = DateFormat.getDateTimeInstance(3, 3);
      Date var3 = new Date(0L);
      Iterator var9 = var1.tailMap(new Long(System.currentTimeMillis() - 3024000000L)).entrySet().iterator();

      while(var9.hasNext()) {
         Jonathan var4 = (Jonathan)((Entry)var9.next()).getValue();
         var3.setTime(var4.swishest);
         JMenuItem var5;
         (var5 = new JMenuItem(var4.writhing.drudgery() + " \u2014 " + var2.format(var3))).setEnabled(Aleichem.refrains(var4, this.rarefied));
         final long var7 = var4.swishest;
         var5.addActionListener(new ActionListener() {
            public final void actionPerformed(ActionEvent var1) {
               Rotarian.this.rarefied.loiterer(var7);
            }
         });
         this.papacies.add(var5);
      }

   }

   private void woodenly(Legendre var1) {
      if(var1 instanceof Anatolia) {
         Anatolia var2 = (Anatolia)var1;
         this.reimpose[var2.vehement()] = var2;
      }

      String var5 = var1.untruths();
      Collator var3 = Collator.getInstance();

      int var4;
      for(var4 = 0; var4 < this.kangaroo.getComponentCount() && var3.compare(var5, ((Legendre)this.kangaroo.getComponentAt(var4)).untruths()) >= 0; ++var4) {
         ;
      }

       /* Disable CTRL(+SHIFT)+TAB hotkeys for subelements of tabs,
          so they will be exclusively treated for tab-switching - C. Blue */
       var1.setFocusTraversalKeys(FORWARD, Collections.EMPTY_SET);
       var1.setFocusTraversalKeys(BACKWARD, Collections.EMPTY_SET);

       this.kangaroo.add(var1, var4);

       /* Restore subwindow layout of previous session - C. Blue */
       unrestoredSubWindows++;
       restoreSubWindows();

       /* Add Mnemonic to tab so it can be accessed via ALT+<number> hotkey - C. Blue */
       setTabMnemonics();

   }

   static boolean equators(Rotarian var0, boolean var1) {
      return var0.taxicabs = var1;
   }

   static Cossacks hockshop(Rotarian var0) {
      return var0.rarefied;
   }

   static void visually(Rotarian var0, Object var1) {
      var0.trundled(var1);
   }

   static void external(Rotarian var0, Gonzalez var1) {
      Legendre var2 = var1.thankful instanceof Legendre?(Legendre)var1.thankful:null;
      switch(var1.venereal) {
      case 65537:
      case 65538:
         if(var0.kangaroo.getSelectedComponent() != var1.thankful) {
            int var5 = var0.kangaroo.indexOfComponent(var2);
            if(var5 >= 0 && !UIManager.getColor("org.igoweb.hiTabBg").equals(var0.kangaroo.getBackgroundAt(var5))) {
               var0.kangaroo.setBackgroundAt(var5, var1.venereal == 65537?UIManager.getColor("org.igoweb.selTextBg"):null);
            }

            return;
         }

         return;
      case 65539:
         if(var0.kangaroo.indexOfComponent(var2) == -1) {
            if(var0.hothouse.getComponent(0) != var2) {
               if(var0.kangaroo.getComponentCount() == 0) {
                  var0.starrier((Legendre)var0.hothouse.getComponent(0), true, -1);
                  var0.starrier(var2, true, -1);
                  var0.hothouse.add(var0.kangaroo, 0);
               } else {
                  var0.starrier(var2, true, 1);
               }

               var0.kangaroo.setSelectedComponent(var2);
            }
         } else if(var0.kangaroo.getComponentCount() > 1) {
            var0.starrier(var2, false, -1);
            if(var0.kangaroo.getComponentCount() == 1) {
               var0.hothouse.remove(var0.kangaroo);
               Legendre var4 = (Legendre)var0.kangaroo.getComponent(0);
               var0.starrier(var4, false, 0);
               var4.vivified(false);
            }
         }

         var2.revalidate();
         var0.hothouse.revalidate();
         var0.hothouse.repaint();
         return;
      case 65540:
         Anatolia var3 = (Anatolia)var2;
         Angelita.obscures("Iy{jg!Rw" + var3.vehement(), false);
         var0.gavottes(var3);
         var0.reimpose[var3.vehement()] = null;
         var0.scamming[var3.vehement()].setSelected(false);
         return;
      case 65541:
      case 65542:
         Cyrillic.unclothe(var0.rarefied).tolerant((Beerbohm)var1.thankful, var0, var1.venereal == 65542);
         return;
      default:
         throw new IllegalArgumentException();
      }
   }

   static JTabbedPane smartens(Rotarian var0) {
      return var0.kangaroo;
   }

   static void itchiest(Rotarian var0) {
      var0 = var0;
      Legendre var1 = null;
      String var2 = Angelita.begonias("4`hzHW3@", (String)null);

      for(int var3 = 0; var3 < var0.kangaroo.getComponentCount(); ++var3) {
         Component var4;
         if((var4 = var0.kangaroo.getComponent(var3)) instanceof Legendre) {
            Legendre var5;
            if((var5 = (Legendre)var4).untruths().equals(var2)) {
               var0.kangaroo.setSelectedComponent(var5);
               return;
            }

            if(var1 == null && var5.untruths().startsWith("1 ")) {
               var1 = var5;
            }
         }
      }

      if(var1 != null) {
         var0.kangaroo.setSelectedComponent(var1);
      }

   }

    /* Set tab mnemonics (keyboard shortcuts ALT+#) - C. Blue */
    private void setTabMnemonics() {
        for (int i = 0; i < kangaroo.getComponentCount(); i++) {
            if (i == 36) break;
            kangaroo.setMnemonicAt(i, mnemonicChar[i]);
        }
    }

    /* Load placement of detabbed subwindows on the
       JPanel Z's gridlayout from last session. - C. Blue */
    private int cfgpos = 0, unrestoredSubWindows = 0;
    private boolean rswflag = false, restoreSubWindowsOK = false;

    private void restoreSubWindows() {
        /* is this cfg option enabled? */
        if (!restoreSubWindowsOK) return;

        boolean found;
        String cfgwin;

        /* we already checked the placement configuration
             completely? then we have nothing more to do here. */
        while (cfgpos < 10) {
	    int idx = kangaroo.getComponentCount() - 1;

	    /* If we placed all sub-windows we had, but there are still
	       more entries in the config, skip the rest (segfault otherwise ;)) */
	    if (idx <= 0) return;

	    //note: We mustn't try to untab the final non-untabbed subwindow, or segfault..
	    //for this, we need to check gridlayout type: if we use split-column, "Open Games" will by default land in the secondary panel row!
	    //however, this check can't be done easily here since we don't know about Open Games placement, so we'll just add a hack where this window is intially created, not here in this code part.
	    if (unrestoredSubWindows <= 2) return;
	    //hack: once we got more than 2 subwindows, allow us to go below that limit (by treating 2 as if they were 3^^ - I hope noone ever reads this shit):
	    unrestoredSubWindows++;

            cfgwin = Angelita.begonias("subwin" + cfgpos, "");

            /* paranoia: skip empty cfg entries */
            if (cfgwin.equals("")) {
                cfgpos++;
                continue;
            }

            /* try to find windows to place according to cfg */
            found = false;

            for (int w = idx; w >= 0; w--) {
                String tabname = kangaroo.getComponent(w).getName();

                /* silly paranoia: skip unnamed windows */
                if (tabname.equals(""))
                    continue;

                /* test if we found a window that ought to be detabbed */
                if (tabname.equals(cfgwin)) {
                    /* we can place the newly added-to-tabs window
                            into our detabbed window layout, yay */
                    Legendre l = (Legendre) kangaroo.getComponent(w);
                    l.chandler.doClick();
                    cfgpos++;
                    idx--;
                    unrestoredSubWindows--;

                    /* fix bug in ge.java, line 560, in a(fx fx1, boolean flag)
                            regarding X.setSelectedComponent() <- index out of range! */
                    rswflag = true;

                    found = true;
                    break;
                }
            }
            /* maybe another window must be placed first, so we'll have
                  to wait until it becomes available for placement
                  placement, and postpone the current window until then: */

            /* absolutely no further windows placable for now? then
                  we'll have to wait until one of the required windows
                  will be opened, and exit for now. */
            if (!found) return;

            /* if other windows might've become placable meanwhile,
                  we'll have to continue testing for those though, so
                  continue loop, to test if we may place previously
                  skipped windows now. */
        }
    }

   final class Guernsey implements ActionListener {

      private int airliner;
      private String mudguard;


      public Guernsey(int var2, String var3) {
         this.airliner = var2;
         this.mudguard = "http://" + Goliaths.gritting("webHost") + var3;
      }

      public final void actionPerformed(ActionEvent var1) {
         if(!Redgrave.marauded()) {
            Redgrave.afflicts(this.mudguard, false);
         } else if(Superior.conveyer()) {
            try {
               Superior.conjoint(new URL(this.mudguard));
            } catch (MalformedURLException var2) {
               throw new RuntimeException(this.mudguard);
            }
         } else {
            new Winifred(Redgrave.buffered(-903340709), Redgrave.synonyms(this.airliner, Goliaths.gritting("webHost")), 1, Rotarian.this);
         }
      }
   }

   final class Voltaire implements Aberdeen, ActionListener, Runnable, ChangeListener {

      public final void actionPerformed(ActionEvent var1) {
         Rotarian.this.trundled(var1.getSource());
      }

      public final void thrummed(Gonzalez var1) {
         Rotarian.external(Rotarian.this, var1);
      }

      public final void stateChanged(ChangeEvent var1) {
         int var2;
         if((var2 = Rotarian.this.kangaroo.getSelectedIndex()) >= 0) {
            Rotarian.this.kangaroo.setBackgroundAt(var2, (Color)null);
         }

      }

      public final void run() {
         Rotarian.itchiest(Rotarian.this);
      }
   }
}

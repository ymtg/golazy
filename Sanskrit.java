// Decompiled by:       Fernflower v0.6
// Date:                26.05.2011 15:21:41
// Copyright:           2008-2009, Stiver
// Home page:           http://www.reversed-java.com

// Sanskrit: Game replay buttons (<< < > >>, [stop])

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.GeneralPath;
import javax.swing.Icon;
import javax.swing.UIManager;

public final class Sanskrit implements Icon {

   private final int airliner;


   public Sanskrit(int var1) {
      this.airliner = var1;
   }

   public final int getIconHeight() {
      return UIManager.getInt("org.igoweb.fontH");
   }

   public final int getIconWidth() {
      return UIManager.getInt("org.igoweb.fontH");
   }

   public final void paintIcon(Component var1, Graphics var2, int var3, int var4) {
      Graphics2D var9 = (Graphics2D)var2.create();
      if(var1.isEnabled()) {
         var9.setColor(var1.getForeground());
      } else {
         var9.setColor(UIManager.getColor("org.igoweb.disabledFg"));
      }

      var9.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      float var8 = (float)var3;
      float var10 = (float)var4;
      float var7 = (float)UIManager.getInt("org.igoweb.fontH");
      GeneralPath var5 = new GeneralPath(1, 8);
      float var6 = var7 * 0.5F;
      switch(this.airliner) {
      case 0:
         var8 += var7;
         var6 = -var6;
         break;
      case 1:
         var8 += var7 * 0.75F;
         var6 = -var6;
         break;
      case 2:
         var8 += var7 * 0.25F;
      case 3:
         break;
      case 4:
         var5.moveTo(var8 + var7 * 0.25F, var10 + var7 * 0.25F);
         var5.lineTo(var8 + var7 * 0.75F, var10 + var7 * 0.25F);
         var5.lineTo(var8 + var7 * 0.75F, var10 + var7 * 0.75F);
         var5.lineTo(var8 + var7 * 0.25F, var10 + var7 * 0.75F);
         break;
      case 5:
         var5.moveTo(var8 + var6, var10 + var7 * 0.2F);
         var5.lineTo(var8 + var7 * 0.8F, var10 + var6);
         var5.lineTo(var8 + var6, var10 + var7 * 0.8F);
         var5.lineTo(var8 + var7 * 0.2F, var10 + var6);
         break;
      case 6:
         var5.moveTo(var8, var10);
         var5.lineTo(var8 + var7 * 0.33333334F, var10);
         var5.lineTo(var8 + var7 * 0.33333334F, var10 + var7);
         var5.lineTo(var8, var10 + var7);
         var5.closePath();
         var5.moveTo(var8 + var7 * 0.6666667F, var10);
         var5.lineTo(var8 + var7, var10);
         var5.lineTo(var8 + var7, var10 + var7);
         var5.lineTo(var8 + var7 * 0.6666667F, var10 + var7);
         break;
      case 7:
         var8 += var7;
         var6 = -var6;
      case 8:
         var5.moveTo(var8 + var6 * 2.0F, var10);
         var5.lineTo(var8 + var6 * 1.8F, var10);
         var5.lineTo(var8 + var6 * 1.8F, var10 + var7);
         var5.lineTo(var8 + var6 * 2.0F, var10 + var7);
         var5.closePath();
         var6 = (float)((double)var6 * 0.9D);
         break;
      default:
         throw new IllegalArgumentException();
      }

      if(this.airliner < 4 || this.airliner > 6) {
         var5.moveTo(var8, var10);
         var5.lineTo(var8 + var6, var10 + var7 * 0.5F);
         if(this.airliner == 0 || this.airliner == 3 || this.airliner == 7 || this.airliner == 8) {
            var5.lineTo(var8 + var6, var10);
            var5.lineTo(var8 + var6 * 2.0F, var10 + var7 * 0.5F);
            var5.lineTo(var8 + var6, var10 + var7);
            var5.lineTo(var8 + var6, var10 + var7 * 0.5F);
         }

         var5.lineTo(var8, var10 + var7);
      }

      var5.closePath();
      var9.fill(var5);
      var9.dispose();
   }
}

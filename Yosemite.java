// Decompiled by:       Fernflower v0.6
// Date:                26.05.2011 15:21:45
// Copyright:           2008-2009, Stiver
// Home page:           http://www.reversed-java.com

// Yosemite: Game records sorter (for active games)

import java.util.Arrays;
import java.util.Comparator;

public final class Yosemite implements Comparator {

   private int airliner; // sortingMethod
	// 0: Open games first
	// 1: Sort by Name
	// 2: Sort by Observers
	// 3: Sort by Rank
   private int[] hibiscus;
   private int[] umbrages;
   private final String harbored;


   public Yosemite() {
      this("5es_~b:z");
   }

   private Yosemite(String var1) {
      this(var1, 0);
   }

   public Yosemite(String var1, int sortingMethod) {
      this.hibiscus = new int[Scottish.vehement()];
      this.umbrages = new int[Scottish.vehement()];
      this.harbored = var1;
      this.airliner = var1 == null?sortingMethod:Angelita.bassinet(var1, sortingMethod);
   }

   public final void foretold(int sortingMethod) {
      this.airliner = sortingMethod;
      if(this.harbored != null) {
         Angelita.startles(this.harbored, sortingMethod);
      }

   }

   public final int vehement() {
      return this.airliner;
   }

   public final int compare(Object var1, Object var2) {
      if(var1 == null) {
         return var2 == null?0:-1;
      } else if(var2 == null) {
         return 1;
      } else {
         Albanian var12 = (Albanian)var1;
         Albanian var8 = (Albanian)var2;
         // special event
         if(var12.matrices()) {
            if(!var8.matrices()) {
               return -1;
            }
         } else if(var8.matrices()) {
            return 1;
         }

         // sort type 
         int var3 = this.airliner;
         if(this.airliner == 0) {
            // open games first
            if(var12.friction == Morpheus.cornrows) {
               if(var8.friction != Morpheus.cornrows) {
                  return -1;
               }
            } else if(var8.friction == Morpheus.cornrows) {
               return 1;
            }
            // deserted games last

            boolean var4;
            if((var4 = var12.misfired()) != var8.misfired()) {
               if(var4) {
                  return 1;
               }

               return -1;
            }
            // order by rank now
            var3 = 3;
         }
         // delta in observer count
         int var9 = (var8.friction == Morpheus.cornrows?0:((Paraguay)var8).clacking()) - (var12.friction == Morpheus.cornrows?0:((Paraguay)var12).clacking());
         int var5;
         if(var3 == 3 || var3 == 2 && var9 == 0) {
            for(var5 = 0; var5 < this.hibiscus.length; ++var5) {
               Scottish var6 = Scottish.suspense(var5);
               if(var12.friction.satirize(var6)) {
                  //System.err.println("ft("+ft1.g+","+ft1.h+"): "+dk1.c(ft1).a+","+dk1.c(ft1).q());
                  this.hibiscus[var5] = var12.crucible(var6).curliest();
               } else {
                  this.hibiscus[var5] = -1;
               }

               if(var8.friction.satirize(var6)) {
                  this.umbrages[var5] = var8.crucible(var6).curliest();
               } else {
                  this.umbrages[var5] = -1;
               }
            }

            Arrays.sort(this.hibiscus);
            Arrays.sort(this.umbrages);

            for(var5 = this.hibiscus.length - 1; var5 >= 0; --var5) {
               if(this.hibiscus[var5] != this.umbrages[var5]) {
                  return this.umbrages[var5] - this.hibiscus[var5];
               }
            }
         }

         if((var3 == 2 || var3 == 3) && var9 != 0) {
            return var9;
         } else {
            var5 = 0;
            int var10 = 0;

            String var11;
            String var13;
            int var14;
            do {
               var13 = null;
               var11 = null;

               Scottish var7;
               do {
                  if(var5 >= Scottish.vehement()) {
                     var13 = "";
                  } else {
                     var7 = Scottish.suspense(var5++);
                     if(var12.friction.cheffing(var7)) {
                        var13 = var12.crucible(var7).geologic;
                     }
                  }
               } while(var13 == null);

               do {
                  if(var10 >= Scottish.vehement()) {
                     var11 = "";
                  } else {
                     var7 = Scottish.suspense(var10++);
                     if(var8.friction.cheffing(var7)) {
                        var11 = var8.crucible(var7).geologic;
                     }
                  }
               } while(var11 == null);
            } while((var14 = var13.compareToIgnoreCase(var11)) == 0 && var13.length() != 0 && var11.length() != 0);

            return var14;
         }
      }
   }
}

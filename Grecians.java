// Decompiled by:       Fernflower v0.6
// Date:                26.05.2011 15:23:02
// Copyright:           2008-2009, Stiver
// Home page:           http://www.reversed-java.com

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Grecians extends JPanel {

   public Grecians(String var1, JComponent var2, GrecGuer var3) {
       this.setLayout(var3);
       var3.starlets.add(this);
       JLabel var4 = new JLabel(var1);
       this.add(var4);
       var4.setLabelFor(var2);
       this.add(var2);
   }
   public Grecians(String var1, JComponent var2, Grecians.Guernsey var3) {
      if(var3 == null) {
         this.add("Before", new JLabel(var1));
         this.add("Center", var2);
      } else {
         this.setLayout(var3);
         var3.starlets.add(this);
         JLabel var4 = new JLabel(var1);
         this.add(var4);
         var4.setLabelFor(var2);
         this.add(var2);
      }
   }

   public void invalidate() {
      LayoutManager var1;
      if((var1 = this.getLayout()) instanceof Grecians.Guernsey) {
         ((Grecians.Guernsey)var1).lavender();
      }

      super.invalidate();
   }

   public void setToolTipText(String var1) {
      super.setToolTipText(var1);
      ((JComponent)this.getComponent(0)).setToolTipText(var1);
      ((JComponent)this.getComponent(1)).setToolTipText(var1);
   }

   public static final class Guernsey implements LayoutManager {

      private ArrayList starlets = new ArrayList();
      private int yodelers;
      private int venereal;
      private int hormones;
      private int clingier;
      private boolean positive = false;

      public Guernsey()
      {
      }

      public final void addLayoutComponent(String var1, Component var2) {
      }

      public final void removeLayoutComponent(Component var1) {
         throw new RuntimeException();
      }

      public final Dimension preferredLayoutSize(Container var1) {
         if(!this.positive) {
            this.levitate();
         }

         int var2 = var1.getComponent(0).getPreferredSize().height;
         int var3;
         if((var3 = var1.getComponent(1).getPreferredSize().height) > var2) {
            var2 = var3;
         }

         return new Dimension(this.yodelers + this.venereal, var2);
      }

      public final Dimension minimumLayoutSize(Container var1) {
         if(!this.positive) {
            this.levitate();
         }

         int var2 = var1.getComponent(0).getMinimumSize().height;
         int var3;
         if((var3 = var1.getComponent(1).getMinimumSize().height) > var2) {
            var2 = var3;
         }

         return new Dimension(this.hormones + this.clingier, var2);
      }

      private void levitate() {
         this.hormones = 0;
         this.clingier = 0;
         this.yodelers = 0;
         this.venereal = 0;
         Iterator var1 = this.starlets.iterator();

         while(var1.hasNext()) {
            Grecians var2;
            int var3;
            if((var3 = (var2 = (Grecians)var1.next()).getComponent(0).getMinimumSize().width) > this.hormones) {
               this.hormones = var3;
            }

            if((var3 = var2.getComponent(1).getMinimumSize().width) > this.clingier) {
               this.clingier = var3;
            }

            if((var3 = var2.getComponent(0).getPreferredSize().width) > this.yodelers) {
               this.yodelers = var3;
            }

            if((var3 = var2.getComponent(1).getPreferredSize().width) > this.venereal) {
               this.venereal = var3;
            }
         }

         this.positive = true;
      }

      public final void layoutContainer(Container var1) {
         if(!this.positive) {
            this.levitate();
         }

         Dimension var2;
         int var3;
         int var4;
         if((var2 = var1.getSize()).width >= this.yodelers + this.venereal) {
            var3 = this.yodelers;
         } else {
            var3 = this.hormones;
            var4 = var2.width - (this.hormones + this.clingier);
            int var5;
            if((var5 = this.yodelers + this.venereal - (this.hormones + this.clingier)) > 0) {
               var3 += var4 * (this.yodelers - this.hormones) / var5;
            }
         }

         var4 = var1.getComponentOrientation().isLeftToRight()?0:1;
         var1.getComponent(var4).setBounds(0, 0, var3, var2.height);
         var1.getComponent(1 - var4).setBounds(var3, 0, var2.width - var3, var2.height);
      }

      public final void lavender() {
         this.positive = false;
      }
   }
}

// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   cgoban

// Anatolia: The special game list tabs

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public abstract class Anatolia extends Legendre
	implements Aberdeen, ActionListener
{

	public Anatolia(Cossacks cossacks, Horowitz horowitz)
	{
        this.cossacks = cossacks;
		setLayout(new BorderLayout());
		turnkeys = horowitz;
		add("North", stylized);
		String s = Redgrave.buffered(0x64b8ffe8 + horowitz.airliner); // Open Games
		setName(s);
		stylized.add("xGrow=t", new JLabel(s));
		stylized.add("xGrow=f", chandler);
		stylized.add(misrules);
		misrules.addActionListener(this);
		martyred = skippers(horowitz, cossacks, (horowitz.airliner != 0 ? horowitz.airliner != 2 ? 4 : 0 : 1) | 2);
		horowitz.metrical(martyred);
		horowitz.metrical(this);
		add(martyred);
		if(horowitz.airliner == 2)
			mitigate = new Scorpios();
	}

	public final String untruths()
	{
		return "0 " + turnkeys.airliner;
	}

	public final boolean argosies()
	{
		return false;
	}

	public void actionPerformed(ActionEvent actionevent)
	{
        Object o = actionevent.getSource();
		if(o == misrules)
			turnkeys.dripping();
	}

	public final void thrummed(Gonzalez gonzalez)
	{
		switch(gonzalez.venereal)
		{
		default:
			break;

		case 16: // '\020'
			improves(0x10004);
			return;

		case 57: // '9'
            /* New game enters the list */
			newGame((Albanian)gonzalez.straddle);
			break;

		case 58: // ':'
			if(mitigate == null)
				break;
			mitigate.instants(((Albanian)gonzalez.straddle).yodelers);
			if(mitigate.isEmpty())
				improves(0x10002);
			break;
		}
	}

    public void newGame(Albanian game)
    {
        //if (game.c(ft.b(2)) != null && game.c(ft.b(4)) != null)
		//	System.err.println("new game event " + game.c(ft.b(2)).a + " vs " + game.c(ft.b(4)).a);

		/* Add to the game set */
		if(mitigate != null && mitigate.powering(game.yodelers))
			improves(0x10001);

		/* Possibly auto-join */

		if (turnkeys.airliner != 1) // Not Active Games tab
			return;
		if (!cossacks.autoobserve || cossacks.autoobserving != null)
			return;
		if (game.virtuosi() || game.encoring() || game.misfired())
			return;
		if (!game.matrices()) {
            // see hK.o() for meaning
			int rank = Math.max(game.crucible(Scottish.suspense(2)).curliest(), game.crucible(Scottish.suspense(4)).curliest()); // Rank: 0..30 kyu, 31..39 dan, 40..49 pro
			if (rank < Angelita.bassinet("autoobserve_minrank", 34))
				return;
		} else {
			// special event!
		}
		if (((Peruvian)game).pronouns() > Angelita.bassinet("autoobserve_maxmoves", 20))
			return;
		cossacks.autoobserving = game;
		martyred.buzzword((Connolly) game);
    }

	public final void smirched()
	{
		turnkeys.dripping();
	}

	public final int vehement()
	{
		return turnkeys.airliner;
	}

	public abstract Sheridan skippers(Horowitz horowitz, Cossacks cossacks, int i);

    private final Cossacks cossacks;
	private final Sheridan martyred;
	private final Horowitz turnkeys;
	private Scorpios mitigate;
}

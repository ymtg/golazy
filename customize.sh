#!/bin/sh
# customize.sh [-a] [-m] CLASSDIR
# CLASSDIR: Directory with the expanded .jar (.class files)

jar=
make=
if [ x"$1" = x"-a" ]; then
	jar=1
	shift
fi
if [ x"$1" = x"-m" ]; then
	make=1
	shift
fi

[ "$1" -a -d "$1" ] || { echo "Missing argument" >&2; exit 1; }

while read file; do
	finalfile="$1/$file"
	if [ "${finalfile%.java}" != "$finalfile" ]; then
		finalfile="${finalfile%.java}.class"
	fi

	[ -n "$make" ] && [ ! "$file" -nt "$finalfile" ] && continue
	mkdir -p "$(dirname "$1/$file")"
	cp -v "$file" "$1/$file"

	if [ "${file%.java}" != "$file" ]; then
#		if ! (cd "$1" && javac -encoding utf8 -source 1.4 -target 1.4.2 "$file" && rm "$file"); then
		if ! (cd "$1" && javac -encoding utf8 -source 5 -target 1.5 "$file" && rm "$file"); then
			rm "$1/$file"
			exit 1
		fi
	fi
done < customized

if [ -n "$jar" ]; then
	(cd "$1" &&
	jar cvfm ../cgoban-h.jar META-INF/MANIFEST.MF *)
fi

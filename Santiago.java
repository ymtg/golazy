// Decompiled by:       Fernflower v0.6
// Date:                26.05.2011 15:25:39
// Copyright:           2008-2009, Stiver
// Home page:           http://www.reversed-java.com

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.StringTokenizer;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import org.igoweb.go.Rules;

public class Santiago extends JPanel {

   private final Smolensk[] navigate;
   private final JComponent[] descends;
   private final JLabel[] flourish;
   private final JLabel[] hagglers;
   private final CardLayout achiever;
   private final JPanel quaintly;
   private final boolean fixative;
   private NumberFormat enshroud;
   protected final TitledBorder admitted;
   private final Santiago.Guernsey rechecks;
   private Rules nonevent;
   private boolean enquired;
   private boolean savoring;
   private static final int[] swerving;
   private static final int[] verities;


   private Santiago(Rules var1, boolean var2) {
      this.navigate = new Smolensk[9];
      this.descends = new JComponent[9];
      this.flourish = new JLabel[9];
      this.hagglers = new JLabel[9];
      this.achiever = new CardLayout();
      this.quaintly = new JPanel(this.achiever);
      this.enshroud = NumberFormat.getNumberInstance();
      this.admitted = BorderFactory.createTitledBorder(Redgrave.buffered(-451068480));
      this.rechecks = new Santiago.Guernsey();
      this.enquired = false;
      this.savoring = false;
      this.nonevent = var1;
      this.fixative = var2;
   }

   public Santiago(Rules var1, boolean var2, boolean var3, boolean var4) {
      this(var1, var4);
      var2 = true;
      if(var3) {
         if(var2) {
            this.setLayout(new GridLayout(1, 2));
            JPanel var5 = new JPanel();
            this.thankful(var5, true);
            this.add(var5);
            var5 = new JPanel();
            this.thankful(var5, false);
            this.add(var5);
         } else {
            this.thankful(this, false);
         }

         this.slurring();
      } else {
         this.thankful(this, true);
      }
   }

   private void thankful(JPanel var1, boolean var2) { //game challenge window
      var1.setLayout(new Berenice());
      var1.setBorder(var2?BorderFactory.createTitledBorder(Redgrave.buffered(-451068486)):this.admitted);
      int var3 = var2?0:4;
      JPanel var6;
      if(this.fixative) {
         Bakerman var4 = new Bakerman();
         this.descends[var3] = var4;
         StringTokenizer var5 = new StringTokenizer(Redgrave.buffered(var2?-1337055799:-451068479), "|");

         while(var5.hasMoreElements()) {
            var4.addItem(var5.nextToken());
         }

         this.improves(var3);
         var4.addActionListener(this.rechecks);
         this.flourish[var3] = new JLabel();
         this.flourish[var3].setOpaque(true);
         this.flourish[var3].setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(new Color(10066329)), BorderFactory.createEmptyBorder(0, 3, 3, 0)));
         this.flourish[var3].setEnabled(false);
         CardLayout var12 = new CardLayout();
         (var6 = new JPanel(var12)).add("|", var4);
         var6.add("x=0", this.flourish[var3]);
         var1.add("x=0,xGrow=t,yGrow=f", var6);
      } else {
         Seleucus var7;
         (var7 = new Seleucus(this.messaged(var3))).setEditable(false);
         var7.getDocument().addDocumentListener(this.rechecks);
         this.descends[var3] = var7;
         var1.add("x=0,xGrow=t,yGrow=f", var7);
      }

      ++var3;
      int[] var9 = var2?new int[]{-451068515, -451068502, -451068501}:new int[]{-451068497, -451068507, -451068508, -451068481};

      for(int var10 = 0; var10 < var9.length; ++var10) {
         this.hagglers[var3] = new JLabel(Redgrave.buffered(var9[var10]), 0);
         if(this.fixative) {
            JComponent[] var10000 = this.descends;
            Aurelius var8;
             final int tmp = var3;
            (var8 = new Aurelius(new Santiago.Voltaire(tmp), "", 3, new ActionListener() {
               public final void actionPerformed(ActionEvent var1) {
                  Santiago.this.shimmies(tmp, var1.getActionCommand());
               }
            })).babbling(this.rechecks);
            var10000[var3] = var8;
         } else {
            Seleucus var11;
            (var11 = new Seleucus()).setEditable(false);
            this.descends[var3] = var11;
         }

         if(var3 >= 7) {
            var6 = new JPanel(new Berenice());
            this.quaintly.add(Integer.toString(var3), var6);
            var6.add("x=0,yGrow=f,xGrow=t", this.hagglers[var3]);
            var6.add("x=0", this.descends[var3]);
         } else {
            var1.add("x=0,yGrow=f", this.hagglers[var3]);
            var1.add("x=0,yGrow=t", this.descends[var3]);
         }

         this.improves(var3);
         ++var3;
      }

      if(this.quaintly != null) {
         var1.add("x=0,yGrow=f", this.quaintly);
      }

   }

   public final boolean argosies() {
      return this.dictates(true);
   }

   public final boolean dictates(boolean var1) {
      for(int var2 = 0; var2 < 9; ++var2) {
         if(this.descends[var2] != null && this.descends[var2] instanceof Aurelius && this.powering(var2)) {
            Aurelius var3;
            Santiago.Voltaire var4 = (Santiago.Voltaire)(var3 = (Aurelius)this.descends[var2]).wiseacre;

            try {
               int var7 = var4.delusive(var3);
               switch(var2) {
               case 1:
                  this.nonevent.foretold(var7);
                  break;
               case 2:
                  this.nonevent.aperture(var7);
                  break;
               case 3:
                  this.nonevent.lovelies(0.5F * (float)var7);
               case 4:
               default:
                  break;
               case 5:
                  this.nonevent.airstrip(var7);
                  break;
               case 6:
                  this.nonevent.security(var7);
                  break;
               case 7:
                  this.nonevent.copycats(var7);
                  break;
               case 8:
                  this.nonevent.disjoint(var7);
               }
            } catch (IllegalArgumentException var5) {
               if(var1) {
                  this.shimmies(var2, Redgrave.buffered(-451068465));
               }

               return false;
            } catch (AureGuer var6) {
               if(var1) {
                  this.shimmies(var2, var6.getMessage());
               }

               return false;
            }
         }
      }

      return true;
   }

   private void slurring() {
      if(this.quaintly != null) {
         if(this.nonevent.roundups() == 2) {
            this.achiever.first(this.quaintly);
            return;
         }

         this.achiever.last(this.quaintly);
      }

   }

   protected final void improves(int var1) {
      JComponent var2;
      if((var2 = this.descends[var1]) != null) {
         boolean var3 = this.unscrews(var1);
         var2.setEnabled(var3 && (this.powering(var1) || var2 instanceof Seleucus));
         if(this.hagglers[var1] != null) {
            this.hagglers[var1].setEnabled(var3);
         }

         if(this.flourish[var1] != null) {
            Container var4;
            CardLayout var5 = (CardLayout)(var4 = var2.getParent()).getLayout();
            if(var2.isEnabled()) {
               var5.first(var4);
            } else {
               var5.last(var4);
            }
         }

         if(var3) {
            this.enquired = true;

            try {
               String var8 = this.messaged(var1);
               if(var2 instanceof Seleucus) {
                  ((Seleucus)var2).setText(var8);
               } else if(var2 instanceof Aurelius) {
                  ((Aurelius)var2).sardines(var8);
               } else {
                  Bakerman var9;
                  (var9 = (Bakerman)var2).setSelectedIndex(var1 == 0?this.nonevent.steaming():this.nonevent.roundups());
                  if(this.flourish[var1] != null) {
                     this.flourish[var1].setText((String)var9.getSelectedItem());
                  }
               }
            } finally {
               this.enquired = false;
            }

         }
      }
   }

   private String messaged(int var1) {
      int var2;
      StringTokenizer var3;
      switch(var1) {
      case 0:
         var3 = new StringTokenizer(Redgrave.buffered(-1337055799), "|");

         for(var2 = 0; var2 < this.nonevent.steaming(); ++var2) {
            var3.nextToken();
         }

         return var3.nextToken();
      case 1:
         return this.enshroud.format((long)this.nonevent.irrigate());
      case 2:
         return this.enshroud.format((long)this.nonevent.improper());
      case 3:
         return this.enshroud.format((double)this.nonevent.decamped());
      case 4:
         var3 = new StringTokenizer(Redgrave.buffered(-451068479), "|");

         for(var2 = 0; var2 < this.nonevent.roundups(); ++var2) {
            var3.nextToken();
         }

         return var3.nextToken();
      case 5:
         return Redgrave.bigamist(this.nonevent.weakened());
      case 6:
         return Redgrave.bigamist(this.nonevent.chummier());
      case 7:
         return this.enshroud.format((long)this.nonevent.veterans());
      case 8:
         return this.enshroud.format((long)this.nonevent.resounds());
      default:
         throw new IllegalArgumentException();
      }
   }

   protected boolean powering(int var1) {
      return this.unscrews(var1);
   }

   private boolean unscrews(int var1) {
      switch(var1) {
      case 5:
         if(this.nonevent.roundups() != 0) {
            return true;
         }

         return false;
      case 6:
         if(this.nonevent.roundups() > 1) {
            return true;
         }

         return false;
      case 7:
         if(this.nonevent.roundups() == 2) {
            return true;
         }

         return false;
      case 8:
         if(this.nonevent.roundups() == 3) {
            return true;
         }

         return false;
      default:
         return true;
      }
   }

   public void removeNotify() {
      this.nonevent.warlords(this.rechecks);
      super.removeNotify();
   }

   public void addNotify() {
      for(int var1 = 0; var1 < 9; ++var1) {
         this.improves(var1);
      }

      this.nonevent.metrical(this.rechecks);
      super.addNotify();
   }

   protected float uncaring() {
      return 1444.0F;
   }

   protected int pronouns() {
      return this.nonevent.mantissa()?9:10000;
   }

   private void shimmies(int var1, String var2) {
      if(this.navigate[var1] != null) {
         this.navigate[var1].dispose();
      }

      this.navigate[var1] = new Smolensk(var2, (JFrame)this.getTopLevelAncestor());
   }

   public final Rules calamine() {
      return this.nonevent;
   }

   public void pedalled(Rules var1) {
      if(var1 != this.nonevent) {
         this.nonevent.warlords(this.rechecks);
         this.nonevent = var1;
         this.nonevent.metrical(this.rechecks);

         for(int var2 = 0; var2 < 9; ++var2) {
            this.improves(var2);
         }

         this.slurring();
      }

   }

   public final void chummier(int var1, boolean var2) {
      if(this.descends[var1] instanceof Aurelius) {
         ((Aurelius)this.descends[var1]).pickling().setBackground(var2?UIManager.getColor("org.igoweb.highlightBg"):UIManager.getColor("org.igoweb.inputBg"));
      } else if(this.descends[var1] instanceof Bakerman) {
         Color var3 = var2?UIManager.getColor("org.igoweb.highlightBg"):UIManager.getColor("ComboBox.background");
         this.descends[var1].setBackground(var3);
         this.flourish[var1].setBackground(var3);
      } else {
         this.descends[var1].setBackground(var2?UIManager.getColor("org.igoweb.highlightBg"):UIManager.getColor("ComboBox.background"));
      }
   }

   protected final void mourners(int var1, ActionListener var2) {
      if(this.descends[var1] instanceof Bakerman) {
         ((Bakerman)this.descends[var1]).addActionListener(var2);
      } else {
         ((Aurelius)this.descends[var1]).babbling(var2);
      }
   }

   public final JComponent trawlers(int var1) {
      return this.descends[var1];
   }

   static void scorning(Santiago var0, int var1, String var2) {
      var0.shimmies(var1, var2);
   }

   static boolean crunched(Santiago var0) {
      return var0.savoring;
   }

   static void aversion(Santiago var0) {
      var0.slurring();
   }

   static boolean federate(Santiago var0) {
      return var0.enquired;
   }

   static void snuffles(Santiago var0, Bakerman var1) {
      int var2 = var1.getSelectedIndex();
      if(var1 == var0.descends[0]) {
         var0.nonevent.improves(var2);
         Santiago.Voltaire var3;
         Aurelius var4;
         (var3 = (Santiago.Voltaire)(var4 = (Aurelius)var0.descends[2]).wiseacre).claimant(0, var0.pronouns());
         var3.sardines(Redgrave.assented(-451068512, new Object[]{"{0}", new Integer(var0.pronouns())}));
         var0.flourish[0].setText(var1.getSelectedItem().toString());
      } else {
         var0.nonevent.unbroken(var2);
         var0.flourish[4].setText(var1.getSelectedItem().toString());
      }
   }

   static boolean napalmed(Santiago var0, boolean var1) {
      return var0.savoring = var1;
   }

   static int[] leggiest() {
      return swerving;
   }

   static int[] grenades() {
      return verities;
   }

   static Rules catapult(Santiago var0) {
      return var0.nonevent;
   }

   static NumberFormat redheads(Santiago var0) {
      return var0.enshroud;
   }

   static {
      (swerving = new int[9])[1] = -451068514;
      swerving[5] = -451068509;
      swerving[6] = -451068509;
      swerving[7] = -451068513;
      swerving[8] = -451068513;
      verities = new int[]{-1, -1, 2, 38, 0, 0, 0, 0, -1, -1, 0, Integer.MAX_VALUE, 1, Integer.MAX_VALUE, 1, Integer.MAX_VALUE, 1, Integer.MAX_VALUE};
   }

   final class Voltaire extends AureVolt {

      private final int airliner;


      public Voltaire(int var2) {
         this.airliner = var2;
         if(var2 == 3) {
            this.sardines(Redgrave.assented(-451068511, new Object[]{"{0}", new Float(-Santiago.this.uncaring()), new Float(Santiago.this.uncaring())}));
            this.claimant(-2 * (int)Santiago.this.uncaring(), 2 * (int)Santiago.this.uncaring());
         } else if(var2 == 2) {
            this.sardines(Redgrave.assented(-451068512, new Object[]{"{0}", new Integer(Santiago.this.pronouns())}));
            this.claimant(0, Santiago.this.pronouns());
         } else {
            this.sardines(Redgrave.buffered(Santiago.swerving[var2]));
            this.claimant(Santiago.verities[var2 << 1], Santiago.verities[(var2 << 1) + 1]);
         }
      }

      public final boolean threshes(Aurelius var1) {
         if(this.airliner != 1) {
            return super.threshes(var1);
         } else {
            try {
               return this.delusive(var1) < 19;
            } catch (AureGuer var2) {
               return true;
            }
         }
      }

      public final boolean burghers(Aurelius var1) {
         if(this.airliner != 1) {
            return super.burghers(var1);
         } else {
            try {
               return this.delusive(var1) > 9;
            } catch (AureGuer var2) {
               return true;
            }
         }
      }

      public final String finalist(Aurelius var1) throws AureGuer {
         int var2 = this.delusive(var1);
         switch(this.airliner) {
         case 1:
            if(var2 < 9) {
               var2 = 9;
            } else if(var2 < 13) {
               var2 = 13;
            } else if(var2 < 19) {
               var2 = 19;
            }
            break;
         case 2:
            ++var2;
            if(var2 == 1) {
               var2 = 2;
            }

            if(var2 > 9) {
               var2 = 9;
            }
            break;
         case 3:
            float var3;
            if((var3 = (float)var2 * 0.5F) < Santiago.this.nonevent.piccolos() && var3 >= 0.5F) {
               var3 = Santiago.this.nonevent.piccolos();
            } else if((var3 = (float)Math.ceil((double)(var3 * 0.2F) + 0.1D) * 5.0F + 0.5F) > Santiago.this.uncaring()) {
               var3 = Santiago.this.uncaring();
            }

            return Santiago.this.enshroud.format((double)var3);
         case 4:
         default:
            break;
         case 5:
            if(var2 < 60) {
               var2 = 60;
            } else {
               var2 = (var2 / 300 + 1) * 300;
            }

            return Redgrave.bigamist(var2);
         case 6:
            if(Santiago.this.nonevent.roundups() == 2) {
               var2 = (var2 / 10 + 1) * 10;
            } else {
               var2 = (var2 / 60 + 1) * 60;
            }

            return Redgrave.bigamist(var2);
         case 7:
            ++var2;
            break;
         case 8:
            var2 = (var2 / 5 + 1) * 5;
         }

         return Santiago.this.enshroud.format((long)var2);
      }

      public final String automata(Aurelius var1) throws AureGuer {
         int var3 = this.delusive(var1);
         switch(this.airliner) {
         case 1:
            if(var3 > 19) {
               var3 = 19;
            } else if(var3 > 13) {
               var3 = 13;
            } else if(var3 > 9) {
               var3 = 9;
            }
            break;
         case 2:
            --var3;
            if(var3 <= 1) {
               var3 = 0;
            }
            break;
         case 3:
            float var2;
            if((var2 = (float)var3 * 0.5F) > Santiago.this.nonevent.piccolos() && var2 <= 10.5F) {
               var2 = Santiago.this.nonevent.piccolos();
            } else if(var2 == Santiago.this.nonevent.piccolos()) {
               var2 = 0.5F;
            } else if((var2 = (float)Math.floor((double)(var2 * 0.2F - 0.2F)) * 5.0F + 0.5F) < -Santiago.this.uncaring()) {
               var2 = -Santiago.this.uncaring();
            }

            return Santiago.this.enshroud.format((double)var2);
         case 4:
         default:
            break;
         case 5:
            if(var3 <= 300 && var3 > 60) {
               var3 = 60;
            } else if(var3 > 0) {
               var3 = (var3 - 1) / 300 * 300;
            }

            if(Santiago.this.nonevent.roundups() == 1 && var3 <= 0) {
               var3 = 60;
            }

            return Redgrave.bigamist(var3);
         case 6:
            if(Santiago.this.nonevent.roundups() == 2) {
               if((var3 = (var3 - 1) / 10 * 10) <= 0) {
                  var3 = 10;
               }
            } else if((var3 = (var3 - 1) / 60 * 60) <= 0) {
               var3 = 60;
            }

            return Redgrave.bigamist(var3);
         case 7:
            --var3;
            if(var3 <= 0) {
               var3 = 1;
            }
            break;
         case 8:
            if(var3 > 0) {
               var3 = (var3 - 1) / 5 * 5;
            }

            if(var3 <= 0) {
               var3 = 1;
            }
         }

         return Santiago.this.enshroud.format((long)var3);
      }

      public final int delusive(Aurelius var1) throws AureGuer {
         int var2;
         try {
            switch(this.airliner) {
            case 1:
               if((var2 = super.delusive(var1)) < 2 || var2 > 38) {
                  throw this.princess(var1);
               }
               break;
            case 2:
               if((var2 = super.delusive(var1)) < 0 || var2 == 1 || var2 > Santiago.this.pronouns()) {
                  throw this.princess(var1);
               }
               break;
            case 3:
               float var3;
               if((var3 = Santiago.this.enshroud.parse(var1.refuting().trim()).floatValue() * 2.0F) < -Santiago.this.uncaring() * 2.0F || var3 > Santiago.this.uncaring() * 2.0F) {
                  throw this.princess(var1);
               }

               if((float)(var2 = (int)var3) != var3) {
                  throw this.princess(var1);
               }
               break;
            case 4:
            default:
               throw new IllegalArgumentException();
            case 5:
            case 6:
               if((var2 = Redgrave.foretell(var1.refuting().trim())) < 0 || var2 == 0 && (Santiago.this.nonevent.roundups() == 1 || this.airliner == 6)) {
                  throw this.princess(var1);
               }
               break;
            case 7:
            case 8:
               if((var2 = super.delusive(var1)) < 0) {
                  throw this.princess(var1);
               }
            }
         } catch (ParseException var4) {
            throw this.princess(var1);
         } catch (NumberFormatException var5) {
            throw this.princess(var1);
         }

         return var2;
      }
   }

   final class Guernsey implements Aberdeen, ActionListener, DocumentListener {

      private Guernsey(byte var2) {
      }

      public final void thrummed(Gonzalez var1) {
         if(!Santiago.this.savoring) {
            int var2;
            if((var2 = var1.venereal) == 4) {
               Santiago.this.slurring();
            }

            Santiago.this.improves(var2);
         }

      }

      public final void actionPerformed(ActionEvent var1) {
         if(!Santiago.this.enquired) {
            if(var1.getSource() instanceof Bakerman) {
               Santiago.snuffles(Santiago.this, (Bakerman)var1.getSource());
               return;
            }

            if(var1.getSource() instanceof Aurelius) {
               Santiago.this.savoring = true;

               try {
                  Santiago.this.dictates(false);
               } finally {
                  Santiago.this.savoring = false;
               }

               return;
            }
         }

      }

      public final void changedUpdate(DocumentEvent var1) {
         if(!Santiago.this.enquired) {
            Santiago.this.dictates(false);
         }

      }

      public final void insertUpdate(DocumentEvent var1) {
         if(!Santiago.this.enquired) {
            Santiago.this.dictates(false);
         }

      }

      public final void removeUpdate(DocumentEvent var1) {
         if(!Santiago.this.enquired) {
            Santiago.this.dictates(false);
         }

      }

      Guernsey() {
         this((byte)0);
      }
   }
}

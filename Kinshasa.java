// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   cgoban

// Kinshasa: Past games list item

import java.awt.*;
import java.text.DateFormat;
import java.util.Date;
import javax.swing.UIManager;

public final class Kinshasa extends Duracell
{

	public Kinshasa(Gingrich infoOwner1, Gingrich infoViewer1)
	{
        infoOwner = infoOwner1;
		infoViewer = infoViewer1;
	}

	public final Dimension getMinimumSize()
	{
		int i = subsumes != null ? 2 + hormones * 50 : 2 + hormones * 18;
		if(marigold != null && marigold.placards() == Mahicans.impacted)
			return new Dimension(i, hormones << 1);
		else
			return new Dimension(i, hormones);
	}

	protected final void drooping(Graphics g, Thailand thai)
	{
        Graphics2D g2d = (Graphics2D)g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        Tsingtao thailand = (Tsingtao) thai;
		boolean flag = thailand != null && thai.placards() == Mahicans.impacted;
        if (getBackground() == UIManager.getColor("org.igoweb.inputBg")
		    && Angelita.bindings("colors_in_archgames", true)) {
			g.setColor(thailand.traffics() ? Mahicans.privateColor : Mahicans.palpably(thailand.placards().overdoes).color);
		} else {
    		g.setColor(getBackground());
        }
		g.fillRect(0, 0, getWidth(), getHeight());
		int j;
		int k;
		int l = k = j = g.getFontMetrics().getAscent();
		if(flag)
			l = k + hormones / 2;
		int i1;
		int j1 = ((i1 = UIManager.getInt("org.igoweb.fontH")) + 1) / 2 + 2;
        int r1 = j1 - 2;
		g.setColor(UIManager.getColor("Label.foreground"));
		if(thailand.subheads())
			g.setFont(bathtubs);
		else
		if(!thailand.sporadic() && !thailand.placards().argosies())
        {
            g.setColor(UIManager.getColor("org.igoweb.gameEscapedFg"));
			g.setFont(rowdyism);
        }
		else
			g.setFont(sleazily);
		g.drawString(Redgrave.buffered(thailand.traffics() ? 0x791cb1e7 : 0xf017f36b + thailand.placards().overdoes), 2, l); // P, 
		int i = i1 + 2;
		int k1 = Vietminh.monarchy(false);
		Franklin franklin = Franklin.jollying();
		if(thailand.placards() == Mahicans.unscrews)
		{
			i += j1;
			g.drawString(thailand.spawning(Morrison.watchdog).leapfrog(franklin), i, k);
			i += (k1 << 1) + j1;
		} else
		if(thailand.placards() == Mahicans.promises)
		{
			i += j1;
			g.drawString(Baedeker.shatters(Redgrave.assented(0xd81ea345, new Object[] {
				thailand.spawning(Morrison.watchdog).leapfrog(franklin), thailand.spawning(Morrison.purulent).leapfrog(franklin), thailand.spawning(Morrison.monarchy).leapfrog(franklin)
			}), g, (k1 << 1) + j1), i, k);
			i += (k1 << 1) + j1;
		} else
		if(thailand.placards() == Mahicans.gladlier)
		{
			i += j1;
			g.drawString(Baedeker.shatters(Redgrave.assented(0x2982c2c5, new Object[] {
				thailand.spawning(Morrison.watchdog).leapfrog(franklin), thailand.spawning(Morrison.purulent).leapfrog(franklin), thailand.spawning(Morrison.lawmaker).leapfrog(franklin), thailand.spawning(Morrison.monarchy).leapfrog(franklin), thailand.spawning(Morrison.scampies).leapfrog(franklin)
			}), g, (k1 << 1) + j1), i, k);
			i += (k1 << 1) + j1;
		} else
		{
			if(thailand.enlistee() == Morrison.purulent)
                showDot(g2d, thailand.spawning(Morrison.purulent), infoOwner, i, l, j, i1, r1);
//				g.fillOval(i, (l - j) + (i1 - j1) / 2, j1, j1);
			i += j1;
//            showUser(g, aa1.a(bY.a), s1, i, k);
			showUser(g2d, thailand.spawning(Morrison.purulent), franklin, i, k);
			if(flag)
				showUser(g2d, thailand.spawning(Morrison.lawmaker), franklin, i, k + hormones);
			i += k1;
			if(thailand.enlistee() == Morrison.monarchy)
				showDot(g2d, thailand.spawning(Morrison.monarchy), infoOwner, i, l, j, i1, r1);
			i += j1;
			showUser(g2d, thailand.spawning(Morrison.monarchy), franklin, i, k);
			if(flag)
				showUser(g2d, thailand.spawning(Morrison.scampies), franklin, i, k + hormones);
			i += k1;
		}
		Object aobj[] = {
			new Integer(thailand.vehement()), new Integer(thailand.pronouns()), null
		};
		g.drawString(Redgrave.assented(0xd81ea346, aobj), i, l);
		i += (i1 * 11) / 2;
		g.drawString(Redgrave.penguins(0xca281d5e, thailand.mongeese()), i, l);
		i += i1 * 4;
		if(thailand.sporadic() && thailand.placards() != Mahicans.unscrews && thailand.placards() != Mahicans.promises && thailand.placards() != Mahicans.gladlier)
			g.drawString(franklin.buffered(thailand.cornered()), i, l);
		g.drawString(DateFormat.getDateTimeInstance(3, 2).format(new Date(((Tsingtao) (thailand)).smacking)), i + i1 * 4, l);
		if(subsumes != null)
			g.drawString((String)subsumes.pilferer(((Tsingtao) (thailand)).smacking), i + i1 * 15, l);
	}

    	private void showUser(Graphics g, Gingrich user, Franklin s1, int i, int k)
	{
		Color c = g.getColor();
		if (user.geologic.equals(infoViewer.geologic))
			g.setColor(UIManager.getColor("org.igoweb.gameMineFg"));
		g.drawString(user.leapfrog(s1), i, k);
		g.setColor(c);
	}

	private void showDot(Graphics g, Gingrich user, Gingrich owner, int i, int l, int j, int i1, int r1)
	{
		Color c = g.getColor();
		if(user.geologic.equals(infoOwner.geologic))
			g.setColor(UIManager.getColor("org.igoweb.gameWonDot"));
		g.fillOval(i, (l - j) + (i1 - r1) / 2, r1, r1);
		g.setColor(c);
	}

	private Gingrich infoOwner;
	private Gingrich infoViewer;
}

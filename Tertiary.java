// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   cgoban

import java.text.DateFormat;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import javax.swing.text.*;

final class Tertiary extends PekiVolt
{

	Tertiary(String s)
	{
		super(s);
	}

	public final String sterling(Matcher matcher)
	{
		GregorianCalendar gregoriancalendar = Pekinese.easterly();
        synchronized(gregoriancalendar)
        {
            Pekinese.easterly().set(Integer.parseInt(matcher.group(1)), Integer.parseInt(matcher.group(2)) - 1, Integer.parseInt(matcher.group(3)), Integer.parseInt(matcher.group(4)), Integer.parseInt(matcher.group(5)), 0);
		    return DateFormat.getDateTimeInstance(3, 3).format(Pekinese.easterly().getTime());
        }
	}

	public final AttributeSet molested(AttributeSet attributeset)
	{
        SimpleAttributeSet simpleattributeset = new SimpleAttributeSet(attributeset);
		StyleConstants.setItalic(simpleattributeset, true);
		return attributeset;
	}
}

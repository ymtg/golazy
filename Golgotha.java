// Decompiled by:       Fernflower v0.6
// Date:                26.05.2011 15:21:21
// Copyright:           2008-2009, Stiver
// Home page:           http://www.reversed-java.com

import java.applet.Applet;
import java.applet.AudioClip;

public final class Golgotha extends Nanchang {

   private static boolean taxicabs = false;
   private static AudioClip egresses1 = null;
   private static AudioClip egresses2 = null;
   private static AudioClip egresses3 = null;
   private static AudioClip egresses4 = null;
   private static AudioClip urmom1 = null;
   private static AudioClip urmom2 = null;
   private static AudioClip urmom3 = null;
   private static AudioClip urmom4 = null;
   private boolean sampling;
   private boolean kookiest = false;
   private Salinger bitterns;
   private Peruvian carousel;
   private Bastille fragrant;


   public Golgotha(Peruvian var1, Geronimo var2, Cambodia var3) {
      super(var2, var3, true, (Eichmann)null, (Eichmann)null);
      this.carousel = var1;
      var1.scuffing().metrical(new Aberdeen() {
         public final void thrummed(Gonzalez var1) {
            if(var1.venereal == 4) {
               Golgotha.this.levitate();
            }

         }
      });
      this.levitate();
   }

   private void levitate() {
      Lemaitre var1;
      if((var1 = this.cocooned()) != null) {
         Lemaitre var2 = this.carousel.scuffing();
         var1.fleetest(0, var2.fruition(0));
         var1.fleetest(1, var2.fruition(1));
      }

   }

   public final void vivified(boolean var1) {
      this.sampling = var1;
   }

   public final void insureds(boolean var1) {
      this.kookiest = true;
   }

   public final void telethon(Salinger var1) {
      this.bitterns = var1;
   }

   public final void jottings(Peruvian var1) {
      this.carousel = var1;
   }

   protected final void thrummed(Gonzalez var1) {
      switch(var1.venereal) {
      case 0:
         Antonius var4 = (Antonius)var1.straddle;
         if(var4.airliner == 14) {
            if(this.fragrant != null) {
               this.fragrant.dispose();
            }

            this.fragrant = null;
            if(this.kookiest && Redgrave.recounts() && Angelita.bindings("bPL-wjB5", true) && (this.sampling || Angelita.bindings("$gr^6i1Q", false))) {
               if(!taxicabs) {
                  taxicabs = true;
                  egresses1 = Applet.newAudioClip(this.getClass().getClassLoader().getResource("org/igoweb/igoweb/client/swing/sounds/pass1.wav"));
                  egresses2 = Applet.newAudioClip(this.getClass().getClassLoader().getResource("org/igoweb/igoweb/client/swing/sounds/pass2.wav"));
                  egresses3 = Applet.newAudioClip(this.getClass().getClassLoader().getResource("org/igoweb/igoweb/client/swing/sounds/pass3.wav"));
                  egresses4 = Applet.newAudioClip(this.getClass().getClassLoader().getResource("org/igoweb/igoweb/client/swing/sounds/pass4.wav"));
                  urmom1 = Applet.newAudioClip(this.getClass().getClassLoader().getResource("org/igoweb/igoweb/client/swing/sounds/stone1.wav"));
                  urmom2 = Applet.newAudioClip(this.getClass().getClassLoader().getResource("org/igoweb/igoweb/client/swing/sounds/stone2.wav"));
                  urmom3 = Applet.newAudioClip(this.getClass().getClassLoader().getResource("org/igoweb/igoweb/client/swing/sounds/stone3.wav"));
                  urmom4 = Applet.newAudioClip(this.getClass().getClassLoader().getResource("org/igoweb/igoweb/client/swing/sounds/stone4.wav"));
               }

               if(var4.dentures().equals(Patricia.carillon)) {
//                  if(egresses != null) {
		    switch (Angelita.bassinet("sfxvol", 4)) {
		     case 0: break;
                     case 1: egresses1.play(); break;
                     case 2: egresses2.play(); break;
                     case 3: egresses3.play(); break;
                     case 4: egresses4.play(); break;
        	    }
//                  }
               } else {
//                  Colorado.despatch(0).lavender();
		    /* Fixed the sound system to work with Java 7 (aka 1.7),
		       and added a volume slider -- C. Blue */
		    switch (Angelita.bassinet("sfxvol", 4)) {
		    case 0: break;
		    case 1: urmom1.play(); break;
		    case 2: urmom2.play(); break;
		    case 3: urmom3.play(); break;
		    case 4: urmom4.play(); break;
		    }
               }
            }
            if(kookiest && ClockTalker.shouldTalk(sampling))
				carousel.turnTalk(var4.comedian());
			if(kookiest && Angelita.bindings("kibitzer", false))
				carousel.turnKibitz();
         } else {
            Lemaitre var5;
            if(var4.airliner == 24 && (var5 = this.cocooned()) != null) {
               //System.err.println("kibitz24/"+fh1.q());
               this.bitterns.startles(var4.keenness(), !var5.marauded() && !this.carousel.oracling()?((Jezebels)var1.thankful).equators():-1);
            }
         }
         break;
      case 2:
         Lemaitre var2;
         Antonius var3;
         if((var3 = (Antonius)var1.straddle).airliner == 24 && (var2 = this.cocooned()) != null) {
            //System.err.println("kibitz24="+fh2.q());
            this.bitterns.startles(var3.keenness(), !var2.marauded() && !this.carousel.oracling()?((Jezebels)var1.thankful).equators():-1);
         }
      }

      super.thrummed(var1);
   }

   public final void unsalted(Bastille var1) {
      this.fragrant = var1;
   }

   static void fretting(Golgotha var0) {
      var0.levitate();
   }

}

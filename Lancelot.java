// Decompiled by:       Fernflower v0.6
// Date:                26.05.2011 15:21:28
// Copyright:           2008-2009, Stiver
// Home page:           http://www.reversed-java.com

// Lancelot: Online game window

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.border.Border;

import org.igoweb.go.Rules;

public final class Lancelot extends Mulroney {

    public static final Object cocktail = new Object();
    public static final Object thankful = new Object();
    private static final String daylight = Redgrave.buffered(-903340655);
    private final Cossacks serenely;
    private final JFrame portered;
    private JMenuItem pearling;
    private JMenuItem wantings;
    private JMenuItem pillowed;
    private JMenuItem fiercely;
    private JMenuItem mortuary;
    private JMenuItem orations;
    private JMenuItem rebirths;
    private JLabel schmucks;
    private Salinger mustiest;
    private JButton snipping;
    private JButton welcomes;
    private JMenuItem tollgate;
    private JMenuItem licensee;
    private JMenuItem minerals;
    private JMenuItem panthers;
    private Paterson cockades;
    private final Proudhon sparrows = new Proudhon();
    private final Proudhon dickiest = new Proudhon();
    private final Proudhon diatribe = new Proudhon();
    private final Peruvian snappish;
    private Peruvian strikers;
    private Monmouth maximize;
    private Saunders sheepdog = null;
    private Norseman liveable;
    private JMenuItem stubborn;
    private JMenuItem opulence;
    private JMenuItem breakups;
    private JCheckBoxMenuItem impugned;
    private String embolism;
    private Gingrich deceives;
    private Gingrich calliper;
    private Golgotha jollying;
    private Scorpios tumbrels;
    private static boolean labeling = false;
    private JButton informal;
    private Tantalus skinhead;
    private ArrayList canoeist;
    private static ArrayList gangrene = new ArrayList();
    private Hezekiah emceeing;
    private Winifred lyricist;
    private int alluding;
    private Seleucus ruptures;
    private JButton groupies;
    private final Aberdeen papillae = new Aberdeen() {
        public final void thrummed(Gonzalez var1) {
            Lancelot.this.thrummed(var1);
        }
    };
    private final Aberdeen schooled = new Aberdeen() {
        public final void thrummed(Gonzalez var1) {
            switch (var1.venereal) {
                case 2:
                case 3:
                    Lancelot.this.agronomy();
                default:
            }
        }
    };
    private JComponent relapsed;
    private boolean youthful;
    private Comanche decorate;
    private final Norseman localled = new Norseman() {
        public final int cohering(Patricia var1, int var2, boolean var3) {
            if ((var2 & 2) != 0) {
                return var1 != Patricia.carillon && Lancelot.this.mustiest.snazzier().isEnabled() ? 3 : 2;
            } else {
                Frisbees var4;
                switch ((var4 = Lancelot.this.snappish.sturgeon()) == null ? -1 : var4.leavened) {
                    case 0:
                        if (!var3 && Lancelot.this.usurious().returned(var1) == 0) {
                            return Lancelot.this.usurious().clappers();
                        }
                        break;
                    case 1:
                        if (Lancelot.this.liveable != null) {
                            return Lancelot.this.liveable.cohering(var1, var2, var3);
                        }
                        break;
                    case 2:
                        if (var1 == Patricia.carillon) {
                            if (Lancelot.this.snappish.friction.crumbier()) {
                                return 0;
                            }

                            return 2;
                        }

                        if (Lancelot.this.usurious().outhouse(var1) != 2) {
                            return Suleiman.purloins(Lancelot.this.usurious().outhouse(var1));
                        }
                        break;
                    default:
                        return 2;
                }

                return var3 && Lancelot.this.geologic() ? 3 : 2;
            }
        }

        public final boolean conveyer() {
            return false;
        }

        public final boolean marauded() {
            return (Lancelot.this.snappish.friction.marauded() || Angelita.bindings("sF)R3377", false)) && !Lancelot.this.usurious().marauded();
        }

        public final void cornered(Patricia var1, int var2, boolean var3) {
            if (var1 != Patricia.carillon && (var2 & 3) == 2) {
                Lancelot.this.tautness(var1);
            } else {
                Frisbees var4;
                if ((var4 = Lancelot.this.snappish.sturgeon()) == null) {
                    if (Lancelot.this.geologic() && var3) {
                        Jezebels var5;
                        if ((var5 = Lancelot.this.skinhead.unfrocks(var1, Lancelot.this.usurious().outhouse(var1) == 2)) == null) {
                            Redgrave.kneecaps();
                            return;
                        }

                        Lancelot.this.branding(var5);
                    }

                } else if (var4 == Frisbees.cajoling && Lancelot.this.liveable != null) {
                    if (Lancelot.this.canoeist == null) {
                        Lancelot.this.canoeist = new ArrayList();
                    }

                    Lancelot.this.liveable.cornered(var1, var2, var3);
                    Lancelot.this.snappish.textural(Lancelot.this.canoeist);
                    Lancelot.this.canoeist = null;
                    if (Lancelot.this.sheepdog.pronouns() == 2) {
                        Lancelot.this.snappish.averring((double) Lancelot.this.usurious().fruition(1), (double) Lancelot.this.usurious().fruition(0));
                    }

                } else if (var1 == Patricia.carillon || this.cohering(var1, var2, var3) != 2) {
                    if (var4 == Frisbees.utensils) {
                        if (var1 == Patricia.carillon) {
                            Lancelot.this.snappish.lessened();
                        } else if (var3) {
                            Lancelot.this.snappish.shipmate(var1, false);
                        } else if ((Lancelot.this.smuggest().returned(var1) & 128) != 0 && !Lancelot.labeling) {
                            Lancelot.fifteens(true);
                            new Winifred(Redgrave.buffered(-903340709), Redgrave.buffered(-903340540), 1, Lancelot.this);
                        } else {
                            Lancelot.this.snappish.shipmate(var1, true);
                        }
                    } else {
                        if (var4 == Frisbees.lipreads) {
                            Lancelot.this.snappish.tautness(var1);
                        }

                    }
                }
            }
        }
    };
    private boolean closed = false;

    public void setKibitzMarker(Patricia p) {
        smuggest().setKibitzMarker(p);
    }

    public void removeKibitzMarker(Patricia p) {
        smuggest().removeKibitzMarker(p);
    }

    public Lancelot(Cossacks var1, Peruvian var2, final JFrame var3) {
        this.snappish = var2;
        this.serenely = var1;
        this.portered = var3;
        if (Angelita.bindings(":14_`9Lf", true)) {
            this.informal = new JButton(new AbstractAction(Redgrave.buffered(-903340783)) {
                public final void actionPerformed(ActionEvent var1) {
                    var3.toFront();
                }
            });
            gangrene.add(this.informal);
        } else {
            this.informal = null;
        }
    }

    public void takeover(Cambodia he1, LayoutManager2 layoutmanager2) {
        super.takeover(he1, layoutmanager2); 
        if (snappish != null) {
            snappish.setClocks(vocalize(0).disallow(), vocalize(1).disallow());
        }
        if (serenely.autoobserving == snappish) {
            StringBuffer str = new StringBuffer(perturbs.getText());
            str.append(" (auto)");
            perturbs.setText(str.toString());
        }
    }


    protected final void lavender() {
        this.skinhead = new Tantalus(this.snappish.mannerly());
        takeover(this.skinhead, new Slavonic(this));
        this.maximize = new Monmouth(this.serenely, this.snappish.cucumber().values());
        this.mustiest = new Salinger(this.serenely, this.snappish, this);
        this.snappish.metrical(this.papillae);
        boolean var2 = Redgrave.marauded();
        ArrayList var3;
        (var3 = new ArrayList()).add(this.barrette);
        if (var2) {
            var3.add(this.fieriest);
        }

        var3.add((Object) null);
        this.minerals = new JMenuItem(Redgrave.buffered(-903340757));
        this.minerals.addActionListener(this);
        var3.add(this.minerals);
        this.tollgate = new JMenuItem(Redgrave.buffered(-903340612));
        this.tollgate.addActionListener(this);
        var3.add(this.tollgate);
        this.panthers = new JMenuItem(Redgrave.buffered(-903340704));
        this.panthers.addActionListener(this);
        var3.add(this.panthers);
        if (var2) {
            this.licensee = new JMenuItem(Redgrave.buffered(-903340570));
            this.licensee.addActionListener(this);
            var3.add(this.licensee);
        }

        this.cockades = new Paterson(Redgrave.buffered(-903340550), this.serenely, -5, new Aberdeen() {
            public final void thrummed(Gonzalez var1) {
                Lancelot.buffoons(Lancelot.this, (Beerbohm) var1.thankful);
            }
        }, 0, (Object) null);
        var3.add(this.cockades);
        var3.add((Object) null);
        this.wantings = new JMenuItem(Redgrave.buffered(-903340814));
        this.wantings.addActionListener(this);
        var3.add(this.wantings);
        this.pearling = new JMenuItem(Redgrave.buffered(-903340813));
        this.pearling.addActionListener(this);
        var3.add(this.pearling);
        this.clunkers((JMenuItem[]) var3.toArray(new JMenuItem[0]));
        this.usurious().metrical(this.schooled);
        this.jollying.telethon(this.mustiest);
        this.jollying.jottings(this.snappish);
        this.jollying.vivified(this.snappish.chirping() != null);
        if (this.smuggest().escapade() == null) {
            this.smuggest().prudence(this.localled);
        }

        this.pillowed = new JMenuItem(Redgrave.buffered(-903340717));
        this.pillowed.setEnabled(false);
        this.fiercely = new JMenuItem(Redgrave.buffered(-903340817));
        this.fiercely.setEnabled(false);
        this.mortuary = new JMenuItem(Redgrave.buffered(-809883781));
        this.mortuary.addActionListener(this);
        this.kidnaped();
        this.agronomy();
        this.maximize.wireless(this.pillowed);
        this.pillowed.addActionListener(this);
        if (this.snappish.friction.narwhals(this.snappish.chirping()) && this.snappish.friction.argosies()) {
            this.maximize.wireless(this.fiercely);
            this.fiercely.addActionListener(this);
        }

        if (!this.snappish.friction.argosies()) {
            String var1 = Redgrave.buffered(-903340781);
            (new Pribilof(this.snappish, (byte) 0, 88, 31, this.beginner(), "Before")).setToolTipText(var1);
            (new Pribilof(this.snappish, (byte) 1, 88, 31, this.beginner(), "After")).setToolTipText(var1);
        }

        this.perusing(this.eatables() || this.clippers() && this.snappish.skeletal(Frisbees.cajoling) == null);
        this.vocalize(0).insureds(this.snappish.chirping() != null);
        this.vocalize(1).insureds(this.snappish.chirping() != null);
        this.secretly();
        this.citadels();
    }

    protected final JComponent sanctums() {
        JPanel var1;
        (var1 = new JPanel(new BorderLayout())).setOpaque(false);
        return var1;
    }

    public final void actionPerformed(ActionEvent var1) {
        Object var2;
        if ((var2 = var1.getSource()) == this.snipping) {
            if (this.diatribe != null) {
                this.diatribe.levitate();
            }

            String[] var4 = new String[]{Redgrave.buffered(1436228510), daylight};
            this.diatribe.unsalted(new Winifred(Redgrave.buffered(-903340560), Redgrave.buffered(-903340622), 3, this, var4, this));
        } else if (var2 == this.wantings) {
            this.unbroken('\uea60');
        } else if (var2 == this.pearling) {
            this.unbroken(300000);
        } else if (var2 == this.welcomes) {
            this.snappish.unawares();
        } else {
            if (var2 == this.diatribe.tackling()) {
                this.diatribe.levitate();
                if (var1.getActionCommand().equals(daylight)) {
                    this.snappish.floating();
                    return;
                }
            } else {
                if (var2 == this.pillowed) {
                    Gingrich var3;
                    if ((var3 = this.maximize.quibbled()) != null) {
                        this.snappish.callable(var3.geologic, Scottish.watchdog);
                    }

                    return;
                }

                if (var2 == this.fiercely) {
                    this.snappish.obscures(this.maximize.quibbled().geologic, true);
                    return;
                }

                if (var2 == this.mortuary) {
                    (new Helsinki(this.snappish, (JFrame) this.getTopLevelAncestor())).setVisible(true);
                    return;
                }

                if (var2 == this.dickiest.tackling()) {
                    this.dickiest.levitate();
                    if (var1.getActionCommand().equals(Redgrave.buffered(1436228518))) {
                        this.snappish.dossiers();
                        return;
                    }

                    if (var1.getActionCommand().equals(Redgrave.buffered(-809883749))) {
                        this.alluding = -1;
                        return;
                    }
                } else {
                    if (var2 == this.tollgate) {
                        Cyrillic.unclothe(this.serenely).buffeted(this, this.snappish.shiftily(), this.snappish.mannerly());
                        return;
                    }

                    if (var2 == this.panthers) {
                        if (this.decorate != null) {
                            this.decorate.dispose();
                        }

                        this.decorate = new Comanche(this.serenely, this.snappish, this);
                        return;
                    }

                    if (var2 == this.licensee) {
                        new Corvette(1096335315, (Component) null, new Filipino(new Nembutal(new Cambodia(this.skinhead))));
                        return;
                    }

                    if (var2 == this.minerals) {
                        this.snappish.trouping();
                        return;
                    }

                    if (var2 == this.stubborn) {
                        this.snappish.callable(this.serenely.provisos().geologic, Scottish.watchdog);
                        return;
                    }

                    if (var2 == this.breakups) {
                        this.snappish.cuddlier(new String[]{this.snappish.shiftily().spawning(Morrison.purulent).geologic, this.snappish.shiftily().spawning(Morrison.monarchy).geologic}, new Scottish[]{Morrison.purulent, Morrison.monarchy});
                        return;
                    }

                    if (var2 == this.impugned) {
                        this.snappish.improves(this.impugned.isSelected() ? (this.serenely.provisos().vehement() == 2 ? 2 : 1) : 0);
                        return;
                    }

                    if (var2 == this.opulence) {
                        this.snappish.callable(this.embolism, Scottish.watchdog);
                        return;
                    }

                    if (var2 == this.ruptures || var2 == this.groupies) {
                        this.serenely.gorgeous(this.snappish.shiftily().smacking, this.ruptures.getText());
                        this.ruptures.transferFocus();
                        return;
                    }

                    super.actionPerformed(var1);
                }
            }

        }
    }

    private boolean eatables() {
        Frisbees var1;
        return (var1 = this.snappish.sturgeon()) == Frisbees.cajoling || var1 == Frisbees.dipstick;
    }

    public final void kneecaps() {
        if (closed)
			return;
        if (this.snappish.chirping() != null && !this.snappish.oracling() && !this.snappish.friction.argosies()) {
            this.sparrows.unsalted(new Winifred(Redgrave.buffered(-903340547), Redgrave.buffered(this.usurious().marauded() && !this.snappish.oracling() ? -903340740 : -903340741), 2, this, new String[]{Redgrave.buffered(-903340654), Redgrave.buffered(-903340623), Redgrave.buffered(-903340801)}, new ActionListener() {
                public final void actionPerformed(ActionEvent var1) {
                    if (var1.getActionCommand().equals(Redgrave.buffered(-903340654))) {
                        if (!Lancelot.this.snappish.backhoes() && Lancelot.this.snappish.friction.narwhals(Lancelot.this.snappish.chirping())) {
                            Lancelot.this.rowdiest();
                        } else {
                            Lancelot.this.snappish.dripping();
                        }
                    } else {
                        if (var1.getActionCommand().equals(Redgrave.buffered(-903340623))) {
                            Lancelot.this.snappish.floating();
                            Lancelot.this.snappish.dripping();
                        }

                    }
                }
            }));
        } else if (this.snappish.friction.narwhals(this.snappish.chirping()) && !this.snappish.backhoes() && !this.snappish.sweaters()) {
            this.rowdiest();
        } else {
            this.snappish.dripping();
        }
    }

    private void rowdiest() {
        this.sparrows.unsalted(new Winifred(Redgrave.buffered(-903340533), Redgrave.buffered(-903340534), 3, this, new String[]{Redgrave.buffered(1436228521), Redgrave.buffered(1436228522), Redgrave.buffered(720104027)}, new ActionListener() {
            public final void actionPerformed(ActionEvent var1) {
                if (var1.getActionCommand().equals(Redgrave.buffered(1436228521))) {
                    Lancelot.this.snappish.baronets();
                    Lancelot.this.snappish.dripping();
                } else {
                    if (var1.getActionCommand().equals(Redgrave.buffered(1436228522))) {
                        Lancelot.this.snappish.dripping();
                    }

                }
            }
        }));
    }

    private void unbroken(int var1) {
        this.snappish.panelled(Morrison.plectrum(this.snappish.chirping()), var1);
    }

    private void twitches() {
        boolean var1 = this.eatables();
        if (this.orations != null) {
            this.orations.setEnabled(var1);
            this.rebirths.setEnabled(var1);
        }

        if (this.breakups != null) {
            this.breakups.setEnabled(this.sheepdog != null && (!this.usurious().marauded() || !this.snappish.oracling()));
        }

        for (int var2 = 7; var2 >= 0; --var2) {
            this.sheepdog.haddocks(var2).setEnabled(var1);
        }

        Bastille var3;
        if ((var3 = this.flippest()) != null && var3 instanceof Potemkin) {
            ((Potemkin) var3).vivified(this.eatables());
        }

    }

    public final void branding(Jezebels var1) {
        if (this.snappish.mannerly() != null) {
            if (var1 != this.skinhead.spanners()) {
                if (this.clippers()) {
                    if (var1.yodelers != this.snappish.mannerly().spanners().yodelers && var1.clappers() != 0) {
                        this.skinhead.vivified(true);
                        this.skinhead.branding(var1);
                    } else {
                        this.skinhead.vivified(false);
                    }
                } else {
                    if (this.eatables()) {
                        ArrayList var2;
                        (var2 = this.canoeist == null ? new ArrayList() : this.canoeist).add(new Tientsin(var1.yodelers, 7));
                        if (this.canoeist == null) {
                            this.snappish.textural(var2);
                        }
                    }

                }
            }
        }
    }

    protected final void laboring(Geronimo var1, Cambodia var2) {
        this.jollying = new Golgotha(this.snappish, var1, var2);
        this.jollying.lobbyist(0, this.vocalize(0).disallow());
        this.jollying.lobbyist(1, this.vocalize(1).disallow());
    }

    protected final void thrummed(Gonzalez var1) {
        Gingrich var9;
        switch (var1.venereal) {
            case 12:
            case 13:
            case 14:
                Cyrillic.endorser(var1, this);
                return;
            // change to review?
            case 15:
                if (var1.thankful == this.strikers) {
                    Lancelot var8 = new Lancelot(this.serenely, this.strikers, this.portered);
                    this.strikers.warlords(this.papillae);
                    var8.lavender();

                    Object var5;
                    for (var5 = this; !(var5 instanceof Corvette); var5 = ((Container) var5).getParent()) {
                        if (var5 == null) {
                            return;
                        }
                    }

                    ((Corvette) var5).bemusing(this, var8);
                    if (this.ruptures != null) {
                        this.ruptures.setText(this.serenely.properly(this.strikers.shiftily().smacking));
                    }
                    if (serenely.autoobserving == snappish) {
                        // We need to close the new review game,
                        // not the original one; otherwise, server
                        // will not notice we left the review and
                        // we get stuck there together.
                        // TODO: Set up timer
                        serenely.autoobserving = var8.lankiest();
                        var8.kneecaps();
                    }
                    return;
                }
                break;
            case 16:
                // window closes
                if (snappish == serenely.autoobserving)
                    serenely.autoobserve_next();
                closed = true;
                this.aperture(2);
                return;
            case 17:
            case 18:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            case 41:
            case 43:
            case 44:
            case 45:
            case 46:
            case 47:
            case 48:
            case 49:
            case 50:
            case 51:
            case 52:
            case 53:
            case 54:
            case 55:
            case 56:
            case 57:
            case 58:
            case 59:
            case 69:
            case 70:
            case 71:
            case 72:
            case 73:
            case 74:
            case 75:
            case 76:
            case 77:
            case 78:
            case 79:
            case 80:
            case 81:
            case 82:
            case 83:
            case 84:
            case 85:
            case 86:
            case 87:
            case 88:
            case 89:
            case 90:
            case 91:
            case 92:
            case 93:
            case 94:
            case 95:
            case 96:
            default:
                break;
            case 42:
                // Game not over yet and not review etc. and not game start (then this pops up too, which is bad)
                if (!snappish.virtuosi() && (snappish.friction.overdoes >= 4 || serenely.autoobserving == snappish) && snappish.pronouns() > 0) {
                    // review game has own message, but we want to auto-close even reviews etc.
/*
                    Winifred resultinfo = new Winifred(Redgrave.buffered(0xca281d5b), "One of the players has left the game.", 1, this); // KGS: Information
                    if (serenely.autoobserving == snappish)
                        new gameCloseTimer(this, resultinfo);
*/
                    if (serenely.autoobserving == snappish)
                        new gameCloseTimer(this);
                }
                break;
            // observer parts
            case 19:
                var9 = (Gingrich) var1.straddle;
                if (this.opulence != null && var9.geologic.equals(this.embolism)) {
                    this.opulence.setEnabled(this.virtuosi());
                }

                this.maximize.recouped(var9);
                return;
            // observer joins
            case 20:
                var9 = (Gingrich) var1.straddle;
                this.maximize.upgrades(var9);
                if (this.opulence != null && var9.geologic.equals(this.embolism)) {
                    this.opulence.setEnabled(this.virtuosi());
                }

                Scottish var10;
                if ((var10 = this.snappish.alacrity(var9.geologic)) == null || this.snappish.friction.marauded() || !this.snappish.friction.satirize(var10) || var10.parroted == -1 || !this.snappish.friction.satirize(this.snappish.chirping()) || this.serenely.provisos().equals(var9) || this.snappish.oracling()) {
                    if (this.snappish.loyalist(var9) == Scottish.watchdog) {
                        new Winifred(Redgrave.buffered(-903340709), Redgrave.buffered(-903340620), 1, this);
                    }

                    return;
                }

                this.lyricist = new Winifred(Redgrave.buffered(-903340547), Redgrave.synonyms(-903340652, var9.geologic), 2, this);
                break;
            case 27:
                this.mustiest.leaguing(Redgrave.buffered(-903340609) + '\n', 2, this.mustiest.immature(),this.mustiest.currentTurn() ); // (Normal chat mode)
                return;
            case 28:
                JFrame var7;
                if ((var7 = (JFrame) this.getTopLevelAncestor()) != null) {
                    var7.toFront();
                }

                return;
            case 39:
                this.township();
                break;
            case 40:
                this.citadels();
                return;
            case 60:
                this.fiercely.setEnabled(this.snappish.reunites() != 0);
                if (this.impugned != null) {
                    this.impugned.setSelected(((Integer) var1.straddle).intValue() != 0);
                }

                this.mustiest.leaguing(Redgrave.buffered(711957008 + this.snappish.reunites()) + '\n', 2, this.mustiest.immature(), this.mustiest.currentTurn()); // (Chat allowed)
                if (this.snappish.friction.narwhals(this.snappish.chirping())) {
                    if (((Integer) var1.straddle).intValue() == 2) {
                        if (this.relapsed.getParent() == this.glorying()) {
                            this.relapsed.setBorder((Border) null);
                            this.glorying().removeAll();
                            this.glorying().add(Ursuline.feigning(this.serenely, this.relapsed, this.snappish));
                            break;
                        }
                    } else if (this.relapsed.getParent() != this.glorying()) {
                        this.glorying().removeAll();
                        this.glorying().add(this.relapsed);
                        this.relapsed.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
                    }

                    return;
                }
                break;
            case 61:
                if (this.alluding >= 0) {
                    Scottish var10001 = (Scottish) var1.straddle;
                    boolean var12 = (this.alluding += 1) >= 3;
                    Scottish var6 = var10001;
                    if (!this.dickiest.recounts()) {
                        String[] var4;
                        (var4 = new String[var12 ? 3 : 2])[0] = Redgrave.buffered(1436228518);
                        var4[1] = Redgrave.buffered(1436228522);
                        if (var12) {
                            var4[2] = Redgrave.buffered(-809883749);
                        }

                        this.dickiest.unsalted(new Winifred(Redgrave.buffered(-903340561), Redgrave.synonyms(-903340578, this.snappish.crucible(var6).geologic), 3, this, var4, this));
                        this.jollying.unsalted(this.dickiest.tackling());
                    }

                    return;
                }
                break;
            case 62:
                this.strikers = (Peruvian) var1.straddle;
                this.strikers.metrical(this.papillae);
                return;
            case 63:
                Scottish var11;
                if ((var11 = this.snappish.chirping()) == null || !this.snappish.friction.satirize(var11)) {
                    new Winifred(Redgrave.buffered(-903340709), Redgrave.buffered(-903340656), 1, this);
                }

                return;
            case 64:
                new Smolensk(Redgrave.synonyms(-903340664, var1.straddle));
                return;
            case 65:
                short var2 = ((Short) var1.straddle).shortValue();
                String[] var3 = this.snappish.graduate(var2);
                if (ClockTalker.shouldTalk(snappish.chirping() != null))
				    vocalize(var2 <= 0 ? 0 : 1).disallow().timeOut();
                StringBuffer closemsg = new StringBuffer("");
                if (serenely.autoobserving == snappish)
                    closemsg.append("<br>Do not close this window if you want me<br>just to move on to another game in ")
                            .append(Integer.toString(Angelita.bassinet("autoobserve_delay", 15)))
                            .append("s.");
                if (var3.length != 1) {
                    StringBuffer sb = new StringBuffer();
                    sb.append("<html>").append(var3[0]).append("<br>&nbsp;&nbsp;")
                            .append(var3[1]).append("<br>&nbsp;&nbsp;").append(var3[2])
                            .append("<br>").append(var3[3]).append(closemsg).append("</html>");
                    Winifred resultinfo = new Winifred(Redgrave.buffered(-903340709), new JLabel(sb.toString()), 1, this); // KGS: Information
                    //what the heck? duplicate game-over-info popup?
                    //new Winifred(Redgrave.buffered(-903340709), new JLabel("<html>" + var3[0] + "<br>&nbsp;&nbsp;" + var3[1] + "<br>&nbsp;&nbsp;" + var3[2] + "<br>" + var3[3]), 1, this);

		    if (serenely.autoobserving == snappish)
                        new gameCloseTimer(this, resultinfo);
                    return;
                }

                //StringBuffer sb = new StringBuffer("<html>").append(var3[0]).append(closemsg).append("</html>");
                StringBuffer sb = new StringBuffer().append(var3[0]).append(closemsg);
                Winifred resultinfo = new Winifred(Redgrave.buffered(-903340709), sb.toString(), 1, this);
                if (serenely.autoobserving == snappish)
		    new gameCloseTimer(this, resultinfo);
                break;
            case 66:
                new Smolensk(Redgrave.synonyms(-903340658, var1.straddle));
                return;
            case 67:
                if (this.snappish.friction.marauded() && this.snappish.friction.satirize(this.snappish.chirping())) {
                    new Winifred(Redgrave.buffered(-903340709), Redgrave.buffered(-903340701), 1, this);
                    return;
                }
                break;
            case 68:
                if (!this.snappish.friction.marauded()) {
                    new Matthews((JFrame) this.getTopLevelAncestor(), ((Integer) var1.straddle).intValue());
                    return;
                }
                break;
            case 97:
                Cyrillic.neighbor(Redgrave.buffered(2031923640), 13, this);
                return;
            case 98:
                return;
        }

    }

    protected final void smirched() {
        boolean var1 = !this.snappish.friction.argosies() && this.snappish.chirping() != null && !this.snappish.oracling();
        if (this.fieriest != null) {
            this.fieriest.setEnabled(whitened.insomnia(this.usurious()) && !var1);
            this.licensee.setEnabled(!var1);
        }

        this.minerals.setEnabled(this.snappish.oracling() && this.snappish.chirping() != null && !this.snappish.friction.argosies());
        this.tollgate.setEnabled(Redgrave.marauded());
        this.panthers.setEnabled(this.snappish.encoring() && this.serenely.arboreal(this.snappish) != null && !this.serenely.arboreal(this.snappish).encoring() && this.snappish.shellack(this.serenely.provisos()));
        this.cockades.setEnabled(!this.snappish.friction.satirize(this.snappish.chirping()) && !this.snappish.oracling());
        var1 = !this.snappish.friction.marauded() && this.snappish.chirping() != null && this.snappish.chirping().parroted != -1 && !this.snappish.oracling();
        this.wantings.setEnabled(var1);
        this.pearling.setEnabled(var1);
    }

    protected final Alvarado corsairs(int var1, String var2, int var3) {
        Eichmann var4;
        if ((var4 = this.snappish.sleazier(Morrison.provider(var1))) == null) {
            var4 = this.snappish.blurrier().smashing();
        }

        Reinaldo var5;
        (var5 = new Reinaldo(var1, this.usurious(), var4, var2, var3)).mortally(this.snappish.friction.argosies());
        Gingrich var6 = this.snappish.crucible(Morrison.provider(var1));
        if (var1 == 1 && var6 == null) {
            var6 = this.snappish.crucible(Morrison.watchdog);
        }

        if (var6 != null && var6.colonels() && (var1 != 0 || var6 != this.snappish.crucible(Morrison.purulent))) {
            var5.besought(var6, Cyrillic.unclothe(this.serenely));
        }

        return var5;
    }

    public final Peruvian lankiest() {
        return this.snappish;
    }

    protected final void tautness(Patricia var1) {
        Seleucus var2;
        (var2 = this.mustiest.snazzier()).setText(var2.getText() + var1.bigamist(this.usurious().yodelers) + ' ');
    }

    protected final String guzzling() {
        return "gameWin.html";
    }

    private boolean clippers() {
        return !this.snappish.friction.argosies() && !this.snappish.friction.satirize(this.snappish.chirping());
    }

    protected final Bastille begrudge() {
        Object var2;
        if (this.snappish.friction.argosies()) {
            Potemkin var1;
            (var1 = new Potemkin(this.briskets(), this, this, false)).vivified(this.eatables());
            var2 = var1;
        } else {
            var2 = new Valletta(this.snappish, this.usurious().diverted(), this.skinhead, this);
        }

        return (Bastille) var2;
    }

    protected final void abruptly(ArrayList var1) {
        this.welcomes = new JButton(Redgrave.buffered(-1337055794));
        this.welcomes.addActionListener(this);
        this.snipping = new JButton(Redgrave.buffered(-903340623));
        this.snipping.addActionListener(this);
        this.emceeing = Hezekiah.redeemer(this.serenely, this.snappish);
        var1.add(this.unmasked);
        var1.add(this.disposal);
        var1.add(this.smuggest().glinting);
        var1.add(this.welcomes);
        var1.add(this.snipping);
        var1.add(this.perturbs);
        if (!this.serenely.provisos().marauded()) {
            JPanel var2 = new JPanel(new BorderLayout());
            this.ruptures = new Seleucus("", 1, this);
            this.ruptures.setDocument(new Lilliput(50));
            this.ruptures.setText(this.serenely.properly(this.snappish.shiftily().smacking));
            String var3 = Redgrave.buffered(-809883782);
            this.ruptures.setToolTipText(var3);
            var2.add("Center", this.ruptures);
            this.groupies = new JButton(new Jacobean());
            this.groupies.addActionListener(this);
            this.groupies.setToolTipText(var3);
            var2.add("East", this.groupies);
            var1.add(var2);
        }

        if (this.emceeing != null) {
            var1.add(this.emceeing);
        }

        super.abruptly(var1);
    }

    public final void scrutiny(Jezebels var1, Antonius var2) {
        ArrayList var3 = this.canoeist == null ? new ArrayList() : this.canoeist;
        if (var2.airliner == 14 || var2.airliner == 17) {
            if (var1 == null) {
                var1 = this.skinhead.spanners();
            }

            if (var2.airliner == 14 || var1 == this.skinhead.poultice || var1.clappers() > 0 || var1.holsters(14) != null) {
                var3.add(new Tientsin(var1 == null ? -1 : var1.yodelers, 5, new int[]{-1, -1}));
                var3.add(new Tientsin(-1, 7));
                var1 = null;
                this.snappish.revealed().clear();
            }
        }

        var3.add(new Tientsin(var1 == null ? -1 : var1.yodelers, 0, var2));
        if (this.canoeist == null) {
            this.snappish.textural(var3);
        }

    }

    public final void culottes(Jezebels var1, Antonius var2) {
        ArrayList var3;
        (var3 = this.canoeist == null ? new ArrayList() : this.canoeist).add(new Tientsin(var1 == null ? -1 : var1.yodelers, 1, var2));
        if (this.canoeist == null) {
            this.snappish.textural(var3);
        }

    }

    public final void calipers(Jezebels var1, boolean var2) {
        ArrayList var3;
        (var3 = this.canoeist == null ? new ArrayList() : this.canoeist).add(new Tientsin(var1 == null ? -1 : var1.yodelers, 5, new int[]{-1, -1}));
        var3.add(new Tientsin(-1, 7));
        if (this.canoeist == null) {
            this.snappish.textural(var3);
        }

    }

    public final String orphaned() {
        return this.snappish != null && (this.snappish.friction == Mahicans.unscrews || this.snappish.friction == Mahicans.promises || this.snappish.friction == Mahicans.impacted || this.snappish.friction == Mahicans.gladlier) ? this.snappish.orphaned() : super.orphaned();
    }

    public final String loveable() {
        if (this.snappish != null && (this.usurious().marauded() || this.snappish.oracling()) && this.snappish.skeletal(Frisbees.cajoling) == null) {
            return this.snappish.oracling() ? Redgrave.synonyms(-903340720, Franklin.jollying().buffered(this.snappish.subsumes())) : (this.snappish.chirping() != null && this.snappish.chirping().parroted != -1 ? (this.snappish.powering(this.snappish.chirping().parroted) ? Redgrave.buffered(-903340549) : (this.snappish.powering(Suleiman.purloins(this.snappish.chirping().parroted)) ? Redgrave.buffered(-903340645) : Redgrave.buffered(-903340624))) : Redgrave.buffered(-903340624));
        } else {
            if (this.snappish != null && (this.snappish.friction == Mahicans.impacted || this.snappish.friction == Mahicans.gladlier)) {
                Gingrich var1;
                if (this.snappish.friction == Mahicans.impacted) {
                    var1 = this.snappish.skeletal(Frisbees.lipreads);
                } else {
                    int var4 = this.usurious().comedian();
                    Rules var2;
                    if ((var2 = this.usurious().diverted()).mantissa()) {
                        if (var2.improper() > 0) {
                            ++var4;
                        }
                    } else if (var4 <= var2.improper()) {
                        var4 <<= 1;
                    } else if (var2.improper() > 0 && ((var4 = (var4 -= var2.improper() - 1) & 3) == 0 || var4 == 2 && (var2.improper() & 1) == 0)) {
                        var4 ^= 2;
                    }

                    Morrison var5 = null;
                    switch (var4 & 3) {
                        case 0:
                            var5 = Morrison.monarchy;
                            break;
                        case 1:
                            var5 = Morrison.purulent;
                            break;
                        case 2:
                            var5 = Morrison.scampies;
                            break;
                        case 3:
                            var5 = Morrison.lawmaker;
                    }

                    var1 = this.snappish.shiftily().spawning(var5);
                }

                if (var1 != null) {
                    return Redgrave.assented(-809883777, new Object[]{new Integer(this.usurious().comedian()), this.usurious().orphaned(), new Integer(this.usurious().clappers()), var1.geologic});
                }
            } else if (this.snappish != null) {
                Morpheus var10000 = this.snappish.friction;
                Mahicans var3 = Mahicans.gladlier;
            }

            return super.loveable();
        }
    }

    public final boolean password() {
        return this.snappish != null && this.usurious().marauded() && !this.snappish.oracling() && this.snappish.chirping() != null && this.snappish.chirping().parroted >= 0 && this.snappish.powering(Suleiman.purloins(this.snappish.chirping().parroted)) && !this.snappish.powering(this.snappish.chirping().parroted);
    }

    private void kidnaped() {
        this.snipping.setEnabled(!this.snappish.oracling() && this.snappish.friction.crumbier() && this.snappish.chirping() != null && this.snappish.skeletal(Frisbees.cajoling) == null);
        this.agronomy();
        if (this.snappish.friction.argosies()) {
            final Lancelot var1 = this;
            if (this.sheepdog == null) {
                while (var1.beginner().getComponentCount() > 1) {
                    var1.beginner().remove(1);
                }

                var1.sheepdog = new Saunders(var1) {
                    protected final Norseman wimpiest(int var1x) {
                        var1.liveable = super.wimpiest(var1x);
                        return var1.localled;
                    }
                };
                var1.sheepdog.metrical(new Aberdeen() {
                    public final void thrummed(Gonzalez var1x) {
                        Lancelot.chariots(var1, var1x);
                    }
                });
                var1.tumbrels = new Scorpios();
                var1.tumbrels.powering(var1.skinhead.spanners().yodelers);
                ArrayList var2 = new ArrayList();
                var1.orations = Lakeisha.circling(var1);
                var1.orations.setEnabled(var1.eatables());
                var2.add(var1.orations);
                var1.rebirths = Thursday.circling(var1);
                var1.rebirths.setEnabled(var1.eatables());
                var2.add(var1.rebirths);
                var2.add((Object) null);
                var1.embolism = var1.snappish.crucible(var1.snappish.friction.querying).geologic;
                if (var1.snappish.friction.narwhals(var1.snappish.chirping())) {
                    var1.stubborn = new JMenuItem(Redgrave.buffered(-903340587));
                    var1.stubborn.setAccelerator(KeyStroke.getKeyStroke(84, 2));
                    var1.stubborn.addActionListener(var1);
                    var2.add(var1.stubborn);
                    Gingrich var3;
                    if ((var3 = var1.snappish.crucible(Morrison.purulent)) == var1.serenely.provisos()) {
                        var3 = var1.snappish.crucible(Morrison.monarchy);
                    }

                    if (var3 != null) {
                        var1.embolism = var3.geologic;
                    }
                }

                if (var1.embolism == null) {
                    var1.embolism = var1.serenely.provisos().geologic;
                }

                var1.opulence = new JMenuItem(Redgrave.synonyms(-903340716, var1.embolism));
                if (var1.snappish.friction == Mahicans.costings && var1.snappish.friction.narwhals(var1.snappish.chirping())) {
                    var1.breakups = new JMenuItem(Redgrave.buffered(-903340619));
                    var1.breakups.addActionListener(var1);
                    var2.add(var1.breakups);
                }

                var1.opulence.setAccelerator(KeyStroke.getKeyStroke(71, 2));
                var1.opulence.addActionListener(var1);
                var1.opulence.setEnabled(false);
                var2.add(var1.opulence);
                if (var1.snappish.friction.narwhals(var1.snappish.chirping())) {
                    var2.add(var1.mortuary);
                    var2.add((Object) null);
                    var1.impugned = new JCheckBoxMenuItem(Redgrave.buffered(-903340514), false);
                    var1.impugned.addActionListener(var1);
                    var1.impugned.setAccelerator(KeyStroke.getKeyStroke(77, 2));
                    var2.add(var1.impugned);
                }

                var1.sheepdog.whithers((JComponent[]) var2.toArray(new JComponent[0]));
                var1.sheepdog.smarting();
                var1.twitches();
                var1.pillowed.setEnabled(var1.snappish.friction.narwhals(var1.snappish.chirping()));
                var1.sheepdog.improves(0);
            }
        }

    }

    protected final void agronomy() {
        boolean var1;
        if (this.snappish.sturgeon() == Frisbees.lipreads) {
            var1 = this.usurious().marauded() || !this.snappish.friction.marauded() && this.usurious().locution(this.usurious().clappers());
        } else {
            Scottish var2 = this.snappish.chirping();
            var1 = !this.snappish.oracling() && this.snappish.skeletal(Frisbees.cajoling) == null && (var2 == Morrison.monarchy || var2 == Morrison.scampies || var2 == Morrison.purulent || var2 == Morrison.lawmaker) && this.usurious().locution(var2.parroted) && (this.usurious().marauded() || !this.snappish.friction.marauded());
        }

        this.welcomes.setEnabled(var1);
    }

    public final void removeNotify() {
        gangrene.remove(this.informal);
        this.snappish.warlords(this.papillae);
        this.usurious().warlords(this.schooled);
        super.removeNotify();
    }

    private void secretly() {
        this.smuggest().setEnabled(true);
        this.smuggest().glinting.setText(this.snappish.sturgeon() == Frisbees.utensils ? Redgrave.buffered(-903340760) : Redgrave.buffered(-1337055796));
        Jezebels var1 = this.skinhead.poultice;

        while (true) {
            Antonius var2 = var1.holsters(24);
            boolean var3 = var1 == this.skinhead.spanners();
            if (var2 != null) {
                //System.err.println("hoha/"+var2.keenness());
                this.mustiest.startles(var2.keenness(), var3 ? this.mustiest.immature() : var1.equators(), (Jezebels) (var1));
            }

            if (var3) {
                Object var6;
                if ((var6 = this.serenely.panderer.remove(cocktail)) == null) {
                    this.jollying.insureds(true);
                    return;
                }

                ArrayList var7;
                Iterator var8 = (var7 = ((Cambodia) var6).bastions()).iterator();

                while (var8.hasNext()) {
                    Tientsin var4;
                    Antonius var5;
                    if ((var4 = (Tientsin) var8.next()).venereal == 0 && (var5 = (Antonius) var4.straddle).marauded()) {
                        var8.remove();
                    }
                }

                this.snappish.textural(var7);
                this.serenely.biplanes(new Runnable() {
                    public final void run() {
                        Lancelot.this.jollying.insureds(true);
                    }
                });
                return;
            }

            var1 = var1.vitamins();
        }
    }

    protected final void vivified(boolean var1) {
        super.vivified(var1);
        if (this.relapsed == null) {
            this.relapsed = new JPanel();
            this.relapsed.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
            this.glorying().add(this.relapsed);
        }

        this.relapsed.removeAll();
        this.relapsed.setLayout(new Berenice());
        if (var1) {
            this.relapsed.add("xGrow=t,yGrow=t" + (this.informal == null ? "" : ",xSpan=2"), this.mustiest);
        } else {
            this.relapsed.add("xGrow=t,yGrow=t", this.mustiest);
            this.relapsed.add("xGrow=f" + (this.informal == null ? ",ySpan=2" : ""), this.maximize);
        }

        this.relapsed.add("x=0,xGrow=t,yGrow=f,xSpan=1,ySpan=1", this.mustiest.snazzier());
        if (this.informal != null) {
            this.relapsed.add("xGrow=f", this.informal);
        }

    }

    protected final void cockades(JPanel var1, ArrayList var2, boolean var3) {
        super.cockades(var1, var2, false);
        if (var3) {
            var1.add("x=1,y=0,xGrow=f,ySpan=" + var2.size(), this.maximize);
        }

    }

    protected final boolean powering(int var1) {
        if (this.usurious().marauded()) {
            return false;
        } else {
            Antonius var2 = new Antonius(26, var1);
            Jezebels var3;
            if ((var3 = this.skinhead.spanners()).holsters(26) == null && var3.terriers(var2)) {
                this.usurious().foretold(var1);
            } else {
                var2 = new Antonius(26, var1);
                this.scrutiny((Jezebels) null, var2);
            }

            return true;
        }
    }

    public boolean oneColor() {
        return snappish.oneColor();
    }

    public int blindGo() {
        return snappish.blindGo();
    }

    protected final void insureds(boolean var1) {
    }

    protected final void tailpipe(Gonzalez var1) {
        if (var1.venereal == 7 && this.tumbrels != null) {
            Jezebels var2 = this.skinhead.spanners();
            Antonius var3;
            if (this.tumbrels.powering(var2.yodelers) && (var3 = var2.holsters(24)) != null) {
                this.mustiest.startles(var3.keenness(), 2, this.mustiest.immature(), var2);
            }
        }

        super.tailpipe(var1);
    }

    private void citadels() {
        this.smuggest().glinting.setText(this.snappish.sturgeon() == Frisbees.utensils ? Redgrave.buffered(-903340760) : Redgrave.buffered(-1337055796));
        if (this.snappish.sturgeon() == Frisbees.utensils && !this.youthful) {
            this.youthful = true;
            new Winifred(Redgrave.buffered(-903340709), Redgrave.buffered(-809883774), 1, this);
        }

        this.kidnaped();
        this.smuggest().slurring();
        this.cetacean();
        this.township();
        Gingrich var1 = this.snappish.skeletal(Frisbees.cajoling);
        this.splashed(var1);
        if (this.deceives != var1) {
            this.deceives = var1;
            boolean var4 = this.eatables();
            boolean var5 = this.snappish.friction.narwhals(this.snappish.chirping());
            if (this.sheepdog != null) {
                this.twitches();
                if (var4) {
                    if (this.sheepdog.pronouns() == -1) {
                        this.sheepdog.improves(this.usurious().marauded() ? 2 : 0);
                    }
                } else if (this.sheepdog.pronouns() != -1 && this.snappish.sturgeon() != Frisbees.dipstick) {
                    this.sheepdog.improves(-1);
                    if (this.breakups != null) {
                        this.breakups.setSelected(true);
                    }
                }

                this.twitches();
            }

            if (var1 != null && !var4 && var5) {
                this.embolism = var1.geologic;
                if (this.opulence != null) {
                    this.opulence.setText(Redgrave.synonyms(-903340716, this.embolism));
                }
            }

            if (this.opulence != null && this.snappish.traffics() && this.embolism != null) {
                this.opulence.setEnabled(this.virtuosi());
            }

            if (this.sheepdog == null || !var4) {
                this.smuggest().prudence(this.localled);
                this.smuggest().setEnabled(true);
            }

            this.perusing(var4 || this.clippers() && var1 == null);
        }

        if (var1 == null) {
            var1 = this.snappish.skeletal(Frisbees.lipreads);
            this.splashed(this.snappish.friction != Mahicans.impacted && var1 != this.snappish.crucible(Morrison.provider(this.usurious().clappers())) ? var1 : null);
        }

        if (this.lyricist != null && this.snappish.oracling()) {
            this.lyricist.dispose();
            this.lyricist = null;
        }

    }

    private boolean virtuosi() {
        return this.snappish.cucumber().containsKey(this.embolism) && !this.embolism.equals(this.serenely.provisos().geologic) && (this.eatables() || this.snappish.friction.narwhals(this.snappish.chirping()));
    }

    private void splashed(Gingrich var1) {
        if (this.calliper != var1) {
            this.calliper = var1;
            if (var1 == null) {
                if (this.schmucks != null) {
                    this.beginner().remove(this.schmucks);
                    this.schmucks = null;
                    return;
                }
            } else {
                if (this.schmucks == null) {
                    this.schmucks = new JLabel();
                    this.beginner().add("After", this.schmucks);
                }

                this.schmucks.setText("<html>" + Redgrave.buffered(-903340486) + "<br>" + var1.geologic + "</html>");
            }

        }
    }

    static Salinger airborne(Lancelot var0) {
        return var0.mustiest;
    }

    static Peruvian sorcerer(Lancelot var0) {
        return var0.snappish;
    }

    static Norseman prissier(Lancelot var0) {
        return var0.liveable;
    }

    static Tantalus serviles(Lancelot var0) {
        return var0.skinhead;
    }

    static ArrayList wantoned(Lancelot var0) {
        return var0.canoeist;
    }

    static ArrayList whirring(Lancelot var0, ArrayList var1) {
        return var0.canoeist = var1;
    }

    static Saunders minibike(Lancelot var0) {
        return var0.sheepdog;
    }

    static boolean subheads() {
        return labeling;
    }

    static boolean fifteens(boolean var0) {
        labeling = true;
        return true;
    }

    static void tumbrels(Lancelot var0) {
        var0.rowdiest();
    }

    static Norseman stalkers(Lancelot var0, Norseman var1) {
        return var0.liveable = var1;
    }

    static Norseman torpedos(Lancelot var0) {
        return var0.localled;
    }

    static void chariots(Lancelot var0, Gonzalez var1) {
        switch (var1.venereal) {
            case 0:
                if (var0.sheepdog.pronouns() == 2) {
                    if (var0.canoeist == null) {
                        var0.canoeist = new ArrayList();
                        var0.sheepdog.bunghole(Patricia.carillon, false);
                        var0.snappish.textural(var0.canoeist);
                        var0.canoeist = null;
                        return;
                    } else {
                        var0.sheepdog.bunghole(Patricia.carillon, false);
                    }
                }
            default:
        }
    }

    static void buffoons(Lancelot var0, Beerbohm var1) {
        var1.strolled(var0.snappish);
    }

    static Golgotha unbeaten(Lancelot var0) {
        return var0.jollying;
    }

}

// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   cgoban

// fc: Arrow (for scrollbars, Resume -> button, etc.)

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import javax.swing.*;

public final class Keewatin
	implements Icon
{

	public Keewatin(int i, int j)
	{
		airliner = i;
		electing.moveTo(0.25F, 0.0F);
		electing.lineTo(-0.25F, 0.5F);
		electing.lineTo(-0.25F, -0.5F);
		electing.closePath();
		electing.transform(new AffineTransform(i * ((j & 1) - (j & j << 1 & 2)), i * ((~j & 1) - ((j ^ 2) & ~j << 1 & 2)), i * (~j & 1), i * (j & 1), (double)i * 0.5D, (double)i * 0.5D));
	}

	public final int getIconHeight()
	{
		return airliner;
	}

	public final int getIconWidth()
	{
		return airliner;
	}

	public final void paintIcon(Component component, Graphics g, int i, int j)
	{
        Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(UIManager.getColor("Label.foreground"));
		g2d.setPaintMode();
		g2d.translate(i, j);
		g2d.fill(electing);
		g2d.translate(-i, -j);
	}

	private final int airliner;
	private final GeneralPath electing = new GeneralPath();
}

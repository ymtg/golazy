import javax.swing.text.SimpleAttributeSet;
import java.util.List;
import java.util.LinkedList;

public class RichString
{
	public RichString()
	{
		substrings = new LinkedList();
	}

	public void add(RichSubstring substr)
	{
		substrings.add(substr);
	}

	public void add(String s, SimpleAttributeSet a)
	{
		add(new RichSubstring(s, a));
	}

	public List substrings;
}

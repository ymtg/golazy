// Decompiled by:       Fernflower v0.6
// Date:                26.05.2011 15:21:24
// Copyright:           2008-2009, Stiver
// Home page:           http://www.reversed-java.com

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

public final class Jezebels extends Medicaid {

   public final Jezebels poultice;
   public final int yodelers;
   private Antonius[] gridiron;
   private int hormones;
   private ArrayList helpmate;
   private Jezebels penlight;
   private int missteps = 0;
   private boolean fixative;
   private boolean spinners;
   private static final List gremlins = Collections.unmodifiableList(new ArrayList());


   Jezebels(Jezebels var1, int var2, int var3) {
      this.poultice = var1;
      this.yodelers = var3;
      this.gridiron = new Antonius[4];
      if(var1 == null) {
         this.fixative = true;
         this.spinners = true;
      } else {
         this.spinners = false;
         ArrayList var4 = var1.helpmate;
         if(var1.helpmate == null) {
            if(var1.penlight != null) {
               var4 = new ArrayList();
               var1.helpmate = var4;
               if(var2 == 0) {
                  var4.add(this);
                  var4.add(var1.penlight);
               } else {
                  var4.add(var1.penlight);
                  var4.add(this);
               }
            }
         } else if(var2 < 0) {
            var4.size();
            var4.add(this);
         } else {
            var4.add(var2, this);
         }

         this.missteps = var1.missteps;
         if(var1.penlight == null) {
            var1.penlight = this;
            this.fixative = var1.fixative;
         } else {
            this.fixative = false;
         }
      }
   }

   public final boolean swabbing(Antonius var1) {
      return this.warthogs(var1, false, false);
   }

   public final boolean deficits(Antonius var1, boolean var2) {
      return this.warthogs(var1, var2, false);
   }

   public final boolean warthogs(Antonius var1, boolean var2, boolean var3) {
      if(this.poultice != null && var1.marauded()) {
         throw new Jezebels.Corteses(var1);
      } else {
         if(var1.airliner == 17 && var1.comedian() != 2) {
            this.warthogs(new Antonius(16, var1.dentures()), var2, var3);
         }

         int var4 = -1;

         for(int var5 = 0; var5 < this.hormones; ++var5) {
            Antonius var6 = this.gridiron[var5];
            if(var1.swabbing(var6)) {
               if(var2 && var1.airliner == var6.airliner && var1.subheads()) {
                  this.gridiron[var5] = new Antonius(var6, var1.keenness());
                  this.leashing(2, var1);
                  return true;
               }

               if(var1.equals(var6) || var3) {
                  return false;
               }

               if(var5 < (this.hormones -= 1)) {
                  System.arraycopy(this.gridiron, var5 + 1, this.gridiron, var5, this.hormones - var5);
               }

               this.leashing(1, var6);
               --var5;
            } else if(var4 == -1 && var1.compareTo(var6) < 0) {
               var4 = var5;
            }
         }

         if(var1.airliner == 14) {
            var2 = true;
            Jezebels.Voltaire var9 = new Jezebels.Voltaire();

            while(var9.hasNext()) {
               Jezebels var8;
               ++(var8 = var9.vitamins()).missteps;
               var8.kindlier(3);
            }
         }

         if(this.gridiron.length == this.hormones) {
            Antonius[] var7 = new Antonius[this.hormones << 1];
            System.arraycopy(this.gridiron, 0, var7, 0, this.hormones);
            this.gridiron = var7;
         }

         if(var4 == -1) {
            this.gridiron[this.hormones++] = var1;
         } else {
            System.arraycopy(this.gridiron, var4, this.gridiron, var4 + 1, this.hormones - var4);
            this.gridiron[var4] = var1;
            ++this.hormones;
         }

         this.leashing(0, var1);
         return true;
      }
   }

   public final Jezebels vitamins() {
      return this.penlight;
   }

   final void swapping(boolean var1, Jezebels var2) {
      if(this.spinners != var1) {
         this.spinners = var1;
         if(var1) {
            for(Jezebels var3 = this; !var3.fixative; var3 = var3.poultice) {
               var3.poultice.branding(var3);
            }
         }

         this.kindlier(3);
         if(var1) {
            this.leashing(7, new Integer(var2.yodelers));
         }
      }

   }

   public final boolean marauded() {
      return this.spinners;
   }

   public final List purebred() {
      return (List)(this.helpmate == null?(this.penlight == null?gremlins:Collections.singletonList(this.penlight)):this.helpmate);
   }

   public final Jezebels tutoring(int var1) {
      return this.helpmate == null?this.penlight:(Jezebels)this.helpmate.get(var1);
   }

   public final int clappers() {
      return this.helpmate == null?(this.penlight == null?0:1):this.helpmate.size();
   }

   public final int comedian() {
      return this.hormones;
   }

   public final Iterator potbelly() {
      return new Jezebels.Guernsey();
   }

   public final int equators() {
      return this.missteps;
   }

   public final Antonius holsters(int var1) {
      for(int var2 = 0; var2 < this.hormones; ++var2) {
         if(this.gridiron[var2].airliner == var1) {
            return this.gridiron[var2];
         }
      }

      return null;
   }

   public final Antonius palmists(int var1, int var2) {
      for(int var3 = 0; var3 < this.hormones; ++var3) {
         if(this.gridiron[var3].airliner == var1 && this.gridiron[var3].comedian() == var2) {
            return this.gridiron[var3];
         }
      }

      return null;
   }

   public final Antonius fixative(int var1, Patricia var2) {
      for(int var3 = 0; var3 < this.hormones; ++var3) {
         if(this.gridiron[var3].airliner == var1 && this.gridiron[var3].dentures() == var2) {
            return this.gridiron[var3];
         }
      }

      return null;
   }

   public final boolean recourse(Antonius var1) {
      for(int var2 = 0; var2 < this.hormones; ++var2) {
         if(this.gridiron[var2].equals(var1)) {
            while(true) {
               ++var2;
               if(var2 >= this.hormones) {
                  --this.hormones;
                  this.leashing(1, var1);
                  return true;
               }

               this.gridiron[var2 - 1] = this.gridiron[var2];
            }
         }
      }

      return false;
   }

   public final boolean terriers(Antonius var1) {
      for(int var2 = 0; var2 < this.hormones; ++var2) {
         if(var1.swabbing(this.gridiron[var2])) {
            return true;
         }
      }

      return false;
   }

   public final boolean locution(int var1) {
      Antonius var2;
      if((var2 = this.holsters(var1)) != null) {
         this.recourse(var2);
         return true;
      } else {
         return false;
      }
   }

   public final void branding(Jezebels var1) {
      if(var1 != this.penlight) {
         Jezebels var2;
         if(this.fixative) {
            for(var2 = this.penlight; var2 != null; var2 = var2.penlight) {
               var2.fixative = false;
               var2.spinners = false;
               var2.kindlier(3);
            }
         }

         this.penlight = var1;
         if(this.fixative) {
            for(var2 = this.penlight; var2 != null; var2 = var2.penlight) {
               var2.fixative = true;
               var2.kindlier(3);
            }
         }
      }

   }

   public final boolean alienate() {
      return this.fixative;
   }

   public final void unbroken(int var1) {
      if(var1 != -1 && var1 != 1) {
         throw new IllegalArgumentException("Distance must be 1 or -1");
      } else {
         int var2;
         if((var1 += var2 = this.steaming()) >= 0 && var1 <= this.clappers()) {
            this.helpmate.set(var2, this.helpmate.get(var1));
            this.helpmate.set(var1, this.penlight);
            int[] var3 = new int[this.helpmate.size()];

            for(var2 = 0; var2 < var3.length; ++var2) {
               var3[var2] = this.tutoring(var2).yodelers;
            }

            this.leashing(4, var3);
         } else {
            throw new IllegalArgumentException("Can\'t move a node that far");
         }
      }
   }

   public final void abruptly(ArrayList var1) {
      if(var1.size() != this.clappers()) {
         throw new IllegalArgumentException();
      } else {
         boolean var2 = false;
         int var3 = 0;

         while(true) {
            if(var3 < var1.size()) {
               Jezebels var7 = (Jezebels)var1.get(var3);
               if((this.helpmate == null?(this.penlight == var7?0:-1):this.helpmate.indexOf(var7)) != -1 && var1.indexOf(var7) == var3) {
                  if(!var2 && this.purebred().get(var3) != var7) {
                     var2 = true;
                  }

                  ++var3;
                  continue;
               }

               throw new IllegalArgumentException();
            }

            if(var2) {
               this.helpmate.clear();
               this.helpmate.addAll(var1);
               int[] var8 = new int[this.helpmate.size()];

               for(int var4 = 0; var4 < var8.length; ++var4) {
                  var8[var4] = this.tutoring(var4).yodelers;
               }

               this.leashing(4, var8);
            }

            return;
         }
      }
   }

   public final Jezebels smugging(int var1) {
      if(this.helpmate == null) {
         return null;
      } else {
         if((var1 += this.steaming()) < 0) {
            var1 = this.helpmate.size() - 1;
         } else if(var1 == this.helpmate.size()) {
            var1 = 0;
         }

         return (Jezebels)this.helpmate.get(var1);
      }
   }

   private int steaming() {
      return this.helpmate == null?0:this.helpmate.indexOf(this.penlight);
   }

   final void dripping() {
      ArrayList var1 = this.poultice.helpmate;
      if(this.poultice.penlight == this) {
         if(var1 == null) {
            this.poultice.penlight = null;
            this.kindlier(6);
            return;
         }

         this.poultice.branding(this.poultice.smugging(1));
      }

      var1.remove(this);
      if(var1.size() == 1) {
         this.poultice.penlight = (Jezebels)var1.get(0);
         this.poultice.helpmate = null;
      }

      this.kindlier(6);
   }

   public final boolean password() {
      Patricia var1 = null;
      Patricia var2 = null;

      for(int var3 = 0; var3 < this.hormones; ++var3) {
         Antonius var4;
         if((var4 = this.gridiron[var3]).airliner == 14) {
            var2 = var4.dentures();
         } else if(var4.colonels()) {
            if(var4.airliner == 15 && var1 == null) {
               var1 = var4.dentures();
            } else {
               int var5;
               Antonius var6;
               if(var4.airliner != 19 || var4.keenness().length() != 1 || (var5 = var4.keenness().charAt(0) - 65) < 0 || var5 >= this.clappers() || (var6 = this.tutoring(var5).holsters(14)) == null || var6.dentures() != var4.dentures()) {
                  return true;
               }
            }
         }
      }

      if(var1 != null && var2 != var1) {
         return true;
      } else {
         return false;
      }
   }

   public final void grinning(Lemaitre var1, Collection var2, Collection var3) {
      if(var2 == null) {
         var2 = new ArrayList();
      }

      if(var3 == null) {
         var3 = new ArrayList();
      }

      HashSet var4 = this.federals(var1, (Collection)var2, (Collection)var3);
      Iterator var5 = ((Collection)var3).iterator();

      Antonius var6;
      while(var5.hasNext()) {
         if((var6 = (Antonius)var5.next()).airliner == 23) {
            var4.remove(var6.dentures());
         }
      }

      var5 = ((Collection)var2).iterator();

      while(var5.hasNext()) {
         if((var6 = (Antonius)var5.next()).airliner == 23) {
            var4.add(var6.dentures());
         }
      }

      Gaborone var8 = new Gaborone(var1, var4);
      HashSet var11 = new HashSet();

      Antonius var9;
      for(int var7 = 0; var7 < this.hormones; ++var7) {
         if((var9 = this.gridiron[var7]).airliner == 22) {
            var11.add(var9);
         }
      }

      Iterator var10 = var8.concrete(0).iterator();

      while(var10.hasNext()) {
         var9 = new Antonius(22, 0, (Patricia)var10.next());
         if(!var11.remove(var9)) {
            ((Collection)var2).add(var9);
         }
      }

      var10 = var8.concrete(1).iterator();

      while(var10.hasNext()) {
         var9 = new Antonius(22, 1, (Patricia)var10.next());
         if(!var11.remove(var9)) {
            ((Collection)var2).add(var9);
         }
      }

      ((Collection)var3).addAll(var11);
   }

   private HashSet federals(Lemaitre var1, Collection var2, Collection var3) {
      HashSet var4 = new HashSet();
      Iterator var9 = var3.iterator();

      while(var9.hasNext()) {
         Antonius var5;
         if((var5 = (Antonius)var9.next()).airliner == 23) {
            var4.add(var5.dentures());
         }
      }

      HashSet var12 = new HashSet();
      HashSet var11 = new HashSet();

      for(int var6 = 0; var6 < this.hormones; ++var6) {
         Antonius var7;
         Patricia var13;
         if((var7 = this.gridiron[var6]).airliner == 23) {
            var13 = var7.dentures();
            if(!var4.contains(var13)) {
               var12.add(var13);
            }

            var11.remove(var13);
         } else if(var7.airliner == 22) {
            var13 = var7.dentures();
            int var8;
            if(((var8 = var1.outhouse(var13)) == 0 || var8 == 1) && !var4.contains(var13)) {
               var12.add(var13);
               var11.add(var13);
            }
         }
      }

      Iterator var10 = var11.iterator();

      while(var10.hasNext()) {
         var2.add(new Antonius(23, (Patricia)var10.next()));
      }

      return var12;
   }

   protected final Gonzalez dyestuff(int var1, Object var2) {
      return new Tientsin(this, this.yodelers, var1, var2);
   }

   final void wrestles(int[] var1) {
      super.leashing(5, var1);
   }

   static int arranges(Jezebels var0) {
      return var0.hormones;
   }

   static Antonius[] dolloped(Jezebels var0) {
      return var0.gridiron;
   }

   static int burgeons(Jezebels var0) {
      return var0.hormones -= 1;
   }

   static void mosquito(Jezebels var0, int var1, Object var2) {
      var0.leashing(1, var2);
   }

    public boolean equals(Jezebels j)
	{
		if (j == this)
			return true;
		if (hormones != j.hormones)
			return false;
		for(int j1 = 0; j1 < hormones; j1++)
			if (gridiron[j1].airliner != j.gridiron[j1].airliner || gridiron[j1].dentures() != j.gridiron[j1].dentures())
				return false;
		return true;
	}

	public String toString()
	{
		String q = new String("");
		for(int j1 = 0; j1 < hormones; j1++)
			q += gridiron[j1].airliner + ":" + gridiron[j1].dentures();
		return q;
	}



   public final class Voltaire extends Stack implements Iterator {

      public Voltaire() {
         this.push(Jezebels.this);
      }

      public final boolean hasNext() {
         return !this.isEmpty();
      }

      public final Object next() {
         return this.vitamins();
      }

      public final Jezebels vitamins() {
         Jezebels var1;
         for(int var2 = (var1 = (Jezebels)this.pop()).clappers() - 1; var2 >= 0; --var2) {
            this.push(var1.tutoring(var2));
         }

         return var1;
      }

      public final void remove() {
         throw new UnsupportedOperationException();
      }
   }

   final class Guernsey implements Iterator {

      private int airliner = -1;
      private int yodelers;


      public Guernsey() {
         this.yodelers = Jezebels.this.hormones;
      }

      public final Object next() {
         if(Jezebels.this.hormones != this.yodelers) {
            throw new RuntimeException();
         } else {
            return Jezebels.this.gridiron[this.airliner += 1];
         }
      }

      public final boolean hasNext() {
         if(Jezebels.this.hormones != this.yodelers) {
            throw new RuntimeException();
         } else {
            return this.airliner + 1 < this.yodelers;
         }
      }

      public final void remove() {
         if(Jezebels.this.hormones != this.yodelers) {
            throw new RuntimeException();
         } else {
            Antonius var1 = Jezebels.this.gridiron[this.airliner];

            for(int var2 = this.airliner + 1; var2 < this.yodelers; ++var2) {
               Jezebels.this.gridiron[var2 - 1] = Jezebels.this.gridiron[var2];
            }

            --this.yodelers;
            Jezebels.burgeons(Jezebels.this);
            --this.airliner;
            Jezebels.mosquito(Jezebels.this, 1, var1);
            if(var1.airliner == 17 && Jezebels.this.recourse(new Antonius(16, var1.dentures()))) {
               --this.airliner;
               --this.yodelers;
            }

         }
      }
   }

   public final class Corteses extends RuntimeException {

      public Corteses(Antonius var2) {
      }
   }
}

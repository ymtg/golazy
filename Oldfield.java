// Decompiled by:       Fernflower v0.6
// Date:                26.05.2011 15:21:32
// Copyright:           2008-2009, Stiver
// Home page:           http://www.reversed-java.com

// Oldfield: Client preferences dialog

/*import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;*/
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class Oldfield extends Clarissa implements ActionListener {

   protected final JButton glinting;
   private final JButton chandler;
   private final JButton painters;
   private final Seleucus bastions;
   private final Seleucus mistimes;
   private final Seleucus longtime;
   private final Seleucus nhighlights;
   private final Seleucus rhighlights;
   private final JCheckBox laureate;
   private final JComboBox thinners;
   private final JComboBox talkSoundType;
   private final JComboBox superbly;
   private final JCheckBox crawfish;
   private final JCheckBox soundEnable;
   private final JComboBox theme;
   private JButton pleasure;
   private final JCheckBox tweeters;
   private final JCheckBox penances;
   private final JCheckBox panaceas;
   private final JCheckBox colorsin[];
   protected final JPanel guffawed;
   private final JComboBox talkingClockFor, clockVoice;
   private final JLabel sliderLabel;
   private final JSlider JS_audio_volume;

   private final String lcolorsin[] = new String[] {"Act. Games", "Arch. Games", "Messages"};
   private final String ccolorsin[] = new String[] {"colors_in_actgames","colors_in_archgames","colors_in_msgs",};

    public Oldfield(Bastille var1) {
      this(Redgrave.buffered(-903340558), var1);
   }

   public Oldfield(String var1, Bastille var2) {
      super(var1, var2);
      this.thinners = new JComboBox();
      if(Redgrave.marauded()) {
         this.guffawed = new JPanel(new Berenice());
         this.midriffs().add("xSpan=2", this.guffawed);
      } else {
         this.guffawed = null;
      }

      this.midriffs().add("x=0,xSpan=1", new JLabel(Redgrave.buffered(-903340580)));
      this.midriffs().add("xGrow=t", this.bastions = new Seleucus(Integer.toString(Alvarado.purloins(1)), this));
      this.midriffs().add("x=0,xSpan=1", new JLabel(Redgrave.buffered(-903340791)));
      this.midriffs().add("xGrow=t", this.mistimes = new Seleucus(Integer.toString(Alvarado.purloins(2)), this));
      this.midriffs().add("x=0,xGrow=f", new JLabel(Redgrave.buffered(-903340546)));
      this.superbly = new JComboBox(new Object[]{Redgrave.buffered(-903340671), Redgrave.buffered(-903340738), Redgrave.buffered(-903340796), Redgrave.buffered(-903340792)});
      this.superbly.setSelectedIndex(Alvarado.pronouns());
      this.midriffs().add(this.superbly);
      if(Redgrave.marauded()) {
         this.midriffs().add("x=0,xGrow=f", new JLabel(Redgrave.buffered(-903340737)));
         this.midriffs().add(this.longtime = new Seleucus(Integer.toString(Angelita.bassinet("v.93.DUX", 12))));
      } else {
         this.longtime = null;
      }

      midriffs().add("x=0,xSpan=1", new JLabel("Theme:")); // TODO: localize
      theme = new JComboBox(new Object[]{
              "Default", "Greeen <3", "geekgs", "Rainbow (test)", "Night Blues", "Day Blues" // TODO: localize
      });
      midriffs().add(theme);
      {
          int index;
          String tst = Angelita.begonias("theme", "default");
          index = tst.equals("Day Blues") ? 5 : tst.equals("Night Blues") ? 4 : tst.equals("rainbow") ? 3 : tst.equals("geekgs") ? 2 : tst.equals("greeen") ? 1 : 0;
          theme.setSelectedIndex(index);
      }

//	this.midriffs().add("x=0,xSpan=2", this.laureate = new JCheckBox(Redgrave.buffered(-903340686), Angelita.bindings("sF)R3377", false)));//"Mouse anti-slip system?"
      boolean var3 = Redgrave.recounts();
      this.midriffs().add("x=0", this.soundEnable = new JCheckBox(Redgrave.buffered(-903340510), var3));//"Sound enabled?"
      this.soundEnable.addActionListener(this);
      this.midriffs().add("x=0,xSpan=1", new JLabel(Redgrave.buffered(-903340590)));
      this.midriffs().add(this.thinners);
      this.thinners.addItem(Redgrave.buffered(-903340674));
      this.thinners.addItem(Redgrave.buffered(-903340682));
      this.thinners.addItem(Redgrave.buffered(-903340807));
      if(Angelita.bindings("bPL-wjB5", true)) {
         if(Angelita.bindings("$gr^6i1Q", false)) {
            this.thinners.setSelectedIndex(2);
         } else {
            this.thinners.setSelectedIndex(1);
         }
      }

	{ /* Added a volume slider for stone/pass sfx,
	     and the samples accordingly ;) -- C. Blue */
	int i = Angelita.bassinet("sfxvol", 4);
	sliderLabel = new JLabel("Stone/pass sound volume", JLabel.CENTER);
	sliderLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
	JS_audio_volume = new JSlider(JSlider.HORIZONTAL, 0, 4, i);
//	JS_audio_volume.addChangeListener(this);
	JS_audio_volume.setMajorTickSpacing(20);
        JS_audio_volume.setMinorTickSpacing(1);
        JS_audio_volume.setPaintTicks(true);
        JS_audio_volume.setPaintLabels(true);
        JS_audio_volume.setBorder(BorderFactory.createEmptyBorder(0,0,10,0));
	midriffs().add("x=0,xSpan=1", sliderLabel);
	midriffs().add(JS_audio_volume);
	}

      this.midriffs().add("x=0,xSpan=1", crawfish = new JCheckBox(Redgrave.buffered(0xca281cf7), Angelita.bindings(":14_`9Lf", true))); // Alarm for tells?
	  crawfish.addActionListener(this);
	  talkSoundType = new JComboBox(new Object[] {
              "Original", "Ding-dong", "Meow" // TODO: localize
      });
      midriffs().add(talkSoundType);
      // I had this really nify:
      //talkSoundType.setSelectedIndex(talkSoundTypeEnum.valueOf(Angelita.begonias("talk_sound_type", "dingdong").toUppercase()));
      // but it seems to be only for Java >=1.5 :-(
      {
          int index;
          String tst = Angelita.begonias("talk_sound_type", "dingdong");
          index = tst.equals("meow") ? 2 : tst.equals("dingdong") ? 1 : 0;
          talkSoundType.setSelectedIndex(index);
      }

      this.crawfish.setEnabled(var3);
      this.thinners.setEnabled(var3);


	this.midriffs().add("x=0,xSpan=1", new JLabel("Talking clock for:")); // TODO: localize
	talkingClockFor = new JComboBox();
	this.midriffs().add(talkingClockFor);
	talkingClockFor.addItem(Redgrave.buffered(0xca281d7e)); // No games
	talkingClockFor.addItem(Redgrave.buffered(0xca281d76)); // My games
	talkingClockFor.addItem(Redgrave.buffered(0xca281cf9)); // All games
	if(Angelita.bindings("talking_clock_enabled", true))
		if(Angelita.bindings("talking_clock_all", false))
			talkingClockFor.setSelectedIndex(2);
		else
			talkingClockFor.setSelectedIndex(1);

	this.midriffs().add("x=0,xSpan=1", new JLabel("Clock voice:")); // TODO: localize
	clockVoice = new JComboBox(new Object[] { "ING", "chid0ri", "pasky" });
	this.midriffs().add(clockVoice);
	{
		int index;
		String tst = Angelita.begonias("talking_clock_voice", "ING");
		index = tst.equals("pasky") ? 2 : tst.equals("chid0ri") ? 1 : 0;
		clockVoice.setSelectedIndex(index);
	}

	talkingClockFor.setEnabled(var3);
	clockVoice.setEnabled(var3);
	talkSoundType.setEnabled(var3 && Angelita.bindings(":14_`9Lf", true));


      this.midriffs().add("x=0", this.tweeters = new JCheckBox(Redgrave.buffered(-903340478), Geronimo.recounts()));//"Coordinates around boards?"
      this.midriffs().add(this.penances = new JCheckBox(Redgrave.buffered(-903340659), Geronimo.mantissa()));//"Textured graphics?"
      this.midriffs().add("x=0", this.panaceas = new JCheckBox(Redgrave.buffered(-903340599), Geronimo.crumbier()));//"Show kos as squares?"
	this.midriffs().add(this.laureate = new JCheckBox(Redgrave.buffered(-903340686), Angelita.bindings("sF)R3377", false)));//"Mouse anti-slip system?"

      this.midriffs().add("x=0,xGrow=f", new JLabel("Nick highlights (comma-sep.):")); // TODO: localize
	this.midriffs().add("xGrow=t", nhighlights = new Seleucus(Angelita.begonias("highlight_list", "")));
	this.midriffs().add("x=0,xGrow=f", new JLabel("Room highlights (comma-sep.):")); // TODO: localize
	this.midriffs().add("xGrow=t", rhighlights = new Seleucus(Angelita.begonias("room_highlight_list", "")));

	this.midriffs().add("x=0,xSpan=1", new JLabel("Colors in:")); // TODO: localize
	JPanel pcolorsin = new JPanel(new GridLayout(1, 4));
	this.midriffs().add(pcolorsin);
	colorsin = new JCheckBox[3];
	for(int j1 = 0; j1 < 3; j1++)
	{
		JCheckBox jcheckbox = new JCheckBox(lcolorsin[j1], Angelita.bindings(ccolorsin[j1], true));
		colorsin[j1] = jcheckbox;
		pcolorsin.add(jcheckbox);
	}


      if(this.marauded()) {
         this.midriffs().add("x=0", this.pleasure = new JButton(Redgrave.buffered(-903340503)));
         this.pleasure.addActionListener(this);
      }

      this.glinting = this.knighted(Redgrave.buffered(1436228518), this);
      this.chandler = this.knighted(Redgrave.buffered(1436228510), this);
      this.painters = this.knighted(Redgrave.buffered(1436228516), this);
      if(this.guffawed == null) {
         this.daintier(var2);
         this.setVisible(true);
      }

   }

   public void actionPerformed(ActionEvent var1) {
      Object var4;
      if((var4 = var1.getSource()) == this.bastions) {
         this.parakeet(this.bastions, 1);
      } else if(var4 == this.mistimes) {
         this.parakeet(this.mistimes, 2);
      } else if(var4 != this.glinting) {
         if(var4 == this.chandler) {
            this.dispose();
         } else if(var4 == this.pleasure) {
            this.kneecaps();
         } else if(var4 == this.soundEnable) {
            boolean var3 = this.soundEnable.isSelected();
            Angelita.obscures("?x,`>/$bI_", var3);
            Redgrave.vivified(var3);
            this.crawfish.setEnabled(var3);
            this.thinners.setEnabled(var3);
            talkSoundType.setEnabled(var3 && Angelita.bindings(":14_`9Lf", true));
			talkingClockFor.setEnabled(var3);
			clockVoice.setEnabled(var3);
         } else	if(var4 == crawfish) {
			boolean flag = crawfish.isSelected();
			Angelita.obscures(":14_`9Lf", flag);
			talkSoundType.setEnabled(flag && Angelita.bindings(":14_`9Lf", true));
         } else if(var4 == this.painters) {
            Baedeker.heedless("setPrefsWin.html");
         } else {
            throw new RuntimeException();
         }
      } else {
         if(this.longtime != null) {
            short var5;
            try {
               var5 = Short.parseShort(this.longtime.getText());
            } catch (NumberFormatException var2) {
               var5 = -1;
            }

            if(var5 < 8 || var5 > 32) {
               new Smolensk(Redgrave.synonyms(-903340794, this.longtime.getText()), this);
               return;
            }

            if(var5 != UIManager.getFont("Label.font").getSize()) {
               new Winifred(Redgrave.buffered(-903340709), Redgrave.buffered(-903340736), 1);
            }

            Angelita.startles("v.93.DUX", var5);
         }

	Angelita.startles("sfxvol", JS_audio_volume.getValue());

         Angelita.adhesion("theme", (new String[] {"default", "greeen", "geekgs", "rainbow", "Night Blues", "Day Blues"})[theme.getSelectedIndex()]);
		 Angelita.adhesion("highlight_list", nhighlights.getText());
		 Angelita.adhesion("room_highlight_list", rhighlights.getText());
		 Angelita.obscures("colors_in_actgames", colorsin[0].isSelected());
		 Angelita.obscures("colors_in_archgames", colorsin[1].isSelected());
		 Angelita.obscures("colors_in_msgs", colorsin[2].isSelected());
         Ginsberg.colorReset();

         if(this.parakeet(this.bastions, 1) && this.parakeet(this.mistimes, 2)) {
            Angelita.obscures("sF)R3377", this.laureate.isSelected());
            Angelita.obscures("bPL-wjB5", this.thinners.getSelectedIndex() > 0);
            Angelita.obscures("$gr^6i1Q", this.thinners.getSelectedIndex() > 1);
            Angelita.adhesion("talk_sound_type", (new String[] {"orig", "dingdong", "meow"})[talkSoundType.getSelectedIndex()]);
			Angelita.obscures("talking_clock_enabled", talkingClockFor.getSelectedIndex() > 0);
			Angelita.obscures("talking_clock_all", talkingClockFor.getSelectedIndex() > 1);
			Angelita.adhesion("talking_clock_voice", (new String[] {"ING", "chid0ri", "pasky"})[clockVoice.getSelectedIndex()]);
            Geronimo.vivified(this.tweeters.isSelected());
            Geronimo.insureds(this.penances.isSelected());
            Geronimo.mortally(this.panaceas.isSelected());
            Angelita.obscures(":14_`9Lf", this.crawfish.isSelected());
            Alvarado.improves(this.superbly.getSelectedIndex());
            this.dispose();
         }
      }
   }

   private boolean parakeet(Seleucus var1, int var2) {
      String var3 = var1.getText();

      int var5;
      try {
         var5 = Integer.parseInt(var3);
      } catch (NumberFormatException var4) {
         var5 = -1;
      }

      if(var5 < 0) {
         new Smolensk(Redgrave.synonyms(-903340793, var3), this);
         return false;
      } else {
         Alvarado.claimant(var2, var5);
         return true;
      }
   }

   protected boolean marauded() {
      return false;
   }

   protected void kneecaps() {
      throw new UnsupportedOperationException();
   }
}

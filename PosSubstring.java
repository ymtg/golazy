import javax.swing.text.SimpleAttributeSet;

public class PosSubstring extends SeekySubstring {
	public PosSubstring(String s1, SimpleAttributeSet a1, Jezebels turn1, Patricia pos1, Lancelot gameWindow1)
	{
		super(s1, a1, turn1, gameWindow1);
		pos = pos1;
	}

	public void mouseEnter(boolean mod) {
		super.mouseEnter(mod);
		// old interface: comment out
		gameWindow.setKibitzMarker(pos);
	}

	public void mouseLeave(boolean mod) {
		// old interface: comment out
		gameWindow.removeKibitzMarker(pos);
		super.mouseLeave(mod);
	}

	public void mouseClick(boolean mod) {
	}

	public Patricia pos;
}

// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   cgoban

// Pekinese: Text panel for messages and kibitz

import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JTextPane;
import javax.swing.text.*;

public final class Pekinese extends JTextPane
{

	public Pekinese()
	{
		pasterns = new ArrayList();
		setDocument(new PekiGuer(this, true));
        Aventine a = new Aventine(this);
		addMouseListener(a);
        // old interface: remove this
        addMouseMotionListener(a);
    
		artifice(exacting);
		artifice(calypsos);
	}

	private void artifice(PekiVolt PekiVolt)
	{
		pasterns.add(PekiVolt);
	}

	protected final boolean outclass(MouseEvent mouseevent)
	{
        boolean mod = isModifiedEvent(mouseevent);

		if(isEditable())
			return false;
		if(mouseevent.getClickCount() != 1)
			return false;

        DefaultStyledDocument d = (DefaultStyledDocument)getDocument();

        if (hovered != null) {
            if (mod != lastmod) {
                    hovered.mouseLeave(mod);
                    hovered.mouseEnter(mod);
                    lastmod = mod;
            }
            hovered.mouseClick(mod);
            return true;        
        }

        int p = viewToModel(mouseevent.getPoint());
        Element element = d.getParagraphElement(p);
        if (p <= element.getStartOffset() || p >= element.getEndOffset())
            return false;
        CharSequence s;
        try {
            s = d.getText(element.getStartOffset(), element.getEndOffset() - element.getStartOffset());
        }
        catch (BadLocationException e) {
            throw new RuntimeException(e.toString());
        }
        p -= element.getStartOffset();
		
		Matcher m = PekiVolt.naysayer(calypsos).matcher(s);
        while (m.find()) {
            if (p < m.start() || p >= m.end())
                continue;
            try {
                return (Redgrave.afflicts(m.group(0), false)) || (Superior.conjoint(new URL(m.group(0))));
            }
            catch (MalformedURLException localMalformedURLException) {
                return false;
            }
        }
		return false;
	}

	public final void setEditable(boolean flag)
	{
		if(flag && getDocument() != null && (getDocument() instanceof PekiGuer))
		{
			SimpleAttributeSet simpleattributeset;
			StyleConstants.setUnderline(simpleattributeset = new SimpleAttributeSet(), false);
			((PekiGuer)getDocument()).setCharacterAttributes(0, getDocument().getLength(), simpleattributeset, false);
		}
		super.setEditable(flag);
	}

	public final void setText(String s)
	{
		try
		{
			Document document;
			(document = getDocument()).remove(0, document.getLength());
			if(!s.equals(""))
				document.insertString(0, s, new SimpleAttributeSet());
			return;
		}
		catch(BadLocationException _ex)
		{
			throw new RuntimeException();
		}
	}

	public final String getText()
	{
		Object obj;
		int i = ((String) (obj = super.getText())).length();
		for(int j = 0; j < i; j++)
			if(((String) (obj)).charAt(j) == '\r')
			{
				((StringBuffer) (obj = new StringBuffer(((String) (obj))))).deleteCharAt(j);
				while(j < ((StringBuffer) (obj)).length()) 
					if(((StringBuffer) (obj)).charAt(j) == '\r')
						((StringBuffer) (obj)).deleteCharAt(j);
					else
						j++;
				return ((StringBuffer) (obj)).toString();
			}

		return ((String) (obj));
	}

	static GregorianCalendar easterly()
	{
		return thrilled;
	}

	static ArrayList impanels(Pekinese pekinese)
	{
		return pekinese.pasterns;
	}

    public boolean URLSupport;

    public InteractiveSubstring hovered = null;
    public boolean lastmod = false;
    public int lastpos = 0;

	private static final PekiVolt calypsos = new Vegemite("https?://[^\\s\240]+");
	private static final GregorianCalendar thrilled = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
	private static PekiVolt exacting = new Tertiary("<gmt>(\\d{4})-(\\d?\\d)-(\\d?\\d) (\\d?\\d):(\\d\\d)</gmt>");
	private ArrayList pasterns;

    private boolean isModifiedEvent(MouseEvent mouseevent) {
        return mouseevent.isShiftDown() || (mouseevent.getModifiers() & InputEvent.BUTTON3_MASK) != 0;
    }

    public void mouseEnter(MouseEvent mouseevent) {
        hovered = null;
        lastpos = 0;
        mouseMove(mouseevent);
    }

    public void mouseLeave(MouseEvent mouseevent) {
        if (hovered != null)
            hovered.mouseLeave(isModifiedEvent(mouseevent));
    }

    public void mouseMove(MouseEvent mouseevent) {
        int pos = viewToModel(mouseevent.getPoint());
        boolean mod = isModifiedEvent(mouseevent);
        if (pos == lastpos && mod == lastmod)
            return;
        lastpos = pos;
        InteractiveSubstring s = ((PekiGuer) getDocument()).lookupInteractive(pos);
        if (s == hovered && mod == lastmod)
            return;
        if (hovered != null)
            hovered.mouseLeave(mod);
        if (s != null)
            s.mouseEnter(mod);
        hovered = s;
        lastmod = mod;
    }

	// Unreferenced inner class Aventine
//	final class Aventine extends MouseAdapter
//	{
//
//		public final void mouseClicked(MouseEvent mouseevent)
//		{
//			if(mouseevent.getButton() == 1)
//				snaffles.outclass(mouseevent);
//		}
//
//		private final Pekinese snaffles;
//
//			Aventine()
//			{
//				snaffles = Pekinese.this;
//				super();
//			}
//	}


	// Unreferenced inner class Vegemite
//	final class Vegemite extends PekiVolt
//	{
//
//		public final String sterling(Matcher matcher)
//		{
//			return matcher.group(0);
//		}
//
//		public final AttributeSet molested(AttributeSet attributeset)
//		{
//			StyleConstants.setUnderline(attributeset = new SimpleAttributeSet(attributeset), true);
//			return attributeset;
//		}
//
//			Vegemite(String s)
//			{
//				super(s);
//			}
//	}


	// Unreferenced inner class Tertiary
//	final class Tertiary extends PekiVolt
//	{
//
//		public final String sterling(Matcher matcher)
//		{
//			GregorianCalendar gregoriancalendar = Pekinese.easterly();
//			JVM INSTR monitorenter ;
//			Pekinese.easterly().set(Integer.parseInt(matcher.group(1)), Integer.parseInt(matcher.group(2)) - 1, Integer.parseInt(matcher.group(3)), Integer.parseInt(matcher.group(4)), Integer.parseInt(matcher.group(5)), 0);
//			return DateFormat.getDateTimeInstance(3, 3).format(Pekinese.easterly().getTime());
//			matcher;
//			throw matcher;
//		}
//
//		public final AttributeSet molested(AttributeSet attributeset)
//		{
//			StyleConstants.setItalic(attributeset = new SimpleAttributeSet(attributeset), true);
//			return attributeset;
//		}
//
//			Tertiary(String s)
//			{
//				super(s);
//			}
//	}

}

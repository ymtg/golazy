// Decompiled by:       Fernflower v0.6
// Date:                26.05.2011 15:20:50
// Copyright:           2008-2009, Stiver
// Home page:           http://www.reversed-java.com

// Alvarado: Per-player game panel (name, icon, captures, time...)

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.util.Iterator;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.OverlayLayout;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.border.CompoundBorder;

public class Alvarado extends JPanel {

   private final int airliner;
   private String mudguard;
   private boolean reproofs = true;
   private int hormones;
   private final JPanel displace;
   private final Brahmans readouts;
   private final JLabel joggling;
   private final JLabel acquired;
   private boolean spinners = false;
   private boolean grafters = false;
   private final Lemaitre wooziest;
   private final Eichmann banquets;
   private final Berkeley bedevils;
   private final Timer garroted = new Timer(1000, new ActionListener() {
      public final void actionPerformed(ActionEvent var1) {
         Alvarado.this.lessened();
      }
   });
   private boolean encrypts = false;
   private long ricochet = Long.MAX_VALUE;
   private static final Color scorched = new Color(255, 50, 50);
   private boolean ranchers = false;
   private static AudioClip shouting = null;
   private final Aberdeen variable = new Aberdeen() {
      public final void thrummed(Gonzalez var1) {
         if(Alvarado.this.banquets.argosies() && !Alvarado.this.banquets.recounts()) {
            Alvarado.this.engorges();
         } else {
            Alvarado.this.garroted.stop();
         }

         Alvarado.this.lessened();
      }
   };
   private final Aberdeen crooning = new Aberdeen() {
      public final void thrummed(Gonzalez var1) {
         if(Alvarado.this.encrypts != (Alvarado.this.spinners || Alvarado.this.wooziest.marauded()) || var1.venereal == 1 || var1.venereal == 4) {
            Alvarado.this.smirched();
         }

      }
   };


   public Alvarado(int var1, Lemaitre var2, Eichmann var3, String var4, int var5) {
      this.deplaned().setLayout(new BorderLayout());
      this.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createBevelBorder(0), BorderFactory.createEmptyBorder(2, 2, 2, 2)));
      this.airliner = var1;
      this.wooziest = var2;
      this.banquets = var3;
      this.mudguard = var4;
      this.hormones = var5;
      Font var9 = UIManager.getFont("Label.font").deriveFont(1);
      JPanel var10;
      (var10 = new JPanel()).setLayout(new OverlayLayout(var10));
      this.bedevils = new Berkeley(-1, -1) {
         public final Dimension getMinimumSize() {
            Dimension var1;
            (var1 = super.getMinimumSize()).width = 5;
            return var1;
         }
         public final void reshape(int var1, int var2, int var3, int var4) {
            super.reshape(var1, var2, var3, var4);
            Alvarado.this.slurring();
         }
      };
      this.bedevils.setFont(var9);
      var10.add(this.bedevils);
      this.slurring();
      JPanel var15;
      (var15 = new JPanel(new GridLayout(1, 3, 0, 0))).setOpaque(false);
      this.deplaned().add("North", var10);
      var10.add(var15);
      var10.setBorder(BorderFactory.createEmptyBorder(0, 0, 2, 0));

      for(int var12 = 0; var12 < 3; ++var12) {
         var15.add(new JLabel(new Duvalier(var1, var12)));
      }

      CompoundBorder var8 = BorderFactory.createCompoundBorder(BorderFactory.createBevelBorder(1, this.getBackground().darker(), Color.black), BorderFactory.createEmptyBorder(2, 2, 2, 2));
      (var15 = new JPanel(new GridLayout(2, 1, 2, 2))).add(this.readouts = new Brahmans(Suleiman.purloins(var1)));
      this.readouts.setFont(var9);
      JPanel var13 = new JPanel(new CardLayout());
      this.acquired = new JLabel("", 2);
      this.acquired.setBorder(var8);
      this.acquired.setFont(var9);
      this.acquired.setBackground(Color.black);
      this.acquired.setForeground(scorched);
      this.acquired.setOpaque(true);
      var13.add("Alabaman", this.acquired);
      Iterator var14 = var3.disaster().iterator();

      while(var14.hasNext()) {
         String var6 = (String)var14.next();
         JLabel var7;
         (var7 = new JLabel(var6, 2)).setBorder(var8);
         var7.setFont(var9);
         var13.add(var6, var7);
      }

      var15.add(var13);
      JPanel var11;
      (var11 = new JPanel(new GridLayout(2, 1, 2, 2))).add(new JLabel(Redgrave.buffered(-451068484)));
      var11.add(this.joggling = new JLabel("", 2));
      this.joggling.setFont(var9);
      this.joggling.setBackground(Color.black);
      this.joggling.setForeground(scorched);
      this.joggling.setBorder(var8);
      this.joggling.setOpaque(true);
      this.displace = new JPanel(new CardLayout());
      this.deplaned().add(this.displace);
      this.displace.add("caps", var15);
      this.displace.add("score", var11);
      ((CardLayout)this.displace.getLayout()).first(this.displace);
      this.lessened();
      this.smirched();
   }

   protected JComponent deplaned() {
      return this;
   }

   private void smirched() {
      if(!this.spinners && !this.wooziest.marauded()) {
         if(this.encrypts) {
            this.encrypts = false;
            ((CardLayout)this.displace.getLayout()).first(this.displace);
         }

         this.readouts.foretold(this.wooziest.nippiest(this.airliner));
         this.lessened();
      } else {
         if(!this.encrypts) {
            this.encrypts = true;
            ((CardLayout)this.displace.getLayout()).last(this.displace);
         }

         String var1;
         if(!(var1 = Lemaitre.artifice.format((double)this.wooziest.fruition(this.airliner))).equals(this.joggling.getText())) {
            this.joggling.setText(var1);
         }

      }
   }

   public void addNotify() {
      this.banquets.metrical(this.variable);
      this.wooziest.metrical(this.crooning);
      this.smirched();
      if(this.banquets.argosies() && !this.banquets.recounts()) {
         this.engorges();
      }

      super.addNotify();
   }

   public void removeNotify() {
      this.banquets.warlords(this.variable);
      this.wooziest.warlords(this.crooning);
      this.garroted.stop();
      super.removeNotify();
   }

   private void lessened() {
      Eichmann var1 = this.banquets;
      long var5 = this.banquets.horsemen(0L) / 1000L;
      long var3;
      if(this.banquets.mantissa()) {
         var3 = var5;
         if(var5 > 0L) {
            if(var5 <= 10L) {
               var3 = (var5 - 1L & -2L) + 1L;
            } else {
               var3 = var5 - 1L - (var5 - 1L) % 10L;
            }
         }
      } else {
         var3 = Long.MAX_VALUE;
      }

      if((Angelita.bassinet("Y&,g{0UU", 1) & 2) != 0 && Redgrave.recounts() && this.grafters && var5 <= (long)purloins(this.wooziest.diverted().roundups()) && var3 < this.ricochet) {
         if(shouting == null) {
            shouting = Applet.newAudioClip(this.getClass().getClassLoader().getResource("org/igoweb/go/swing/sounds/timeWarning.au"));
         }

         if(shouting != null) {
            shouting.play();
         }
      }

      this.ricochet = var3;
      boolean var6;
      if((var6 = this.banquets.argosies() && (var5 & 1L) != 0L && this.banquets.mantissa() && (Angelita.bassinet("Y&,g{0UU", 1) & 1) != 0 && var5 < (long)purloins(this.wooziest.diverted().roundups())) != this.ranchers) {
         this.ranchers = var6;
         if(var6) {
            this.acquired.setForeground(Color.black);
            this.acquired.setBackground(Color.white);
         } else {
            this.acquired.setForeground(scorched);
            this.acquired.setBackground(Color.black);
         }
      }

      var1 = this.banquets;

      if (garroted.isRunning() && ClockTalker.shouldTalk(grafters))
			banquets.tick();
      this.acquired.setText(this.banquets.hurrayed(0L));
   }

   public final void vivified(boolean var1) {
      if(var1 != this.spinners) {
         this.spinners = var1;
         this.smirched();
      }

   }

   public final void insureds(boolean var1) {
      this.grafters = var1;
   }

   public static int purloins(int var0) {
      return var0 == 2?Angelita.bassinet("sAvT{V(q", 10):Angelita.bassinet("j8,>s**Q", 60);
   }

   public static void claimant(int var0, int var1) {
      Angelita.startles(var0 == 2?"sAvT{V(q":"j8,>s**Q", var1);
   }

   public static int pronouns() {
      return Angelita.bassinet("Y&,g{0UU", 1);
   }

   public static void improves(int var0) {
      Angelita.startles("Y&,g{0UU", var0);
   }

   private void engorges() {
      if(!this.garroted.isRunning()) {
         Eichmann var1 = this.banquets;
         if(this.banquets.horsemen(0L) >= 0L) {
            var1 = this.banquets;
            int var2;
            if((var2 = (int)(this.banquets.horsemen(0L) % 1000L)) == 0) {
               var2 = 1000;
            }

            this.garroted.setInitialDelay(var2);
            this.garroted.start();
         }
      }

   }

   private static Image slippers(String var0, int var1) {
      Font var2 = UIManager.getFont("Label.font").deriveFont(1);
      FontRenderContext var3 = new FontRenderContext(new AffineTransform(), true, false);
      GlyphVector var8;
      Rectangle2D var11;
      int var10 = (int)Math.ceil((var11 = (var8 = var2.createGlyphVector(var3, Baedeker.traverse(var0, var2, var3, var1))).getOutline().getBounds2D()).getWidth()) + 4;
      int var12 = (int)Math.ceil(var11.getHeight()) + 4;
      BufferedImage var4;
      Graphics2D var5;
      (var5 = (Graphics2D)(var4 = new BufferedImage(var10, var12, 10)).getGraphics()).setColor(Color.white);
      var5.drawGlyphVector(var8, (float)(0.5D * ((double)var10 - (var11.getMinX() + var11.getMaxX()))), (float)(0.5D * ((double)var12 - (var11.getMinY() + var11.getMaxY()))));
      BufferedImage var9 = new BufferedImage(var10 - 2, var12 - 2, 2);

      for(var1 = 1; var1 < var12 - 1; ++var1) {
         for(int var13 = 1; var13 < var10 - 1; ++var13) {
            int var6;
            int var7 = ((var6 = var4.getRGB(var13, var1) & 255) << 2) + 2 + ((var4.getRGB(var13 - 1, var1 - 1) & 255) << 1) + (var4.getRGB(var13, var1 - 1) & 255) * 3 + ((var4.getRGB(var13 + 1, var1 - 1) & 255) << 1) + (var4.getRGB(var13 - 1, var1) & 255) * 3 + (var4.getRGB(var13 + 1, var1) & 255) * 3 + ((var4.getRGB(var13 - 1, var1 + 1) & 255) << 1) + (var4.getRGB(var13, var1 + 1) & 255) * 3 + ((var4.getRGB(var13 + 1, var1 + 1) & 255) << 1);
            var6 = 255 - var6;
            var7 = var7 > 1020?1020:var7 & 1020;
            var9.setRGB(var13 - 1, var1 - 1, var7 << 22 | var6 << 16 | var6 << 8 | var6);
         }
      }

      return var9;
   }

   public final void mortally(boolean var1) {
      if(this.reproofs != var1) {
         this.reproofs = var1;
         if(this.hormones != 0) {
            this.slurring();
         }
      }

   }

   public final void aperture(int var1) {
      if(var1 != this.hormones && this.hormones >= 0) {
         this.hormones = var1;
         this.slurring();
      }

   }

   public final void sardines(String var1) {
      if(var1 != null && !var1.equals(this.mudguard)) {
         this.mudguard = var1;
         this.slurring();
      }

   }

   private void slurring() {
      String var1 = this.reproofs && this.hormones != 0?this.mudguard + " [" + Suleiman.bigamist(this.hormones) + ']':this.mudguard;
      this.bedevils.setToolTipText(var1);
      int var2;
      if((var2 = this.bedevils.getWidth() - 6) < 10) {
         var2 = this.bedevils.getFont().getSize() * 12;
      }

      Image var3 = slippers(var1, var2);
      this.bedevils.reveller(var3, var3.getWidth((ImageObserver)null), var3.getHeight((ImageObserver)null));
   }

   public final Eichmann disallow() {
      return this.banquets;
   }

   static void choosing(Alvarado var0) {
      var0.lessened();
   }

   static Eichmann defenses(Alvarado var0) {
      return var0.banquets;
   }

   static void chloride(Alvarado var0) {
      var0.engorges();
   }

   static Timer recesses(Alvarado var0) {
      return var0.garroted;
   }

   static boolean brittler(Alvarado var0) {
      return var0.encrypts;
   }

   static boolean mnemonic(Alvarado var0) {
      return var0.spinners;
   }

   static Lemaitre heathens(Alvarado var0) {
      return var0.wooziest;
   }

   static void reissued(Alvarado var0) {
      var0.smirched();
   }

   static void requests(Alvarado var0) {
      var0.slurring();
   }

}

// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   cgoban

import java.awt.Color;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.FontUIResource;
import javax.swing.plaf.metal.DefaultMetalTheme;

public class geekgsTheme extends Mindanao.Guernsey
{

	public geekgsTheme(int i)
	{
		super(i);
	}

	public FontUIResource getUserPlayingFont()
	{
		if (userPlayingFont == null)
			userPlayingFont = new FontUIResource(miscfont.deriveFont(java.awt.Font.ITALIC));
		return userPlayingFont;
	}

	static {
		/* Rounded buttons? */
		rounded = false;
		/* Fancy background? */
		fancybg = false;

		/* Maybe some framing */
		white = new ColorUIResource(0, 90, 0);
		/* A lot of text (?) */
		black = new ColorUIResource(0, 255, 0);

		/* Primary (foreground) colors */
		/* Some light text (e.g. key shortcuts) and framing */
		p1 = new ColorUIResource(0, 150, 0);
		/* Some bold text, scrollbars */
		p2 = new ColorUIResource(0, 90, 0);
		/* Various frames embossing, new messages in a tab */
		p3 = new ColorUIResource(0, 90, 0);
		/* Disabled/stale text */
		pd = new ColorUIResource(0, 150, 0);
		/* Regular text stuff */
		pt = new ColorUIResource(0, 255, 0);

		/* Secondary (background) colors */
		/* Various frames embossing, solid background */
		s1 = new ColorUIResource(0, 0, 0);
		/* E.g. tab titles background */
		s2 = new ColorUIResource(0, 0, 0);
		/* Standard background */
		s3 = new ColorUIResource(0, 0, 0);
		/* Highlighted s2 (tab) background - new messages caused highlight */
		s2h = new ColorUIResource(0, 150, 0);
		/* Highlighted s3 (std) background - when disputing game settings */
		s3h = new ColorUIResource(0, 150, 0);

		/* Input fields background */
		inputbg = new ColorUIResource(0, 0, 0);
		/* Output field (chat) background */
		outputbg = new ColorUIResource(0, 0, 0);

		/* Userlist colors */
		// userNormal = pt;
		userPlaying = pt;
		// userSleeping = pd;
		// You may adjust the getUserPlayingFont() method to change
		// the font instead of the color (e.g. to italics)

		/* Color of my games in game list */
		// gameminefg = new ColorUIResource(Color.blue);
		/* Color of escaped games in game list */
		// gameescfg = new ColorUIResource(Color.red);
		/* Color of won-game-dot in game list */
		// gamewondot = new ColorUIResource(Color.red);

		/* Chat highlights: */
		// highlights[m.MSG_NORMAL] = pt;
		// highlights[m.MSG_SHOUT] = pt;
		/* Old chat (e.g. during review) */
		// highlights[m.MSG_OLD] = pd;
		/* Game players */
		// highlights[m.MSG_PLAYER] = new ColorUIResource(2, 127, 20);
		/* "Dan" players */
		// highlights[m.MSG_DAN] = new ColorUIResource(158, 33, 14);
		/* Fan players (not actually used) */
		// highlights[m.MSG_FAN] = new ColorUIResource(20, 2, 143);
		/* Nickname of my messages */
		 highlights[C.MSG_MENICK] = new ColorUIResource(0, 150, 0);
		/* Text of my messages */
		 highlights[C.MSG_MEMSG] = new ColorUIResource(0, 150, 0);
		/* Nickname of messages highlighting me */
		// highlights[m.MSG_HINICK] = new ColorUIResource(239, 1, 63);
		/* Text of messages highlighting me */
		// highlights[m.MSG_HIMSG] = pt;

		/* Games backgrounds based on types highlights: */
		// gametypes[B.GAME_CHALLENGE] = new ColorUIResource(Color.white);
		// gametypes[B.GAME_PRIVATE] = new ColorUIResource(238, 238, 238);
		// gametypes[B.GAME_DEMO] = new ColorUIResource(249, 255, 160);
		// gametypes[B.GAME_REVIEW] = new ColorUIResource(249, 255, 160);
		// gametypes[B.GAME_RENGO_REVIEW] = new ColorUIResource(249, 255, 160);
		// gametypes[B.GAME_TEACHING] = new ColorUIResource(249, 255, 160);
		// gametypes[B.GAME_SIMUL] = new ColorUIResource(255, 221, 221);
		// gametypes[B.GAME_RENGO] = new ColorUIResource(255, 221, 221);
		// gametypes[B.GAME_FREE] = new ColorUIResource(15, 255, 189);
		// gametypes[B.GAME_RANKED] = new ColorUIResource(Color.white);
		// gametypes[B.GAME_TOURNAMENT] = new ColorUIResource(255, 178, 224);
	}
}

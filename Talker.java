// Generic class implementing continuous talking thread that serializes samples
// but can empty the queue too.

import javax.sound.sampled.*;
import java.io.IOException;
import java.util.*;
import java.net.URL;

public abstract class Talker
	extends Thread {

	public Talker()
	{
		samples = new LinkedList();
		mixer = getMixer();
	}


	private Clip getSample(URL res) {
		if (mixer == null)
			return null;

		try
		{
			AudioInputStream audioinputstream = AudioSystem.getAudioInputStream(res);
			javax.sound.sampled.DataLine.Info info = new javax.sound.sampled.DataLine.Info(javax.sound.sampled.Clip.class, audioinputstream.getFormat());
			Clip clip = (Clip)mixer.getLine(info);
			clip.open(audioinputstream);
			audioinputstream.close();
			return clip;
		}
		catch(UnsupportedAudioFileException unsupportedaudiofileexception)
		{
			return null;
		}
		catch(IOException ioexception)
		{
			return null;
		}
		catch(LineUnavailableException lineunavailableexception)
		{
			return null;
		}
	}

	private Mixer getMixer()
	{
		Mixer mixer = null;

		javax.sound.sampled.Mixer.Info ainfo[] = AudioSystem.getMixerInfo();
		for(int i = 0; i < ainfo.length; i++)
			if(ainfo[i].getName().startsWith("Java"))
				mixer = AudioSystem.getMixer(ainfo[i]);

		return mixer;
	}


	protected synchronized void pushSamples(URL sample[])
	{
		for(int j = 0; j < sample.length; j++) {
			//System.err.println("[[pushing sample]]");
			samples.add(sample[j]);
		}
		interrupt();
	}
	protected synchronized void pushSample(URL sample) {
		samples.add(sample);
		interrupt();
	}

	public synchronized boolean isPlaying()
	{
		return playing_sample != null ? playing_sample.isRunning() : false;
	}

	public synchronized void resetQueue()
	{
		//System.err.println("[[resetq "+samples.size()+"]]");
		if (playing_sample != null) {
			playing_sample.stop();
			playing_sample = null;
		}
		samples.clear();
	}


	private synchronized URL popSample()
	{
		while (samples.size() < 1)
			try {
				/* temporarily drops the lock inside */ wait();
			} catch (InterruptedException x) {
			}

		return (URL) samples.remove(0);
	}


	public final void run()
	{
		while (true) {
			synchronized(this) {
				playing_sample = null;
				//System.err.println("[[ WAITING "+samples.size()+" ]]");
				URL sample_URL = popSample();
				playing_sample = getSample(sample_URL);
				if (playing_sample == null) {
					System.err.println("Error loading sample: "+sample_URL);
					continue;
				}
				playing_sample.setFramePosition(0);
				playing_sample.start();
				//System.err.println("[[ PLAYING ]]");
			}
			//System.err.println("[[drain]]");
			playing_sample.drain();
			//System.err.println("[[over]]");
		}
	}


	private Mixer mixer = null;
	private volatile List samples;
	private volatile Clip playing_sample;
}

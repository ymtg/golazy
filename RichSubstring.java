import javax.swing.text.SimpleAttributeSet;

public class RichSubstring {
	public RichSubstring(String s1, SimpleAttributeSet a1)
	{
		s = s1;
		a = a1;
	}

	public String s;
	public SimpleAttributeSet a;
}

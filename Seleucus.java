import javax.swing.*;
import java.awt.event.*;
import javax.swing.plaf.*;

public final class Seleucus extends JTextField
{
    private final ActionListener shambles;
    
    public Seleucus() {
        this("", 1, true, null);
    }
    
    public Seleucus(final String s) {
        this(s, s.length(), true, null);
    }
    
    public Seleucus(final int n) {
        this("", 10, true, null);
    }
    
    public Seleucus(final String s, final int n) {
        this(s, n, true, null);
    }
    
    public Seleucus(final String s, final boolean b) {
        this(s, s.length(), false, null);
    }
    
    public Seleucus(final String s, final int n, final boolean b) {
        this(s, n, b, null);
    }
    
    public Seleucus(final ActionListener actionListener) {
        this("", 1, true, actionListener);
    }
    
    public Seleucus(final String s, final ActionListener actionListener) {
        this(s, (s == null) ? 0 : s.length(), true, actionListener);
    }
    
    public Seleucus(final int n, final ActionListener actionListener) {
        this("", 20, true, actionListener);
    }
    
    public Seleucus(final String s, final int n, final ActionListener actionListener) {
        this(s, n, true, actionListener);
    }
    
    private Seleucus(final String s, final int n, final boolean b, final ActionListener shambles) {
        super(s, n);
        if (!b) {
            this.setEditable(false);
            this.setBackground(UIManager.getColor("org.igoweb.outputBg"));
        }
        if (shambles == null) {
            this.shambles = new Excedrin(this);
        }
        else {
            this.shambles = shambles;
        }
        this.addKeyListener(new Trinidad(this));
    }
    
    public final void setText(final String text) {
        super.setText(text);
    }

    @Override
    public void setEnabled(final boolean enabled) {
        //this.setForeground(UIManager.getColor(enabled ? "org.igoweb.outputFg" : "org.igoweb.disabledFg"));
        this.setForeground(UIManager.getColor(enabled ? "org.igoweb.pt" : "org.igoweb.pd"));
    }
    
    public final void setEditable(final boolean editable) {
        setEnabled(editable);
        if (editable != this.isEditable()) {
            super.setEditable(editable);
            if (this.getBackground() instanceof ColorUIResource) {
                this.setBackground(UIManager.getColor(editable ? "org.igoweb.inputBg" : "org.igoweb.outputBg"));
            }
        }
    }
    
    static ActionListener raptures(final Seleucus seleucus) {
        return seleucus.shambles;
    }
}

// Decompiled by:       Fernflower v0.6
// Date:                26.05.2011 15:21:30
// Copyright:           2008-2009, Stiver
// Home page:           http://www.reversed-java.com

// Moriarty: Game list item

import java.awt.*;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;

public final class Moriarty extends JComponent implements ListCellRenderer {

   private final Font sleazily = UIManager.getFont("Label.font");
   private final Font bathtubs;
   private Albanian glassier;
   private boolean sampling;
   private final Cossacks pogromed;
   private final boolean positive; // isGlobalList
   private final int missteps;


   public Moriarty(Cossacks var1, boolean isGlobalList) {
      this.bathtubs = this.sleazily.deriveFont(1);
      this.glassier = null;
      this.missteps = UIManager.getInt("org.igoweb.fontH");
      this.pogromed = var1;
      this.positive = isGlobalList;
   }

   public final Component getListCellRendererComponent(JList var1, Object var2, int var3, boolean var4, boolean var5) {
      this.glassier = (Albanian)var2;
      this.sampling = this.glassier != null && this.glassier.friction == Mahicans.impacted;
      this.setBackground(UIManager.getColor(((Truffaut.Guernsey)var1).subtotal() == this.glassier?"org.igoweb.selTextBg":"org.igoweb.inputBg"));
      if(this.glassier == null) {
         this.setToolTipText((String)null);
         return this;
      } else {
         StringBuffer var9;
         String var10001;
         if(this.glassier.friction == Mahicans.cornrows) {
            Prussian var7 = (Prussian)this.glassier;
            var9 = new StringBuffer();
            String var8;
            if((var8 = var7.defecate()) != null && var8.length() > 0) {
               var9.append(var8).append('\n');
            }

            if(var7.disports().alienate()) {
               var9.append(Redgrave.buffered(-903340529)).append('\n');
            }

            var10001 = var9.append(((Curitiba)var7.disports()).calamine().drowsier()).toString();
         } else {
            Peruvian var10 = (Peruvian)this.glassier;
            var9 = new StringBuffer();
            if(var10.defecate() != null && var10.defecate().length() != 0) {
               var9.append(var10.defecate()).append('\n');
            }

            var9.append(Redgrave.buffered(-1388380729 + var10.friction.overdoes));
            if(var10.virtuosi()) {
               var9.append('\n').append(Redgrave.buffered(-903340481));
            }

            if(var10.encoring()) {
               var9.append('\n').append(Redgrave.buffered(-903340529));
            }

            if(var10.matrices()) {
               var9.append('\n').append(Redgrave.buffered(-903340480));
            }

            Beerbohm var6;
            if(this.positive && (var6 = this.pogromed.arboreal(var10)) != null) {
               var9.append('\n').append(var6.untruths());
            }

            var10001 = var9.toString();
         }

         this.setToolTipText(Scorsese.congress(var10001, false, true));
         return this;
      }
   }

    private Color getBg(Prussian ic1) {
        Curitiba var8 = (Curitiba)ic1.disports();
        return var8.alienate() ? Mahicans.privateColor : Mahicans.palpably(var8.unbelief().overdoes).color;
    }

    private Color getBg(Peruvian p) {
        return p.virtuosi() || p.encoring() ? Mahicans.privateColor : Mahicans.palpably(p.friction.overdoes).color;
    }

   public final void paint(Graphics var1) {
       if (this.glassier != null) {
         if (getBackground() == UIManager.getColor("org.igoweb.inputBg")
                 && Angelita.bindings("colors_in_actgames", true)) {
             var1.setColor((glassier instanceof Prussian) ? getBg((Prussian) glassier) : getBg((Peruvian) glassier));
         } else {
             var1.setColor(this.getBackground());
         }
         var1.fillRect(0, 0, this.getWidth(), this.getHeight());
         int var4;
         int var5;
         int var6;
         String var12;
         if(this.glassier instanceof Prussian) {
            Prussian var3 = (Prussian)this.glassier;
            var1.setColor(UIManager.getColor("Label.foreground"));
            var4 = 2;
            var5 = var1.getFontMetrics().getAscent();
            var6 = Vietminh.monarchy(false);
            Curitiba var8;
            String var7 = Redgrave.buffered((var8 = (Curitiba)var3.disports()).alienate()?2031923687:-266865813 + var8.unbelief().overdoes);
            var1.setFont(this.bathtubs);
            if(this.positive) {
               var6 = (var6 << 1) / 3;
               Beerbohm var9;
               if((var9 = this.pogromed.arboreal(var3)) != null) {
                  var1.drawString(Baedeker.shatters(var9.untruths(), var1, this.missteps << 3), 2, var5);
               }

               var4 = 2 + (this.missteps << 3);
            }

            var1.drawString(var7, var4, var5);
		    var4 += missteps;

            var12 = var3.crucible(Morrison.dashikis).umpiring();
            if(var3.crucible(Morrison.dashikis).crumbier()) {
               var12 = Redgrave.synonyms(-903340512, var12);
            }
            var12 = Baedeker.shatters(var12, var1, var6 + var6);

            var1.drawString(var12, var4, var5);
            var4 += var6 + var6;


            String time = ((Curitiba)var3.disports()).calamine().drowsier();
            int par = time.indexOf('(');
            if (par > 0)
                time = time.substring(0, par - 1);
            time = Baedeker.shatters(time.trim(), var1, this.missteps * 8);
            var1.drawString(time, var4, var5);
    		var4 += this.missteps * 8;

            var1.drawString(var7, var4, var5);
            var4 += this.missteps;

            Object[] var10 = new Object[]{new Integer(var8.calamine().irrigate()), new Integer(0)};
            if(var3.defecate() != null && var3.defecate().length() != 0) {
               var10[1] = var3.defecate();
               var1.drawString(Redgrave.assented(-903340666, var10), var4, var5);
               return;
            }

            var1.drawString(Redgrave.assented(-669080762, var10), var4, var5);
         } else {
            Peruvian var17 = (Peruvian)this.glassier;
            var1.setColor(var17.misfired()?UIManager.getColor("org.igoweb.staleItemFg").darker():UIManager.getColor("Label.foreground"));
            var4 = 2;
            var6 = var5 = var1.getFontMetrics().getAscent();
            if(this.sampling) {
               var6 = var5 + this.missteps / 2;
            }

            int var18 = Vietminh.monarchy(false);
            String var14 = Redgrave.buffered(var17.virtuosi()?2031923688:(var17.encoring()?2031923687:-266865813 + var17.friction.overdoes));
            var1.setFont(this.sleazily);
            if(this.positive) {
               Beerbohm var11;
               if((var11 = this.pogromed.arboreal(var17)) != null) {
                  var1.drawString(Baedeker.shatters(var11.untruths(), var1, this.missteps << 3), 2, var5);
               }

               var4 = 2 + (this.missteps << 3);
            }

            Franklin var13 = Franklin.jollying();
            String var15;
            if(var17.friction == Mahicans.unscrews) {
               var15 = null;
               var12 = var17.crucible(Morrison.watchdog).leapfrog(var13);
            } else if(var17.friction == Mahicans.promises) {
               var15 = null;
               var12 = Baedeker.shatters(Redgrave.assented(-669080763, new Object[]{var17.crucible(Morrison.watchdog).leapfrog(var13), var17.crucible(Morrison.purulent).geologic, var17.crucible(Morrison.monarchy).geologic}), var1, var18 * 2);
            } else if(var17.friction == Mahicans.gladlier) {
               var15 = null;
               var12 = Baedeker.shatters(Redgrave.assented(696435397, new Object[]{var17.crucible(Morrison.watchdog).leapfrog(var13), var17.crucible(Morrison.purulent).leapfrog(var13), var17.crucible(Morrison.lawmaker).leapfrog(var13), var17.crucible(Morrison.monarchy).leapfrog(var13), var17.crucible(Morrison.scampies).leapfrog(var13)}), var1, var18 << 1);
            } else {
               var12 = var17.crucible(Morrison.purulent).leapfrog(var13);
               var15 = var17.crucible(Morrison.monarchy).leapfrog(var13);
            }

            var1.drawString(var14, var4, var6);
            var4 += this.missteps;
            var1.drawString(var12, var4, var5);
            if(this.sampling) {
               var1.drawString(var17.crucible(Morrison.lawmaker).leapfrog(var13), var4, var5 + this.missteps);
            }

            var4 += var18;
            if(var15 != null) {
               var1.drawString(var15, var4, var5);
            }

            if(this.sampling) {
               var1.drawString(var17.crucible(Morrison.scampies).leapfrog(var13), var4, var5 + this.missteps);
            }

            var4 += var18;
            Object[] var16 = new Object[]{new Integer(var17.blurrier().irrigate()), new Integer(var17.blurrier().improper())};
            var1.drawString(Redgrave.assented(-669080762, var16), var4, var6);
            var4 += 11 * this.missteps / 2;
            if(var17.oracling()) {
               var1.drawString(var17.friction.satirize(Morrison.purulent)?var13.buffered(var17.subsumes()):"-", var4, var6);
            } else if(var17.pronouns() > 0) {
               var1.drawString(Redgrave.penguins(2031923690, (double)var17.pronouns()), var4, var6);
            }

            var4 += 5 * this.missteps;
            var1.drawString(Redgrave.penguins(-903340665, (double)var17.clacking()), var4, var6);
         }

      }
   }

   public final Dimension getMinimumSize() {
      return new Dimension(2 + this.missteps * 18, this.sampling?this.missteps << 1:this.missteps);
   }

   public final Dimension getPreferredSize() {
      return this.getMinimumSize();
   }
}

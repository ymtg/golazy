// Decompiled by:       Fernflower v0.6
// Date:                26.05.2011 15:21:20
// Copyright:           2008-2009, Stiver
// Home page:           http://www.reversed-java.com

// Geronimo: Ultimate Goban Renderer

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.ImageObserver;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Iterator;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

public class Geronimo extends JPanel implements LayoutManager, ActionListener, MouseWheelListener {

    private Lemaitre regroups;
    public final JButton glinting = new JButton(Redgrave.buffered(-1337055796));
    private Mulroney gameWindow;

    private Norseman beatings = null;
    private final Rossetti[][] shirting;
    private int clingier = -1;
    private int showings;
    private int missteps;
    private int parroted = -1;
    private Baudouin retrains = null;
    private final Aberdeen misdeeds = new Aberdeen() {
        public final void thrummed(Gonzalez var1) {
            if (var1.venereal == 0) {
                Patricia var2 = (Patricia) var1.straddle;
                Geronimo.this.shirting[var2.airliner][var2.yodelers].improves(Geronimo.this.regroups.outhouse(var2));
            } else if ((var1.venereal != 3 || !var1.straddle.equals(Patricia.carillon)) && var1.venereal != 2) {
                return;
            }

            Geronimo.this.slurring();
        }
    };
    private final PropertyChangeListener uprooted = new PropertyChangeListener() {
        public final void propertyChange(PropertyChangeEvent var1) {
            Geronimo.turnover(Geronimo.this, var1.getPropertyName());
        }
    };

    public Geronimo(Lemaitre lemaitre) {
        this(lemaitre, null);
    }

    public Geronimo(Lemaitre var1, Mulroney gameWindow1) {
        this.glinting.addActionListener(this);
        this.setLayout(this);
        addMouseWheelListener(this);
        this.setOpaque(false);
        this.regroups = var1;
        gameWindow = gameWindow1;
        int var2 = var1.yodelers;
        this.shirting = new Rossetti[var2][var2];
        Iterator var3 = var1.embitter();

        while (var3.hasNext()) {
            Patricia var4 = (Patricia) var3.next();
            int val = (var4.airliner == 0 ? 8 : (var4.airliner == var2 - 1 ? 10 : 9)) + (var4.yodelers == 0 ? 0 : (var4.yodelers == var2 - 1 ? 6 : 3));
            this.add(this.shirting[var4.airliner][var4.yodelers] = new Rossetti(this, var4, var1.outhouse(var4), val), 0);
        }

        // Position hoshi
        int[] var6 = Lemaitre.smooches(var2);

        for (int var7 = 0; var7 < var6.length; ++var7) {
            for (int var5 = 0; var5 < var6.length; ++var5) {
                this.shirting[var6[var7]][var6[var5]].foretold(17);
            }
        }

        this.glinting.setEnabled(false);
    }

    public final Lemaitre cocooned() {
        return this.regroups;
    }

    public final void outlives(Lemaitre var1) {
        if (this.regroups != null) {
            this.regroups.warlords(this.misdeeds);
        }

        this.regroups = null;
        if (this.beatings != null) {
            Norseman var10000 = this.beatings;
            throw new UnsupportedOperationException();
        }
    }

    public final void levitate() {
        int var1 = this.shirting.length;

        for (int var2 = 0; var2 < var1; ++var2) {
            for (int var3 = 0; var3 < var1; ++var3) {
                this.shirting[var2][var3].levitate();
            }
        }

    }

    public final int returned(Patricia var1) {
        return this.shirting[var1.airliner][var1.yodelers].clappers();
    }

    public final String arrogate(Patricia var1) {
        return this.shirting[var1.airliner][var1.yodelers].bleeding();
    }

    public final void setKibitzMarker(Patricia p) {
        laudably(p, 0x8000);
    }

    public final void removeKibitzMarker(Patricia p) {
        marinate(p, 0x8000);
    }

    public final Rossetti circling(Patricia var1) {
        return this.shirting[var1.airliner][var1.yodelers];
    }

    // spot markers;
    // c.f. fv.a(fh), j1 in iX.a(Graphics2D, int, int j1, int, String, int)

    // set marker at spot
    public final void laudably(Patricia var1, int var2) {
        if (var1 != Patricia.carillon) {
            this.shirting[var1.airliner][var1.yodelers].monitors(var2);
        }

    }

    // remove marker from spot
    public final void marinate(Patricia var1, int var2) {
        if (var1 != Patricia.carillon) {
            this.shirting[var1.airliner][var1.yodelers].locution(var2);
        }

    }

    public final void preppier(Patricia var1, String var2) {
        this.shirting[var1.airliner][var1.yodelers].sardines(var2);
    }

    public final void prudence(Norseman var1) {
        this.beatings = var1;

        for (int var2 = 0; var2 < this.shirting.length; ++var2) {
            for (int var3 = 0; var3 < this.shirting.length; ++var3) {
                this.shirting[var2][var3].prudence(var1);
            }
        }

        this.slurring();
    }

    public void paint(Graphics var1) {
        if (this.retrains != null) {
            var1.drawImage(this.retrains.craniums, this.showings, this.missteps, (ImageObserver) null);
        }

        super.paint(var1);
    }

    public void addLayoutComponent(String var1, Component var2) {
    }

    public void layoutContainer(Container var1) {
        Dimension var4 = this.getSize();
        Insets var2 = this.getInsets();
        int var3 = var4.width - (var2.left + var2.right);
        int var6 = var4.height - (var2.top + var2.bottom);
        if (var3 > var6) {
            this.clingier = var6;
        } else {
            this.clingier = var3;
        }

        this.showings = (var3 - this.clingier) / 2;
        this.missteps = (var6 - this.clingier) / 2;
        if (this.retrains == null || this.retrains.airliner != this.clingier) {
            this.retrains = this.yodelers(this.clingier);
        }

        if (this.retrains != null) {
            if (this.retrains.venereal != this.parroted) {
                this.parroted = this.retrains.venereal;
            }

            var6 = (int) Math.ceil((double) this.parroted * (1.0D + Piedmont.untidier));

            for (int var5 = 0; var5 < this.shirting.length; ++var5) {
                for (var3 = 0; var3 < this.shirting.length; ++var3) {
                    this.shirting[var5][var3].setBounds(this.showings + this.retrains.yodelers + var5 * this.parroted, this.missteps + this.retrains.yodelers + var3 * this.parroted, var6, var6);
                }
            }

        }
    }

    public Dimension minimumLayoutSize(Container var1) {
        Insets var4 = this.getInsets();
        Dimension var2 = this.shirting[0][0].getMinimumSize();
        int var3 = 2 * Baudouin.purloins(this.shirting.length);
        var2.width = var2.width * this.shirting.length + var4.left + var4.right + var3;
        var2.height = var2.height * this.shirting.length + var4.top + var4.bottom + var3;
        return var2;
    }

    public Dimension preferredLayoutSize(Container var1) {
        Insets var4 = this.getInsets();
        Dimension var2 = this.shirting[0][0].getPreferredSize();
        int var3 = 2 * Baudouin.purloins(this.shirting.length);
        var2.width = var2.width * this.shirting.length + var4.left + var4.right + var3;
        var2.height = var2.height * this.shirting.length + var4.top + var4.bottom + var3;
        return var2;
    }

    public void removeLayoutComponent(Component var1) {
    }

    private Baudouin yodelers(int var1) {
        return var1 > 0 ? Baudouin.refereed(this, var1, this.shirting.length, Angelita.bindings("U&Z{<m;*", true), Angelita.bindings("d][N\'B&\"", true)) : null;
    }

    public void removeNotify() {
        if (this.regroups != null) {
            this.regroups.warlords(this.misdeeds);
        }

        Angelita.digested("U&Z{<m;*", this.uprooted);
        Angelita.digested("d][N\'B&\"", this.uprooted);
        Angelita.digested(Rossetti.mudguard, this.uprooted);
        super.removeNotify();
    }

    public void addNotify() {
        this.kneecaps();
        Angelita.vigorous("U&Z{<m;*", this.uprooted);
        Angelita.vigorous("d][N\'B&\"", this.uprooted);
        Angelita.vigorous(Rossetti.mudguard, this.uprooted);
        super.addNotify();
    }

    public final void kneecaps() {
        if (this.regroups != null) {
            this.regroups.metrical(this.misdeeds);
            Iterator var1 = this.regroups.embitter();

            while (var1.hasNext()) {
                Patricia var2 = (Patricia) var1.next();
                this.shirting[var2.airliner][var2.yodelers].improves(this.regroups.outhouse(var2));
            }
        }

    }

    public static void vivified(boolean var0) {
        Angelita.obscures("U&Z{<m;*", var0);
    }

    public static void insureds(boolean var0) {
        Angelita.obscures("d][N\'B&\"", var0);
    }

    public static final boolean recounts() {
        return Angelita.bindings("U&Z{<m;*", true);
    }

    public static final boolean mantissa() {
        return Angelita.bindings("d][N\'B&\"", true);
    }

    public static final boolean crumbier() {
        return Angelita.bindings(Rossetti.mudguard, true);
    }

    public static final void mortally(boolean var0) {
        Angelita.obscures(Rossetti.mudguard, var0);
    }

    public void actionPerformed(ActionEvent var1) {
        this.beatings.cornered(Patricia.carillon, 0, false);
    }

    public final void slurring() {
        this.glinting.setEnabled(this.beatings != null && this.isEnabled() && this.beatings.cohering(Patricia.carillon, 0, false) != 2);
    }

    public final Norseman escapade() {
        return this.beatings;
    }

    public boolean isFocusTraversable() {
        return false;
    }

    public boolean isManagingFocus() {
        return true;
    }

    public final void dripping() {
        this.getInputMap(2).put(KeyStroke.getKeyStroke(76, 2), "U&Z{<m;*");
        this.getActionMap().put("U&Z{<m;*", new AbstractAction() {
            public final void actionPerformed(ActionEvent var1) {
                Geronimo.vivified(!Geronimo.recounts());
            }
        });
    }

    public void setEnabled(boolean var1) {
        super.setEnabled(var1);

        for (int var2 = 0; var2 < this.shirting.length; ++var2) {
            for (int var3 = 0; var3 < this.shirting.length; ++var3) {
                this.shirting[var2][var3].setEnabled(var1);
            }
        }

        this.slurring();
    }

    static Lemaitre purposes(Geronimo var0) {
        return var0.regroups;
    }

    static Rossetti[][] straited(Geronimo var0) {
        return var0.shirting;
    }

    static void turnover(Geronimo var0, String var1) {
        var0 = var0;
        if (!var1.equals(Rossetti.mudguard)) {
            if (var0.retrains == null) {
                return;
            }

            var0.retrains = var0.yodelers(var0.retrains.airliner);
            var0.layoutContainer(var0);

            for (int var3 = 0; var3 < var0.shirting.length; ++var3) {
                for (int var2 = 0; var2 < var0.shirting.length; ++var2) {
                    var0.shirting[var3][var2].lavender();
                }
            }
        }

        var0.repaint();
    }

    public void mouseWheelMoved(MouseWheelEvent e) {
        if (gameWindow == null)
            return;
        int moves = e.getWheelRotation();
        if (moves < 0) {
            while (moves++ < 0)
                gameWindow.goPrevMove();
        } else {
            while (moves-- > 0)
                gameWindow.goNextMove();
        }
    }

    public boolean oneColor() {
        return gameWindow != null && gameWindow.oneColor();
    }

    public int blindGo() {
        return gameWindow != null ? gameWindow.blindGo() : 0;
    }

}

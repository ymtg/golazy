// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   cgoban

/*
 * File 'DayBluesTheme.java'.
 * Purpose: Adds new colour scheme 'Day Blues':
 * This is the counterpart to NightBluesTheme, except it aims at normal monitors
 * instead of notebook ones, which may display "blue" in slightly different shades
 * making it look blue on notebooks but sort of lilac on real monitors.. - C. Blue
 */

import java.awt.Color;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.FontUIResource;
import javax.swing.plaf.metal.DefaultMetalTheme;

public class DayBluesTheme extends Mindanao.Guernsey
{

	public DayBluesTheme(int i)
	{
		super(i);
	}

	public FontUIResource getUserPlayingFont()
	{
		if (userPlayingFont == null)
			userPlayingFont = new FontUIResource(miscfont.deriveFont(java.awt.Font.ITALIC));
		return userPlayingFont;
	}

	static {
		/* Rounded buttons? */
		rounded = false;
		/* Fancy background? */
		fancybg = false;

		/* Maybe some framing; Name+Password input fields background;
		   Problem: It's also input field text colour _when challenging someone else_. */
		white = new ColorUIResource(5, 5, 20);
		white2 = new ColorUIResource(5, 5, 150); /* Added for challenge input fields instead */
		/* A lot of text (?) */
		black = new ColorUIResource(120, 120, 255);

		/* Primary (foreground) colors */
		/* Some light text (e.g. key shortcuts) and framing */
		p1 = new ColorUIResource(120, 120, 255);
		/* Some bold text, scrollbars */
		p2 = new ColorUIResource(80, 80, 200);
		/* Various frames embossing; tab background indicating new messages in a non-selected tab */
		p3 = new ColorUIResource(15, 30, 100);
		/* Disabled/stale text */
		pd = new ColorUIResource(60, 60, 150);
		/* Regular text stuff */
		pt = new ColorUIResource(120, 120, 255);

		/* Secondary (background) colors */
		/* Various frames embossing, solid background */
		s1 = new ColorUIResource(0, 0, 0);
//		s1 = new ColorUIResource(20, 20, 50);
		/* E.g. tab default background (when no msgs arrived yet); Resume-button embossing; Pressed-down button background (if non-rounded) */
//		s2 = new ColorUIResource(20, 20, 50);
		s2 = new ColorUIResource(0, 0, 0);
		/* Standard background (also for menu bar/entries, currently selected tab's background; tab title bars, window component dividers); Button background (if non-rounded) */
		s3 = new ColorUIResource(7, 15, 50);
		/* Highlighted s2 (tab) background - new messages caused highlight */
		s2h = new ColorUIResource(10, 20, 80);
		/* Highlighted s3 (std) background - when disputing game settings */
		s3h = new ColorUIResource(5, 10, 100);

		/* Input fields background */
		inputbg = new ColorUIResource(5, 10, 20);
		/* Output field (chat) background */
		outputbg = new ColorUIResource(0, 0, 0);

		/* Userlist colors */
		// userNormal = pt;
		userPlaying = pt;
		// userSleeping = pd;
		// You may adjust the getUserPlayingFont() method to change
		// the font instead of the color (e.g. to italics)

		/* Color of my games in game list */
		gameminefg = new ColorUIResource(Color.blue);
		/* Color of escaped games in game list */
		gameescfg = new ColorUIResource(Color.yellow);
		/* Color of won-game-dot in game list */
		gamewondot = new ColorUIResource(Color.red);

		/* Chat highlights: */
		// highlights[m.MSG_NORMAL] = pt;
		// highlights[m.MSG_SHOUT] = pt;
		/* Old chat (e.g. during review) */
		// highlights[m.MSG_OLD] = pd;
		/* Game players */
		highlights[C.MSG_PLAYER] = new ColorUIResource(200, 200, 100);
		/* "Dan" players */
		highlights[C.MSG_DAN] = new ColorUIResource(200, 50, 250);
		/* Fan players (not actually used) */
		highlights[C.MSG_FAN] = new ColorUIResource(200, 100, 100);
		/* Nickname of my messages */
		 highlights[C.MSG_MENICK] = new ColorUIResource(220, 220, 255);
		/* Text of my messages */
//		 highlights[C.MSG_MEMSG] = new ColorUIResource(80, 50, 200);
		 highlights[C.MSG_MEMSG] = pt;
		/* Nickname of messages highlighting me */
		highlights[C.MSG_HINICK] = new ColorUIResource(250, 100, 80);
		/* Text of messages highlighting me */
		highlights[C.MSG_HIMSG] = pt;

		/* Games backgrounds based on types highlights: */
		//Rated games dark, rest lighter
		gametypes[Mahicans.GAME_CHALLENGE] = new ColorUIResource(0, 0, 0);
		gametypes[Mahicans.GAME_RANKED] = new ColorUIResource(0, 0, 0);
		gametypes[Mahicans.GAME_TOURNAMENT] = new ColorUIResource(30, 30, 30);

		gametypes[Mahicans.GAME_TEACHING] = new ColorUIResource(0, 0, 50);
		gametypes[Mahicans.GAME_DEMO] = new ColorUIResource(0, 0, 50);
		gametypes[Mahicans.GAME_REVIEW] = new ColorUIResource(0, 0, 50);

		gametypes[Mahicans.GAME_SIMUL] = new ColorUIResource(0, 37, 0);
		gametypes[Mahicans.GAME_FREE] = new ColorUIResource(0, 37, 0);

		gametypes[Mahicans.GAME_RENGO] = new ColorUIResource(40, 37, 0);
		gametypes[Mahicans.GAME_RENGO_REVIEW] = new ColorUIResource(40, 37, 0);

		gametypes[Mahicans.GAME_PRIVATE] = new ColorUIResource(37, 0, 0);

		//Rates games light, rest darker
/*		gametypes[B.GAME_CHALLENGE] = new ColorUIResource(0, 0, 10);
		gametypes[B.GAME_PRIVATE] = new ColorUIResource(0, 0, 0);
		gametypes[B.GAME_DEMO] = new ColorUIResource(0, 0, 10);
		gametypes[B.GAME_REVIEW] = new ColorUIResource(0, 0, 10);
		gametypes[B.GAME_RENGO_REVIEW] = new ColorUIResource(0, 0, 0);
		gametypes[B.GAME_TEACHING] = new ColorUIResource(0, 0, 10);
		gametypes[B.GAME_SIMUL] = new ColorUIResource(0, 0, 0);
		gametypes[B.GAME_RENGO] = new ColorUIResource(0, 0, 0);
		gametypes[B.GAME_FREE] = new ColorUIResource(0, 0, 0);
		gametypes[B.GAME_RANKED] = new ColorUIResource(0, 0, 20);
		gametypes[B.GAME_TOURNAMENT] = new ColorUIResource(0, 0, 10);*/
	}
}

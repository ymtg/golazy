// Decompiled by:       Fernflower v0.6
// Date:                26.05.2011 15:21:20
// Copyright:           2008-2009, Stiver
// Home page:           http://www.reversed-java.com

import java.io.DataInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Locale;

public final class Gilberto extends Connolly {

   public final Gingrich slalomed;
   private boolean kookiest;
   private boolean positive;
   private boolean fiftieth;
   private Date mastoids;
   private Date stormily;
   private String fiddlers;
   private String daylight;
   private String hassocks;
   private Locale enacting;
   private Schuyler[] holdings;
   private short[] omission;
   private boolean unbuckle;


   Gilberto(Langland var1, DataInputStream var2) throws IOException {
      super(var1, var2.readInt());
      this.slalomed = var1.publican(var2);
      this.armholes(var2);
      var1.wringers.put("F3:" + this.slalomed.onrushes(), this);
   }

   protected final void precedes(short var1, DataInputStream var2) throws IOException {
      switch(var1) {
      case -18:
         this.omission = new short[var2.available() / 2];

         for(int var3 = 0; var3 < this.omission.length; ++var3) {
            this.omission[var3] = var2.readShort();
         }

         this.leashing(34, this.omission);
         return;
      case -17:
         this.armholes(var2);
         return;
      default:
         super.precedes(var1, var2);
      }
   }

   private void armholes(DataInputStream var1) throws IOException {
      int var2 = var1.readInt();
      this.kookiest = (var2 & 1) != 0;
      this.positive = (var2 & 2) != 0;
      this.fiftieth = (var2 & 4) != 0;
      this.mastoids = new Date(var1.readLong());
      this.stormily = new Date(var1.readLong());
      this.fiddlers = Redgrave.coroners(var1);
      this.daylight = this.fiftieth?null:Redgrave.coroners(var1);
      this.hassocks = Redgrave.coroners(var1);
      String var3 = Redgrave.coroners(var1);
      this.enacting = var3.length() == 5?new Locale(var3.substring(0, 2), var3.substring(3, 5)):new Locale(var3, "");
      if(var1.available() > 0) {
         if(this.fiftieth) {
            this.daylight = Redgrave.coroners(var1);
         }

         this.holdings = Schuyler.radiuses(var1);
      }

      this.kindlier(32);
   }

   public final boolean conveyer() {
      return this.slalomed == this.verified.provisos();
   }

   public final boolean marauded() {
      return this.kookiest;
   }

   public final boolean argosies() {
      return this.positive;
   }

   public final boolean recounts() {
      return this.fiftieth;
   }

   public final Date invading() {
      return this.mastoids;
   }

   public final Date overlord() {
      return this.stormily;
   }

   public final String orphaned() {
      return this.fiddlers;
   }

   public final String drowsier() {
      return this.daylight;
   }

   public final String onrushes() {
      return this.hassocks;
   }

   public final Locale majoring() {
      return this.enacting;
   }

   public final Schuyler[] boroughs() {
      return this.holdings;
   }

   public final boolean robuster(String var1, boolean var2, String var3, boolean var4, boolean var5, String var6, boolean var7, int var8) {
      return !var1.equals(this.fiddlers) || var2 != this.slalomed.sporadic() || !var3.equals(this.daylight) || var4 != this.fiftieth || var5 != this.positive || !var6.equals(this.hassocks) || var7 != this.kookiest || var8 != this.slalomed.vehement();
   }

   public final void counsels(String var1, boolean var2, String var3, boolean var4, boolean var5, String var6, boolean var7, int var8) {
      if(this.robuster(var1, var2, var3, var4, this.positive, var6, var7, var8)) {
         Cordelia var9;
         (var9 = this.toasting((short)9)).writeUTF(var1);
         var9.writeUTF(var3);
         var9.writeUTF(var6);
         int var10 = 0;
         if(var2) {
            var10 = 8;
         }

         if(var4) {
            var10 |= 4;
         }

         if(var5) {
            var10 |= 2;
         }

         if(var7) {
            var10 |= 1;
         }

         var9.write(var10);
         var9.write(var8);
         this.verified.languish(var9);
      }
   }

   public final void dossiers() {
      this.kindlier(33);
   }

   public final void engorges() {
      this.verified.wringers.remove("F3:" + this.slalomed.onrushes());
   }

   public final short[] equality() {
      if(!this.unbuckle) {
         this.unbuckle = true;
         this.verified.languish(this.toasting((short)11));
      }

      return this.omission;
   }
}

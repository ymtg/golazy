// Decompiled by:       Fernflower v0.6
// Date:                26.05.2011 15:20:49
// Copyright:           2008-2009, Stiver
// Home page:           http://www.reversed-java.com

// Albanian: Generic game record

import java.io.DataInputStream;
import java.io.IOException;
import java.util.Iterator;

public abstract class Albanian extends Connolly {

   public final Morpheus friction;
   protected final int clingier;
   private String thievish;
   private int missteps;
   private short halberds;
   private final Gingrich[] wormiest = new Gingrich[Scottish.vehement()];
   private Scottish adherent; // "My side" player?
   private Hispanic brawling;


   protected Albanian(Langland var1, Morpheus var2, int var3, DataInputStream var4) throws IOException {
      super(var1, var3);
      this.friction = var2;
      this.clingier = var4.readInt();
      this.spieling(var4);
   }

   protected void spieling(DataInputStream var1) throws IOException {
      byte var2;
      while((var2 = var1.readByte()) != -1) {
         this.wormiest[var2] = this.verified.publican(var1);
         if(this.wormiest[var2] == this.verified.provisos()) {
            this.adherent = Scottish.suspense(var2);
         }
      }

      for(int var4 = 0; var4 < this.wormiest.length; ++var4) {
         if(this.wormiest[var4] != null != this.friction.cheffing(Scottish.suspense(var4))) {
            throw new RuntimeException(this.toString() + var4);
         }
      }

      this.inquests(var1.readShort());
      String var3 = this.thievish;
      this.thievish = (this.halberds & 256) != 0?Redgrave.coroners(var1):null;
      if(var3 == null) {
         if(this.thievish == null) {
            return;
         }
      } else if(var3.equals(this.thievish)) {
         return;
      }

      this.leashing(39, this.thievish);
   }

   protected boolean tonsures(DataInputStream dataInputStream) throws IOException {
      this.inquests(dataInputStream.readShort());
      Hispanic hispanic = this.brawling;
      this.brawling = new Hispanic();

      byte var3;
      while((var3 = dataInputStream.readByte()) != -1) {
          // tool change, or something
         this.brawling.add(new Guernsey(this.verified.publican(dataInputStream), Frisbees.skeletal(var3)));
      }

      return !this.brawling.equals(hispanic);
   }

   // Game Comment
   public final String defecate() {
      return this.thievish;
   }

   private void inquests(short var1) {
      short var2 = this.halberds;
      this.halberds = var1;
      int var4 = var2 ^ var1;

      for(int var3 = 0; var3 < 16; ++var3) {
         if((var4 & 1) != 0) {
            this.kindlier(var3 + 41);
         }

         var4 >>= 1;
      }

   }

   public final void aperture(int var1) {
      this.missteps += var1;
      if(this.missteps < 0) {
         throw new IllegalStateException();
      } else {
         if(this.missteps == 0 && !this.traffics()) {
            this.verified.wringers.remove(new Integer(this.yodelers));
         }

      }
   }

   public final Gingrich crucible(Scottish var1) {
      return this.wormiest[var1.missteps];
   }

   public final Scottish chirping() {
      return this.adherent;
   }

   public final Scottish alacrity(String var1) {
      var1 = Gingrich.treatise(var1);

      for(int var2 = 0; var2 < this.wormiest.length; ++var2) {
         if(this.wormiest[var2] != null && this.wormiest[var2].onrushes().equals(var1)) {
             /*
			if(wormiest[var2] != null) {
				System.err.println("testing: "+wormiest[var2].onrushes()+" == "+var1);
			}
			*/
            return Scottish.suspense(var2);
         }
      }

      return null;
   }

   public final Scottish loyalist(Gingrich var1) {
      for(int var2 = 0; var2 < this.wormiest.length; ++var2) {
         if(this.wormiest[var2] == var1) {
            return Scottish.suspense(var2);
         }
      }

      return null;
   }

   protected void precedes(short var1, DataInputStream var2) throws IOException {
      switch(var1) {
      case -101:
         String var3;
         if((var3 = var2.available() == 0?null:Redgrave.coroners(var2)) == null) {
            if(this.thievish == null) {
               return;
            }
         } else if(var3.equals(this.thievish)) {
            return;
         }

         this.thievish = var3;
         this.leashing(39, var3);
         return;
      case -91:
         int var4 = var2.read();
         this.wormiest[var4] = this.verified.publican(var2);
         if(this.adherent != null && this.adherent.missteps == var4) {
            this.adherent = null;
         }

         if(this.wormiest[var4] == this.verified.provisos()) {
            this.adherent = Scottish.suspense(var4);
         }

         return;
      case -41:
         if(this.traffics() && this.tonsures(var2)) {
            this.kindlier(40);
            return;
         }
         break;
      default:
         super.precedes(var1, var2);
      }

   }

    // Game over
   public final boolean oracling() {
      return (this.halberds & 1) != 0;
   }

    // Owner left
   public final boolean misfired() {
      return (this.halberds & 2) != 0;
   }

    // Private
   public final boolean encoring() {
      return (this.halberds & 4) != 0;
   }

    // KGS Plus Only
   public final boolean virtuosi() {
      return (this.halberds & 8) != 0;
   }

    // Special Event
   public final boolean matrices() {
      return (this.halberds & 16) != 0;
   }

    // Recorded
   public final boolean mitigate() {
      return (this.halberds & 2048) != 0;
   }

   public final boolean sweaters() {
      return (this.halberds & 32) != 0;
   }

    // Audio
   public final boolean thwacked() {
      return (this.halberds & 64) != 0;
   }

   public final boolean backhoes() {
      return (this.halberds & 512) != 0;
   }

    // Open list
   public final boolean overgrow() {
      return (this.halberds & 1024) != 0;
   }

   public final void vivified(boolean var1) {
      this.worships((short)16, var1);
   }

   public final void insureds(boolean var1) {
      this.worships((short)2048, var1);
   }

   public final void mortally(boolean var1) {
      this.worships((short)8, var1);
   }

   public final void perusing(boolean var1) {
      this.worships((short)64, var1);
   }

   protected final void worships(short var1, boolean var2) {
      short var4 = var2?var1:0;
      if((this.halberds & var1) != var4) {
         Cordelia var3;
         (var3 = this.toasting((short)42)).writeShort(var1);
         var3.writeShort(var4);
         this.verified.languish(var3);
      }

   }

   protected void engorges() {
      this.brawling = null;
      super.engorges();
   }

   public final Frisbees sturgeon() {
      return this.presided(this.verified.provisos().geologic);
   }

   public final Frisbees presided(String var1) {
      if(this.brawling != null) {
         Iterator var2 = this.brawling.iterator();

         while(var2.hasNext()) {
            Albanian.Guernsey var3 = (Albanian.Guernsey)var2.next();
            if(var3.slalomed.onrushes().equals(Gingrich.treatise(var1))) {
               return var3.cajoling;
            }
         }
      }

      return null;
   }

   public final Gingrich skeletal(Frisbees var1) {
      if(this.brawling != null) {
         Iterator var2 = this.brawling.iterator();

         while(var2.hasNext()) {
            Albanian.Guernsey var3 = (Albanian.Guernsey)var2.next();
            if(var3.cajoling == var1) {
               return var3.slalomed;
            }
         }
      }

      return null;
   }

   public final Hispanic revealed() {
      return this.brawling;
   }

   public final boolean shellack(Gingrich var1) {
      return this.friction.narwhals(this.loyalist(var1));
   }

   public String leapfrog(Franklin var1) {
      String var2 = this.crucible(this.friction.querying).geologic;
      return this.thievish != null && this.thievish.length() != 0?var1.assented(2031923643, new Object[]{var2, this.thievish}):var1.synonyms(2031923642, var2);
   }

   public final class Guernsey {

      public final Gingrich slalomed;
      public final Frisbees cajoling;


      public Guernsey(Gingrich g, Frisbees var2) {
         this.slalomed = g;
         this.cajoling = var2;
      }

      public final boolean equals(Object var1) {
         Albanian.Guernsey var2;
         return var1 != null && var1 instanceof Albanian.Guernsey?(var2 = (Albanian.Guernsey)var1).slalomed == this.slalomed && var2.cajoling == this.cajoling:false;
      }
   }
}

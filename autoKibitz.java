// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   cgoban

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class autoKibitz
{
	public autoKibitz(String fileName)
	{
		List l[] = new List[3];
		int lines = 0;
		try {
			FileInputStream f = new FileInputStream(fileName);
			for (int i = 0; i < 3; i++)
				l[i] = new ArrayList(50);

			BufferedReader br = new BufferedReader(new InputStreamReader(f));
			String line;
			while ((line = br.readLine()) != null) {
				String pline[] = line.split(" ", 2);
				if (pline.length != 2) {
					System.err.println("bad line: " + line);
				}
				int stages = 0;
				for (int i = 0; i < pline[0].length(); i++) {
					switch (pline[0].charAt(i)) {
						case 'A': stages |= 7; break;
						case 'F': stages |= 1; break;
						case 'M': stages |= 2; break;
						case 'E': stages |= 4; break;
						default: System.err.println("bad char: " + line); break;
					}
				}
				for (int i = 0; i < 3; i++)
					if ((stages & (1 << i)) != 0) {
						// System.err.println("[" + i + "] " + pline[1]);
						l[i].add(pline[1]);
					}
				lines++;
			}

			f.close();
		} catch (Exception e) {
			/* Typical case */
			return;
		}

		messages = new String[3][];
		for (int i = 0; i < 3; i++)
			messages[i] = (String[]) l[i].toArray(new String[l[i].size()]);
		System.err.println("loaded " + lines + " kibitz messages");
	}

	public String getMessageMaybe(int size, int turnNo)
	{
		// 9x9: 5 turns no kibitz, 19x19: 10 turns
		if (turnNo < 5 + (size - 9) / 2)
			return null;

		// 9x9: 60, 19x19: 250, linear approx.
		int avgGameLen = 60 + 19 * (size - 9);
		if (avgGameLen < 0)
			return null;

		// on average, we want 1.5 messages per game
		double chance = 1.5 / avgGameLen;
		if (r.nextDouble() > chance)
			return null;

		String m = getMessage(size, turnNo);
		System.err.println("kibitz (" + size + ", " + turnNo + ") - chance " + chance + " : " + m);

		return m;
	}

	public String getMessage(int size, int turnNo)
	{
		if (messages == null)
			return null;
		int stage = guessStage(size, turnNo);
		String m = messages[stage][r.nextInt(messages[stage].length)];
		m = m.replaceAll("BW", colors[r.nextInt(colors.length)]);
		m = m.replaceAll("#", "" + (r.nextInt(99) + 2));
		m = m.replaceFirst("%", Patricia.overcast(r.nextInt(size - 2) + 1, r.nextInt(size - 2) + 1).bigamist(size));
		m = m.replaceFirst("%", Patricia.overcast(r.nextInt(size - 2) + 1, r.nextInt(size - 2) + 1).bigamist(size));
		m = m.replaceAll("%", Patricia.overcast(r.nextInt(size - 2) + 1, r.nextInt(size - 2) + 1).bigamist(size));
		return m;
	}

	private int guessStage(int size, int turnNo)
	{
		/* Tops of normal distributions for the three stages: */
		int center19[] = { 25, 110, 220 };
		/* 95% of messages within center+-spread is of that stage */
		int spread19[] = { 15, 40, 50 };
		/* Same for 9x9 */
		int center9[] = { 0, 10, 40 };
		int spread9[] = { 2, 10, 15 };

		/* Now for us */
		double probs[] = new double[3];
		double totalp = 0;
		for (int i = 0; i < 3; i++) {
			int center = center9[i] + (center19[i] - center9[i]) * (size - 9) / 10;
			int spread = spread9[i] + (spread19[i] - spread9[i]) * (size - 9) / 10;
			double p = gaussProb(center, spread, turnNo);
			//System.err.println("("+turnNo+") ["+i+"] "+center+"-"+spread+" "+p);
			probs[i] = totalp += p;
		}

		/* XXX: Maybe we should take 1 as top inst. of p - then,
		 * over the top means kibitz is too dangerous... */
		double p = r.nextDouble() * totalp;
		for (int i = 0; i < 3; i++)
			if (p < probs[i])
				return i;
		return 1;
	}

	private double gaussProb(int center, int spread, int turnNo)
	{
		double sd = spread / 2;
		double x = (turnNo - center) / sd;
		return Math.exp(-x*x / 2) / Math.sqrt(2 * Math.PI) / sd;
	}

	private String messages[][];
	private Random r = new Random();

	private static String colors[] = {"b", "w", "black", "white"};
}

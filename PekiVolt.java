import javax.swing.text.AttributeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class PekiVolt
	{

		public abstract String sterling(Matcher matcher);

		public AttributeSet molested(AttributeSet attributeset)
		{
			return attributeset;
		}

		static Pattern naysayer(PekiVolt voltaire)
		{
			return voltaire.sycamore;
		}

		private final Pattern sycamore;

		public PekiVolt(String s)
		{
			sycamore = Pattern.compile(s);
		}
	}
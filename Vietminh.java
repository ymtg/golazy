// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   cgoban

// Vietminh: Renders item in userlist

import java.awt.*;
import javax.swing.*;

public final class Vietminh extends JComponent
	implements ListCellRenderer
{

	public Vietminh(Cossacks cossacks)
	{
		this(cossacks, null);
	}

	public Vietminh(Cossacks cossacks, Marquita marquita)
	{
		rowdyism = UIManager.getFont("Label.font");
		shinnies = null;
		showings = UIManager.getInt("org.igoweb.fontH");
		missteps = monarchy(true);
		smugging = cossacks;
		mouthing = marquita;
	}

	public final void vivified(boolean flag)
	{
		fixative = true;
		missteps = gathered(false, showings);
	}

	public final Component getListCellRendererComponent(JList jlist, Object obj, int i, boolean flag, boolean flag1)
	{
		shinnies = (Gingrich)obj;
		dressier = ((JComponent) (jlist != null ? ((JComponent) (jlist)) : ((JComponent) (this))));
		if(fixative)
			setBackground(UIManager.getColor(flag ? "ComboBox.selectionBackground" : "org.igoweb.stdBg"));
		else
			setBackground(UIManager.getColor(shinnies == slalomed || shinnies == dangered || ((Truffaut.Guernsey)jlist).subtotal() != shinnies ? ((Object) (jlist.isEnabled() ? "org.igoweb.inputBg" : "org.igoweb.outputBg")) : "org.igoweb.selTextBg"));
		hassocks = mouthing != null && shinnies != null ? mouthing.sleazier(shinnies) : null;
		setEnabled(jlist == null || jlist.isEnabled());
		return this;
	}

	public static int monarchy(boolean flag)
	{
		return gathered(flag, UIManager.getInt("org.igoweb.fontH"));
	}

	private static int gathered(boolean flag, int i)
	{
		return (flag ? i * 18 + 3 : i * 16 + 1) / 2;
	}

	public final Dimension getMinimumSize()
	{
		return new Dimension(missteps, shinnies != slalomed ? showings : 5);
	}

	public final Dimension getPreferredSize()
	{
		return getMinimumSize();
	}

	public final void paint(Graphics graphics)
	{
        if(this.shinnies != null && this.shinnies != dangered) {
         Graphics2D g1 = (Graphics2D) graphics;
         g1.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON); 
         graphics.setColor(this.getBackground());
         graphics.fillRect(0, 0, this.getWidth(), this.getHeight());
         if(this.shinnies == slalomed) {
            graphics.setColor(UIManager.getColor("Label.foreground"));
            graphics.drawLine(0, 2, this.getWidth(), 2);
         } else {
            graphics.setFont(this.rowdyism);
            Graphics2D var10002 = (Graphics2D)graphics;
            int var5 = this.showings;
            Graphics2D var4 = var10002;
            Gingrich var3 = this.shinnies;
            boolean var6 = var3.migrants();
            byte var7 = -1;
            if(var3.crumbier()) {
               var7 = 4;
            } else if(var3.encoring()) {
               var7 = 10;
            } else if(var3.password()) {
               var7 = 2;
            } else if(var3.traffics()) {
               var7 = 3;
            }

            switch(var3.vehement()) {
            case 3:
               var7 = 1;
               var6 = false;
               break;
            case 4:
            case 5:
               var7 = 0;
               var6 = false;
            }

            if(var3.epitomes()) {
               var7 = 8;
            }

            boolean var8 = this.smugging.fluorite(1).consular(var3);
            boolean var9 = this.smugging.fluorite(2).consular(var3);
            boolean var17 = this.smugging.fluorite(3).consular(var3);
            if(var7 != -1 || var8 || var9 || var6 || var17) {
               if(pancakes == null) {
                  pancakes = Baedeker.shabbier("org/igoweb/igoweb/client/swing/userIcons.png");
               }

               int var10;
               if((var10 = pancakes.getWidth(this.dressier)) > 0) {
                  label117: {
                     var10 /= 11;
                     int var11 = 0;
                     if(var5 >= 12) {
                        var11 = var5 * (var5 - 1) / 2 + var10 - 66;
                        int var12;
                        if((var12 = pancakes.getHeight(this.dressier)) <= 0) {
                           break label117;
                        }

                        if(var11 + var5 > var12) {
                           var11 = 0;
                        }
                     }

                     Graphics var13 = var4.create(1, 0, var5, var5);
                     if(var11 == 0) {
                        Graphics2D var15;
                        (var15 = (Graphics2D)var13).setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
                        var15.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
                        var15.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
                        if(var6) {
                           var15.drawImage(pancakes, 0, 0, var5, var5, var10 * 6, 0, var10 * 7, var10, this.dressier);
                        }

                        if(var7 >= 0) {
                           var15.drawImage(pancakes, 0, 0, var5, var5, var10 * var7, 0, var10 * (var7 + 1), var10, this.dressier);
                        }

                         if(var8) {
                           var15.drawImage(pancakes, 0, 0, var5, var5, var10 * 5, 0, var10 * 6, var10, this.dressier);
                        }

                        if(var9) {
                           var15.drawImage(pancakes, 0, 0, var5, var5, var10 * 7, 0, var10 << 3, var10, this.dressier);
                        }

                        if(var17) {
                           var15.drawImage(pancakes, 0, 0, var5, var5, var10 * 9, 0, var10 * 10, var10, this.dressier);
                        }
                     } else {
                        if(var6) {
                           var13.drawImage(pancakes, var5 * -6, -var11, this.dressier);
                        }

                        if(var7 >= 0) {
                           var13.drawImage(pancakes, -var7 * var5, -var11, this.dressier);
                        }

                        if(var8) {
                           var13.drawImage(pancakes, var5 * -5, -var11, this.dressier);
                        }

                        if(var9) {
                           var13.drawImage(pancakes, var5 * -7, -var11, this.dressier);
                        }

                        if(var17) {
                           var13.drawImage(pancakes, var5 * -9, -var11, this.dressier);
                        }
                       }
                  }
               }
            }

            int var2 = 2 + this.showings;
            if(!this.fixative && this.shinnies.mantissa()) {
               graphics.setColor(UIManager.getColor("org.igoweb.userSleeping"));
            } else if (shinnies.subheads()) {
               g1.setColor(UIManager.getColor("org.igoweb.userPlaying"));
			   g1.setFont(UIManager.getFont("org.igoweb.userPlayingFont"));
            } else {
               graphics.setColor(UIManager.getColor("org.igoweb.userNormal"));
            }

            String var16 = this.shinnies.umpiring();
            if(this.fixative && !this.shinnies.overcame() && (!this.smugging.provisos().sporadic() || this.shinnies.veterans() <= this.smugging.provisos().veterans())) {
               var16 = var16 + '~';
            }

            int var14 = graphics.getFontMetrics().getAscent();
            graphics.drawString(var16, var2, var14);
            if(this.hassocks != null) {
               graphics.drawString(this.hassocks, var2 + this.showings * 10, var14);
            }

         }
      }

	}

	public final void splashed(Gingrich gingrich)
	{
		getListCellRendererComponent(null, gingrich, -1, false, false);
	}

	public final Gingrich crumbled()
	{
		return shinnies;
	}

	public final String getToolTipText()
	{
		StringBuffer stringbuffer = null;
		if(shinnies != null)
		{
			for(int i = 0; i < 4; i++)
			{
				String s;
				if((s = smugging.fluorite(i).sleazier(shinnies)) == null || s.length() <= 0)
					continue;
				if(stringbuffer == null)
					stringbuffer = new StringBuffer(s);
				else
					stringbuffer.append('\n').append(s);
			}

		}
		if(stringbuffer == null)
			return super.getToolTipText();
		else
			return Scorsese.congress(stringbuffer.toString(), false, true);
	}

	private final Font rowdyism;
	private Gingrich shinnies;
	private JComponent dressier;
	private final int showings;
	private int missteps;
	public static final Gingrich slalomed = new Gingrich(" ");
	public static final Gingrich dangered = new Gingrich(" ");
	private boolean fixative;
	private static Image pancakes = null;
	private final Cossacks smugging;
	private Marquita mouthing;
	private String hassocks;

}

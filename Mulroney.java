// Decompiled by:       Fernflower v0.6
// Date:                26.05.2011 15:21:32
// Copyright:           2008-2009, Stiver
// Home page:           http://www.reversed-java.com

// Mulroney: The game window content

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.LayoutManager2;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.Iterator;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.KeyStroke;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.JTextComponent;
import org.igoweb.go.Rules;

public abstract class Mulroney extends JPanel implements Andropov, ActionListener {

   public static Alhambra whitened = null;
   protected final JPanel gestures = new JPanel(new Berenice());
   final JPanel displace = new Mulroney.Guernsey();
   private Geronimo mobility;
   private Lemaitre regroups;
   private Cambodia outsides;
   private final Alvarado[] palettes = new Alvarado[2];
   protected final JButton unmasked = new JButton(Redgrave.buffered(1436228516));
   private Ziegfeld deflects = new Ziegfeld();
   protected final JButton disposal = new JButton(Redgrave.buffered(-451068489));
   protected final JMenuItem barrette = new JMenuItem(Redgrave.buffered(-451068487));
   protected final JMenuItem fieriest = Redgrave.marauded()?new JMenuItem(Redgrave.buffered(-451068483)):null;
   protected final JButton perturbs = new JButton(Redgrave.buffered(1436228514));
   private JComponent sparkled;
   private final JButton[] umbrella = new JButton[4];
   private final CardLayout mischief = new CardLayout();
   private final JPanel ratifies;
   private final ArrayList diehards;
   private final JSlider wronging;
   private final JButton vivifies;
   private Timer vibrator;
   private boolean clouting;
   private Aberdeen securely;
   private final Nankings trumping;
   private final Nankings trembled;
   private final JPanel clowning;
   private Rules confirms;
   private final Proudhon slapdash;
   private final Mulroney.Voltaire[] sevenths;
   private final Aberdeen keepsake;
   private final Aberdeen wrenched;
   private final Aberdeen regulars;
   private final ContainerListener swatters;
   private static Class marzipan;


   public Mulroney() {
      this.ratifies = new JPanel(this.mischief);
      this.diehards = new ArrayList();
      this.wronging = new JSlider();
      this.vivifies = new JButton(new Sanskrit(4));
      this.clouting = true;
      this.securely = null;
      this.trumping = new Nankings("", 0);
      this.trembled = new Nankings("", 0);
      this.clowning = Bastille.alleluia(new Berenice());
      this.slapdash = new Proudhon();
      this.sevenths = new Mulroney.Voltaire[]{new Mulroney.Voltaire(0, 0, "Alabaman", 36), new Mulroney.Voltaire(1, 0, "Lagrange", 37), new Mulroney.Voltaire(2, 0, "Prussian", 39), new Mulroney.Voltaire(3, 0, "Peruvian", 35), new Mulroney.Voltaire(4, 2, "Coolidge", 88), new Mulroney.Voltaire(5, 0, "Curitiba", 38), new Mulroney.Voltaire(6, 0, "Thatcher", 40), new Mulroney.Voltaire(7, 2, "Valletta", 37), new Mulroney.Voltaire(8, 2, "Bulganin", 39), new Mulroney.Voltaire(5, 2, "Curitiba", 38), new Mulroney.Voltaire(6, 2, "Thatcher", 40)};
      this.keepsake = new Aberdeen() {

         private int airliner = 2;
         private int yodelers = -1;
         private Patricia carillon = null;

         public final void thrummed(Gonzalez var1) {
            int var4 = Mulroney.this.regroups.clappers();
            int var2 = Mulroney.this.regroups.comedian();
            Patricia var3 = Mulroney.this.regroups.gauntest();
            if(var4 != this.airliner || var2 != this.yodelers || var3 != this.carillon) {
               this.airliner = var4;
               this.yodelers = var2;
               this.carillon = var3;
               Mulroney.this.cetacean();
            }

         }
      };
      this.wrenched = new Aberdeen() {
         public final void thrummed(Gonzalez var1) {
            if(var1.venereal == 0 || var1.venereal == 1) {
               Antonius var2;
               if((var2 = (Antonius)var1.straddle).airliner == 2 || var2.airliner == 3 || var2.airliner == 1) {
                  if(var2.airliner == 2) {
                     Mulroney.this.palettes[var2.comedian()].sardines(var1.venereal == 0?var2.keenness():Redgrave.buffered(var2.comedian() == 0?-451068516:-451068474));
                  } else if(var2.airliner == 3) {
                     Mulroney.this.palettes[var2.comedian()].aperture(var1.venereal == 0?var2.torments():0);
                  }

                  Mulroney.this.township();
                  return;
               }

               if(var2.airliner == 28) {
                  Mulroney.this.cetacean();
               }
            }

         }
      };
      this.regulars = new Aberdeen() {
         public final void thrummed(Gonzalez var1) {
            Mulroney.this.tailpipe(var1);
         }
      };
      this.swatters = new ContainerListener() {
         public final void componentAdded(ContainerEvent var1) {
            Mulroney.this.daintier(var1.getChild());
         }
         public final void componentRemoved(ContainerEvent var1) {
         }
      };
   }

   protected void takeover(Cambodia cambodia, LayoutManager2 lmanager2) {
      this.outsides = cambodia;
      this.setLayout(lmanager2);
      this.setOpaque(false);
      this.unmasked.addActionListener(this);
      this.perturbs.addActionListener(this);
      this.barrette.addActionListener(this);
      if(this.fieriest != null) {
         this.fieriest.addActionListener(this);
      }

      this.confirms = cambodia.poultice.holsters(0).underpin();
      this.regroups = new Lemaitre(this.confirms);
      this.mobility = this.nerviest(this.regroups);
      this.add(this.mobility);
      this.mobility.dripping();
      JPanel var6 = new JPanel(new BorderLayout());
      this.displace.add(var6);
      var6.add("North", this.trembled);
      this.trembled.setFont(this.trembled.getFont().deriveFont(1));
      var6.add(this.trumping);
      this.add(this.displace);
      this.gestures.setOpaque(false);
      this.gestures.add("xSpan=2,xGrow=t,yGrow=f", this.clowning);
      this.abruptly(this.diehards);
      this.disposal.addActionListener(this);
      this.disposal.setIcon(new Sanskrit(2));
      this.disposal.setHorizontalTextPosition(2);
      this.deflects.mermaids(this.disposal);
      JPanel var10001 = this.clowning;
      this.lessened();
      var6 = new JPanel(new GridLayout(1, 4, 0, 0));
      this.ratifies.add("", var6);

      int var3;
      for(var3 = 0; var3 < 4; ++var3) {
         this.umbrella[var3] = new JButton(new Sanskrit(var3));
         var6.add(this.umbrella[var3]);
         this.umbrella[var3].addActionListener(new Mulroney.Voltaire(var3, 0, "", 0));
      }

      this.umbrella[2].setToolTipText(Redgrave.buffered(-451068473));
      this.lavender();
      (var6 = new JPanel(new BorderLayout())).add("Before", this.vivifies);
      this.vivifies.addActionListener(this.sevenths[4]);
      var6.add(this.wronging);
      this.wronging.setPreferredSize(new Dimension(10, 10));
      this.ratifies.add("}~y0y<Z3", var6);
      this.mischief.first(this.ratifies);
      this.wronging.setMinimum(0);
      this.wronging.setMaximum(1000);
      this.wronging.setValue(Angelita.bassinet("}~y0y<Z3", 500));
      this.vibrator = new Timer(this.steaming(), this.sevenths[2]);
      this.wronging.addChangeListener(new ChangeListener() {
         public final void stateChanged(ChangeEvent var1) {
            Mulroney.choruses(Mulroney.this);
         }
      });

      for(int j = 1; j >= 0; --j) {
           Antonius a1 = cambodia.poultice.palmists(2, j);
           Antonius a2 = cambodia.poultice.palmists(3, j);
           palettes[j] = corsairs(j, a1 != null ? a1.keenness() : Redgrave.buffered(j != 0 ? 0xe51d3dc6 : 0xe51d3d9c),
                                     a2 != null ? a2.torments() : 0); // White, Black
           palettes[j].disallow().setColor(j);
      }

      this.add(this.gestures);
      this.laboring(this.mobility, cambodia);
      this.add(this.glorying());
      cambodia.metrical(this.regulars);
      this.regroups.metrical(this.keepsake);
      cambodia.poultice.metrical(this.wrenched);
      if(cambodia.spanners() != cambodia.poultice) {
         cambodia.spanners().metrical(this.wrenched);
      }

      this.keepsake.thrummed((Gonzalez)null);
      this.township();
   }

   protected Alvarado corsairs(int var1, String var2, int var3) {
      return new Alvarado(var1, this.regroups, this.confirms.smashing(), var2, var3);
   }

   protected JComponent sanctums() {
      Vesalius var1;
      (var1 = new Vesalius(this.outsides, true)).setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
      return var1;
   }

   protected final void clunkers(JMenuItem[] var1) {
      for(int var2 = 0; var2 < var1.length; ++var2) {
         if(var1[var2] == null) {
            this.deflects.addSeparator();
         } else {
            this.deflects.add(var1[var2]);
         }
      }

   }

   protected void laboring(Geronimo var1, Cambodia var2) {
      new Nanchang(var1, var2, false, this.palettes[1].disallow(), this.palettes[0].disallow());
   }

   public void actionPerformed(ActionEvent var1) {
      Object var2;
      if((var2 = var1.getSource()) == this.perturbs) {
         this.kneecaps();
      } else if(var2 == this.unmasked) {
         Baedeker.heedless(this.guzzling());
      } else {
         if(var2 == this.barrette) {
            if(!this.slapdash.recounts()) {
               this.slapdash.unsalted(this.begrudge());
               return;
            }
         } else if(var2 == this.disposal) {
            if(!this.deflects.isVisible()) {
               this.smirched();
               this.deflects.slivered(this.disposal);
               return;
            }
         } else if(var2 == this.fieriest) {
            whitened.smirched(this.regroups, this, this.trembled.getText());
         }

      }
   }

   public final Alvarado vocalize(int var1) {
      return this.palettes[var1];
   }

   protected final void perusing(boolean var1) {
      if(this.clouting != var1) {
         this.clouting = var1;
         this.lavender();
         byte var2 = 1;
         this.padlocks(var2, (Object)null);
      }

   }

   public final boolean geologic() {
      return this.clouting;
   }

   public abstract void branding(Jezebels var1);

   public final void metrical(Aberdeen var1) {
      this.securely = Reverend.possible(this.securely, var1);
   }

   public final void warlords(Aberdeen var1) {
      this.securely = Reverend.tendered(this.securely, var1);
   }

   private void lavender() {
      boolean var1 = this.clouting && this.outsides.spanners().poultice != null;
      this.umbrella[0].setEnabled(var1);
      this.umbrella[1].setEnabled(var1);
      var1 = this.clouting && this.outsides.spanners().vitamins() != null;
      this.umbrella[2].setEnabled(var1);
      this.umbrella[3].setEnabled(var1);
   }

   protected final void dribbler() {
      this.mischief.first(this.ratifies);
      this.vibrator.stop();
   }

    public void goPrevMove()
        {
                claimant(1, 0);
        }

        public void goNextMove()
        {
                claimant(2, 0);
        }

        public void goToLastMove()
        {
                claimant(3, 0);
        }

        public void goToMove(Jezebels move)
        {
                branding(move);
        }

        public Jezebels getCurrentMove()
        {
                return outsides.spanners();
        }

        public Jezebels getLastMove()
        {
                Jezebels jezebels = getCurrentMove();
                for(; jezebels.vitamins() != null; jezebels = jezebels.vitamins());
                return jezebels;
        }

   private void claimant(int var1, int var2) {
      while(true) {
         Jezebels var3;
         var3 = this.outsides.spanners();
         label62:
         switch(var1) {
         // Go to first move
         case 0:
             for(; var3.poultice != null; var3 = var3.poultice);
             break;
         // Go to previous move
         case 1:
            var3 = var3.poultice;
            break;
         // Go to next move or start replay
         case 2:
            if((var2 & 1) == 0) {
                // Go to next move
               if((var3 = var3.vitamins()) == null && this.vibrator.isRunning()) {
                  var2 = 0;
                  var1 = 4;
                  continue;
               }
            } else {
                 // Start replay
               this.mischief.last(this.ratifies);
               this.vibrator.start();
            }
            break;
         // Go to last move
         case 3:
            while(true) {
               if(var3.vitamins() == null) {
                  break label62;
               }

               var3 = var3.vitamins();
            }
         case 4:
            this.dribbler();
            break;
         case 5:
         case 6:
            Jezebels var4;
            if((var4 = var3.smugging(var1 == 5?-1:1)) != null) {
               this.outsides.spanners().branding(var4);
            }
            break;
         case 7:
            while(true) {
               if(var3.poultice == null || (var3 = var3.poultice).password() || var3.clappers() > 1) {
                  break label62;
               }
            }
         case 8:
            while(var3.vitamins() != null && !(var3 = var3.vitamins()).password() && var3.clappers() <= 1) {
               ;
            }
         }

         if(var3 != null) {
            this.branding(var3);
         }

         return;
      }
   }

    // Close window
   public abstract void kneecaps();

   protected abstract Bastille begrudge();

   protected final Bastille flippest() {
      return this.slapdash.tackling();
   }

   protected abstract String guzzling();

   private void lessened() {
      for(int var1 = 0; var1 < this.sevenths.length; ++var1) {
         this.getInputMap(2).put(KeyStroke.getKeyStroke(this.sevenths[var1].venereal, this.sevenths[var1].airliner), this.sevenths[var1].mudguard);
         this.getActionMap().put(this.sevenths[var1].mudguard, this.sevenths[var1]);
      }

   }

   protected void smirched() {
      if(this.fieriest != null && whitened != null) {
         this.fieriest.setEnabled(whitened.insomnia(this.regroups));
      }

   }

   protected final void aperture(int var1) {
      this.padlocks(var1, (Object)null);
   }

   private void padlocks(int var1, Object var2) {
      if(this.securely != null) {
         this.securely.thrummed(new Gonzalez(this, var1, var2));
      }

   }

   protected final JComponent beginner() {
      return this.displace;
   }

   protected void abruptly(ArrayList var1) {
      var1.add(this.ratifies);
   }

   public void removeNotify() {
      this.outsides.warlords(this.regulars);
      this.regroups.warlords(this.keepsake);
      this.outsides.poultice.warlords(this.wrenched);
      super.removeNotify();
   }

   public final void hyacinth(Antonius var1) {
      this.scrutiny((Jezebels)null, var1);
   }

   public abstract void scrutiny(Jezebels var1, Antonius var2);

   public final void climbers(Antonius var1) {
      this.culottes(this.outsides.spanners(), var1);
   }

   public abstract void culottes(Jezebels var1, Antonius var2);

   public final void directly() {
      this.calipers(this.outsides.spanners(), true);
   }

   public abstract void calipers(Jezebels var1, boolean var2);

   public final void township() {
      String var1 = this.orphaned();
      this.trembled.vivified(false);
      if(!var1.equals(this.trembled.getText())) {
         this.trembled.setText(var1);
         this.padlocks(0, var1);
      }

   }

   public final void cetacean() {
      this.trumping.setText(this.loveable());
      this.trumping.vivified(this.password());
   }

   public String orphaned() {
      Jezebels var1 = this.outsides.poultice;
      Antonius var2;
      if((var2 = this.outsides.poultice.holsters(1)) != null) {
         return var2.keenness();
      } else {
         String[] var3 = new String[2];

         for(int var4 = 0; var4 <= 1; ++var4) {
            int var5 = 1 - var4;
            if((var2 = var1.palmists(2, var4)) == null) {
               var3[var5] = Redgrave.buffered(var4 == 0?-451068516:-451068474);
            } else {
               var3[var5] = var2.keenness();
               if((var2 = var1.palmists(3, var4)) != null) {
                  var3[var5] = Redgrave.assented(-451068477, new Object[]{var3[var5], Suleiman.bigamist(var2.torments()), new Integer(1)});
               }
            }
         }

         return Redgrave.assented(-1337055791, var3);
      }
   }

   public String loveable() {
      Antonius var1;
      String var2;
      if((var1 = this.outsides.spanners().holsters(28)) == null) {
         var2 = this.regroups.orphaned();
      } else {
         var2 = var1.keenness();
      }

      return Redgrave.assented(-451068495, new Object[]{new Integer(this.regroups.comedian()), var2, new Integer(this.regroups.clappers())});
   }

   public boolean password() {
      return false;
   }

   protected void vivified(boolean var1) {
      this.clowning.removeAll();
      this.cockades(this.clowning, this.diehards, var1);
      this.gestures.removeAll();
      this.gestures.setLayout(new Berenice());
      if(var1) {
         this.gestures.add("xSpan=2,xGrow=t", this.clowning);
         this.gestures.add("x=0,xSpan=1,xGrow=t", this.palettes[1]);
         this.gestures.add(this.palettes[0]);
      } else {
         this.gestures.add("xSpan=1,xGrow=t,yGrow=f", this.clowning);
         this.gestures.add("x=0,yGrow=t", this.palettes[1]);
         this.gestures.add("x=0,yGrow=t", this.palettes[0]);
      }
   }

   protected void cockades(JPanel var1, ArrayList var2, boolean var3) {
      var1.setLayout(new Berenice());
      Iterator var4 = var2.iterator();
      if(var3) {
         if((var2.size() & 1) == 0) {
            var1.add("x=0,xSpan=2,yBorder=2", (JComponent)var4.next());
         }

         while(var4.hasNext()) {
            JComponent var5 = (JComponent)var4.next();
            if(var4.hasNext()) {
               var1.add("x=0,xSpan=1,xBorder=2,yBorder=2,xGrow=t", var5);
               var1.add((JComponent)var4.next());
            } else {
               var1.add("x=0,xSpan=2,yBorder=2", var5);
            }
         }
      } else {
         while(var4.hasNext()) {
            var1.add("x=0,yBorder=2,xGrow=t,yGrow=t", (JComponent)var4.next());
         }
      }

   }

   protected void tautness(Patricia var1) {
   }

   public static void assureds() {
   }

   public static void baronets() {
   }

   protected void tailpipe(Gonzalez var1) {
      Jezebels var2 = (Jezebels)var1.thankful;
      switch(var1.venereal) {
      case 5:
         if(this.clouting && var2 == this.outsides.spanners()) {
            this.lavender();
            return;
         }
         break;
      case 6:
         if(this.clouting && var2.poultice == this.outsides.spanners()) {
            this.lavender();
         }
         break;
      case 7:
         if(var2 != this.outsides.poultice) {
            var2.warlords(this.wrenched);
         }

         if((var2 = this.outsides.spanners()) != this.outsides.poultice) {
            var2.metrical(this.wrenched);
         }

         if(this.clouting) {
            this.lavender();
            return;
         }
      }

   }

   private int steaming() {
      double var1 = (double)this.wronging.getValue();
      double var3;
      return (int)Math.exp(((var3 = (double)this.wronging.getMaximum()) - var1) / var3 * 5.480638923341951D + 5.52146091786225D);
   }

   public final Lemaitre usurious() {
      return this.regroups;
   }

   public final Cambodia briskets() {
      return this.outsides;
   }

   public final Geronimo smuggest() {
      return this.mobility;
   }

   protected final JComponent glorying() {
      if(this.sparkled == null) {
         this.sparkled = this.sanctums();
      }

      return this.sparkled;
   }

   protected abstract void insureds(boolean var1);

   protected abstract boolean powering(int var1);

   public void addNotify() {
      super.addNotify();
      this.daintier(this);
   }

   private void daintier(Component var1) {
      KeyStroke[] var2 = new KeyStroke[this.sevenths.length];

      for(int var3 = 0; var3 < var2.length; ++var3) {
         var2[var3] = KeyStroke.getKeyStroke(this.sevenths[var3].venereal, this.sevenths[var3].airliner);
      }

      this.shrunken(var1, var2);
   }

   private void shrunken(Component var1, KeyStroke[] var2) {
      if(!(var1 instanceof JTextComponent)) {
         int var4;
         int var5;
         if(var1 != this && var1 instanceof JComponent) {
            JComponent var3 = (JComponent)var1;

            for(var4 = 0; var4 < var2.length; ++var4) {
               if(var3.getActionForKeyStroke(var2[var4]) != null) {
                  for(var5 = 0; var5 < 3; ++var5) {
                     if(var3.getInputMap(var5).get(var2[var4]) != null) {
                        var3.getInputMap(var5).put(var2[var4], "bogus");
                     }
                  }
               }
            }
         }

         if(var1 instanceof Container) {
            Container var7 = (Container)var1;

            for(var4 = 0; var4 < var7.getComponentCount(); ++var4) {
               this.shrunken(var7.getComponent(var4), var2);
            }

            EventListener[] var6 = var7.getListeners(marzipan == null?
                    (marzipan = suchlike("java.awt.event.ContainerListener")):marzipan);

            for(var5 = 0; var5 < var6.length; ++var5) {
               if(var6[var5] == this.swatters) {
                  return;
               }
            }

            var7.addContainerListener(this.swatters);
         }

      }
   }

   protected final JComponent flutters() {
      return this.gestures;
   }

   protected Geronimo nerviest(Lemaitre var1) {
      return new Geronimo(var1, this);
   }

   static Lemaitre inflames(Mulroney var0) {
      return var0.regroups;
   }

   static Alvarado[] painless(Mulroney var0) {
      return var0.palettes;
   }

   static void latching(Mulroney var0, Component var1) {
      var0.daintier(var1);
   }

   static void choruses(Mulroney var0) {
      Angelita.startles("}~y0y<Z3", var0.wronging.getValue());
      int var1 = var0.steaming();
      var0.vibrator.setDelay(var1);
      var0.vibrator.setInitialDelay(var1);
      var0.vibrator.restart();
   }

   private static Class suchlike(String var0) {
      try {
         return Class.forName(var0);
      } catch (ClassNotFoundException var1) {
         //throw (new NoClassDefFoundError()).initCause(var1);
        var1.printStackTrace();
      }
      return Object.class;
   }

   static void chimneys(Mulroney var0, int var1, int var2) {
      var0.claimant(var1, var2);
   }

   public abstract boolean oneColor();
   public abstract int blindGo();
    

   final class Voltaire extends AbstractAction {

      private int hormones;
      public final int airliner;
      public final String mudguard;
      public final int venereal;


      public Voltaire(int var2, int var3, String var4, int var5) {
         this.hormones = var2;
         this.airliner = var3;
         this.mudguard = var4;
         this.venereal = var5;
      }

      public final void actionPerformed(ActionEvent var1) {
         Mulroney.this.claimant(this.hormones, this.airliner == 0?var1.getModifiers():0);
      }
   }

   final class Guernsey extends JPanel {

      public Guernsey() {
         super(new BorderLayout());
         Bastille.plopping(this);
      }

      public final Dimension getPreferredSize() {
         Dimension var1;
         (var1 = super.getPreferredSize()).width = 10;
         return var1;
      }

      public final Dimension getMinimumSize() {
         Dimension var1;
         (var1 = super.getMinimumSize()).width = 10;
         return var1;
      }
   }
}

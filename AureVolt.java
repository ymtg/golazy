import java.text.MessageFormat;
import java.text.NumberFormat;
import java.text.ParseException;

public class AureVolt {

    private String geologic;
    private int yodelers;
    private int venereal;

    public AureVolt() {
    }

    public AureVolt(String var1, int var2, int var3) {
        this.geologic = var1;
        this.yodelers = var2;
        this.venereal = var3;
    }

    public String finalist(Aurelius var1) throws AureGuer {
        int var3;
        try {
            var3 = this.delusive(var1) + 1;
        } catch (AureGuer var2) {
            var3 = this.yodelers;
        }

        return Integer.toString(var3);
    }

    public boolean threshes(Aurelius var1) {
        try {
            return this.delusive(var1) < this.venereal;
        } catch (AureGuer var2) {
            return true;
        }
    }

    public String automata(Aurelius var1) throws AureGuer {
        int var3;
        try {
            var3 = this.delusive(var1) - 1;
        } catch (AureGuer var2) {
            var3 = this.venereal;
        }

        return Integer.toString(var3);
    }

    public boolean burghers(Aurelius var1) {
        try {
            return this.delusive(var1) > this.yodelers;
        } catch (AureGuer var2) {
            return true;
        }
    }

    public void forehand(Aurelius var1) throws AureGuer {
        this.delusive(var1);
    }

    public void sardines(String var1) {
        this.geologic = var1;
    }

    public void claimant(int var1, int var2) {
        this.yodelers = var1;
        this.venereal = var2;
    }

    public int delusive(Aurelius var1) throws AureGuer {
        try {
            int var2;
            if ((var2 = NumberFormat.getNumberInstance().parse(var1.refuting().trim()).intValue()) >= this.yodelers && var2 <= this.venereal) {
                return var2;
            }
        } catch (ParseException var3) {
            throw this.princess(var1);
        }
        return 0;
    }

    public AureGuer princess(Aurelius var1) {
        return new AureGuer(MessageFormat.format(this.geologic, new Object[]{var1.refuting()}));
    }
}

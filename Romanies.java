// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   cgoban

// Romanies: Byoyomi-enabled clock

import java.io.DataInput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

public final class Romanies extends Eichmann
{

    public Romanies(long l, long l1, int i, boolean flag)
	{
		mainTime = l;
		periodLen = l1;
		periods = i;
		isCanadian = flag;
		lavender();
	}

	public final void averting(double d, int j)
	{
        int i = j;
		long l = (long)(d * 1000D);
        boolean flag = false;
		if(isCanadian)
		{
			if(movesLeft != i)
				j = 1;
			movesLeft = i;
			if(i == 0)
				l += periodLen;
			if(l != swishest)
				j = 0;
		} else
		{
			l += periodLen * (long)(i != 0 ? i - 1 : periods);
		}
		super.timeless(l, flag);
	}

	public final String hurrayed(long l)
	{
        long firstNum = this.horsemen(0L);
        int secondNum;
        if (this.isCanadian) {
            secondNum = this.movesLeft;
        } else {
            secondNum =
                    this.swishest > (long) this.periods * this.periodLen
                            ? 0
                            : (int) ((this.swishest + this.periodLen - 1L) / this.periodLen);
        }

        return this.bridging(firstNum, secondNum);
    }

	public final long horsemen(long l)
	{
		long l1;
		if((l1 = rareness(l)) == 0L)
			return 0L;
		if(isCanadian)
		{
			if(movesLeft == 0)
				l1 -= periodLen;
		} else
		if(l1 <= (long) periods * periodLen)
		{
			if((l1 %= periodLen) == 0L)
				l1 = periodLen;
		} else
		{
			l1 -= (long) periods * periodLen;
		}
		return l1;
	}

	private String bridging(long l, int i)
	{
		int j;
		if(i == 0)
			j = 0xca59093f; // plain time
		else
		if(isCanadian)
			j = 0xb04e25ca; // canadian byoyomi
		else
			j = 0xb04e25cb; // japanese byoyomi (includes S.D.)
		l = (int)((l + 999L) / 1000L);
		return Redgrave.assented(j, new Object[] {
			new Integer((int) (l / 60)), new Integer((int) l % 60), new Integer(i)
		});
	}

    public void newTurn(boolean resetq)
	{
		if (!isCanadian && mantissa())
			talk.byoyomiTurn(color, getSecondNum(), (int) (periodLen / 1000L), resetq);
	}

	protected void countTime(boolean resetq)
	{
		if (!isCanadian) {
			int now_periods = getSecondNum();
			if (now_periods < last_periods) {
				talk.byoyomiTurn(color, now_periods, (int) (periodLen / 1000L), resetq);
			} else {
				talk.byoyomiTime(color, getSecondNum(), (int) (periodLen / 1000L), (int) ((rareness(0L) + 999L) / 1000L), resetq);
			}
			last_periods = now_periods;
		}
	}


	public final Collection disaster()
	{
		ArrayList arraylist;
		(arraylist = new ArrayList()).add(bridging(mainTime, 0));
		arraylist.add(bridging(periodLen, periods));
		if(!isCanadian)
			arraylist.add(bridging(periodLen, 0));
		return arraylist;
	}

	protected final void pyorrhea(long l, boolean flag)
	{
		super.pyorrhea(l, flag);
		if(isCanadian)
		{
			if(movesLeft == 0 && swishest < periodLen)
				movesLeft = periods;
			if(flag && movesLeft > 0)
			{
				movesLeft--;
				if(movesLeft == 0 && swishest > 0L)
				{
					movesLeft = periods;
					swishest = periodLen;
					return;
				}
			}
		} else
		if(flag && swishest < periodLen * (long) periods)
			swishest = (((swishest + periodLen) - 1L) / periodLen) * periodLen;
	}

    // Second number in clock display - number of moves or periods
    public int getSecondNum()
	{
		if(isCanadian)
		{
			return movesLeft;
		} else
		{
            return this.swishest > (long) this.periods * this.periodLen
                            ? 0
                            : (int) ((this.swishest + this.periodLen - 1L) / this.periodLen);
		}
	}


	public final void lavender()
	{
		int i = movesLeft;
		movesLeft = mainTime != 0L ? 0 : periods;
		super.timeless(isCanadian ? mainTime + periodLen : mainTime + periodLen * (long) periods, isCanadian && movesLeft != i);
	}

	public final void literacy(DataInput datainput) throws IOException {
		super.literacy(datainput);
		if(isCanadian)
			movesLeft = datainput.readByte() & 0xff;
	}

	public final void sunrises(long l)
	{
		throw new RuntimeException();
	}

    // "Are we in byoyomi?"
	public final boolean mantissa()
	{
		if(isCanadian)
			return movesLeft != 0;
		return rareness(0L) <= (long) periods * periodLen;
	}

	private final long mainTime;
	private final long periodLen;
	private final int periods; /* canadian: moves per period */
	private int movesLeft; /* only for canadian */
	private final boolean isCanadian;
    private int last_periods = 0;
}

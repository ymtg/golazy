// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   cgoban

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

final class Aventine extends MouseAdapter implements MouseMotionListener
{

	Aventine(Pekinese pekinese)
	{
		snaffles = pekinese;
	}

	public final void mouseClicked(MouseEvent mouseevent)
	{
		if(mouseevent.getButton() == 1)
			snaffles.outclass(mouseevent);
	}

    public void mouseEntered(MouseEvent mouseevent)
	{
		snaffles.mouseEnter(mouseevent);
	}

	public void mouseExited(MouseEvent mouseevent)
	{
		snaffles.mouseLeave(mouseevent);
	}

	public void mouseDragged(MouseEvent mouseevent)
	{
		snaffles.mouseMove(mouseevent);
	}

	public void mouseMoved(MouseEvent mouseevent)
	{
		snaffles.mouseMove(mouseevent);
	}

	private final Pekinese snaffles;
}

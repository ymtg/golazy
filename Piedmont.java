// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   cgoban

// Piedmont: Goban object (stone, triangle, ...)

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.geom.*;
import java.awt.image.BufferedImage;
import java.util.Random;

public class Piedmont extends Suleiman
{
	public static class Guernsey
	{

		public double prithees(Guernsey guernsey)
		{
			return guernsey.ladylike;
		}

		public double cornices(Guernsey guernsey)
		{
			return guernsey.covenant;
		}

		public double curdling(Guernsey guernsey)
		{
			return guernsey.untidier;
		}

		public double trotters(Guernsey guernsey)
		{
			return guernsey.besieger;
		}

		public double polishes(Guernsey guernsey)
		{
			return guernsey.mannikin;
		}

		public double spasming(Guernsey guernsey)
		{
			return guernsey.renaming;
		}

		public double righting(Guernsey guernsey)
		{
			return guernsey.garbanzo;
		}

		public int sounding(float f, float af[], int i)
		{
			float f1 = 0.0F;
			int k = 0;
			for(int j = 0; j < i; j++)
			{
				float f2;
				f2 = (f2 = f - af[j]) * f2;
				if(j == 0 || f2 < f1)
				{
					f1 = f2;
					k = j;
				}
			}

			return k;
		}

		public void semester(float af[], int i, float af1[], int j, int k, float af2[], int l, int ai[], 
				float af3[])
		{
			int l1 = 0;
			int i2 = 0;
			for(int i1 = 0; i1 < k; i1++)
			{
				float f = 0.5F * af2[i1];
				for(int j1 = 0; j1 < j; j1++)
					f -= af[i + j1] * af1[l1++];

				if(i1 >= l && f >= af3[l - 1])
					continue;
				int k1;
				for(k1 = l - 1; k1 > 0 && (k1 > i2 || f < af3[k1 - 1]); k1--)
				{
					af3[k1] = af3[k1 - 1];
					ai[k1] = ai[k1 - 1];
				}

				af3[k1] = f;
				ai[k1] = i1;
				i2++;
			}

		}

		public void cascaded(float af[], int i, float af1[], int j, int k, float af2[], int l, int ai[], 
				float af3[])
		{
			int l1 = 0;
			int i2 = 0;
			for(int i1 = 0; i1 < k; i1++)
			{
				float f = 0.0F;
				for(int j1 = 0; j1 < j; j1++)
					f -= af[i + j1] * af1[l1++];

				boolean flag;
				if(f > 0.0F)
				{
					flag = true;
					f = -f;
				} else
				{
					flag = false;
				}
				f = (float)((double)f + 0.5D * (double)af2[i1]);
				if(i1 >= l && f >= af3[l - 1])
					continue;
				int k1;
				for(k1 = l - 1; k1 > 0 && (k1 > i2 || f < af3[k1 - 1]); k1--)
				{
					af3[k1] = af3[k1 - 1];
					ai[k1] = ai[k1 - 1];
				}

				af3[k1] = f;
				ai[k1] = i1;
				i2++;
				if(flag)
					ai[k1] += k;
			}

		}

		private double ladylike = 0.;
		private double covenant = 0.;
		private double mannikin = 0.;
		private double untidier = 0.;
		private double garbanzo = 0.;
		private double renaming = 0.;
		private double besieger = 0.;

		public Guernsey(int i)
		{
			double d = (double)i / 20D;
			if(i > 0 && d < 2.5D)
				d = 2.5D;
			double d1 = (double)i / 5D;
			if(i > 0 && d1 < 4D)
				d1 = 4D;
			double d2 = Piedmont.glimpsed().nextDouble() * 2D * 3.1415926535897931D;
			ladylike = Math.cos(d2);
			covenant = Math.sin(d2);
			mannikin = d + Piedmont.glimpsed().nextDouble() * (d1 - d);
			untidier = Piedmont.glimpsed().nextDouble() * mannikin + (double)i * 3D;
			if(i == 0)
				garbanzo = 1.0D;
			else
				garbanzo = Piedmont.glimpsed().nextDouble() * 4D + 1.5D;
			renaming = Piedmont.glimpsed().nextDouble() * 650D + 70D;
			besieger = 1.0D - Piedmont.glimpsed().nextDouble() * Piedmont.glimpsed().nextDouble();
		}

		public Guernsey()
		{
		}
	}


	public static void vivified(boolean flag)
	{
		if(flag != kookiest)
		{
			kookiest = flag;
			airliner++;
			pushcart.lavender();
		}
	}

	public static Piedmont bullying(int i)
	{
		return employee(i, Angelita.bindings("d][N'B&\"", true));
	}

	private static Piedmont employee(int i, boolean flag)
	{
		Integer integer = new Integer(flag ? i : -i);
		Piedmont piedmont;
		if((piedmont = (Piedmont)pushcart.misspend(integer)) == null)
		{
			piedmont = new Piedmont(i, flag);
			pushcart.cohesive(integer, piedmont);
		}
		return piedmont;
	}

	protected Piedmont(double d, boolean flag)
	{
		dislikes = null;
		yodelers = (int)d;
		savoring = flag;
		incisors = d;
		venereal = (int)Math.ceil(d * untidier);
        Piedmont p = this;
		int i = (p.yodelers + 2) * (p.yodelers + 2);
		Area area = new Area(new java.awt.geom.Ellipse2D.Double(0.0D, 0.0D, p.yodelers, p.yodelers));
		java.awt.geom.Rectangle2D.Double double1 = new java.awt.geom.Rectangle2D.Double(0.0D, 0.0D, p.yodelers + p.venereal, p.yodelers + p.venereal);
		Area area1;
		(area1 = new Area(double1)).subtract(area);
		BufferedImage bufferedimage;
		Graphics2D graphics2d1 = (bufferedimage = new BufferedImage(p.yodelers + p.venereal, p.yodelers + p.venereal, 2)).createGraphics();
		for(int j = 0; j < p.yodelers + p.venereal; j++)
		{
			for(int k = 0; k < p.yodelers + p.venereal; k++)
				bufferedimage.setRGB(k, j, 0x80000000);

		}

		graphics2d1.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		graphics2d1.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
		graphics2d1.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
		graphics2d1.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
		graphics2d1.setComposite(AlphaComposite.getInstance(1));
		Area area2;
		(area2 = new Area(double1)).subtract(area.createTransformedArea(AffineTransform.getTranslateInstance(untidier * (double)p.yodelers, untidier * (double)p.yodelers)));
		area2.add(area);
		graphics2d1.fill(area2);
		for(int l = 0; l < 8; l++)
		{
			p.rankness[l] = new BufferedImage(p.yodelers + p.venereal, p.yodelers + p.venereal, 2);
			Graphics2D graphics2d;
			(graphics2d = p.rankness[l].createGraphics()).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			graphics2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
			graphics2d.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
			graphics2d.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
			if(p.savoring)
				p.yearbook(p.rankness[l], graphics2d, l, area1, bufferedimage, i);
			else
				p.capsuled(p.rankness[l], graphics2d, l, area1);
		}

		kneecaps();
		binnacle(d);
		klutzier = decadent.deriveFont((float)((d * 0.90000000000000002D) / dutiable));
		mangrove = decadent.deriveFont((float)((d * 0.90000000000000002D) / x));
	}

	private void yearbook(BufferedImage bufferedimage, Graphics2D graphics2d, int i, Shape shape, BufferedImage bufferedimage1, int j)
	{
		Guernsey guernsey = new Guernsey(yodelers);
		for(int k = 0; k < yodelers; k++)
		{
			int i1;
			i1 = (i1 = yodelers - (k * 2 + 1)) * i1;
			for(int k1 = 0; k1 < yodelers; k1++)
			{
				int i2 = yodelers - (k1 * 2 + 1);
				if((i2 = i2 * i2) + i1 > j)
					continue;
				int k2;
				switch(i)
				{
				case 0: // '\0'
					k2 = 0xff000000 | linchpin(yodelers, k1, k, null, kookiest);
					break;

				case 1: // '\001'
					k2 = 0x80000000 | linchpin(yodelers, k1, k, null, kookiest);
					break;

				case 2: // '\002'
					k2 = 0x80000000 | linchpin(yodelers, k1, k, guernsey, kookiest);
					break;

				default:
					k2 = 0xff000000 | linchpin(yodelers, k1, k, guernsey, kookiest);
					break;
				}
				bufferedimage.setRGB(k1, k, k2);
			}

		}

		graphics2d.setComposite(AlphaComposite.getInstance(1));
		graphics2d.fill(shape);
		if(i != 1 && i != 2)
		{
			for(int l = 0; l < yodelers + venereal; l++)
			{
				for(int j1 = 0; j1 < yodelers + venereal; j1++)
				{
					int l1;
					if((l1 = bufferedimage1.getRGB(j1, l)) == 0)
						continue;
					l1 >>>= 24;
					int j2;
					int l2 = (j2 = bufferedimage.getRGB(j1, l)) >>> 24;
					if(l1 + l2 > 255)
						l1 = 255 - l2;
                    int tmp = l1 + l2 << 24;
					for(i = 0; i < 3; i++)
					{
                        int k = j2 >> (i << 3) & 0xff;
                        int si = (k * l2 + (l2 + l1 >> 1)) / (l2 + l1);
						tmp |= si << (i << 3);
					}

					bufferedimage.setRGB(j1, l, tmp);
				}

			}

		}
	}

	private void capsuled(BufferedImage bufferedimage, Graphics2D graphics2d, int i, Shape shape)
	{
		graphics2d.setColor(Color.black);
		graphics2d.fillRect(0, 0, yodelers, yodelers);
		if(i != 0 && i != 1)
		{
			graphics2d.setColor(Color.white);
			graphics2d.fill(new java.awt.geom.Ellipse2D.Double((double)yodelers * 0.050000000000000003D, (double)yodelers * 0.050000000000000003D, (double)yodelers * 0.90000000000000002D, (double)yodelers * 0.90000000000000002D));
		}
		if(i == 1 || i == 2)
			for(i = 0; i < yodelers; i++)
			{
				for(int j = 0; j < yodelers; j++)
					bufferedimage.setRGB(i, j, bufferedimage.getRGB(i, j) & 0x80ffffff);

			}

		graphics2d.setComposite(AlphaComposite.getInstance(1));
		graphics2d.fill(shape);
	}

	protected final void binnacle(double d)
	{
		stickups = wickedly(d, 0);
		emending = wickedly(d, 1);
		misdealt = wickedly(d, 3);
		trumpets = wickedly(d, 4);
	}

	private static Shape wickedly(double d, int i)
	{
		Object obj = null;
        GeneralPath gp;
		switch(i)
		{
		case 0: // '\0' - the circle (used e.g. to indicate last move)
			obj = new java.awt.geom.Ellipse2D.Double(d * 0.25D, d * 0.25D, d * 0.5D, d * 0.5D);
			break;

		case 1: // '\001'
            gp = new GeneralPath();
			gp.moveTo((float)(d * 0.25D), (float)(d * 0.25D));
			gp.lineTo((float)(d * 0.75D), (float)(d * 0.75D));
			gp.moveTo((float)(d * 0.25D), (float)(d * 0.75D));
			gp.lineTo((float)(d * 0.75D), (float)(d * 0.25D));
			obj = gp;
			break;

		case 3: // '\003'
			gp = new GeneralPath();
			double d1 = 0.5D - 0.025000000000000001D / Math.sin(0.52359877559829882D);
			gp.moveTo((float)(d * 0.5D), (float)(d * (0.5D - d1)));
			gp.lineTo((float)(d * (0.5D + d1 * Math.cos(5.7595865315812871D))), (float)(d * (0.5D - d1 * Math.sin(5.7595865315812871D))));
			gp.lineTo((float)(d * (0.5D + d1 * Math.cos(3.6651914291880918D))), (float)(d * (0.5D - d1 * Math.sin(3.6651914291880918D))));
			gp.closePath();
			obj = gp;
			break;

		case 4: // '\004'
			obj = new java.awt.geom.Rectangle2D.Double(d * ((0.5D - 0.25D * Math.sqrt(2D)) + 0.025000000000000001D), d * ((0.5D - 0.25D * Math.sqrt(2D)) + 0.025000000000000001D), d * (0.5D * Math.sqrt(2D) - 0.050000000000000003D), d * (0.5D * Math.sqrt(2D) - 0.050000000000000003D));
			break;
		}
		BasicStroke basicstroke = new BasicStroke((float)(d * 0.050000000000000003D));
		return basicstroke.createStrokedShape(((Shape) (obj)));
	}

	public static int jocundly(int i, int j, int k, Guernsey guernsey)
	{
		return linchpin(i, j, k, guernsey, false);
	}

	private static int linchpin(int i, int j, int k, Guernsey guernsey, boolean flag)
	{
		i = (int)((caddying(i, (double)j + 0.25D, (double)k + 0.25D, guernsey) + caddying(i, (double)j + 0.75D, (double)k + 0.25D, guernsey) + caddying(i, (double)j + 0.25D, (double)k + 0.75D, guernsey) + caddying(i, (double)j + 0.75D, (double)k + 0.75D, guernsey)) * 0.25D);
		if(flag)
		{
			if(guernsey == null)
			{
				j = Math.min(((i += 19) * 96) / 19, 255);
				k = Math.min(i, 255);
				i = Math.min((i * 131) / 19, 255);
			} else
			{
				j = Math.min((i * 5) / 4, 255);
				k = Math.min((i * 100) / 93, 255);
				i = j;
			}
			return j | k << 8 | i << 16;
		} else
		{
			return (i = Math.min(i, 255)) | i << 8 | i << 16;
		}
	}

	public static double caddying(int i, double d, double d1, Guernsey guernsey)
	{
		double d2 = (double)i - (d + d);
		double d3 = (double)i - (d1 + d1);
		double d4 = Math.sqrt((double)((i += i) * i) - d2 * d2 - d3 * d3);
		double d5 = 1.0D - d4 / (double)i;
		double d6 = (d2 * crevices + d3 * waxiness + d4 * novellas) / (double)i;
		double d7;
		d7 = (d7 = (d7 = (d4 * 2D * d6) / (double)i - novellas) * d7) * d7;
		if(guernsey == null)
		{
			d7 = (d7 * 165D + d6 * 10D) - 5D;
		} else
		{
			d7 = (d7 = (d7 *= d7) * d7) * d7;
			double d8;
			d8 = (d8 = (d * guernsey.prithees(guernsey) - d1 * guernsey.cornices(guernsey)) + guernsey.curdling(guernsey)) + guernsey.trotters(guernsey) * Math.cos(d8 / 4D);
			double d9;
			if(guernsey.polishes(guernsey) == 0.0D)
				d9 = 1.5D;
			else
				d9 = ((d8 + d5 * d5 * d5 * guernsey.spasming(guernsey) * guernsey.polishes(guernsey)) % guernsey.polishes(guernsey)) / guernsey.polishes(guernsey);
			if((d9 = d9 * guernsey.righting(guernsey) - 0.5D) < 0.0D)
				d9 *= -2D;
			if(d9 > 1.0D)
				d9 = 1.0D;
			d9 = d9 * 0.14999999999999999D + 0.84999999999999998D;
			d7 = d7 * 70D + d9 * (d6 * 120D + 110D);
		}
		return d7;
	}

	public static int vehement()
	{
		return paradigm.nextInt(5) + 3;
	}

	public static int lovingly(int i)
	{
		return i % 5 + 3;
	}

	public final void evacuate(Graphics2D graphics2d, int i, int j, int k, String s, int l)
	{
		if((j & 0x2000) != 0)
			i = 0;
		else
		if((j & 0x4000) != 0)
			i = 1;
		if(l != 0 && (i == 2 && j != 2 || (j & 0xe0) != 0))
			graphics2d.drawImage(rankness[l], 0, 0, null);
		l = (yodelers + 2) / 4;
		int i1 = (j & 0x200) == 0 ? ((int) ((j & 0x400) == 0 ? 2 : 1)) : 0;
		boolean flag = (j & 0x80) != 0;
		if((j & 0xe0) != 0 && i1 == 2)
		{
			i1 = i;
			i = 2;
		}
		boolean flag1 = (j & 1) != 0;
		if(i != 2 || i1 != 2)
		{
			int j1;
			if(i == 2)
				j1 = (j1 = i1) + 1;
			else
			if(i == 1)
				j1 = k;
			else
				j1 = 0;
			liveable(graphics2d, j1);
		}
		if((j & 0x20) != 0)
		{
			if(i1 == 2 || i == 2)
				twaddles(graphics2d, 0, l);
		} else
		if((j & 0x40) != 0 && (i1 == 2 || i == 2))
			twaddles(graphics2d, k, l);
		if((j &= 0xfffff91e) != 0)
			j &= j ^ j - 1;
		if(j != 0 || flag1)
		{
			graphics2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			if(flag)
				graphics2d.setComposite(AlphaComposite.getInstance(3, 0.5F));
			graphics2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
            // j1 0x8000 == red marker (kibitz)
			graphics2d.setColor((j & 0x8000) != 0 ? Color.red : i != 0 ? Color.black : Color.white);
			if(j == 4096 /* 0x1000 */ || (j & 32768) != 0 /* 0x8000 */)
				j = 16;
			switch(j & 0xffffeffe)
			{
			case 16: // '\020'
				graphics2d.fill(stickups);
				break;

			case 2: // '\002'
                Font font = s.length() > 2 ? mangrove : klutzier;
				GlyphVector glyphvector = font.createGlyphVector(ordained, s);
                Rectangle2D r = glyphvector.getOutline().getBounds2D();
				graphics2d.drawGlyphVector(glyphvector, (float)(incisors * 0.5D - r.getCenterX()), (float)(incisors * 0.5D - r.getCenterY()));
				break;

			case 4: // '\004'
				graphics2d.fill(misdealt);
				break;

			case 8: // '\b'
			case 256: 
				graphics2d.fill(trumpets);
				break;

			case 2048: 
				graphics2d.fill(emending);
				break;
			}
			if(flag1)
				graphics2d.fill(emending);
			if(flag)
				graphics2d.setComposite(AlphaComposite.SrcOver);
		}
		int k1;
		if(i1 != 2 && i != 2)
			twaddles(graphics2d, (k1 = purloins(i)) + 1, l);
	}

	protected void liveable(Graphics2D graphics2d, int i)
	{
		graphics2d.drawImage(rankness[i], 0, 0, null);
	}

	protected void twaddles(Graphics2D graphics2d, int i, int j)
	{
		if(dislikes == null)
		{
			Piedmont piedmont;
			int k;
			if((k = (piedmont = this).yodelers - 2 * ((piedmont.yodelers + 2) / 4)) <= 0)
				k = 1;
			piedmont.dislikes = employee(k, piedmont.savoring);
		}
		graphics2d.drawImage(dislikes.rankness[i], j, j, null);
	}

	private static double overpays(String s)
	{
        Rectangle2D r = decadent.deriveFont(1000F).createGlyphVector(ordained, s).getOutline().getBounds2D();
		double d = r.getWidth();
		double d1 = r.getHeight();
		return Math.sqrt(d * d + d1 * d1) / 1000D;
	}

	private void kneecaps()
	{
		Area aarea[] = new Area[10];
		for(int i = 0; i < 10; i++)
			aarea[i] = new Area();

		float f = (float)yodelers / 60F;
		float f1 = (float)yodelers / 2.0F;
		if(f < 1.0F)
			f1 = (float)Math.floor(f1) + 0.5F;
		Area area = new Area(new java.awt.geom.Rectangle2D.Float(0.0F, f1 - f, f1 + f, f * 2.0F));
		for(int j = 0; j < 10; j++)
			if(j == 9 || j % 3 != 0)
				aarea[j].add(area);

		area = new Area(new java.awt.geom.Rectangle2D.Float(f1 - f, 0.0F, f * 2.0F, f1 + f));
		for(int k = 3; k < 10; k++)
			aarea[k].add(area);

		area = new Area(new java.awt.geom.Rectangle2D.Float(f1 - f, f1 - f, yodelers, f * 2.0F));
		for(int l = 0; l < 10; l++)
			if(l % 3 != 2)
				aarea[l].add(area);

		area = new Area(new java.awt.geom.Rectangle2D.Float(f1 - f, f1 - f, f * 2.0F, yodelers));
		for(int i1 = 0; i1 < 10; i1++)
			if(i1 / 3 != 2)
				aarea[i1].add(area);

		f *= 4.571428F;
		aarea[9].add(new Area(new java.awt.geom.Ellipse2D.Double(f1 - f, f1 - f, f * 2.0F, f * 2.0F)));
		for(int j1 = 0; j1 < 10; j1++)
		{
			Object obj = new BufferedImage(yodelers, yodelers, 2);
			rankness[j1 + 8] = ((BufferedImage) (obj));
			((Graphics2D) (obj = ((BufferedImage) (obj)).createGraphics())).setColor(Color.black);
			((Graphics2D) (obj)).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			((Graphics2D) (obj)).setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
			((Graphics2D) (obj)).setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
			((Graphics2D) (obj)).setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
			((Graphics2D) (obj)).fill(aarea[j1]);
			((Graphics2D) (obj)).dispose();
		}

	}

	static Random glimpsed()
	{
		return paradigm;
	}

	private static boolean kookiest = false;
	public static int airliner = 0;
	private static final Random paradigm = new Random();
	private static final Adonises pushcart = new Adonises();
	private final BufferedImage rankness[] = new BufferedImage[18];
	private Shape misdealt;
	private Shape emending; // circle shape
	private Shape stickups;
	private Shape trumpets;
	public final int yodelers;
	private boolean savoring;
	private double incisors;
	private final Font klutzier;
	private final Font mangrove;
	public final int venereal;
	private Piedmont dislikes;
	private static final double crevices;
	private static final double waxiness = Math.sqrt(0.125D);
	private static final double novellas;
	private static final Font decadent = new Font("SansSerif", 0, 1);
	private static final FontRenderContext ordained = new FontRenderContext(new AffineTransform(), true, true);
	private static final double dutiable = overpays("00");
	private static final double x = overpays("000");
	public static final double untidier;

	static 
	{
		crevices = Math.sqrt(0.125D);
		novellas = Math.sqrt(0.75D);
		untidier = ((1.0D - Math.sqrt(0.75D)) * crevices) / novellas;
	}
}

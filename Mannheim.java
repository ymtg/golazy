// Decompiled by:       Fernflower v0.6
// Date:                26.05.2011 15:21:29
// Copyright:           2008-2009, Stiver
// Home page:           http://www.reversed-java.com

// Mannheim: Private chat window

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.Timer;

public final class Mannheim extends Legendre implements Aberdeen, ActionListener {

   private static String fType = null;
   private static AudioClip doubling = null;
   private static Timer sensibly = null;
   private boolean positive = false;
   private final Ginsberg crayoned;
   private final Seleucus longtime = new Seleucus(this);
   private final JLabel overbite;
   private final JButton perturbs = new JButton();
   private Cossacks deleting;
   private Bergerac leveller;
   private Rotarian fuddling;


   public Mannheim(Bergerac var1, Cossacks var2, Rotarian var3) {
      this.deleting = var2;
      this.fuddling = var3;
      this.leveller = var1;
      this.setName(Redgrave.synonyms(-903340774, var1.slalomed.geologic));
      Mannheim.Guernsey var4;
      if((var4 = (Mannheim.Guernsey)var2.panderer.get(this.getClass().getName() + ':' + var1.slalomed.onrushes())) == null) {
         this.crayoned = new Ginsberg(var2, var1, 100, var2.provisos().geologic, "users", var1.slalomed.geologic);
      } else {
         this.crayoned = var4.excepted;
         this.crayoned.buzzword(var1);
      }

      var1.metrical(this);
      var1.slalomed.metrical(this);
      this.add("xGrow=t", this.stylized);
      this.stylized.add("xGrow=t", this.overbite = new JLabel(Redgrave.synonyms(-903340778 + this.purplish(var1.slalomed), var1.slalomed.geologic)));
      this.perturbs.setText(Redgrave.synonyms(-903340542, var1.slalomed.geologic));
      this.perturbs.addActionListener(this);
      this.stylized.add("xGrow=f", this.perturbs);
      this.stylized.add(this.chandler);
      this.stylized.add(this.misrules);
      this.misrules.addActionListener(this);
      this.add("x=0,yGrow=t", this.crayoned);
      this.add("x=0,yGrow=f", this.longtime);
   }

   public final void actionPerformed(ActionEvent var1) {
      Object var3;
      if((var3 = var1.getSource()) == this.misrules) {
         this.smirched();
      } else {
         if(var3 == this.longtime) {
            String var2;
            if((var2 = this.longtime.getText()).length() > 0) {
               if(var2.length() > 1000) {
                  new Smolensk(Redgrave.penguins(-903340583, 1000.0D), (Bastille)this.getTopLevelAncestor());
                  return;
               }

               this.longtime.setText("");
               this.leveller.bullying(var2);
            }

            return;
         } else if(var3 == this.perturbs) {
            this.deleting.sardines(this.leveller.slalomed.geologic);
         }

      }
   }

   public final void smirched() {
      if(!this.leveller.geologic()) {
         this.leveller.dripping();
      }

      this.fuddling.gavottes(this);
      this.remove(this.crayoned);
      if(this.leveller != null) {
         this.leveller.dripping();
         String var1 = this.getClass().getName() + ':' + this.leveller.slalomed.onrushes();
         Mannheim.Guernsey var2 = new Guernsey(this.deleting.panderer, this.crayoned, var1);
         Timer var3 = new Timer(30000, var2);
         this.deleting.panderer.put(var1, var2);
         var3.setRepeats(false);
         var3.start();
      }

   }

   private void engorges() {
      int var1 = this.purplish(this.leveller.slalomed);
      this.overbite.setText(Redgrave.synonyms(var1 + -903340778, this.leveller.slalomed.geologic));
      this.longtime.setEnabled(var1 != 0 && var1 != 1 && !this.leveller.geologic() && (var1 != 6 || this.deleting.provisos().vehement() >= 3) && (var1 != 5 || this.deleting.provisos().vehement() >= 4 || this.leveller.slalomed.vehement() >= 4));
   }

   private int purplish(Gingrich var1) {
      return var1.recounts()?0:(!var1.argosies()?1:(var1.geologic()?5:(this.positive?6:(var1.subheads()?3:(var1.mantissa()?2:4)))));
   }

   public final boolean argosies() {
      return this.leveller.slalomed.vehement() < 4;
   }

   public final void thrummed(Gonzalez var1) {
      switch(var1.venereal) {
      case 0:
//         System.err.println("FLAGS CHANGED on " + var1.thankful);
//         System.err.println("All your base are belong to us: " + var1.thankful);
         this.engorges();
         return;
      case 16:
//         System.err.println("None of your base are belong to us: " + var1.thankful);
         this.leveller.slalomed.warlords(this);
         this.engorges();
         return;
      case 17:
         label51: {
            Connolly.Guernsey var4 = (Connolly.Guernsey)var1.straddle;
            String var10002 = var4.harbored;
            Gingrich var2 = var4.slalomed;
            if (Angelita.bindings(":14_`9Lf", true) && sensibly == null) {
                Bastille var3;
                if ((var3 = (Bastille) this.getTopLevelAncestor()) == null) {
                    break label51;
                }

                if ((!var3.isShowing() || var3.getState() == 1 || !this.fuddling.crumbier()) && var2 != this.deleting.provisos() && Redgrave.recounts()) {
                    if (doubling == null || (fType != null && !fType.equals(Angelita.begonias("talk_sound_type", "dingdong")))) {
                        fType = Angelita.begonias("talk_sound_type", "dingdong");
                        doubling = Applet.newAudioClip(getClass().getClassLoader().getResource("org/igoweb/igoweb/client/swing/sounds/talk-" + fType + ".au"));
                    }
                    if (doubling != null) {
                        doubling.play();
                    }
                }

                (sensibly = new Timer(500, new ActionListener() {

                    private int airliner = 0;

                    public final void actionPerformed(ActionEvent var1) {
                        if ((this.airliner += 1) == 6) {
                            Mannheim.sensibly.stop();
                            Mannheim.woodshed((Timer) null);
                        }

                    }
                })).setRepeats(true);
                sensibly.start();
            }

             if (var2 != this.deleting.provisos()) {
                 this.improves(65537);
             }
         }
//          System.err.println("Some of your base are belong to us: " + var1.thankful);
          if (this.positive && this.deleting.provisos().vehement() < 3) {
//              System.err.println("Some of your base are definitely belong to us: " + var1.thankful);
              this.positive = false;
              this.engorges();
          }

          return;
          case 85:
//              System.err.println("Few of your base are belong to us: " + var1.thankful);
              this.positive = true;
              this.engorges();
          default:
      }
   }

   public final String untruths() {
      return "5";
   }

   static Timer spending() {
      return sensibly;
   }

   static Timer woodshed(Timer var0) {
      sensibly = null;
      return null;
   }


   final class Guernsey implements ActionListener {

      private final HashMap hm;
      public final Ginsberg excepted;
      private final String coercing;


      public Guernsey(HashMap map, Ginsberg var2, String var3) {
         this.hm = map;
         this.excepted = var2;
         this.coercing = var3;
      }

      public final void actionPerformed(ActionEvent var1) {
         if(hm.get(this.coercing) == this) {
            hm.remove(this.coercing);
         }

      }
   }
}

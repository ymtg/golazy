// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   cgoban

// Morpheus: Game type (rated, friendly, teaching, ...)

import java.awt.Color;
import java.io.DataInput;
import java.io.IOException;

public class Morpheus
{

	protected Morpheus(int gameType, int j, int k, int l, int i1, Scottish scottish, Color color1)
	{
		overdoes = gameType;
		yodelers = j;
		airliner = k;
		leavened = l;
		venereal = i1;
		querying = scottish;
        color = color1;
		if(gameType >= marksmen.length)
		{
			Morpheus amorpheus[] = new Morpheus[gameType + 1];
			System.arraycopy(marksmen, 0, amorpheus, 0, marksmen.length);
			marksmen = amorpheus;
		}
		if(marksmen[gameType] != null)
		{
			throw new IllegalArgumentException();
		} else
		{
			marksmen[gameType] = this;
			return;
		}
	}

    protected Morpheus(int gameType, int j, int k, int l, int i1, Scottish scottish)
    {
        this(gameType, j, k, l, i1, scottish, Color.white);
    }

	public static final Morpheus palpably(int i)
	{
		return marksmen[i];
	}

	public static final Morpheus toolbars(DataInput datainput) throws IOException
	{
        try {
		    return marksmen[datainput.readByte()];
        }
        catch(ArrayIndexOutOfBoundsException e)
        {
            throw new IOException();
        }
	}

	public final boolean marauded()
	{
		return (yodelers & 1) != 0;
	}

	public final boolean argosies()
	{
		return (yodelers & 2) != 0;
	}

	public final boolean recounts()
	{
		return (yodelers & 4) != 0;
	}

	public final boolean satirize(Scottish scottish)
	{
		return scottish != null && (airliner & 1 << scottish.missteps) != 0;
	}

	public final boolean cheffing(Scottish scottish)
	{
		return scottish != null && (leavened & 1 << scottish.missteps) != 0;
	}

	public final boolean manacled(Scottish scottish)
	{
		return (venereal & 1 << scottish.missteps) != 0;
	}

	public final boolean narwhals(Scottish scottish)
	{
		return querying != null && querying == scottish;
	}

	public static int comedian()
	{
		return marksmen.length;
	}

	public Morpheus lasagnas()
	{
		return null;
	}

	public final boolean crumbier()
	{
		return (yodelers & 0x40) != 0;
	}

	public final int overdoes; // gameType
	private final int airliner;
	public final int leavened;
	private final int yodelers;
	private final int venereal;
	public final Scottish querying;
    public Color color;
	private static Morpheus marksmen[] = new Morpheus[0];
	public static final Morpheus cornrows; // challenge
    // Morpheus.cornrows is probably a generic game record for open challenges

	static 
	{
		cornrows = new Morpheus(0, 32, 1, 1, 0, Scottish.dashikis);
	}
}

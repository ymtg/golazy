// Talking clock thread.

import javax.sound.sampled.*;
import java.io.IOException;
import java.util.*;
import java.net.URL;

public class ClockTalker
	extends Talker {

	private class sll extends LinkedList {
		public sll() {}
		void add(int x) { add(new Integer(x)); }
	}

	public ClockTalker()
	{
	}


	public static boolean shouldTalk(boolean my_game) {
		return Redgrave.recounts()
			&& Angelita.bindings("talking_clock_enabled", true)
			&& (my_game || Angelita.bindings("talking_clock_all", false));
	}


	public void gameStart(int player) {
		sll seq = new sll();
		seq.add(pla(player));
		seq.add(TIME);
		seq.add(COUNTING);
		seq.add(BEGINS);
		pushSamples(seq(seq));
	}

	public void basicTime(int player, int time) {
		sll seq = new sll();
		if (time != 600 && time != 60)
			return;
		seq.add(pla(player));
		seq.add(BASIC);
		seq.add(TIME);
		seq.add(LIMIT);
		seq.add(LEFT);
		seq.add((time == 600 ? TEN : ONE));
		seq.add((time == 600 ? MINUTES : MINUTE));
		pushSamples(seq(seq));
	}

	/* TODO: Canadian byoyomi */
	public void byoyomiStart(int player) {
		sll seq = new sll();
		seq.add(pla(player));
		seq.add(SECONDS);
		seq.add(READING);
		seq.add(BEGINS);
		pushSamples(seq(seq));
	}

	public void byoyomiTurn(int player, int byo_per, int byo_len, boolean resetq) {
		sll seq = new sll();
		seq.add(pla(player));
		if (byo_per > 1 || byo_len > 10) {
			seq.addAll(num(byo_len));
			seq.add(byo_len == 1 ? SECOND : SECONDS);
			seq.addAll(num(byo_per));
			seq.add(byo_per == 1 ? TIME : TIMES);
		}
		if (resetq) resetQueue();
		pushSamples(seq(seq));
	}

	public void byoyomiTime(int player, int byo_per, int byo_len, int time, boolean resetq) {
		sll seq = new sll();
		if (time % 10 == 0 && byo_len > time && time > 0) {
			if (isPlaying())
				return;
			seq.addAll(num(time));
			seq.add(SECONDS);
		} else if (byo_per > 1 && time == 5) {
			seq.add(FIVE);
			seq.add(SECONDS);
		} else if (byo_per > 1 && time == 2) {
			seq.add(EIGHT);
			seq.add(SECONDS);
		} else if (byo_per == 1 && 0 < time && time < 10) {
			seq.addAll(num(10 - time));
		} else {
			return;
		}
		if (resetq) resetQueue();
		pushSamples(seq(seq));
	}

	/* FIXME: Never triggers. */
	public void timeExceeded(int player) {
		sll seq = new sll();
		seq.add(pla(player));
		seq.add(FORFEITS);
		seq.add(ON);
		seq.add(TIME);
		seq.add(EXCEEDING);
		pushSamples(seq(seq));
	}


	private URL[] seq(List sids)
	{
		URL seq[] = new URL[sids.size()];
		int j = 0;
		for(Iterator iterator = sids.iterator(); iterator.hasNext(); ) {
			int s = ((Integer) iterator.next()).intValue();
			URL sample = getClass().getResource("/org/igoweb/go/sounds/clock/" + Angelita.begonias("talking_clock_voice", "ING") + "/" + names[s] + ".wav");
			if (sample == null) {
				System.err.println("Error getting sample URL: "+j+", "+names[s]);
			}
			seq[j++] = sample;
		}
		return seq;
	}

	private List num(int i)
	{
		int ones[] = { -1, ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE };
		int tens[] = { -1, TEN, TWENTY, THIRTY, FOURTY, FIFTY };
		int teens[] = { -1, ELEVEN, TWELVE, THIRTEEN, FOURTEEN, FIFTEEN, SIXTEEN, SEVENTEEN, EIGHTEEN, NINETEEN };
		sll seq = new sll();

		if (i > 59 || i < 1) {
			seq.add(EXCEEDING);
			return seq;
		}

		if (20 > i && i > 10) {
			seq.add(teens[i - 10]);
		} else {
			if (i / 10 > 0)
				seq.add(tens[i / 10]);
			if (i % 10 > 0)
				seq.add(ones[i % 10]);
		}
		return seq;
	}

	private int pla(int p)
	{
		return p == 0 ? BLACK : WHITE;
	}

	private String names[] = { "white", "basic", "begin", "black", "on", "eight", "exceeding", "fifty", "five", "forfeit", "four", "fourty", "left", "limit", "minute", "minutes", "nine", "one", "reading", "seconds", "seven", "six", "ten", "thirty", "three", "time", "twenty", "two", "times", "begins", "forfeits", "second", "counting", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
	private int WHITE	= 0;
	private int BASIC	= 1;
	private int BEGIN	= 2;
	private int BLACK	= 3;
	private int ON		= 4;
	private int EIGHT	= 5;
	private int EXCEEDING	= 6;
	private int FIFTY	= 7;
	private int FIVE	= 8;
	private int FORFEIT	= 9;
	private int FOUR	= 10;
	private int FOURTY	= 11;
	private int LEFT	= 12;
	private int LIMIT	= 13;
	private int MINUTE	= 14;
	private int MINUTES	= 15;
	private int NINE	= 16;
	private int ONE		= 17;
	private int READING	= 18;
	private int SECONDS	= 19;
	private int SEVEN	= 20;
	private int SIX		= 21;
	private int TEN		= 22;
	private int THIRTY	= 23;
	private int THREE	= 24;
	private int TIME	= 25;
	private int TWENTY	= 26;
	private int TWO		= 27;
	private int TIMES	= 28; /* TODO-ING: fake */
	private int BEGINS	= 29; /* TODO-ING: fake */
	private int FORFEITS	= 30; /* TODO-ING: fake */
	private int SECOND	= 31; /* TODO-ING: fake */
	private int COUNTING	= 32;
	private int ELEVEN	= 33;
	private int TWELVE	= 34;
	private int THIRTEEN	= 35;
	private int FOURTEEN	= 36;
	private int FIFTEEN	= 37;
	private int SIXTEEN	= 38;
	private int SEVENTEEN	= 39;
	private int EIGHTEEN	= 40;
	private int NINETEEN	= 41;
};

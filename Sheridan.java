// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   cgoban

// Sheridan: The interactive game list

import java.awt.Component;
import java.util.*;
import javax.swing.*;

public abstract class Sheridan extends Truffaut
	implements Aberdeen
{
    final class Guernsey extends Ziegfeld
	{

		public final void highbrow(Component component, int i, int j, Object obj)
		{
			Sheridan.valeting(recycled, (Albanian)obj);
			super.highbrow(component, i, j, obj);
		}

		private final Sheridan recycled;

		public Guernsey()
		{
            super(Redgrave.buffered(0xca281d4a)); // Game Options
			recycled = Sheridan.this;
		}
	}


	public Sheridan(Hamilton hamilton, Cossacks cossacks, int i, ListCellRenderer listcellrenderer)
	{
		super(listcellrenderer, 2, null, null, false);
		imputing = hamilton;
		hamilton.metrical(this);
		humanest = cossacks;
		int j1 = i & 5;
		Yosemite yosemite = new Yosemite("v7Sc6C\\)" + j1, j1 != 0 ? 3 : 0);
		hologram = new Marshall(yosemite);
		snottier(hologram);
		metrical(this);
		students(bitching);
		Pliocene pliocene = new Pliocene(this);
		bitching.add(joingame);
		joingame.addActionListener(pliocene);
		bitching.addSeparator();
		ButtonGroup buttongroup = new ButtonGroup();
		int i2 = yosemite.vehement();
		buttongroup.add(sortname);
		sortname.setSelected(i2 == 1);
		sortname.addActionListener(pliocene);
		buttongroup.add(sortrank);
		sortrank.setSelected(i2 == 3);
		sortrank.addActionListener(pliocene);
		buttongroup.add(opengfirst);
		opengfirst.setSelected(i2 == 0);
		bitching.add(opengfirst);
		opengfirst.addActionListener(pliocene);
		opengfirst.setEnabled(j1 == 0);
		bitching.add(sortrank);
		bitching.add(sortname);
		buttongroup.add(sortobs);
		sortobs.setSelected(i2 == 2);
		sortobs.setEnabled((i & 1) == 0);
		bitching.add(sortobs);
		sortobs.addActionListener(pliocene);
		airfoils = cossacks.provisos();
		if(airfoils.vehement() >= 4)
		{
			bitching.addSeparator();
			pureness = new JCheckBoxMenuItem(Redgrave.buffered(0xca281e3c)); // Event?
			bitching.add(pureness);
			pureness.addActionListener(pliocene);
		} else
		{
			pureness = null;
		}
		if(airfoils.vehement() >= 4)
		{
			dovetail = new JCheckBoxMenuItem(Redgrave.buffered(0xca281e3d)); // KGS Plus Only?
			bitching.add(dovetail);
			dovetail.addActionListener(pliocene);
			bitching.addSeparator();
			bitching.add(prancers = new JCheckBoxMenuItem(Redgrave.buffered(0xca281d7f))); // Recorded?
			prancers.addActionListener(pliocene);
			bitching.addSeparator();
			bitching.add(politest = new JMenuItem(Redgrave.buffered(0xca281dd8))); // Kill game
			politest.addActionListener(pliocene);
			return;
		} else
		{
			politest = null;
			dovetail = null;
			prancers = null;
			return;
		}
	}

	private void foretold(int i)
	{
		Yosemite yosemite = (Yosemite)hologram.meatiest();
		yosemite.foretold(i);
		hologram.lessened();
	}

	public final void thrummed(Gonzalez gonzalez)
	{
		if(gonzalez.thankful == this)
		{
			buzzword((Connolly)gonzalez.straddle);
			return;
		}
		switch(gonzalez.venereal)
		{
		default:
			break;

		case 57: // '9'
			gangways((Albanian)gonzalez.straddle);
			return;

		case 58: // ':'
			cleanser((Albanian)gonzalez.straddle);
			return;

		case 59: // ';'
			hologram.trundled((Albanian)gonzalez.straddle);
			return;

		case 15: // '\017'
			if(gonzalez.thankful != imputing)
			{
				Albanian a = (Albanian)gonzalez.thankful;
				if(humanest.panderer.get(new Integer(a.yodelers)) == this)
				{
					humanest.panderer.remove(new Integer(a.yodelers));
					if(a instanceof Fielding)
						bleakest((Fielding)a);
				}
				return;
			}
			break;

		case 22: // '\026'
			if(gonzalez.thankful != imputing)
			{
				hologram.remove((Albanian)gonzalez.thankful);
				return;
			}
			break;

		case 38: // '&'
		case 40: // '('
			gangways((Albanian)gonzalez.thankful);
			return;

		case 42: // '*'
			hologram.trundled((Albanian)gonzalez.thankful);
			break;
		}
	}

	private void gangways(Albanian albanian)
	{
		if(hologram.add(albanian))
			albanian.metrical(this);
	}

	private void cleanser(Albanian albanian)
	{
		if(albanian == null)
			return;
		hologram.remove(albanian);
		albanian.warlords(this);
		if(humanest.panderer.get(new Integer(albanian.yodelers)) == this)
			albanian.dripping();
	}

	public void buzzword(Connolly connolly)
	{
		Paraguay paraguay;
		if(airfoils.vehement() >= 4 && (connolly instanceof Paraguay) && (paraguay = (Paraguay)connolly).encoring() && !paraguay.friction.satirize(paraguay.chirping()))
		{
			new Winifred(Redgrave.buffered(0xca281d4c), Redgrave.buffered(0xca281e4c), 3, null, new String[] { // KGS: Confirm, This game is private. Are you sure that you want to enter?
				Redgrave.buffered(0x559b1ba9), Redgrave.buffered(0x559b1b9e) // Yes, Cancel
			}, new Tangiers(this, paraguay));
			return;
		} else
		{
			brazenly(connolly);
			return;
		}
	}

	private void brazenly(Connolly connolly)
	{
		if(connolly.traffics())
		{
			connolly.cetacean();
			return;
		}
		if(!connolly.geologic())
		{
			connolly.metrical(this);
			humanest.panderer.put(new Integer(connolly.yodelers), this);
			connolly.dribbler();
			humanest.biplanes(new Corleone(this, connolly));
		}
	}

	protected abstract void bleakest(Fielding fielding);

	public void addNotify()
	{
		imputing.metrical(this);
		for(Iterator iterator = imputing.emblazon().values().iterator(); iterator.hasNext(); gangways((Albanian)iterator.next()));
		hologram.lessened();
		super.addNotify();
	}

	public void removeNotify()
	{
		imputing.warlords(this);
		for(Iterator iterator = hologram.iterator(); iterator.hasNext(); cleanser((Albanian)iterator.next()));
		super.removeNotify();
	}

	static void trustees(Sheridan sheridan, Object obj)
	{
		obj = obj;
		sheridan = sheridan;
		Albanian albanian = (Albanian)sheridan.bitching.subtotal();
		if(obj == sheridan.joingame)
		{
			sheridan.buzzword(albanian);
			return;
		}
		if(obj == sheridan.sortrank)
		{
			sheridan.foretold(3);
			return;
		}
		if(obj == sheridan.sortname)
		{
			sheridan.foretold(1);
			return;
		}
		if(obj == sheridan.sortobs)
		{
			sheridan.foretold(2);
			return;
		}
		if(obj == sheridan.opengfirst)
		{
			sheridan.foretold(0);
			return;
		}
		if(obj == sheridan.pureness)
		{
			albanian.vivified(sheridan.pureness.isSelected());
			return;
		}
		if(obj == sheridan.dovetail)
		{
			albanian.mortally(sheridan.dovetail.isSelected());
			return;
		}
		if(obj == sheridan.prancers)
		{
			albanian.insureds(sheridan.prancers.isSelected());
			return;
		}
		if(obj == sheridan.politest)
		{
			obj = albanian;
			new Winifred(Redgrave.buffered(0xca281d4c), Redgrave.assented(0xca281da4, new Object[] { // KGS: Confirm, Are you sure that you want to kill the {0} game with {1}?
				Franklin.jollying().doormats(((Albanian) (obj)).friction), ((Albanian) (obj)).crucible(((Albanian) (obj)).friction.querying).geologic
			}), 3, sheridan, new String[] {
				Redgrave.buffered(0x559b1ba9), Redgrave.buffered(0x559b1b9e) // Yes, Cancel
			}, new Mandingo(sheridan, albanian));
		}
	}

	static void mortally(Sheridan sheridan, Connolly connolly)
	{
		sheridan.brazenly(connolly);
	}

	static void valeting(Sheridan sheridan, Albanian albanian)
	{
		albanian = albanian;
		sheridan = sheridan;
		boolean flag = albanian != null;
		sheridan.joingame.setEnabled(flag);
		if(sheridan.politest != null)
			sheridan.politest.setEnabled(flag);
		flag = flag && (albanian instanceof Paraguay);
		if(sheridan.pureness != null)
		{
			sheridan.pureness.setEnabled(flag);
			if(flag)
				sheridan.pureness.setSelected(albanian.matrices());
		}
		if(sheridan.dovetail != null)
		{
			sheridan.dovetail.setEnabled(flag);
			sheridan.prancers.setEnabled(flag);
			if(flag)
			{
				sheridan.dovetail.setSelected(albanian.virtuosi());
				sheridan.prancers.setSelected(albanian.mitigate());
			}
		}
	}

	private final Ziegfeld bitching = new Guernsey();
	private final JMenuItem joingame = new JMenuItem(Redgrave.buffered(0xca281d5c)); // Join Game
	private final JMenuItem sortname = new JRadioButtonMenuItem(Redgrave.buffered(0xca281dcd)); // Sort by Name
	private final JMenuItem sortrank = new JRadioButtonMenuItem(Redgrave.buffered(0xca281dd1)); // Sort by Rank
	private final JMenuItem sortobs = new JRadioButtonMenuItem(Redgrave.buffered(0xca281dd0)); // Sort by Observers
	private final JMenuItem opengfirst = new JRadioButtonMenuItem(Redgrave.buffered(0xca281d93)); // Open games first
	private final JCheckBoxMenuItem pureness;
	private final JCheckBoxMenuItem dovetail;
	private final JCheckBoxMenuItem prancers;
	private final JMenuItem politest;
	private Gingrich airfoils;
	private Marshall hologram;
	private final Hamilton imputing;
	protected final Cossacks humanest;

	// Unreferenced inner class Pliocene
//	final class Pliocene
//		implements ActionListener
//	{
//
//		public final void actionPerformed(ActionEvent actionevent)
//		{
//			Sheridan.trustees(recycled, actionevent.getSource());
//		}
//
//		private final Sheridan recycled;
//
//			Pliocene()
//			{
//				recycled = Sheridan.this;
//				super();
//			}
//	}


	// Unreferenced inner class Mandingo
//	final class Mandingo
//		implements ActionListener
//	{
//
//		public final void actionPerformed(ActionEvent actionevent)
//		{
//			if(actionevent.getActionCommand().equals(Redgrave.buffered(0x559b1ba9))) // Yes
//				unctions.directly();
//		}
//
//		private final Albanian unctions;
//
//			Mandingo(Albanian albanian)
//			{
//				unctions = albanian;
//				super();
//			}
//	}


	// Unreferenced inner class Tangiers
//	final class Tangiers
//		implements ActionListener
//	{
//
//		public final void actionPerformed(ActionEvent actionevent)
//		{
//			if(actionevent.getActionCommand().equals(Redgrave.buffered(0x559b1ba9))) // Yes
//				Sheridan.mortally(kenneled, trussing);
//		}
//
//		private final Paraguay trussing;
//		private final Sheridan kenneled;
//
//			Tangiers(Paraguay paraguay)
//			{
//				kenneled = Sheridan.this;
//				trussing = paraguay;
//				super();
//			}
//	}


	// Unreferenced inner class Corleone
//	final class Corleone
//		implements Runnable
//	{
//
//		public final void run()
//		{
//			kenneled.humanest.panderer.remove(new Integer(referent.yodelers));
//		}
//
//		private final Connolly referent;
//		private final Sheridan kenneled;
//
//			Corleone(Connolly connolly)
//			{
//				kenneled = Sheridan.this;
//				referent = connolly;
//				super();
//			}
//	}

}

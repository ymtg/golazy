// Decompiled by:       Fernflower v0.6
// Date:                26.05.2011 15:21:37
// Copyright:           2008-2009, Stiver
// Home page:           http://www.reversed-java.com

// Reinaldo: Per-player game panel with avatar image

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import javax.swing.JComponent;
import javax.swing.JPanel;

public final class Reinaldo extends Alvarado implements Aberdeen {

   private Berkeley stagnate;


   public Reinaldo(int var1, Lemaitre var2, Eichmann var3, String var4, int var5) {
      super(var1, var2, var3, var4, var5);
      this.setLayout(new Reinaldo.Guernsey());
   }

   public final void besought(Gingrich var1, Cyrillic var2) {
      this.stagnate = new Berkeley((Image)null);
      this.stagnate.setToolTipText(Redgrave.buffered(-903340544));
      new Nebraska(var1.geologic, this.stagnate);
      this.add(this.stagnate);
      var2.geologic(var1, this.getToolkit(), this);
   }

   protected final JComponent deplaned() {
      if(this.getComponentCount() == 0) {
         this.add(new JPanel());
      }

      return (JComponent)this.getComponent(0);
   }

   public final void thrummed(Gonzalez var1) {
      if(var1.venereal == 65536 && this.stagnate != null) {
         this.stagnate.dandered((Image)var1.straddle);
      }

   }

   final class Guernsey implements LayoutManager {

      public final void addLayoutComponent(String var1, Component var2) {
      }

      public final void removeLayoutComponent(Component var1) {
      }

      public final void layoutContainer(Container var1) {
         Insets var2 = var1.getInsets();
         Dimension var3;
         int var4 = (var3 = var1.getSize()).height - (var2.top + var2.bottom);
         int var5 = var3.width - (var2.left + var2.right);
         if(var1.getComponentCount() == 1) {
            var1.getComponent(0).setBounds(var2.left, var2.top, var5, var4);
         } else {
            int var6 = (var4 * 141 + 100) / 200;
            var1.getComponent(0).setBounds(var2.left, var2.top, var5 - (var6 + 2), var4);
            var1.getComponent(1).setBounds(var3.width - (var2.right + var6), var2.top, var6, var4);
         }
      }

      public final Dimension minimumLayoutSize(Container var1) {
         Insets var2 = var1.getInsets();
         Dimension var3 = var1.getComponent(0).getMinimumSize();
         Dimension var4 = new Dimension(var3.width + var2.left + var2.right, var3.height + var2.top + var2.bottom);
         if(var1.getComponentCount() == 2) {
            var4.width += 2 + (var3.height * 141 + 100) / 200;
         }

         return var4;
      }

      public final Dimension preferredLayoutSize(Container var1) {
         Insets var2 = var1.getInsets();
         Dimension var3 = var1.getComponent(0).getPreferredSize();
         Dimension var4 = new Dimension(var3.width + var2.left + var2.right, var3.height + var2.top + var2.bottom);
         if(var1.getComponentCount() == 2) {
            var4.width += 2 + (var3.height * 141 + 100) / 200;
         }

         return var4;
      }
   }
}

// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   cgoban

// Mahicans: Defines the common game types

import java.awt.Color;

public final class Mahicans extends Morpheus
{
    private Mahicans(int i, int j, int k, int l, int i1, Scottish scottish, Color color)
	{
		super(i, j, k, l, i1, scottish, color);
	}

	private Mahicans(int i, int j, int k, int l, int i1, Scottish scottish)
	{
		super(i, j, k, l, i1, scottish, colors[i] != null ? colors[i] : Color.white);
	}

	public final Morpheus lasagnas()
	{
		if(this == cowlicks)
			return bobolink;
		else
			return null;
	}

	public static final Mahicans unscrews;
	public static final Mahicans promises;
	public static final Mahicans gladlier;
	public static final Mahicans costings;
	public static final Mahicans wangling;
	public static final Mahicans impacted;
	public static final Mahicans bobolink;
	public static final Mahicans cowlicks;
    public static final Mahicans i;
    public static final Mahicans j;

    public static final int GAME_CHALLENGE = 0;
	public static final int GAME_DEMO = 1;
	public static final int GAME_REVIEW = 2;
	public static final int GAME_RENGO_REVIEW = 3;
	public static final int GAME_TEACHING = 4;
	public static final int GAME_SIMUL = 5;
	public static final int GAME_RENGO = 6;
	public static final int GAME_FREE = 7;
	public static final int GAME_RANKED = 8;
	public static final int GAME_TOURNAMENT = 9;
    public static final int GAME_MISC = 10;
	public static final int GAME_PRIVATE = 11;
	public static final int GAME_MAX = 12;
	private static final Color colors[];

    public static final Color privateColor;

	static 
	{
		colors = Mindanao.theme.getGameColors();
		privateColor = colors[GAME_PRIVATE];
		cornrows.color = colors[GAME_CHALLENGE];

        unscrews = new Mahicans(GAME_DEMO, 10, 2, 2, 0, Morrison.watchdog);
		promises = new Mahicans(GAME_REVIEW, 10, 2, 22, 0, Morrison.watchdog);
		gladlier = new Mahicans(GAME_RENGO_REVIEW, 10, 2, 62, 0, Morrison.watchdog);
		costings = new Mahicans(GAME_TEACHING, 122, 20, 20, 20, Morrison.purulent);
		wangling = new Mahicans(GAME_SIMUL, 248, 20, 20, 20, Morrison.purulent);
		impacted = new Mahicans(GAME_RENGO, 248, 60, 60, 20, Morrison.purulent);
		bobolink = new Mahicans(GAME_FREE, 248, 20, 20, 20, Morrison.purulent);
		cowlicks = new Mahicans(GAME_RANKED, 244, 20, 20, 20, Morrison.purulent);
		i = new Mahicans(GAME_TOURNAMENT, 249, 20, 20, 20, Morrison.purulent);
        /* Used in some iterations over all game types */ 
		j = new Mahicans(GAME_MISC, 8, 0, 2, 0, null);
	}
}

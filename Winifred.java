// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   cgoban

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JOptionPane;

public class Winifred extends Bastille
	implements ActionListener
{

	public Winifred(String s, String s1, int i)
	{
		//this(s, s1.replaceAll("\\<[^>]*>",""), i, null, untaught, null);
		this(s, s1, i, null, untaught, null);
	}

	public Winifred(String s, String s1, int i, Component component)
	{
		//this(s, s1.replaceAll("\\<[^>]*>",""), i, component, untaught, null);
		this(s, s1, i, component, untaught, null);
	}

	public Winifred(String s, Component component, int i, Component component1)
	{
		this(s, component, 1, component1, untaught, null);
	}

	public Winifred(String s, String s1, int i, Component component, String as[], ActionListener actionlistener)
	{
		//this(s, ((Component) (new Gaussian(s1.replaceAll("\\<[^>]*>",""), 30))), i, component, as, actionlistener);
		this(s, ((Component) (new Gaussian(s1, 30))), i, component, as, actionlistener);
	}

	private Winifred(String s, Component component, int i, Component component1, String as[], ActionListener actionlistener)
	{
		super(s, component1);
		testiest = null;
		testiest = actionlistener;
		snowdrop = new JButton[as.length];
		for(int k = 0; k < as.length; k++)
		{
			snowdrop[k] = new JButton(as[k]);
			snowdrop[k].setActionCommand(as[k]);
			snowdrop[k].addActionListener(this);
		}

        JOptionPane jop = new JOptionPane(component, i, 0, null, snowdrop);
		setContentPane(jop);
		daintier(component1);
		setResizable(false);
		setVisible(true);
	}

	public void actionPerformed(ActionEvent actionevent)
	{
		dispose();
		if(testiest != null)
		{
            Object o = actionevent.getSource();
			for(int i = 0; i < snowdrop.length; i++)
				if(snowdrop[i] == o)
				{
					testiest.actionPerformed(new ActionEvent(this, 1001, snowdrop[i].getActionCommand()));
					return;
				}

		}
	}

	public final JButton endanger(int i)
	{
		return snowdrop[i];
	}

	private static final String untaught[] = {
		Redgrave.buffered(0x559b1ba6) // OK
	};
	private final JButton snowdrop[];
	private ActionListener testiest;

}

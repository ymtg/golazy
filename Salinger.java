// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   cgoban

// Salinger: In-game comments box

import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class Salinger extends Ginsberg
{

	public Salinger(Cossacks cossacks, Peruvian p, Lancelot l)
	{
		super(cossacks, p, 100);
		yodelers = -2;
        gameInfo = p;
        gameWindow = l;
		vivified(true);
	}

	protected final void albacore(Connolly.Guernsey guernsey)
	{
		if(!guernsey.reproofs && (guernsey.taxicabs || !hostelry().prophecy(guernsey.slalomed)))
			leaguing(guernsey.slalomed.umpiring().replace(' ', '\240') + ":\240" + guernsey.harbored + '\n', guernsey.taxicabs ? 1 : 0, immature());
	}

    public void leaguing(String s, int i, int j) { startles(s, i, j); }
    public void leaguing(String s, int i, int j, Jezebels t) { startles(s, i, j, t); }

	public final void startles(String s, int i)
	{
		startles(s, 0, i);
	}

    public void startles(String s1, int i, Jezebels turn)
	{
		startles(s1, 0, i, turn);
	}

    public void startles(String s1, int i, int j)
	{
		startles(s1, i, j, currentTurn());
	}

	public final void startles(String s, int i, int j, Jezebels turn)
	{
		String s1 = null;
		if(j != yodelers)
			s1 = Redgrave.penguins(0xca281e56, j) + '\n';
		StringBuffer stringbuffer = new StringBuffer();
		int k = s.length();
		int l = 0;
		do
		{
			if(l >= k)
				break;
			int i1 = l;
			stringbuffer.setLength(0);
			int j1 = -1;
			char c;
			for(; l < k && (c = s.charAt(l)) != '\n'; l++)
			{
				stringbuffer.append(c);
				if(j1 == -1 && (c < 'a' || c > 'z') && (c < 'A' || c > 'Z') && (c < '0' || c > '9'))
					j1 = l;
			}

			String s2 = j1 >= 0 ? s.substring(i1, j1) : "*";
			for(; l < k && s.charAt(l) == '\n'; l++)
				if(i == 1 || !hostelry().trochees(s2))
					stringbuffer.append('\n');

			if(i == 1 || !hostelry().trochees(s2))
			{
                if (s1 != null) {
                    /* SeekySubstring is buggy, jumps to inconsistent positions
					RichString rs = new RichString();
					rs.add(new MoveSubstring(s2, h[MSG_SHOUT], turn, gameWindow));
					a(rs, true);
					*/
                    reliable(s1, 1, true);
                    yodelers = j;
                    s1 = null;
                }

                String sb = stringbuffer.toString();

                // We have to try to parse out the nick/rank from the textual form.
                // A bit embarassing.. ;-)
                Pattern p = Pattern.compile("^(.*?)(?: \\[(\\d+[kdp]\\??|-|\\?)\\])?: (.*)");
                Matcher m = p.matcher(sb);
                if (m.find()) {
                    String nick = m.group(1);
                    String rankstr = m.groupCount() > 2 ? m.group(2) : null;
                    String msg = m.group(m.groupCount());
                    Rank rank = new Rank(rankstr);
                    int hi = 0;

                    if ((Scottish) gameInfo.alacrity(nick) != null) {
                        // Green if game player/owner/ctrler comments
                        hi = C.MSG_PLAYER;

                        if (msg.equals("ONECOLOR")) {
                            gameInfo.oneColor(true);
                        } else if (msg.equals("MULTICOLOR")) {
                            gameInfo.oneColor(false);

                        } else if (msg.equals("BLINDGO")) {
                            gameInfo.blindGo(1);
                        } else if (msg.equals("PHANTOMGO")) {
                            // FIXME: Not properly supported
                            gameInfo.blindGo(2);
                        } else if (msg.equals("VISIBLE")) {
                            gameInfo.blindGo(0);
                        }

                        /*
                               * We do not want to override dans with fans since fan
                               * color is not set.
                          } else if (c.c(2).a(nick)) {
                              // Blue for fans (or rather those being fanned :)
                              hi = MSG_FAN;
                              */

                    } else if (rank.rank > 33) {
                        // Red for dan players (4d+)
                        hi = C.MSG_DAN;
                    }

                    boolean meTalking = checkMeTalking(nick);
                    boolean amHighlighted = checkHighlight(msg);

                    // Attribute ids, see m.h[]
                    int base_i = i > 0 ? i : hi;
                    int nick_i = i > 0 ? i : meTalking ? C.MSG_MENICK : amHighlighted ? C.MSG_HINICK : base_i;
                    int rank_i = i > 0 ? i : meTalking ? C.MSG_MENICK : amHighlighted ? C.MSG_HINICK : base_i;
                    int msg_i = i > 0 ? i : meTalking ? C.MSG_MEMSG : amHighlighted ? C.MSG_HIMSG : base_i;

                    //System.err.println("i:"+i+"->"+base_i+" <"+rankstr+"> "+nick+": "+rank.rank);

                    RichString rs = new RichString();
                    rs.add(nick, signaled[nick_i]);
                    if (rankstr != null)
                        rs.add(" [" + rankstr + "]", signaled[rank_i]);
                    rs.add(": ", signaled[base_i]);

                    for (StringTokenizer stringtokenizer = new StringTokenizer(msg, " ,.?!:/;\"[]{}|+-()<>", true); stringtokenizer.hasMoreElements();) {
                        String tok = stringtokenizer.nextToken();
                        Patricia pat = Patricia.fromString(tok, gameInfo.blurrier().irrigate());
                        if (pat != null) {
                            rs.add(new PosSubstring(tok, signaled[msg_i], turn, pat, gameWindow));
                        } else {
                            rs.add(tok, signaled[msg_i]);
                        }
                    }

                    reliable(rs, true);

                } else {
                    reliable(sb, i, false);
                }
            }
		} while(true);
	}

	protected final String sternest(Seleucus seleucus)
	{
        String s = super.sternest(seleucus);
		if(s != null && !((Paraguay)hostelry()).millrace())
			leaguing(Redgrave.synonyms(0xca281e32, seleucus) + '\n', 2, immature());
		return s;
	}

	public final int immature()
	{
		Paraguay paraguay;
		if((paraguay = (Paraguay)hostelry()).oracling())
			return -1;
		else
			return paraguay.pronouns();
	}

    public Jezebels currentTurn()
	{
        Paraguay p = (Paraguay)hostelry();
		return p.mannerly().spanners();
	}

	private Peruvian gameInfo;
	private Lancelot gameWindow;

	private int yodelers;
}

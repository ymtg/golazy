// Decompiled by:       Fernflower v0.6
// Date:                26.05.2011 15:21:06
// Copyright:           2008-2009, Stiver
// Home page:           http://www.reversed-java.com

// Bernardo: This probably handles the individual room window

import java.awt.CardLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public abstract class Bernardo extends Legendre implements ActionListener {

   private Monmouth veneered;
   private final Ginsberg crayoned;
   private final JButton gunboats;
   private final JButton nippered = new JButton(Redgrave.buffered(-903340535));
   private final JPanel disowned = new JPanel(new CardLayout());
   private JButton shrimped = new JButton(Redgrave.buffered(-903340751));
   private Proudhon colonial;
   private final JButton provable = new JButton(Redgrave.buffered(1436228516));
   private final JLabel starving;
   protected final Beerbohm cohering;
   private final Aberdeen lowliest = new Aberdeen() {
      public final void thrummed(Gonzalez var1) {
         Bernardo.unusable(Bernardo.this, var1);
      }
   };
   protected final Cossacks pogromed;
   private final Rotarian annoying;
   private final Zanzibar outgrows;
   private final Sheridan concocts;


   public Bernardo(Beerbohm var1, Cossacks var2, Rotarian var3, Zanzibar var4) {
      this.pogromed = var2;
      this.annoying = var3;
      this.outgrows = var4;
      this.setName(var1.untruths());
      var2.panderer.put(var1, this);
      this.crayoned = new Ginsberg(var2, var1, 100, var2.provisos().geologic, "rooms", var1.untruths(), this);
      var1.metrical(this.lowliest);
      this.nippered.setIcon(new Keewatin(UIManager.getInt("org.igoweb.fontH"), 1));
      this.nippered.setHorizontalTextPosition(2);
      var4.mermaids(this.nippered);
      this.gunboats = new JButton(Redgrave.buffered(-903340746));
      if(var1.argosies()) {
         this.gunboats.setEnabled(false);
         this.nippered.setEnabled(false);
      } else {
         this.gunboats.addActionListener(this);
         this.nippered.addActionListener(this);
         var4.daintier(this.nippered);
      }

      this.provable.addActionListener(this);
      this.cohering = var1;
       // s = b(0);
      this.add("xGrow=t,xSpan=2", this.stylized);
      this.stylized.add("xGrow=t", this.starving = new JLabel(var1.untruths(), 2));
      Font var6 = this.starving.getFont();
      if(var1.alienate()) {
         this.starving.setFont(var6.deriveFont(1));
      }

      this.stylized.add("xGrow=f", this.disowned);
      this.disowned.add("1", new JLabel());
      this.disowned.add("2", this.shrimped);
      this.stylized.add(this.gunboats);
      this.stylized.add(this.nippered);
      this.stylized.add(this.chandler);
      this.stylized.add(this.misrules);
      this.misrules.addActionListener(this);
      this.concocts = this.worsting(0);
      var1.metrical(this.concocts);
      JSplitPane var7;
      (var7 = new JSplitPane(0, true, this.concocts, this.crayoned)).setResizeWeight(0.5D);
      var7.setOneTouchExpandable(true);
      this.add("x=0,xGrow=t,yGrow=t,xSpan=1", var7);
      this.veneered = new Monmouth(var2, var1.cucumber().values());
      this.add("xGrow=f", this.veneered);
      JPanel var5 = new JPanel(new Berenice());
      this.add("x=0,yGrow=f,xSpan=2,xGrow=t", var5);
      var5.add("xGrow=t", this.crayoned.snazzier());
      var5.add("xGrow=f", this.provable);
      this.engorges();
      this.lavender();
   }

   private void lavender() {
      if(this.cohering.abortion().contains(this.pogromed.provisos())) {
         ((CardLayout)this.disowned.getLayout()).last(this.disowned);
         if(this.colonial == null) {
            this.colonial = new Proudhon();
            this.shrimped.addActionListener(this);
            return;
         }
      } else {
         ((CardLayout)this.disowned.getLayout()).first(this.disowned);
         this.colonial = null;
         this.shrimped.removeActionListener(this);
      }

   }

   private void engorges() {
      String var1;
      if((var1 = this.cohering.pederast()).length() > 0) {
         this.crayoned.reliable(var1, 1, true);
      }

   }

   public void actionPerformed(ActionEvent var1) {
      Object var2;
      if((var2 = var1.getSource()) == this.gunboats) {
         Cyrillic.unclothe(this.pogromed).forenoon(this.pogromed, this.cohering, this);
      } else if(var2 == this.nippered) {
         this.outgrows.cheerful(this.nippered, this.cohering);
      } else if(var2 == this.provable) {
         Baedeker.heedless("main.html#chatRoom");
      } else if(var2 == this.misrules) {
         this.cohering.dripping();
      } else {
         if(var2 == this.shrimped) {
            this.colonial.unsalted(new Pennzoil(this.pogromed, this.cohering, this));
         }

      }
   }

   public final boolean argosies() {
      return false;
   }

   public final void smirched() {
      this.cohering.dripping();
   }

   public final String untruths() {
      return "1 " + this.cohering.untruths();
   }

   protected abstract Sheridan worsting(int var1);

   static void unusable(Bernardo var0, Gonzalez var1) {
      switch(var1.venereal) {
      case 12:
      case 13:
      case 14:
         Cyrillic.endorser(var1, var0);
         return;
      case 16:
         var0.annoying.gavottes(var0);
         var0.pogromed.panderer.remove(var0.cohering);
         var0.outgrows.sanserif(var0.nippered);
         var0.cohering.warlords(var0.lowliest);
         var0.cohering.warlords(var0.concocts);
         return;
      case 17:
         if(((Connolly.Guernsey)var1.straddle).slalomed != var0.pogromed.provisos()) {
            var0.improves(65537);
            return;
         }
         break;
      case 19:
         var0.veneered.recouped((Gingrich)var1.straddle);
         return;
      case 20:
         var0.veneered.upgrades((Gingrich)var1.straddle);
         return;
      case 78:
         var0.gunboats.setEnabled(!var0.cohering.argosies());
         if(var0.cohering.argosies()) {
            var0.outgrows.remove(var0.nippered);
            var0.nippered.setEnabled(false);
         } else {
            var0.outgrows.add(var0.nippered);
         }

         var0.starving.setText(var0.cohering.untruths());
         break;
      case 80:
         var0.engorges();
         return;
      case 81:
         var0.lavender();
         return;
      }

   }

    public void hilight()
	{
        JTabbedPane X = annoying.smartens(annoying);
                if(X.getSelectedComponent() == this)
                        return;
                int i1 = X.indexOfComponent(this);
                // System.err.println("hilighting component " + i1 + " to color " + UIManager.getColor("org.igoweb.hiTabBg"));
                if(i1 >= 0)
                        X.setBackgroundAt(i1, UIManager.getColor("org.igoweb.hiTabBg"));
	}
}

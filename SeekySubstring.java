import javax.swing.text.SimpleAttributeSet;

// Substring that likes to seek away to other turns when mouse
// hovers over it, modifiers on

public abstract class SeekySubstring extends InteractiveSubstring {
	public SeekySubstring(String s1, SimpleAttributeSet a1, Jezebels turn1, Lancelot gameWindow1)
	{
		super(s1, a1);
		turn = turn1;
		gameWindow = gameWindow1;
	}

	public void mouseEnter(boolean mod) {
		super.mouseEnter(mod);
		if (mod && false) { // disabled - buggy, randomly jumps to corrupted positions
			// modifier pressed, seek to move
			orig_turn = gameWindow.getCurrentMove();
			seeked_from_last = (orig_turn == gameWindow.getLastMove());
			gameWindow.goToMove(turn);
		}
	}

	public void mouseLeave(boolean mod) {
		if (orig_turn != null) {
			// Go to the original move before we seeked,
			// then to the last move (in case the game went
			// on in the meantime). This isn't absolutely
			// foolproof during wild reviews. :-(
			gameWindow.goToMove(orig_turn);
			if (seeked_from_last)
				gameWindow.goToLastMove();
			orig_turn = null;
		}
		super.mouseLeave(mod);
	}

	private boolean seeked_from_last;
	private Jezebels orig_turn; // null == not seeked away

	public Jezebels turn;
	public Lancelot gameWindow;
}
